<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Facades
use Cviebrock\EloquentSluggable\Sluggable;

class SubTab extends Model
{

	// Slug
    use Sluggable;

	/**
     * Save slug for dependencias.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => ['title']
            ],
        ];
    }


    public function tabs(){
    	return $this->belongsTo('App\Tab');
    }
}
