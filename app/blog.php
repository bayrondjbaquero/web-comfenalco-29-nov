<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Facades
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\SoftDeletes;

// Models
use App\Blog_category;


class blog extends Model
{
	// Slug
    use SoftDeletes, Sluggable;

    protected $table        = 'blogs';
    protected $primaryKey   = 'id';

    /**
     * Save slug for dependencias.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => ['title']
            ],
        ];
    }

    protected $fillable = [
        'title',
        'body',
        'summary',
        'slug',
        'alt',
        'status',
        'image'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date_published'
    ];

    public function categorys()
    {
        return $this->belongsToMany('App\Category','blog_category','blog_id','category_id')->withTimestamps();
    }

    public function blog_categories()
    {
        return $this->hasMany(Blog_category::class);
    }
    // public function categories()
    // {
    //     return $this->belongsToMany(Category::class);
    // }

    // public function commets(){
    //     return $this->hasMany('App\Comment','blogs_blog_id');
    // }

    /* Mutators */
    public function getCreatedAtAttribute($created_at)
    {
        return new Date($created_at);
    }

     public function getDatePublishedAttribute($date_published)
    {
        return new Date($date_published);
        
    }
}
