<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner as Banner;
use App\Menu as Menu;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::all();
        $menu = Menu::all();
        return view('templates-admin.banners.principal.index', compact('menu', 'banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::all();
        return view('templates-admin.banners.principal.create', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Banner;
        $store->alt = $request->input('alt');
        $store->url = $request->input('url');
        $store->menus_id = $request->input('menus_id');
        if ($request->hasFile('thumbnail')) {
           $fileas = $request->file('thumbnail');
           $destinationPathas = 'img/img-banners/';
           $fileas->move($destinationPathas,$fileas->getClientOriginalName());
           $store->image = $destinationPathas.''.$fileas->getClientOriginalName();
        }else{
           $store->image = 'img/img-banners/logo.png';
        }
        $store->save();
        return back()->with('success','Se guardo su item correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        $menus = Menu::all();
        return view('templates-admin.banners.principal.edit', compact('menus', 'banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Banner::find($id);
        $update->alt = $request->input('alt');
        $update->url = $request->input('url');
        $update->menus_id = $request->input('menus_id');
        if ($request->hasFile('image')) {
           $filea = $request->file('image');
           $destinationPatha = 'img/img-banners/';
           $filea->move($destinationPatha,$filea->getClientOriginalName());
           $update->image = $destinationPatha.''.$filea->getClientOriginalName();
        }
        $update->update();
        return back()->with('success','Se actualizó su item correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Banner::find($id);
        $delete->delete();
        return back()->with('success','Se eliminó su item correctamente');
    }
}
