<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Proyect;

// Facades
use \Cviebrock\EloquentSluggable\Services\SlugService;
use Validator;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use Storage;
use Jenssegers\Date\Date;

class ProyectController extends Controller
{
    private $singular;
    private $plural;
    /* 
     * Configure vars global for this controllert
     */
    public function __construct(){
        $this->singular   = "Proyecto";
        $this->plural     = "Proyectos";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        Date::setLocale('es');
        $objects = Proyect::orderBy('id','desc')->get();
        return view('templates-admin.proyectos.index', compact('objects','singular','plural'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        return view('templates-admin.proyectos.create', compact('singular','plural'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $singular   = $this->singular;

        $validator = Validator::make($request->all(), [
            'title'             => 'required',
            'description'       => 'required',
            'thumbnail'         => 'required',
            'condition'         => ['required', Rule::in(['venta','arriendo'])],
            'type'              => ['required', Rule::in(['casa','apartamento'])],
            'alt'               => '',
            'city'              => 'required',
            'neighborhood'      => '',
            'status'            => 'required|in:1,0',
        ]);
        
        // Si la validación falla 
        if ($validator->fails())
        {
            return redirect()->back()->with('error','Ups, ha ocurrido un error inesperado en la aplicación. por favor intente nuevamente')->withInput()->withErrors($validator->errors());
        }

        $proyect = new Proyect();
        $proyect->title = $request['title'];
        $proyect->description = $request['description'];
        $proyect->condition = $request['condition'];
        $proyect->type = $request['type'];
        $proyect->status = $request['status'];
        $proyect->city = $request['city'];
        if($request['neighborhood']){
            $proyect->neighborhood = $request['neighborhood'];
        }

        $imagen = $request['thumbnail'];
        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $proyect->thumbnail = $name;
            \Storage::disk('proyect_images')->put($name, \File::get($imagen));
        }

        $proyect->save();

        return back()->with('success','Se ha guardado correctamente un '.$singular);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        $object = Proyect::findOrFail($id);
        return view('templates-admin.proyectos.edit', compact('object','singular', 'plural'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = Proyect::findOrFail($id);
        $singular   = $this->singular;

        $request['condition'] = 'venta';

        $validator = Validator::make($request->all(), [
            'title'             => 'required',
            'description'       => 'required',
            'thumbnail'         => 'image',
            'condition'         => ['required', Rule::in(['venta'])],
            'type'              => ['required', Rule::in(['casa','apartamento'])],
            'alt'               => '',
            'city'              => 'required',
            'neighborhood'      => '',
            'status'            => 'required|in:1,0',
        ]);

        // Si la validación falla 
        if ($validator->fails())
        {
            return redirect()->back()->with('error','Ups, ha ocurrido un error inesperado en la aplicación. por favor intente nuevamente')->withInput()->withErrors($validator->errors());
        }

        if (!$object->isClean()) {
            return back()->with('error','Se debe especificar al menos un valor diferente para actualizar');
        }

        $object->update($request->only('title', 'description', 'condition', 'type', 'city', 'neighborhood', 'status'));
        $object->condition = 'venta';
        // Validar si el SLUG fue cambiado
        if($request['slug'] != $object->slug && $request['slug'] != null){
            $validar = Proyect::where('slug',$request['slug'])->get();
            if(count($validar)!=0){
                return back()->with('error','Error Slug, no se aceptan duplicados');
            }
            $newslug = SlugService::createSlug(Proyect::class, 'slug', $request['slug']);
            $object->slug = $newslug;    
        }

        // Actializar imagen
        $imagen                    = $request['thumbnail'];
        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $object->thumbnail   = $name;
            \Storage::disk('proyect_images')->put($name, \File::get($imagen));
        }

        $object->save();

        return redirect()->back()->with('success','Se ha creado correctamente una '.$singular);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $proyect    = Proyect::findOrFail($id);
        $galleries  = Gallery::where('proyect_id',$proyect->id)->get();

        Storage::disk('proyect_images')->delete($galleries->image);
        $proyect->status = 0;
        $proyect->save();
        $proyect->delete();
        $galleries->delete();
        return back()->with('success','Se ha eliminado correctamente un proyecto');
    }
}
