<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Facades
use Validator;
use Carbon\Carbon;
use Storage;

// Models
use App\Gallery;
use App\Proyect;

class GalleryController extends Controller
{
    private $singular;
    private $plural;
    /* 
     * Configure vars global for this controllert
     */
    public function __construct(){
        $this->singular   = "Galería";
        $this->plural     = "Galería";
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        $objects = Gallery::all();
        return view('templates-admin.proyectos.galerias.index', compact('objects','plural','singular'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_proyect_gallery($proyect_id)
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        $proyect = Proyect::where('id',$proyect_id)->firstOrFail();
        return view('templates-admin.proyectos.galerias.create', compact('plural','singular','proyect_id','proyect'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        return view('templates-admin.proyectos.galerias.create', compact('plural','singular'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $singular   = $this->singular;

        $validator = Validator::make($request->all(), [
            'image'            => 'required|image',
            'alt'               => '',
            'proyect_id'        => 'required'
        ]);
        
        // Si la validación falla 
        if ($validator->fails())
        {
            return redirect()->back()->with('error','Ups, ha ocurrido un error inesperado en la aplicación. por favor intente nuevamente')->withInput()->withErrors($validator->errors());
        }

        $gallery = new Gallery();
        $gallery->alt = $request['alt'];
        $gallery->proyect_id = $request['proyect_id'];
        $imagen = $request['image'];
        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $gallery->image = $name;
            \Storage::disk('proyect_images')->put($name, \File::get($imagen));
        }

        $gallery->save();

        return back()->with('success','Se ha guardado correctamente un '.$singular);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $singular   = $this->singular;
        $plural     = $this->plural;
        $objects = Gallery::where('proyect_id',$id)->get();
        $proyect = Proyect::where('id',$id)->firstOrFail();
        return view('templates-admin.proyectos.galerias.show', compact('objects','singular','plural','id','proyect'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $singular = $this->singular;
        $object = Gallery::findOrFail($id);
        Storage::delete($object->imagen);
        $object->delete();
        return back()->with('success','Se ha eliminado correctamente una '.$singular);
    }
}