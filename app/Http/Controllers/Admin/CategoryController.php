<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Category::all();
        return view('templates-admin.entradas.categorias.index', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('templates-admin.entradas.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $rules = [
        //     'namddde'          => 'required',
        //     'ordenation'    => ''
        // ];

        // $this->validate($request, $rules);
        // $request['ordenation'] = Category::count() + 1;

        Category::create($request->only('name','ordenation'));
        // $category = Category::create($request->all());
        return back()->with('success','Se ha creado correctamente una nueva categoría.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Category::findOrFail($id);
        return view('templates-admin.entradas.categorias.edit', compact('object'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = Category::findOrFail($id);

        $reglas = [
            'name'      => 'required|max:180',
            'slug'      => 'required|unique:categories,slug,'.$object->id,
        ];

        $this->validate($request, $reglas);
        
        $object->fill($request->only([
            'name', 'slug'
        ]));

        if ($object->isClean()) {
            return back()->with('error','Debe especificar al menos un valor diferente para actualizar');
        }

        $object->save();

        return back()->with('success','Se ha actualizado correctamente una Categoría');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Category::findOrFail($id);
        $object->delete();
        return back()->with('success','Se ha eliminado correctamente una categoría');
    }
}
