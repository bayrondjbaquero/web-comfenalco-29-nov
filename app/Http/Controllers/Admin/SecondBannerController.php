<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SecondBanner as SecondBanner;
use App\Tab as Tab;

class SecondBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = SecondBanner::all();
        $menu = Tab::all();
        return view('templates-admin.banners.secundarios.index', compact('menu', 'banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Tab::all();
        return view('templates-admin.banners.secundarios.create', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new SecondBanner;
        $store->alt = $request->input('alt');
        $store->url = $request->input('url');
        $store->tabs_id = $request->input('menus_id');
        if ($request->hasFile('thumbnail')) {
           $fileas = $request->file('thumbnail');
           $destinationPathas = 'img/img-banners-tabs/';
           $fileas->move($destinationPathas,$fileas->getClientOriginalName());
           $store->image = $destinationPathas.''.$fileas->getClientOriginalName();
        }else{
           $store->image = 'img/img-banners-tabs/logo.png';
        }
        $store->save();
        return back()->with('success','Se guardo su item correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = SecondBanner::find($id);
        $menus = Tab::all();
        return view('templates-admin.banners.secundarios.edit', compact('menus', 'banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = SecondBanner::find($id);
        $update->alt = $request->input('alt');
        $update->url = $request->input('url');
        $update->tabs_id = $request->input('menus_id');
        if ($request->hasFile('thumbnail')) {
           $filea = $request->file('thumbnail');
           $destinationPatha = 'img/img-banners-tabs/';
           $filea->move($destinationPatha,$filea->getClientOriginalName());
           $update->image = $destinationPatha.''.$filea->getClientOriginalName();
        }
        $update->update();
        return back()->with('success','Se actualizó su item correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = SecondBanner::find($id);
        $delete->delete();
        return back()->with('success','Se eliminó su item correctamente');
    }
}
