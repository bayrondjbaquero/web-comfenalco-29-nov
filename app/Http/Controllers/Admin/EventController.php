<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Facades
use Storage;
use Validator;
use Carbon\Carbon;
use Jenssegers\Date\Date;

// Models
use App\Event;

class EventController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Date::setLocale('es');
        $objects = Event::orderBy('id','desc')->get();
        return view('templates-admin.eventos.index', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('templates-admin.eventos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'                         => 'required|max:190',
            'body'                          => 'required',
            'status'                        => 'required|in:0,1',
            'date_published'                => 'nullable|date',
            'event_start'                   => 'date',
            'event_end'                     => 'date',
            'alt'                           => 'nullable|max:255',
            'image'                         => 'required|image'
        ];

        $this->validate($request, $rules);
        
        $event = new Event;
        $event->title           = $request['title'];
        $event->body            = $request['body'];
        $event->date_published  = $request['date_published'];
        $event->event_start     = $request['event_start'];
        $event->event_end       = $request['event_end'];
        $event->alt             = $request['alt'];
        $event->status          = $request['status'];

        // Actualiza imagen
        $imagen                 = $request['image'];

        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $event->image   = $name;
            \Storage::disk('event_images')->put($name, \File::get($imagen));
        }

        $event->save();
        
        return back()->with('success','Se ha creado correctamente un nuevo evento.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Event::findOrFail($id);
        return view('templates-admin.eventos.edit', compact('object'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = Event::findOrFail($id);

        $reglas = [
            'slug'                          => 'required|unique:events,slug,'.$object->id,
            'title'                         => 'required|max:190',
            'body'                          => 'required',
            'status'                        => 'required|in:0,1',
            'date_published'                => 'nullable|date',
            'event_start'                   => 'date',
            'event_end'                     => 'date',
            'alt'                           => 'nullable|max:255',
            'image'                         => 'image'
        ];

        $this->validate($request, $reglas);
        
        $object->fill($request->only([
            'slug', 'title', 'body', 'status', 'date_published', 'event_start', 'event_end', 'alt'
        ]));


        $object->date_published = $request['date_published'];
        $object->event_start    = $request['event_start'];
        $object->event_end      = $request['event_end'];
        
        // Actualiza imagen
        $imagen                 = $request['image'];

        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $object->image   = $name;
            Storage::disk('event_images')->put($name, \File::get($imagen));
        }

        $object->save();

        return back()->with('success','Se ha actualizado correctamente un evento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Event::findOrFail($id);
        Storage::disk('event_images')->delete($object->image);
        $object->status = 0;
        $object->save();
        $object->delete();
        return back()->with('success','Se ha eliminado correctamente un evento');
    }
}
