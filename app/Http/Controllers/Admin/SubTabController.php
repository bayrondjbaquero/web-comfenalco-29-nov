<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\SubTab;

class SubTabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('templates-admin.tabs.subtab.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new SubTab;
        $store->title = $request->input('title');
        $store->tabs_id = $request->input('tabs_id');
        $store->description = $request->input('description');
        $store->status = $request->input('status');
        $store->save();
        return back()->with('success','Se guardó su item correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subtabs = SubTab::find($id);
        return view('templates-admin.tabs.subtab.edit', compact('subtabs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = SubTab::find($id);
        $store->title = $request->input('title');
        $store->description = $request->input('description');
        $store->update();
        return back()->with('success','Se actualizó su item correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = SubTab::find($id);
        $store->delete();
        return back()->with('success','Se eliminó su item correctamente');
    }
}
