<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models 
use App\QuestionCategory;
use App\Question;

class FrecuentQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = Question::orderBy('id','desc')->get();
        return view('templates-admin.preguntas-frecuentes.index', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = QuestionCategory::select('id','name')->where('status',1)->get();
        return view('templates-admin.preguntas-frecuentes.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reglas = [
            'question'                      => 'required|max:190',
            'answer'                        => 'required',
            'question_question_category_id' => 'required|exists:question_categories,id',
            'status'                        => 'required|in:0,1'
        ];

        $this->validate($request, $reglas);

        $pregunta = new Question;
        $pregunta->question                         = $request['question'];
        $pregunta->answer                           = $request['answer'];
        $pregunta->question_question_category_id    = $request['question_question_category_id'];
        $pregunta->status                           = 1;
        $pregunta->save();

        return back()->with('success', 'Se ha creado correctamente una nueva pregunta.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = Question::findOrFail($id);
        $categories = QuestionCategory::select('id','name')->where('status',1)->get();
        return view('templates-admin.preguntas-frecuentes.edit', compact('categories','object'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = Question::findOrFail($id);

        $reglas = [
            'question'                      => 'required|max:190',
            'answer'                        => 'required',
            'question_question_category_id' => 'required|exists:question_categories,id',
            'status'                        => 'required|in:0,1'
        ];

        $this->validate($request, $reglas);

        //$object->update($request->only('question', 'answer', 'question_question_category_id', 'status'));
        $object->fill($request->only([
            'question', 'answer', 'question_question_category_id', 'status'
        ]));

        if ($object->isClean()) {
            return back()->with('error','Debe especificar al menos un valor diferente para actualizar');
        }

        $object->save();

        return back()->with('success','Se ha actualizado correctamente una pregunta');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = Question::findOrFail($id);
        $object->delete();
        return back()->with('success','Se ha eliminado correctamente una categoría');
    }
}
