<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tab as Tab;
use App\Menu as Menu;

class TabsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabs = Tab::all();
        $menu = Menu::all();
        return view('templates-admin.tabs.principal.index', compact('menu', 'tabs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::all();
        return view('templates-admin.tabs.principal.create', compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Tab;
        $store->title = $request->input('title');
        $store->slug = str_slug($request->input('title'));
        $store->description = $request->input('description');
        $store->menus_id = $request->input('menus_id');
        if ($request->hasFile('thumbnail')) {
           $fileas = $request->file('thumbnail');
           $destinationPathas = 'img/img-tabs/';
           $fileas->move($destinationPathas,$fileas->getClientOriginalName());
           $store->image = $destinationPathas.''.$fileas->getClientOriginalName();
        }else{
           $store->image = 'img/img-tabs/logo.png';
        }
        $store->save();
        return back()->with('success','Se guardó su item correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tabs = Tab::find($id);
        return view('templates-admin.tabs.principal.show', compact('tabs'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tabs = Tab::find($id);
        $menu = Menu::all();
        return view('templates-admin.tabs.principal.edit', compact('menu', 'tabs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Tab::find($id);
        $store->title = $request->input('title');
        $store->slug = str_slug($request->input('title'));
        $store->description = $request->input('description');
        if ($request->hasFile('thumbnail')) {
           $fileas = $request->file('thumbnail');
           $destinationPathas = 'img/img-tabs/';
           $fileas->move($destinationPathas,$fileas->getClientOriginalName());
           $store->image = $destinationPathas.''.$fileas->getClientOriginalName();
        }
        $store->update();
        return back()->with('success','Se actualizó su item correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Tab::find($id);
        $store->delete();
        return back()->with('success','Se eliminó su item correctamente');
    }
}
