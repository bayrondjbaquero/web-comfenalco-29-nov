<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\QuestionCategory;

class CategoryQuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = QuestionCategory::all();
        return view('templates-admin.preguntas-frecuentes.categorias.index', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('templates-admin.preguntas-frecuentes.categorias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'      => 'required|max:190',
            'status'    => 'required|in:1,0',
        ];
        
        $this->validate($request, $rules);
        $request['ordenation'] = QuestionCategory::count()+1;

        QuestionCategory::create($request->only('name', 'status','ordenation'));
        return back()->with('success','Se ha creado correctamente una nueva categoría para preguntas frecuentes.');
    }

    /**
     * Display the specified category with their questions.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $object = QuestionCategory::findOrFail($id);
        return view('templates-admin.preguntas-frecuentes.categorias.show', compact('object'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = QuestionCategory::findOrFail($id);
        return view('templates-admin.preguntas-frecuentes.categorias.edit', compact('object'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $object = QuestionCategory::findOrFail($id);

        $reglas = [
            'name'      => 'required|max:150',
            'slug'      => 'required|unique:question_categories,slug,'.$object->id,
            'status'    => 'required|in:0,1'
        ];

        $this->validate($request, $reglas);

        //$object->update($request->only('question', 'answer', 'question_question_category_id', 'status'));
        $object->fill($request->only([
            'name', 'slug', 'status'
        ]));

        if ($object->isClean()) {
            return back()->with('error','Debe especificar al menos un valor diferente para actualizar');
        }

        $object->save();

        return back()->with('success','Se ha actualizado correctamente una Categoría');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = QuestionCategory::findOrFail($id);
        $object->delete();
        return back()->with('success','Se ha eliminado correctamente una categoría');
    }

     /**
     * Set order from menús.
     *
     * @return \Illuminate\Http\Response
     */
    public function order_edit(){
        $objects = QuestionCategory::orderBy('ordenation','asc')->get();
        return view('templates-admin.preguntas-frecuentes.categorias.order', compact('objects'));
    }

    public function order_update(Request $request){
        $id     = $request['id'];
        $array  = $request['numeracion'];

        $reset_valid = QuestionCategory::where('ordenation',null)->get();
        if(count($reset_valid) === 0){
            if(count(array_unique($array)) != count($array)){
                return back()->with('error','No puedes duplicar numeros en la enumeración, tampoco dejar campos vacíos');
            }

            for ($i=0; $i < count($array); $i++) {
                if($array[$i] > count($array) or $array[$i] == 0){
                    return back()->with('error','No puedes agregar numeros mayores al numero de items en la lista, tampoco incluir numeros iguales a cero');
                }
                $new_array[] = QuestionCategory::where('id',$id[$i])->FirstOrFail();
            }

            for ($j=0; $j < count($array); $j++) {
                $new_array[$j]->ordenation = $array[$j];
                $new_array[$j]->save();
            }
        }else{
            $all = QuestionCategory::all();
            foreach ($all as $one => $index) {
                $index->ordenation = $one+1;
                $index->save();
            }
        }
        return back()->with('success', 'Se ha completado correctamente la enumeración de categorías.');
    }   
}
