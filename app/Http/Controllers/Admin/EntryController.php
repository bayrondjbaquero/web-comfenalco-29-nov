<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Facades
use Storage;
use Validator;
use Carbon\Carbon;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\Input;

// Models 
use App\Blog;
use App\Category;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Date::setLocale('es');
        $objects = Blog::orderBy('id','desc')->get();
        return view('templates-admin.entradas.index', compact('objects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objects = Category::select('id','name')->get();
        return view('templates-admin.entradas.create', compact('objects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'                         => 'required|max:190',
            'body'                          => 'required',
            'summary'                       => 'nullable|required|max:500',
            'categorys_id'                  => 'required|exists:categories,id',
            'status'                        => 'required|in:0,1',
            'date_published'                => 'nullable|date',
            'alt'                           => 'nullable|max:255',
            'imagen'                        => 'required|image'
        ];

        $this->validate($request, $rules);
        
        // $object = Blog::create($request->only('title','body', 'summery', 'date_published', 'alt', 'status'));
        $entry = new Blog;
        $entry->title          = $request['title'];
        $entry->body           = $request['body'];
        $entry->summary        = $request['summary'];
        $entry->date_published = $request['date_published'];
        $entry->alt            = $request['alt'];
        $entry->status         = $request['status'];

        // Actualiza imagen
        $imagen                 = $request['imagen'];

        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $entry->image   = $name;
            \Storage::disk('blog_images')->put($name, \File::get($imagen));
        }

        $entry->save();

        // Guardamos las categorías
        
        //$validar = $request['categorys_id'];

        // $count = Category::whereIn('id',$validar)->count();
        // $speakers  = (array) Input::get('categorys_id'); // related ids
        // $pivotData = [];
        // for($i=0;$i<count($speakers);$i++){
        //     $pivotData[$i] = ['blog_id'=>$entry->id, 'category_id' => $speakers[$i]];
        // }

        $entry->categorys()->syncWithoutDetaching($request['categorys_id']);
        
        return back()->with('success','Se ha creado correctamente una nueva entrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object     = Blog::findOrFail($id);
        $categories = Category::all();
        return view('templates-admin.entradas.edit', compact('object','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entry      = Blog::findOrFail($id);
        //$category   = Category::whereIn('id', $request['categorys_id'])->get();

        $reglas = [
            'title'                         => 'required|max:190',
            'body'                          => 'required',
            'slug'                          => 'required|unique:blogs,slug,'.$entry->id,
            'summary'                       => 'nullable|required|max:500',
            'categorys_id'                  => 'required|exists:categories,id',
            'status'                        => 'required|in:0,1',
            'date_published'                => 'nullable|date',
            'alt'                           => 'nullable|max:255',
            'image'                         => 'image'
        ];

        $this->validate($request, $reglas);

        $entry->fill($request->only([
            'name', 'slug', 'title', 'body', 'summary', 'date_published', 'alt', 'status'
        ]));

        $entry->date_published = $request['date_published'];
        
        // Actualiza imagen
        $imagen                    = $request['image'];

        if(!empty($imagen)){
            $name = Carbon::now()->second.$imagen->getClientOriginalName();
            $entry->image   = $name;
            Storage::disk('blog_images')->put($name, \File::get($imagen));
        }

        // if ($entry->isClean()) {
        //     return back()->with('error','Debe especificar al menos un valor diferente para actualizar');
        // }

        $entry->save();

        //sync, attach, syncWithoutDetaching
        $entry->categorys()->sync($request['categorys_id']);
        return back()->with('success','Se ha actualizado correctamente una Entrada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = Blog::findOrFail($id);
        Storage::disk('blog_images')->delete($entry->image);
        $entry->status = 0;
        $entry->save();
        $entry->delete();
        return back()->with('success','Se ha eliminado correctamente una entrada');
    }
}