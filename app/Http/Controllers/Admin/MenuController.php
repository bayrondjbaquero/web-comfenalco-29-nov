<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu as Menu;

class MenuController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menu = Menu::all();
        return view('templates-admin.menus.principal.index', compact('menu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('templates-admin.menus.principal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store = new Menu;
        $store->title = $request->input('title');
        $store->slug = str_slug($request->input('title'));
        $store->type_menu = $request->input('type_menu');
        if ($request->hasFile('archivo')) {
           $fileas = $request->file('archivo');
           $destinationPathas = 'img/iconos-menu/';
           $fileas->move($destinationPathas,$fileas->getClientOriginalName());
           $store->icon = $destinationPathas.''.$fileas->getClientOriginalName();
        }else{
           $store->icon = 'img/iconos-menu/logo.png';
        }
        $store->url = str_slug($request->input('title'), '_');
        $store->description = $request->input('description');
        $store->type_contact = $request->input('type');
        $store->status = $request->input('status');
        $store->save();
        return back()->with('success','Se guardó su item correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menus = Menu::find($id);
        return view('templates-admin.menus.principal.edit', compact('menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $store = Menu::find($id);
        $store->title = $request->input('title');
        $store->slug = str_slug($request->input('slug'));
        $store->description = $request->input('description');
        $store->type_menu = $request->input('type_menu');
        if ($request->hasFile('archivo')) {
           $fileas = $request->file('archivo');
           $destinationPathas = 'img/iconos-menu/';
           $fileas->move($destinationPathas,$fileas->getClientOriginalName());
           $store->icon = $destinationPathas.''.$fileas->getClientOriginalName();
        }
        $store->type_contact = $request->input('type');
        $store->status = $request->input('status');
        $store->update();
        return back()->with('success','Se actualizó su item correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = Menu::find($id);
        $store->delete();
        return back()->with('success','Se eliminó su item correctamente');
    }
}
