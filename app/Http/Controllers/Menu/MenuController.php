<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu as Menu;
use App\Tab as Tab;
use App\Banner as Banner;
use Route;

class MenuController extends Controller
{
    // Menú Principal
    public function afiliaciones(){
        $var = Route::current();
        $afiliaciones = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $afiliaciones[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $afiliaciones[0]['id'])->get();
        return view('templates.menu-principal.afiliaciones', compact('banners','afiliaciones','tabs'));
    }
    
    public function credito_social(){
        $var = Route::current();
        $creditosocial = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $creditosocial[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $creditosocial[0]['id'])->get();
        return view('templates.menu-principal.credito-social', compact('banners','creditosocial','tabs'));
    }

    public function vivienda(Request $request){
        $var = Route::current();
        $vivienda = Menu::where('slug', $var->uri)->get();
        if ($request->session()->has('proyects_store')) {
            $request->session()->forget('proyects_store');
        }
        $tabs = Tab::where('menus_id', '=', $vivienda[0]['id'])->get();
        $banners = Banner::where('menus_id', '=', $vivienda[0]['id'])->get();
        return view('templates.menu-principal.vivienda', compact('vivienda', 'banners', 'tabs'));
    }

	// public function vivienda_proyectos(){
 //        return view('templates.menu-principal.vivienda_proyectos');
 //    }

    public function deportes(){
         $var = Route::current();
        $deportes = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $deportes[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $deportes[0]['id'])->get();
        return view('templates.menu-principal.deportes', compact('banners', 'deportes','tabs'));
    }

    public function recreacion(){
        $var = Route::current();
        $recreacion = Menu::where('slug', $var->uri)->firstOrFail();
        $banners = Banner::where('menus_id', '=', $recreacion['id'])->get();
        $tabs = Tab::where('menus_id', '=', $recreacion['id'])->get();
        return view('templates.menu-principal.recreacion', compact('banners','recreacion','tabs'));
    }

    public function proteccion_social(){
        $var = Route::current();
        $proteccion = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $proteccion[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $proteccion[0]['id'])->get();
        return view('templates.menu-principal.proteccion-social', compact('proteccion', 'banners','tabs'));
    }
    // public function viajes_comfenalco(){
    //     return view('templates.menu-principal.viajes-comfenalco');
    // }

    public function agencia_de_empleo(){
        $var = Route::current();
        $agenciaempleos = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $agenciaempleos[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $agenciaempleos[0]['id'])->get();
        return view('templates.menu-principal.agencia-de-empleo', compact('banners','agenciaempleos','tabs'));
    }

    public function educacion(){
        $var = Route::current();
        $educacion = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $educacion[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $educacion[0]['id'])->get();
        return view('templates.menu-principal.educacion', compact('banners', 'educacion','tabs'));
    }

    public function cultura(){
        $var = Route::current();
        $cultura = Menu::where('slug', $var->uri)->get();
        $banners = Banner::where('menus_id', '=', $cultura[0]['id'])->get();
        $tabs = Tab::where('menus_id', '=', $cultura[0]['id'])->get();
        return view('templates.menu-principal.cultura', compact('cultura', 'banners','tabs'));
    }

    public function links() {
        $links = Menu::all();
        return $links;
    }

}
