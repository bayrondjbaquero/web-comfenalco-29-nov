<?php

namespace App\Http\Controllers\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    // Menú Principal
    public function afiliaciones(){
        return view('templates.menu-principal.afiliaciones');
    }
    
    public function credito_social(){
        return view('templates.menu-principal.credito-social');
    }

    public function vivienda(Request $request){
        if ($request->session()->has('proyects_store')) {
            $request->session()->forget('proyects_store');
        }
        return view('templates.menu-principal.vivienda');
    }

	// public function vivienda_proyectos(){
 //        return view('templates.menu-principal.vivienda_proyectos');
 //    }

    public function deportes(){
        return view('templates.menu-principal.deportes');
    }

    public function recreacion(){
        return view('templates.menu-principal.recreacion');
    }

    public function proteccion_social(){
        return view('templates.menu-principal.proteccion-social');
    }
    public function viajes_comfenalco(){
        return view('templates.menu-principal.viajes-comfenalco');
    }

    public function agencia_de_empleo(){
        return view('templates.menu-principal.agencia-de-empleo');
    }

    public function educacion(){
        return view('templates.menu-principal.educacion');
    }

    public function cultura(){
        return view('templates.menu-principal.cultura');
    }

}
