<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Proyect;
use App\Gallery;

class ProyectController extends Controller
{
    public function cargar_proyectos(Request $request){
    	if($request->ajax()) {
            $proyects = Proyect::where('status',1)->paginate(8);
            
            return view('templates.menu-principal.vivienda.proyectos-de-vivienda._cargar-proyectos', compact('proyects'));
        }
        return redirect()->route('vivienda');
    }

    public function cargar_proyecto_slug(Request $request, $slug){
    	if($request->ajax()) {
            $proyect = Proyect::where('slug',$slug)->firstOrFail();
            $galleries = Gallery::where('proyect_id',$proyect->id)->get();
            return view('templates.menu-principal.vivienda.proyectos-de-vivienda._cargar-proyecto-slug', compact('proyect','galleries'));
        }
        return redirect()->route('vivienda');
    }

    public function cargar_proyectos_pagination(Request $request){
        // Paginador
        if($request->ajax()){
            if ($request->session()->has('proyects_store')) {
                $object = $request->session()->get('proyects_store');
                $proyects = Proyect::where('type',$request->session()->get('proyects_store.type'))
                ->where('city','LIKE','%'.$request->session()->get('proyects_store.city').'%')
                ->where('status',1)
                ->OrderBy('created_at','desc')
                ->paginate(8);
                return response()->json(view('templates.menu-principal.vivienda.proyectos-de-vivienda._cargar-proyectos-pagination',compact('proyects'))->render());
            }else{
                $proyects = Proyect::where('status',1)->paginate(8);
                return response()->json(view('templates.menu-principal.vivienda.proyectos-de-vivienda._cargar-proyectos-pagination',compact('proyects'))->render());
            }
        }
        return redirect()->route('vivienda');
    }

    public function busqueda_proyectos(Request $request){
        if($request->ajax()){
            // if ($request->session()->has('proyects_store') && $) {
            //     $object = $request->session()->get('proyects_store');
            //     $proyects = Proyect::where('condition',$request->session()->get('proyects_store.condition'))        
            //     ->where('type',$request->session()->get('proyects_store.type'))
            //     ->where('city','LIKE','%'.$request->session()->get('proyects_store.city').'%')
            //     ->where('status',1)
            //     ->OrderBy('created_at','desc')
            //     ->paginate(8);
            //     return response()->json(view('templates.menu-principal.vivienda.proyectos-de-vivienda._cargar-proyectos-pagination',compact('proyects'))->render());
            // }else{
            //     if($request->session()->has('proyects_store'){

            //     }else{

            //     }
                // obtener datos de consulta
                $type       = $request['type'];
                // $condition  = $request['condition'];
                $city       = $request['city'];

                // busqueda en base de datos según datos obtenidos
                $proyects = Proyect::where('type',$type)
                ->where('city','LIKE','%'.$city.'%')
                ->where('status',1)
                ->OrderBy('created_at','desc')
                ->paginate(8);

                $object = new Proyect();
                $object->type= $type;
                // $object->condition= $condition;
                $object->city= $city;

                $request->session()->put('proyects_store', $object);
                return view('templates.menu-principal.vivienda.proyectos-de-vivienda._cargar-proyectos', compact('proyects'));
            //}
            
        }
        
        return redirect()->route('vivienda');
    }
}
