<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\QuestionCategory;
use App\Question;
use App\Event;
use App\Contact;
use App\Banner as Banner;

// Facades
use Route;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var = Route::current();
        $banners = Banner::where('menus_id', '=', 14)->get();
        $events = Event::where('status',1)->get();
        return view('home', compact('events','banners'));
    }

    // Menú TOP
    public function organizacion(){
        return view('templates.menu-top.organizacion');
    }
    public function afiliado(){
        return view('templates.menu-top.afiliado');
    }

    public function servicios_en_linea(){
        return view('templates.menu-top.servicios-en-linea');
    }

    public function contacto(){
        return view('templates.menu-top.contacto');
    }

    public function contacto_preguntas_frecuentes(){
        $question_categories = QuestionCategory::with('questions')->where('status',1)->get();
        // dd($question_categories);
        return view('templates.menu-top.contacto.contacto_preguntas_frecuentes', compact('question_categories'));
    }

    public function mapa_comfenalco(){
        return view('templates.paginas.mapa');
    }

    public function empresas(){
        return view('templates.paginas.empresas');
    }

    // public function contacto_post(Request $request){
    //     $validator = Validator::make($request->all(), [
    //         'nombre'            => 'required|max:200',
    //         'email'             => 'required|email|unique:suscribers',
    //         'telefono'          => 'max:85',
    //         'mensaje'           => 'required',         
    //         'estado'            => 'required|accepted'
    //     ]);

    //     if ($validator->fails())
    //     {
    //         $request->session()->flash('error','Ups! Por favor, vuelva a intentarlo.');
    //         return redirect()->back()->withInput()->withErrors($validator->errors());
    //     }

    //     try{
    //         $contacto = new Contact);
    //         $contacto->name  = $request['nombre'];
    //         $contacto->phone = $request['telefono'];
    //         $contacto->body = $request['mensaje'];
    //         $contacto->email   = $request['email'];

    //         $suscriber->save();

    //         $nombre     = $request['nombre'];
    //         $email      = $request['email'];
    //         $telefono   = $request['telefono'];
    //         $mensaje    = $request['mensaje'];

    //         // Send
    //         $para       = "digital@guidoulloa.com";
    //         $titulo     = "Delta Ingeniería S.A - Contacto";
    //         $ok         = "Nombre y Apellidos: $nombre \r\n".
    //             "Correo:  $email \r\n".
    //             "Telefono:  $telefono \r\n".
    //             "Mensaje: $mensaje \r\n";

    //         $cabeceras = "From: Delta Ingeniería S.A";
    //         if(mail($para, $titulo, $ok, $cabeceras)){
    //             return back()->with('success', 'Tu suscripción se completo correctamente.');
    //         }else{
    //             return back()->with('error','Ups! Por favor, vuelve a intentarlo.');
    //         }        
    //     }catch(Exception $e){
    //         return "Fatal error -".$e->getMessage();
    //     }
    // }
    // }
}

