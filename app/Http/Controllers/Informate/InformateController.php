<?php

namespace App\Http\Controllers\Informate;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Facades
use Jenssegers\Date\Date;

// Models
use App\Blog;
use App\Category;

class InformateController extends Controller
{
    public function Informate(){
    	Date::setLocale('es');
    	$objects = Blog::where('status',1)->with('categorys')->get();
    	$categories = Blog::where('status',1)
            ->with('categorys')
            ->get()
            ->pluck('categorys')
            ->collapse()
            ->unique('id')
            ->values();
    	return view('templates.paginas.blog.informate', compact('objects','categories'));
    }

	public function detalle_de_post($slug){
		Date::setLocale('es');
		$object   = Blog::with('categorys')->where('slug',$slug)->FirstOrFail();
        $recients = Blog::where('status',1)->where('id','!=',$object->id)->orderBy('created_at','desc')->take(5)->get();
		return view('templates.paginas.blog.detalle-del-post', compact('object','recients'));
	}

	public function post_comment_save(){
		//
	}

	public function posts_por_categoria($slug){
		Date::setLocale('es');
		$object = Category::whereSlug($slug)->FirstOrFail();
		$objects = $object->blog_categories()->get()->pluck('blog')->where('status',1)->unique('id')->values();
		//$object   = Category::has('blogs')->where('slug',$slug)->FirstOrFail();
		$categories = Category::has('blogs')->where('id','!=',$object->id)->orderBy('created_at','desc')->get();
		return view('templates.paginas.blog.categorias.detalle-de-posts-por-categoria', compact('object', 'categories','objects'));
	}

}
