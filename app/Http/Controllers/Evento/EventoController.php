<?php

namespace App\Http\Controllers\Evento;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

// Models
use App\Event;

class EventoController extends Controller
{

	public function eventos(){
		$objects = Event::where('status',1)->get();
		return view('templates.paginas.eventos.eventos', compact('objects'));
	}

	public function detalle_de_evento($slug){
		$object = Event::where('slug',$slug)->where('status',1)->firstOrFail();
		return view('templates.paginas.eventos.detalle-de-evento', compact('object'));
	}
}
