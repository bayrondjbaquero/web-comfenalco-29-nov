<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tab extends Model
{
    public function sub_tabs(){
    	return $this->hasMany('App\SubTab', 'tabs_id');
    }
    public function second_banners(){
    	return $this->hasMany('App\SecondBanner', 'tabs_id');
    }
}
