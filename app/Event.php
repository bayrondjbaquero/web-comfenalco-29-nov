<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Facades
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    // Slug
    use SoftDeletes, Sluggable;

    protected $table        = 'events';

    /**
     * Save slug for dependencias.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => ['title']
            ],
        ];
    }

    protected $fillable = [
        'title',
        'body',
        'slug',
        'alt',
        'status',
        'image'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'date_published',
        'event_start',
        'event_end'
    ];

    /* Mutators */
    public function getCreatedAtAttribute($created_at)
    {
        return new Date($created_at);
    }

     public function getDatePublishedAttribute($date_published)
    {
        return new Date($date_published);
        
    }

    public function getEventStartAttribute($event_start)
    {
        return new Date($event_start);
    }

     public function getEventEndAttribute($event_end)
    {
        return new Date($event_end);
        
    }

}
