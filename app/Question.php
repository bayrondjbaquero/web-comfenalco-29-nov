<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

	protected $table = 'questions';

	protected $fillable = [
    	'question',
    	'answer',
    	'question_question_category_id',
    	'status'
    ];

    public function question_categories()
    {
        return $this->belongsTo('App\QuestionCategory','question_question_category_id');
    }
}
