<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Blog;
use App\Category;

class Blog_Category extends Model
{
     protected $table = 'blog_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'blog_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function blog()
    {
        return $this->belongsTo(Blog::class);
    }
}
