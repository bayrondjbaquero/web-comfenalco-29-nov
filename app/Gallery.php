<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'galleries';

    protected $fillable = [
    	'alt',
    	'image',
        'preference',
    	'status',
    	'proyect_id'
    ];

    public function proyects()
    {
        return $this->belongsTo('App\Proyect','proyect_id');
    }
}
