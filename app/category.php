<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Facades
use Cviebrock\EloquentSluggable\Sluggable;

// Models
use App\Blog_category;

class Category extends Model
{
	// Slug
    use Sluggable;

    protected $table      = 'categories';
    protected $primaryKey = 'id';

    /**
     * Save slug for dependencias.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => ['name']
            ],
        ];
    }

    protected $fillable = [
    	'name',
    	'slug'
    ];

    public function blogs()
    {
        return $this->belongsToMany('App\Blog','blog_category','category_id','blog_id')->withTimestamps();
    }

    public function blog_categories()
    {
        return $this->hasMany(Blog_category::class);
    }

    // public function blogs()
    // {
    //     return $this->belongsToMany(Blog::class);
    // }
}
