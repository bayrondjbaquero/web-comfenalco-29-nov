<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecondBanner extends Model
{
    public function tabs(){
    	return $this->belongsTo('App\Tab');
    }
}
