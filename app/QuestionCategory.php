<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Facades
use Cviebrock\EloquentSluggable\Sluggable;

class QuestionCategory extends Model
{
	// Slug
    use Sluggable;

    protected $table = 'question_categories';

    /**
     * Save slug for dependencias.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => ['name']
            ],
        ];
    }

    protected $fillable = [
    	'name',
        'slug',
    	'status',
        'ordenation'
    ];

    public function setNameAttribute($valor)
    {
        $this->attributes['name'] = strtoupper($valor);
    }

    public function questions()
    {
        return $this->hasMany('App\Question','question_question_category_id');
    }
}
