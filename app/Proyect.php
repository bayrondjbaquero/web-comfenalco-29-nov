<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Facades
use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Date\Date;
use Illuminate\Database\Eloquent\SoftDeletes;

class Proyect extends Model
{
	protected $table = 'proyects';

    // Slug
    use SoftDeletes, Sluggable;

	const PROYECTO_HABILITADO   = '1';
    const PROYECTO_INHABILITADO = '0';

    protected $dates = ['deleted_at'];
    protected $fillable = [
    	'title',
    	'slug',
        'alt',
    	'city',
    	'description',
    	'thumbnail',
    	'status',
        'neighborhood'
    ];

    /**
     * Save slug for dependencias.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => ['title']
            ],
        ];
    }

    public function galleries()
    {
        return $this->hasMany('App\Gallery');
    }

    public function getCreatedAtAttribute($created_at)
    {
        return new Date($created_at);
    }
}
