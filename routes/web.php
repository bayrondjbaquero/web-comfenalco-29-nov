<?php

/*
|--------------------------------------------------------------------------
| Routes - Comfenalco
|--------------------------------------------------------------------------
*/

// Home
Route::get('/', 			'HomeController@index')->name('home');

Auth::routes();

// Panel de Administración Routes...
Route::group(['namespace' => 'Admin', 'prefix' => 'admin','middleware'=>['auth',]], function () {
    // Admin Routes...
    Route::get('/',                         'AdminController@index')->name('admin');
    Route::get('/mi-cuenta/{id}/edit',      'AdminController@edit')->name('usuario.edit');
    Route::put('/mi-cuenta/update/{id}',    'AdminController@update')->name('usuario.update');

    Route::resource('menus',                'MenuController');
    Route::resource('banners',              'BannerController');
    Route::resource('second_banners',       'SecondBannerController');
    Route::get('contenido/listado',         'ContenidoController@listado')->name('contenido.listado');
    Route::resource('contenido',            'ContenidoController');
    Route::resource('tabs',                 'TabsController');
    Route::resource('campanas',             'CampanasController');
    Route::resource('subtabs',              'SubTabController');

    // Proyectos
    Route::resource('proyects',             'ProyectController');
    Route::resource('galleries',            'GalleryController');

    // Eventos
    Route::resource('events',               'EventController');

    // Preguntas frecuentes
    Route::resource('questions',            'FrecuentQuestionController');
    Route::resource('question_categories',  'CategoryQuestionController');

    // Blog
    Route::resource('entries',              'EntryController');
    Route::resource('categories',           'CategoryController');

    // Ordenar category
    Route::get('question_categories/order/category_view', 'CategoryQuestionController@order_edit')->name('question_categories.order.edit');
    Route::post('question_categories/order/category_update', 'CategoryQuestionController@order_update')->name('question_categories.order.update');
    
    // Crear gallery since proyect
    Route::get('galleries/create/proyecto/{proyect_id}', 'GalleryController@create_proyect_gallery')->name('galleries.create.proyect_id');
});

// Menú TOP
Route::get('/organizacion',                 'HomeController@organizacion')->name('organizacion');
Route::get('/afiliado',                     'HomeController@afiliado')->name('afiliado');
Route::get('/servicios-en-linea',           'HomeController@servicios_en_linea')->name('servicios_en_linea');
Route::get('/contactanos',                  'HomeController@contacto')->name('contacto');

// Contacto
Route::post('/contactanos',                 'HomeController@contacto_post')->name('contacto_post');

// Preguntas frecuentes
Route::get('/preguntas-frecuentes',         'HomeController@contacto_preguntas_frecuentes')->name('contacto_preguntas_frecuentes');

// Mapa MyMaps - (templates.páginas)
Route::get('/mapa-comfenalco',              'HomeController@mapa_comfenalco')->name('mapa_comfenalco');

// Blog
Route::get('/informate',                    'Informate\InformateController@informate')->name('informate');
Route::get('/informate/{slug}',             'Informate\InformateController@detalle_de_post')->name('detalle_de_post');
Route::get('/informate/category/{slug}',    'Informate\InformateController@posts_por_categoria')->name('posts_por_categoria');
Route::post('/informate/{slug}',            'Informate\InformateController@post_comment_save')->name('post_comment_save');

// Eventos
Route::get('/eventos',                      'Evento\EventoController@eventos')->name('eventos');
Route::get('/eventos/{slug}',               'Evento\EventoController@detalle_de_evento')->name('detalle_de_evento');

// Página link banner inicio "Empresas"
Route::get('/empresas',                     'HomeController@empresas')->name('empresas');

// Menú principal
Route::get('/afiliaciones',			'Menu\MenuController@afiliaciones')->name('afiliaciones');
Route::get('/educacion',			'Menu\MenuController@educacion')->name('educacion');
Route::get('/vivienda',				'Menu\MenuController@vivienda')->name('vivienda');
Route::get('/recreacion-y-turismo',	'Menu\MenuController@recreacion')->name('recreacion');
Route::get('/proteccion-social',	'Menu\MenuController@proteccion_social')->name('proteccion_social');
Route::get('/deportes',				'Menu\MenuController@deportes')->name('deportes');
Route::get('/cultura',				'Menu\MenuController@cultura')->name('cultura');
Route::get('/agencia-de-empleo',	'Menu\MenuController@agencia_de_empleo')->name('agencia_de_empleo');
Route::get('/credito-social',		'Menu\MenuController@credito_social')->name('credito_social');

// Cargar los proyectos en vivienda
Route::get('/vivienda/proyectos',   'Proyecto\ProyectController@cargar_proyectos')->name('cargar_proyectos');

// Consultar proyectos segun el tipo, la condición y ubicación
Route::post('/vivienda/proyectos',  'Proyecto\ProyectController@busqueda_proyectos')->name('busqueda_proyectos');

// Cargar proyectos por paginación
Route::get('/vivienda/proyectos/page',      'Proyecto\ProyectController@cargar_proyectos_pagination')->name('cargar_proyectos_pagination');

// Cargar información del proyecto
Route::get('/vivienda/proyectos/{slug}',    'Proyecto\ProyectController@cargar_proyecto_slug')->name('cargar_proyecto_slug');