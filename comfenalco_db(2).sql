-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 01-12-2017 a las 09:55:44
-- Versión del servidor: 5.7.20-0ubuntu0.17.04.1
-- Versión de PHP: 7.0.22-0ubuntu0.17.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `comfenalco_dbb`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menus_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `banners`
--

INSERT INTO `banners` (`id`, `alt`, `image`, `url`, `section_id`, `menus_id`, `created_at`, `updated_at`) VALUES
(1, 'Afiliación comfenalco', 'img/img-banners/banner_afiliate.jpg', NULL, NULL, 1, '2017-11-23 20:00:44', '2017-11-30 14:22:19'),
(3, 'Educación', 'img/img-banners/Banners 1110x300-03.jpg', NULL, NULL, 2, '2017-11-23 20:32:42', '2017-11-30 14:24:51'),
(4, 'Banner vivienda', 'img/img-banners/banner_sanagustin.jpg', NULL, NULL, 3, '2017-11-23 20:34:02', '2017-11-30 14:29:59'),
(5, 'Banner recreación', 'img/img-banners/banner_agenciadeturismo.jpg', NULL, NULL, 4, '2017-11-23 20:34:39', '2017-11-30 14:36:27'),
(6, 'Banner protección social', 'img/img-banners/banner_proteccionsocial.jpg', NULL, NULL, 5, '2017-11-23 20:35:11', '2017-11-30 14:44:43'),
(8, 'Banner cultura', 'img/img-banners/Comfenalco - Cursos de Instrumentos - 1110x300px.jpg', NULL, NULL, 7, '2017-11-23 20:47:12', '2017-11-30 14:47:15'),
(9, 'Banner agencia de empleo', 'img/img-banners/banner-agencia-de-empleo.jpg', NULL, NULL, 8, '2017-11-23 20:48:11', '2017-11-30 14:48:52'),
(10, 'Credito social', 'img/img-banners/Banner-Crédito.jpg', NULL, NULL, 9, '2017-11-23 20:49:49', '2017-11-30 14:49:17'),
(11, 'Banner Afiliate', 'img/img-banners/Banner-afíliate.jpg', NULL, NULL, 1, '2017-11-30 14:22:55', '2017-11-30 14:22:55'),
(12, 'Educación', 'img/img-banners/Banners 1110x300-06.jpg', NULL, NULL, 2, '2017-11-30 14:25:18', '2017-11-30 14:25:18'),
(13, 'Educación', 'img/img-banners/Banners 1110x300-02.jpg', NULL, NULL, 2, '2017-11-30 14:25:39', '2017-11-30 14:25:39'),
(14, 'Educación', 'img/img-banners/Comfenalco 1.jpg', NULL, NULL, 2, '2017-11-30 14:25:57', '2017-11-30 14:25:57'),
(15, 'Afiliación comfenalco', 'img/img-banners/Comfenalco - Pensionado - 1110x300px.jpg', NULL, NULL, 1, '2017-11-30 14:27:47', '2017-11-30 14:27:47'),
(16, 'Banner  Educación', 'img/img-banners/Banner-Cedesarrollo.jpg', NULL, NULL, 2, '2017-11-30 14:28:26', '2017-11-30 14:28:26'),
(17, 'Banner vivienda', 'img/img-banners/banner_mirador.jpg', NULL, NULL, 3, '2017-11-30 14:30:10', '2017-11-30 14:30:10'),
(18, 'Banner vivienda', 'img/img-banners/banner_atlantic.jpg', NULL, NULL, 3, '2017-11-30 14:30:19', '2017-11-30 14:30:19'),
(19, 'Banner recreación', 'img/img-banners/banner_takurika.jpg', NULL, NULL, 4, '2017-11-30 14:31:37', '2017-11-30 14:43:14'),
(21, 'Banner recreación', 'img/img-banners/Banner-Recreación y turismo.jpg', NULL, NULL, 4, '2017-11-30 14:31:52', '2017-11-30 14:43:29'),
(24, 'Banner recreación', 'img/img-banners/Comfenalco - Takurika Eventos - 1110x300px.jpg', NULL, NULL, 4, '2017-11-30 14:43:51', '2017-11-30 14:43:51'),
(25, 'Banner recreación', 'img/img-banners/Comfenalco 4.jpg', NULL, NULL, 4, '2017-11-30 14:43:59', '2017-11-30 14:43:59'),
(26, 'Banner recreación', 'img/img-banners/Comfenalco 5.jpg', NULL, NULL, 4, '2017-11-30 14:44:05', '2017-11-30 14:44:05'),
(27, 'Banner protección social', 'img/img-banners/Comfenalco - Protección Social - 1110x300px.jpg', NULL, NULL, 5, '2017-11-30 14:45:03', '2017-11-30 14:45:03'),
(28, 'Banner deportes', 'img/img-banners/Banner-Deportes.jpg', NULL, NULL, 6, '2017-11-30 14:45:15', '2017-11-30 14:45:34'),
(29, 'Banner deportes', 'img/img-banners/Banners 1110x300-07.jpg', NULL, NULL, 6, '2017-11-30 14:46:03', '2017-11-30 14:46:03'),
(30, 'Banner deportes', 'img/img-banners/Banners 1110x300-08.jpg', NULL, NULL, 6, '2017-11-30 14:46:10', '2017-11-30 14:46:10'),
(31, 'Banner dvivienda', 'img/img-banners/BannerWeb_CalendarioVivienda2018.png', NULL, NULL, 3, '2017-11-30 14:46:44', '2017-11-30 14:46:44'),
(32, 'Banner deportes', 'img/img-banners/Banner-Magangué.jpg', NULL, NULL, 6, '2017-11-30 14:48:14', '2017-11-30 14:48:14'),
(33, 'Credito social', 'img/img-banners/Banners 1110x300-01.jpg', NULL, NULL, 9, '2017-11-30 14:49:32', '2017-11-30 14:49:32'),
(35, 'Banner inicio', 'img/img-banners/Comfenalco 3.jpg', NULL, NULL, 14, '2017-11-30 14:53:00', '2017-11-30 14:53:00'),
(36, 'Banner inicio', 'img/img-banners/Banners 1110x300-04.jpg', NULL, NULL, 14, '2017-11-30 14:53:12', '2017-11-30 14:53:12'),
(37, 'Banner inicio', 'img/img-banners/Banner-afíliate.jpg', NULL, NULL, 14, '2017-11-30 14:53:38', '2017-11-30 14:54:00'),
(38, 'Banner inicio', 'img/img-banners/Banner-Crédito.jpg', NULL, NULL, 14, '2017-11-30 14:54:36', '2017-11-30 14:54:36'),
(39, 'Banner inicio', 'img/img-banners/Banner-Deportes.jpg', NULL, NULL, 14, '2017-11-30 14:54:46', '2017-11-30 14:54:46'),
(40, 'Banner inicio', 'img/img-banners/Banner-Recreación y turismo.jpg', NULL, NULL, 14, '2017-11-30 14:54:56', '2017-11-30 14:54:56'),
(41, 'Banner inicio', 'img/img-banners/Banners 1110x300-02.jpg', NULL, NULL, 14, '2017-11-30 14:55:11', '2017-11-30 14:55:11'),
(42, 'Banner inicio', 'img/img-banners/Banners 1110x300-04.jpg', NULL, NULL, 14, '2017-11-30 14:55:20', '2017-11-30 14:55:20'),
(43, 'Banner afiliacion', 'img/img-banners/Comfenalco - Pague Aqui - 1110x300px.jpg', NULL, NULL, 1, '2017-11-30 14:55:39', '2017-11-30 14:55:39'),
(44, 'Banner recreación', 'img/img-banners/Banner-Centro Empresarial.jpg', NULL, NULL, 4, '2017-11-30 14:56:10', '2017-11-30 14:56:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_published` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category_blog`
--

CREATE TABLE `category_blog` (
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `blog_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configs`
--

CREATE TABLE `configs` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_start` datetime NOT NULL,
  `event_end` datetime NOT NULL,
  `date_published` date NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `preference` int(11) NOT NULL DEFAULT '1',
  `proyect_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `type_contact` tinyint(4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type_menu` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `title`, `slug`, `icon`, `url`, `description`, `type_contact`, `status`, `type_menu`, `created_at`, `updated_at`) VALUES
(1, 'Afiliaciones', 'afiliaciones', NULL, 'afiliaciones', NULL, 0, 1, 1, '2017-11-23 15:57:14', '2017-11-23 17:06:12'),
(2, 'Educación', 'educacion', NULL, 'educacion', NULL, 0, 1, 1, '2017-11-23 15:58:32', '2017-11-23 17:06:12'),
(3, 'Vivienda', 'vivienda', NULL, 'vivienda', '<p><strong class=\"text-marron\">El servicio de vivienda</strong> trabaja por el bienestar de nuestros afiliados, ofrece diferentes servicios para solucionar las necesidades de vivienda de nuestra ciudad y de todo el departamento de Bol&iacute;var.</p>', 0, 1, 1, '2017-11-23 15:58:39', '2017-11-23 17:06:12'),
(4, 'Recreación y turismo', 'recreacion-y-turismo', NULL, 'recreacion', NULL, 0, 1, 1, '2017-11-23 15:59:06', '2017-11-23 17:09:50'),
(5, 'Proteccion Social', 'proteccion-social', NULL, 'proteccion_social', '<p><strong class=\"text-orange\">CALIDAD DE VIDA DE LA COMUNIDAD: NUESTRO COMPROMISO</strong><br />Pensando en tu bienestar, desarrollamos en comunidades programas y proyectos sociales que contribuyen a mejorar la calidad de vida de nuestros beneficiarios y población vulnerable en cada una de las etapas de su vida.<br /><br /><strong class=\"text-orange\">CERTIFICADOS EN CALIDAD</strong><br />Nuestros procesos administrativos están certificados bajo la norma 9001: 2008.</p>', 0, 1, 1, '2017-11-23 15:59:26', '2017-11-23 17:09:50'),
(6, 'Deportes', 'deportes', NULL, 'deportes', NULL, 0, 1, 1, '2017-11-23 15:59:39', '2017-11-23 17:06:12'),
(7, 'Cultura', 'cultura', NULL, 'cultura', '<p>COMFENALCO &ndash; CARTAGENA, comprometida con el progreso y el desarrollo de los cartageneros y bolivarenses, ha puesto en marcha el <strong class=\"text-purple\">Servicio de Cultura</strong>, cuyo objetivo es gestionar, promover y desarrollar programas y proyectos que contribuyan a mejorar la calidad de vida de afiliados y no afiliados mediante acciones que propicien integralmente el desarrollo de la m&uacute;sica; las artes y el fomento al patrimonio hist&oacute;rico y arquitect&oacute;nico de Cartagena y la regi&oacute;n.</p>', 0, 1, 1, '2017-11-23 15:59:44', '2017-11-23 17:06:12'),
(8, 'Agencia de empleo', 'agencia-de-empleo', NULL, 'agencia_de_empleo', NULL, 0, 1, 1, '2017-11-23 15:59:53', '2017-11-23 17:09:50'),
(9, 'Crédito social', 'credito-social', NULL, 'credito_social', NULL, 0, 1, 1, '2017-11-23 16:00:01', '2017-11-23 17:09:50'),
(10, 'Organización', 'organizacion', 'img/iconos-menu/organizacion.png', 'organizacion', NULL, 0, 1, 2, '2017-11-30 13:26:26', '2017-11-30 13:26:26'),
(11, 'Afiliado', 'afiliado', 'img/iconos-menu/afiliado.png', 'afiliado', NULL, 0, 1, 2, '2017-11-30 13:29:58', '2017-11-30 13:29:58'),
(12, 'Servicios en Línea', 'servicios-en-linea', 'img/iconos-menu/servicios-en-linea.png', 'servicios_en_linea', NULL, 0, 1, 2, '2017-11-30 13:30:26', '2017-11-30 13:30:26'),
(13, 'Contáctanos', 'contactanos', 'img/iconos-menu/contacto.png', 'contactanos', NULL, 0, 1, 2, '2017-11-30 13:30:47', '2017-11-30 13:30:47'),
(14, 'inicio', 'inicio', NULL, '/', NULL, 1, 1, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_11_03_171332_create_contacts_table', 1),
(4, '2017_11_03_171419_create_proyects_table', 1),
(5, '2017_11_03_171432_create_events_table', 1),
(6, '2017_11_03_171450_create_blogs_table', 1),
(7, '2017_11_03_171502_create_categories_table', 1),
(8, '2017_11_03_171514_create_menus_table', 1),
(9, '2017_11_03_171629_create_configs_table', 1),
(10, '2017_11_07_093937_create_admin_table', 1),
(11, '2017_11_07_103002_create_category_blog_table', 1),
(12, '2017_11_14_165659_create_galleries_table', 1),
(13, '2017_11_22_100332_create_question_categories_table', 1),
(14, '2017_11_22_105907_create_questions_table', 1),
(16, '2017_11_23_094917_create_banners_table', 1),
(17, '2017_11_23_164431_create_tabs_table', 2),
(18, '2017_11_24_100457_create_sub_tabs_table', 3),
(19, '2017_11_23_091954_create_second_banners_table', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyects`
--

CREATE TABLE `proyects` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` text COLLATE utf8mb4_unicode_ci,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('casa','apartamento') COLLATE utf8mb4_unicode_ci NOT NULL,
  `condition` enum('arriendo','venta') COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `neighborhood` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `preference` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `question_categories`
--

CREATE TABLE `question_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `second_banners`
--

CREATE TABLE `second_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `alt` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabs_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `second_banners`
--

INSERT INTO `second_banners` (`id`, `alt`, `image`, `url`, `section_id`, `tabs_id`, `created_at`, `updated_at`) VALUES
(1, 'Takurika', 'img/img-banners-tabs/recreacion-banner-takurika.jpg', NULL, NULL, 14, '2017-11-24 22:46:21', '2017-11-24 22:46:21'),
(3, 'Banner centro empresarial', 'img/img-banners-tabs/recreacion-centro-empresarial.jpg', NULL, NULL, 15, '2017-12-01 14:30:16', '2017-12-01 14:30:16'),
(4, 'Banner jardín botanico', 'img/img-banners-tabs/recreacion-jardin-botanico.jpg', NULL, NULL, 16, '2017-12-01 14:31:50', '2017-12-01 14:31:50'),
(5, 'Banner sede recreacional', 'img/img-banners-tabs/recreacion-magangue.jpg', NULL, NULL, 17, '2017-12-01 14:36:54', '2017-12-01 14:36:54'),
(6, 'Banner hotel corales de indias', 'img/img-banners-tabs/recreacion-hotel-corales-de-indias.jpg', NULL, NULL, 18, '2017-12-01 14:37:49', '2017-12-01 14:37:49');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_tabs`
--

CREATE TABLE `sub_tabs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tabs_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type_design` int(11) NOT NULL DEFAULT '1',
  `slug` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sub_tabs`
--

INSERT INTO `sub_tabs` (`id`, `title`, `tabs_id`, `status`, `type_design`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'INFORMACIÓN GENERAL', 1, 0, 1, 'informacion-general', NULL, '2017-11-27 21:28:17'),
(2, 'AFILIACIÓN TRABAJADOR', 1, 0, 1, 'afiliacion-trabajador', NULL, '2017-11-27 21:28:17'),
(6, 'AFILIACIÓN DE GRUPO FAMILIAR', 1, 1, 2, 'afiliacion-de-grupo-familiar', '2017-11-24 21:59:32', '2017-11-27 21:28:17'),
(7, 'SUBSIDIO FAMILIAR', 1, 1, 1, 'subsidio-familiar', '2017-11-24 21:59:56', '2017-11-27 21:28:17'),
(8, 'AFÍLIATE', 1, 1, 1, 'afiliate', '2017-11-24 22:00:09', '2017-11-27 21:28:17'),
(9, 'ASOPAGOS', 1, 1, 1, 'asopagos', '2017-11-24 22:00:21', '2017-11-27 21:28:17'),
(10, 'AFILIACIÓN DISCAPACITADOS', 1, 1, 1, 'afiliacion-discapacitados', '2017-11-24 22:00:27', '2017-11-27 21:28:17'),
(11, 'INFORMACIÓN GENERAL', 4, 1, 1, 'informacion-general-1', '2017-11-24 22:00:57', '2017-11-27 21:28:17'),
(12, 'AFILIACIÓN TRABAJADOR', 4, 1, 1, 'afiliacion-trabajador-1', '2017-11-24 22:01:24', '2017-11-27 21:28:18'),
(13, 'AFILIACIÓN GRUPO FAMILIAR', 4, 1, 2, 'afiliacion-grupo-familiar', '2017-11-24 22:01:37', '2017-11-27 21:28:18'),
(15, 'BÁSICA Y MEDIA', 5, 1, 1, 'basica-y-media', '2017-11-24 22:02:29', '2017-11-27 21:28:18'),
(16, 'ADMISIONES', 5, 1, 1, 'admisiones', '2017-11-24 22:02:36', '2017-11-27 21:28:18'),
(17, 'PORTAL EDUCATIVO', 5, 1, 1, 'portal-educativo', '2017-11-24 22:03:12', '2017-11-27 21:28:18'),
(18, 'LOGROS Y RECONOCIMIENTOS', 5, 1, 1, 'logros-y-reconocimientos', '2017-11-24 22:03:32', '2017-11-27 21:28:18'),
(19, 'DOBLE CERTIFICACIÓN', 5, 1, 1, 'doble-certificacion', '2017-11-24 22:03:47', '2017-11-27 21:28:18'),
(20, 'BIBLIOTECA', 5, 1, 1, 'biblioteca', '2017-11-24 22:03:56', '2017-11-27 21:28:18'),
(21, 'INFORMACIÓN INSTITUCIONAL', 5, 1, 1, 'informacion-institucional', '2017-11-24 22:04:18', '2017-11-27 21:28:18'),
(22, 'CEDESARROLLO', 6, 1, 1, 'cedesarrollo', '2017-11-24 22:04:42', '2017-11-27 21:28:18'),
(23, 'PROGRAMAS TÉCNICO LABORALES', 6, 1, 1, 'programas-tecnico-laborales', '2017-11-24 22:04:54', '2017-11-27 21:28:18'),
(24, 'Admisiones y Registros', 6, 1, 1, 'admisiones-y-registros', '2017-11-24 22:05:00', '2017-11-27 21:28:18'),
(25, 'Facilidades de Financiación', 6, 1, 1, 'facilidades-de-financiacion', '2017-11-24 22:05:14', '2017-11-27 21:28:18'),
(26, 'INFORMACIÓN GENERAL', 8, 1, 1, 'informacion-general', '2017-11-24 22:05:39', '2017-11-27 21:28:19'),
(27, 'CAPACITACIONES EMPRESARIALES', 8, 1, 1, 'capacitaciones-empresariales', '2017-11-24 22:05:46', '2017-11-27 21:28:19'),
(28, 'DIAGNÓSTICO EMPRESARIAL Y CONSULTORÍAS', 8, 1, 1, 'diagnostico-empresarial-y-consultorias', '2017-11-24 22:05:52', '2017-11-27 21:28:19'),
(29, 'CÓMO POSTULARTE', 10, 1, 1, 'como-postularte', '2017-11-24 22:22:57', '2017-11-27 21:28:19'),
(30, 'MODALIDADES', 10, 1, 2, 'modalidades', '2017-11-24 22:23:04', '2017-11-27 21:28:19'),
(31, 'VALOR SUBSIDIO DE VIVIENDA', 10, 1, 1, 'valor-subsidio-de-vivienda', '2017-11-24 22:23:29', '2017-11-27 21:28:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tabs`
--

CREATE TABLE `tabs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(9000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `menus_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `type_tabs` tinyint(1) NOT NULL DEFAULT '1',
  `type_design` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `tabs`
--

INSERT INTO `tabs` (`id`, `title`, `slug`, `image`, `description`, `menus_id`, `status`, `type_tabs`, `type_design`, `created_at`, `updated_at`) VALUES
(1, 'EMPRESAS', 'empresas', 'img/img-tabs/afiliaciones-info.jpg', '<h3 class=\"text-blue-2\">EMPRESAS:</h3>\r\n<p>Para la afiliaci&oacute;n presenta la siguiente documentaci&oacute;n en nuestros Puntos de Atenci&oacute;n o env&iacute;ala al correo <a href=\"mailto:afiliacionesempresariales@comfenalco.com\">afiliacionesempresariales@comfenalco.com</a></p>\r\n<p>&nbsp;</p>\r\n<p class=\"mb-0\"><img class=\"d-flex mr-2\" src=\"/website-comfenalco/public/img/check-yellow.jpg\" alt=\"\" /> C&aacute;mara de Comercio (90 d&iacute;as de vigencia)</p>\r\n<div class=\"col-md-12 pt-2 pb-2\">\r\n<div class=\"media\"><img class=\"d-flex mr-2\" src=\"/website-comfenalco/public/img/check-yellow.jpg\" alt=\"\" />Fotocopia de RUT</div>\r\n</div>\r\n<div class=\"col-md-12 pt-2 pb-2\">\r\n<div class=\"media\"><img class=\"d-flex mr-2\" src=\"/website-comfenalco/public/img/check-yellow.jpg\" alt=\"\" />Fotocopia de CC de representante legal</div>\r\n</div>\r\n<div class=\"col-md-12 pt-2 pb-2\">\r\n<div class=\"media\"><img class=\"d-flex mr-2\" src=\"/website-comfenalco/public/img/check-yellow.jpg\" alt=\"\" />Relaci&oacute;n de n&oacute;mina</div>\r\n</div>', 1, NULL, 2, 0, '2017-11-23 22:06:57', '2017-11-23 22:06:57'),
(2, 'INDEPENDIENTES', 'independientes', 'img/img-tabs/afiliaciones-independientes.jpg', '<p class=\"text-justify\">Comfenalco le ofrece a los independientes la oportunidad de afiliarse a la caja de compensaci&oacute;n para disfrutar de todos los servicios que ofrecemos.<br /> La afiliaci&oacute;n voluntaria y tiene 2 opciones de pago de aportes:</p>\r\n<p>Aquellos independientes que aportes el 0.6% de sus ingresos mensuales tendr&aacute;n derecho a nuestros servicios de Recreaci&oacute;n, Capacitaci&oacute;n y Turismo.</p>\r\n<p>Aquellos independientes que voluntariamente aporten el 2% de sus ingresos mensuales tendr&aacute;n derecho a disfrutar de todos nuestros servicios, excepto subsidio familiar y cr&eacute;dito social.</p>\r\n<p>&nbsp;</p>', 1, NULL, 1, 1, '2017-11-23 22:08:41', '2017-11-23 22:08:41'),
(3, 'PENSIONADOS', 'pensionados', 'img/img-tabs/logo.png', '<p>Comfenalco le ofrece a los pensionados, el derecho a disfrutar todos los servicios que ofrece la Caja de Compensaci&oacute;n, de acuerdo a su tipo de afiliaci&oacute;n, basado en el siguiente cuadro.</p>\r\n<p>&nbsp;</p>\r\n<p><a href=\"/website-comfenalco/public/img/tabla-pensionados.png\" target=\"_blank\" rel=\"noopener\"><img class=\"img-fluid d-block m-auto\" src=\"/website-comfenalco/public/img/tabla-pensionados.png\" alt=\"\" /></a></p>', 1, NULL, 1, 1, '2017-11-23 22:16:28', '2017-11-23 22:16:28'),
(4, 'SERVICIO DOMESTICO', 'servicio-domestico', 'img/img-tabs/logo.png', '<p class=\"text-justify\">En el Decreto 00721 de 2013, que reglamenta el Numeral 4&deg; del Art. 7&deg; de la Ley 21 de 1982, estipula que todos los empleadores que ocupen uno o m&aacute;s trabajadores permanentes, incluyendo los trabajadores de servicio dom&eacute;stico, est&aacute;n obligados a pagar el subsidio familiar.</p>\r\n<p><span class=\"h4 text-blue-2\">&iquest;A qui&eacute;nes se consideran trabajadores del servicio dom&eacute;stico?</span></p>\r\n<p class=\"text-justify\">Se considera trabajador del servicio dom&eacute;stico a la persona natural que, a cambio de una remuneraci&oacute;n, presta su servicio personal de manera directa, habitual y bajo continuada subordinaci&oacute;n o dependencia, a una o varias personas naturales, para la ejecuci&oacute;n de tareas de aseo, cocina, lavado, planchado, cuidado de ni&ntilde;os y dem&aacute;s labores propias del hogar del empleador.</p>\r\n<p class=\"text-justify\">Otro de los beneficios a los que tambi&eacute;n pueden acceder los trabajadores dom&eacute;sticos afiliados a las Cajas es el de recibir la cuota moderadora o subsidio familiar si se cumple con un m&iacute;nimo de 96 horas laboradas en el mes, por cada persona a cargo del afiliado, as&iacute; como los dem&aacute;s servicios que ofrece nuestro portafolio (Educaci&oacute;n, Recreaci&oacute;n, Deportes, Vivienda, Cr&eacute;dito, entre otros).</p>\r\n<p><span class=\"h4 text-blue-2 pb-2\">&iquest;C&oacute;mo afiliar a los trabajadores de servicio dom&eacute;stico?</span></p>\r\n<p class=\"text-justify\">Antes de afiliar a los trabajadores del servicio dom&eacute;stico, la persona natural debe afiliarse como empleador de Servicio Dom&eacute;stico.<br /><br />La afiliaci&oacute;n se puede realizar diligenciando el formulario de empleadores y de afiliaci&oacute;n de trabajadores anexando fotocopia de c&eacute;dula del empleador y del empleado.</p>', 1, NULL, 2, 0, '2017-11-23 22:16:53', '2017-11-23 22:16:53'),
(5, 'EDUCACIÓN', 'educacion', 'img/img-tabs/logo.png', NULL, 2, NULL, 2, 0, '2017-11-23 22:44:45', '2017-11-23 22:44:45'),
(6, 'EDUCACIÓN PARA EL TRABAJO', 'educacion-para-el-trabajo', 'img/img-tabs/logo.png', NULL, 2, NULL, 2, 0, '2017-11-23 22:45:17', '2017-11-23 22:45:17'),
(7, 'EDUCACIÓN CONTINUA', 'educacion-continua', 'img/img-tabs/logo.png', NULL, 2, NULL, 1, 1, '2017-11-23 22:45:38', '2017-11-23 22:45:38'),
(8, 'FOMENTO EMPRESARIAL', 'fomento-empresarial', 'img/img-tabs/logo.png', NULL, 2, NULL, 2, 0, '2017-11-23 22:45:52', '2017-11-23 22:45:52'),
(9, 'SUBSIDIO DE VIVIENDA', 'subsidio-de-vivienda', 'img/img-tabs/logo.png', NULL, 3, NULL, 1, 1, '2017-11-23 22:46:54', '2017-11-23 22:46:54'),
(10, 'SUBSIDIO DE VIVIENDA PARA AFILIADOS', 'subsidio-de-vivienda-para-afiliados', 'img/img-tabs/logo.png', NULL, 3, NULL, 2, 0, '2017-11-23 22:47:16', '2017-11-23 22:47:16'),
(11, 'SUBSIDIO DE VIVIENDA PARA NO AFILIADOS', 'subsidio-de-vivienda-para-no-afiliados', 'img/img-tabs/logo.png', NULL, 3, NULL, 1, 1, '2017-11-23 22:47:33', '2017-11-23 22:47:33'),
(12, 'PROYECTOS DE VIVIENDA', 'proyectos-de-vivienda', 'img/img-tabs/logo.png', NULL, 3, NULL, 1, 1, '2017-11-23 22:47:47', '2017-11-23 22:47:47'),
(13, 'PROMOCIÓN INMOBILIARIA', 'promocion-inmobiliaria', 'img/img-tabs/logo.png', NULL, 3, NULL, 1, 1, '2017-11-23 22:47:59', '2017-11-24 15:25:00'),
(14, 'TAKURIKA', 'takurika', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:48:30', '2017-11-23 22:48:30'),
(15, 'CENTRO EMPRESARIAL COMFENALCO', 'centro-empresarial-comfenalco', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:48:47', '2017-11-23 22:48:47'),
(16, 'JARDÍN BOTÁNICO', 'jardin-botanico', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:49:17', '2017-11-23 22:49:17'),
(17, 'SEDE RECREACIONAL MAGANGUE', 'sede-recreacional-magangue', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:49:44', '2017-11-23 22:49:44'),
(18, 'HOTEL CORALES DE INDIAS', 'hotel-corales-de-indias', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:49:59', '2017-11-23 22:49:59'),
(19, 'AGENCIA DE VIAJES Y TURISMO', 'agencia-de-viajes-y-turismo', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:50:22', '2017-11-23 22:50:22'),
(20, 'ALIANZA Y EVENTOS', 'alianza-y-eventos', 'img/img-tabs/logo.png', NULL, 4, NULL, 1, 1, '2017-11-23 22:50:57', '2017-11-23 22:50:57'),
(21, 'PROGRAMA DE ATENCIÓN INTEGRAL A LA NIÑEZ-PAIN', 'programa-de-atencion-integral-a-la-ninez-pain', 'img/img-tabs/logo.png', NULL, 5, NULL, 1, 1, '2017-11-23 22:52:39', '2017-11-23 22:52:39'),
(22, 'JORNADA ESCOLAR COMPLEMENTARIA - JEC', 'jornada-escolar-complementaria-jec', 'img/img-tabs/logo.png', NULL, 5, NULL, 1, 1, '2017-11-23 22:52:50', '2017-11-23 22:52:50'),
(23, 'ATENCIÓN INTEGRAL AL ADULTO MAYOR', 'atencion-integral-al-adulto-mayor', 'img/img-tabs/logo.png', NULL, 5, NULL, 1, 1, '2017-11-23 22:53:01', '2017-11-23 22:53:01'),
(24, 'ATENCIÓN BIOPSICOSOCIAL DIRIGIDO A PERSONAS CON DISCAPACIDAD Y SUS FAMILIAS', 'atencion-biopsicosocial-dirigido-a-personas-con-discapacidad-y-sus-familias', 'img/img-tabs/logo.png', NULL, 5, NULL, 1, 1, '2017-11-23 22:53:10', '2017-11-23 22:53:10'),
(25, 'ATENCIÓN PSICOSOCIAL A VICTIMAS DEL CONFLICTO ARMADO', 'atencion-psicosocial-a-victimas-del-conflicto-armado', 'img/img-tabs/logo.png', NULL, 5, NULL, 1, 1, '2017-11-23 22:53:27', '2017-11-23 22:53:27'),
(26, 'ESCUELAS DEPORTIVAS', 'escuelas-deportivas', 'img/img-tabs/logo.png', NULL, 6, NULL, 1, 1, '2017-11-23 22:53:40', '2017-11-23 22:53:40'),
(27, 'ESCENARIOS DEPORTIVOS', 'escenarios-deportivos', 'img/img-tabs/logo.png', NULL, 6, NULL, 1, 1, '2017-11-23 22:53:59', '2017-11-23 22:53:59'),
(28, 'CLUB DEPORTIVO', 'club-deportivo', 'img/img-tabs/logo.png', NULL, 6, NULL, 1, 1, '2017-11-23 22:54:09', '2017-11-23 22:54:09'),
(29, 'JUEGOS INTEREMPRESAS', 'juegos-interempresas', 'img/img-tabs/logo.png', NULL, 6, NULL, 1, 1, '2017-11-23 22:54:20', '2017-11-23 22:54:20'),
(30, 'SERVICIOS DEPORTIVOS', 'servicios-deportivos', 'img/img-tabs/logo.png', NULL, 6, NULL, 1, 1, '2017-11-23 22:54:30', '2017-11-23 22:54:30'),
(31, 'ALIANZAS', 'alianzas', 'img/img-tabs/logo.png', NULL, 6, NULL, 1, 1, '2017-11-23 22:54:38', '2017-11-23 22:54:38'),
(32, 'ESCUELA DE MÚSICA', 'escuela-de-musica', 'img/img-tabs/logo.png', NULL, 7, NULL, 1, 1, '2017-11-23 22:54:52', '2017-11-23 22:54:52'),
(33, 'FOMENTO A LA CULTURA', 'fomento-a-la-cultura', 'img/img-tabs/logo.png', NULL, 7, NULL, 1, 1, '2017-11-23 22:55:02', '2017-11-23 22:55:02'),
(34, 'ALIANZAS INTERNACIONALES', 'alianzas-internacionales', 'img/img-tabs/logo.png', NULL, 7, NULL, 1, 1, '2017-11-23 22:55:13', '2017-11-23 22:55:13'),
(35, 'SUBSIDIO AL DESEMPLEO', 'subsidio-al-desempleo', 'img/img-tabs/logo.png', NULL, 8, NULL, 1, 1, '2017-11-23 22:55:25', '2017-11-23 22:55:25'),
(36, 'AGENCIA DE EMPLEO', 'agencia-de-empleo', 'img/img-tabs/logo.png', NULL, 8, NULL, 1, 1, '2017-11-23 22:55:40', '2017-11-23 22:55:40'),
(37, 'CUPOCRÉDITO', 'cupocredito', 'img/img-tabs/logo.png', NULL, 9, NULL, 1, 1, '2017-11-23 22:55:49', '2017-11-23 22:55:49'),
(38, 'LIBRANZA', 'libranza', 'img/img-tabs/logo.png', NULL, 9, NULL, 1, 1, '2017-11-23 22:55:56', '2017-11-23 22:55:56'),
(39, 'LÍNEA EXPRESS', 'linea-express', 'img/img-tabs/logo.png', NULL, 9, NULL, 1, 1, '2017-11-23 22:56:05', '2017-11-23 22:56:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verified` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `verification_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'false',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `verified`, `verification_token`, `admin`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'administrador web', 'bayronbb@gmail.com', '$2y$10$DjpwjoCYSdpHTqe6NZhBd.m9Tjtsjx0344Sj.QtrL9HLYArnr3wAS', '0', NULL, 'false', '6nHfahRGY4aJGF5JDy7r8n0f3fyZaZgQO2eU8i3J0eZiCmnCLU0KvVhOy6vu', '2017-11-23 14:53:16', '2017-11-23 14:53:16', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indices de la tabla `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `banners_menus_id_foreign` (`menus_id`);

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blogs_title_unique` (`title`),
  ADD UNIQUE KEY `blogs_slug_unique` (`slug`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indices de la tabla `category_blog`
--
ALTER TABLE `category_blog`
  ADD KEY `category_blog_category_id_foreign` (`category_id`),
  ADD KEY `category_blog_blog_id_foreign` (`blog_id`);

--
-- Indices de la tabla `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `events_slug_unique` (`slug`),
  ADD KEY `events_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `galleries_proyect_id_foreign` (`proyect_id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_slug_unique` (`slug`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `proyects`
--
ALTER TABLE `proyects`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `question_categories`
--
ALTER TABLE `question_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `question_categories_slug_unique` (`slug`);

--
-- Indices de la tabla `second_banners`
--
ALTER TABLE `second_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `second_banners_tabs_id_foreign` (`tabs_id`);

--
-- Indices de la tabla `sub_tabs`
--
ALTER TABLE `sub_tabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_tabs_tabs_id_foreign` (`tabs_id`);

--
-- Indices de la tabla `tabs`
--
ALTER TABLE `tabs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tabs_menus_id_foreign` (`menus_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `proyects`
--
ALTER TABLE `proyects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `question_categories`
--
ALTER TABLE `question_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `second_banners`
--
ALTER TABLE `second_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `sub_tabs`
--
ALTER TABLE `sub_tabs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `tabs`
--
ALTER TABLE `tabs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `banners`
--
ALTER TABLE `banners`
  ADD CONSTRAINT `banners_menus_id_foreign` FOREIGN KEY (`menus_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `category_blog`
--
ALTER TABLE `category_blog`
  ADD CONSTRAINT `category_blog_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_blog_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `galleries`
--
ALTER TABLE `galleries`
  ADD CONSTRAINT `galleries_proyect_id_foreign` FOREIGN KEY (`proyect_id`) REFERENCES `proyects` (`id`);

--
-- Filtros para la tabla `second_banners`
--
ALTER TABLE `second_banners`
  ADD CONSTRAINT `second_banners_tabs_id_foreign` FOREIGN KEY (`tabs_id`) REFERENCES `tabs` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sub_tabs`
--
ALTER TABLE `sub_tabs`
  ADD CONSTRAINT `sub_tabs_tabs_id_foreign` FOREIGN KEY (`tabs_id`) REFERENCES `tabs` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `tabs`
--
ALTER TABLE `tabs`
  ADD CONSTRAINT `tabs_menus_id_foreign` FOREIGN KEY (`menus_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
