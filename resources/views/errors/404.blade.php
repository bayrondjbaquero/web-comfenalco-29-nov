<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Página 404 - COMFENALCO Cartagena / Bolívar</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script type="text/javascript" src="{{ asset('js/chatlivecomfenalco.js') }}"></script>
</head>
<body>
    <a href="{{ route('home') }}"><img class="img-fluid" src="{{asset('img/Error-pagina-404-Comfenalco.jpg')}}" alt=""></a>
</body>
</html>
