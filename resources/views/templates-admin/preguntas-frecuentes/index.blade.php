@extends('layouts.admin.dashboard')

@section('titulo_dash')
	Preguntas Frecuentes
@endsection

@section('style')
@endsection

@section('titulo_page')
	Preguntas Frecuentes
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-list"></i> Listar Preguntas Frecuentes</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@include('includes.alert')
	          	<div class="col-xs-12">
					<div class="table-responsive">
					    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
					        <thead>
						        <tr>
						        	<th>#</th>
						            <th>Pregunta</th>
						            <th>Respuesta</th>
									<th>Categoría</th>
									<th>Estado</th>
						            <th>Acción</th>
						        </tr>
					        </thead>
					        <tbody>
					        @forelse($objects as $object)
					            <tr>
					            	<td>{{$loop->iteration}}</td>
									<td>{{$object->question}}</td>
									<td>{!! \Illuminate\Support\Str::words($object->answer,150) !!}</td>
									<td>{{$object->question_categories->name}}</td>
									<td>
										@if($object->status == 0)
											No publicado
										@else 
											Publicado
										@endif
									</td>
									<td style="width:10%">
										<ul style="list-style: none;padding-left: 0;">
											<li style="display:inline-block;vertical-align: top">
												{!!Form::open(['route'=>['questions.destroy',$object->id],'method'=>'DELETE'])!!}
												{{ csrf_field() }}
								               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente una pregunta frecuente?');" class="btn btn-danger btn-block text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i></button>
								                {!! Form::close() !!}
						            		</li>
						            		<li style="display:inline-block">
												<a href="{{URL::route('questions.edit',$object->id)}}" class="btn btn-primary btn-block text-left"><i class="fa fa-pencil"></i></a>
											</li>
										</ul>
									</td>
					            </tr>
					        @empty
					            <div class="alert alert-dismissable alert-warning text-white">
					                <button type="button" class="close" data-dismiss="alert">×</button>
					                <h4>Mensaje del sistema!</h4>
					                <p>Ups!, no se encontraron Preguntas Frecuentes.</p>
					            </div>
					            <td colspan="6">No hay preguntas frecuentes para mostrar</td>
					        @endforelse
					        </tbody>
					    </table>
					</div>		      		    			
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('script')
@endsection