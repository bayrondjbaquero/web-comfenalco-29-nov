@extends('layouts.admin.dashboard')

@section('titulo_dash')
	Preguntas frecuentes 
@endsection

@section('styles')
@endsection

@section('titulo_page')
	Preguntas frecuentes 
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Añadir Categoría</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
	          	<!-- Formulario ADD PREGUNTAS -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'questions.store','method'=> 'POST']) !!}
		          		{!! csrf_field() !!}
		          		<div class="form-group">
	                        <label for="question">Pregunta *</label>
	                        {!! Form::text('question',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
	                    </div>
	                    <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('answer', 'Respuesta') !!}
                            <textarea name="answer" class="form-control my-editor">{{old('answer')}}</textarea>
                        </div>
	                    <div class="form-group">
	                    	<label for="question_question_category_id" class="label-control">Categoría</label>
		                    <select class="form-control" name="question_question_category_id" id="preguntas">
		                    	<option value="">Selecciona una categoría para tu pregunta</option>
		                    	@foreach($categories as $category)
		                    		<option value="{{$category->id}}">{{$category->name}}</option>
		                    	@endforeach
		                    </select>
	                    </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="status">
                                <option value="1">Publicado</option>
                                <option value="0">No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Guardar Pregunta',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
	@include('includes.tinymce')
@endsection
