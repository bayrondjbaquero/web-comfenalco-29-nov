@extends('layouts.admin.dashboard')

@section('titulo_dash')
    Categoría
@endsection

@section('styles')
@endsection

@section('titulo_page')
    Categoría
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus-square"></i> Modificar Categoría</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if ($errors->any())
                <div class="alert alert-danger text-white" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @include('includes.alert')
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    {!! Form::model($object, ['route'=>['question_categories.update', $object->id],'method'=>'PUT','class'=>'form-horizontal']) !!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="name">Nombre *</label>
                            {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug *</label>
                            {!! Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado" required>
                                <option value="1" @if($object->status==1) selected @endif>Publicado</option>
                                <option value="0" @if($object->status==0) selected @endif>No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Actualizar Pregunta',['class'=>'center-block btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection