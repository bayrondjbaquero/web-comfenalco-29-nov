@extends('layouts.admin.dashboard')

@section('titulo_dash')
    Categoría
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/datatables.min.css')}}"/>
    <style>
        #example_filter label input[type=search]{
            padding-right: 0 !important;
            padding-left: 0 !important;
            margin-left: 0 !important;
        }
    </style>
@endsection

@section('titulo_page')
    Categoría
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-list"></i> Ordenar Categoría</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if(count($errors)!= 0)
                    <div class="alert alert-warning text-white" role="alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @include('includes.alert')
                <!-- Formulario ADD NOTICIAS -->
                <div class="col-xs-12">
                    <form action="{{URL::route('question_categories.order.update')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th width="20%">#</th>
                                        <th>Categoría</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($objects as $object)
                                    <tr>
                                        <td>
                                            <input class="form-control" name="numeracion[]" value="{{$loop->iteration}}" type="text" required>
                                            {!! Form::hidden('id[]',$object->id,['class'=>'form-control']) !!}
                                        </td>
                                        <td>{{$object->name}}</td>
                                    </tr>
                                @empty
                                    <div class="alert alert-dismissable alert-warning text-white">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        <h4>Mensaje del sistema!</h4>
                                        <p>Ups!, no se encuentraron menús.</p>
                                    </div>
                                    <td colspan="3">No hay menús para mostrar</td>
                                @endforelse
                                </tbody>
                            </table>
                            <button class="btn btn-success" type="submit">Guardar Orden</button>
                        </div>                                  
                    </div>
                </form>
                <!-- /End Formulario ADD NOTICIAS -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection

