@extends('layouts.admin.dashboard')

@section('titulo_dash')
    Preguntas Frecuentes
@endsection

@section('styles')
@endsection

@section('titulo_page')
    Preguntas Frecuentes
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus-square"></i> Modificar Pregunta</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if ($errors->any())
                <div class="alert alert-danger text-white" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @include('includes.alert')
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    {!! Form::model($object, ['route'=>['questions.update', $object->id],'method'=>'PUT','class'=>'form-horizontal']) !!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="question">Pregunta *</label>
                            {!! Form::text('question',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
                        </div>
                        <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('answer', 'Descripción') !!}
                            {!! Form::textarea('answer',old('answer'),['class'=>'form-control my-editor','placeholder'=>'']) !!}
                        </div>
                        <div class="form-group">
                            <label for="question_question_category_id" class="label-control">Categoría</label>
                            <select class="form-control" name="question_question_category_id" id="preguntas">
                                <option value="">Selecciona una categoría para tu pregunta frecuente</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if($category->id==$object->question_question_category_id) selected @endif>{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" @if($object->status==1) selected @endif>Publicado</option>
                                <option value="0" @if($object->status==0) selected @endif>No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Actualizar Pregunta',['class'=>'center-block btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('includes.tinymce')
@endsection