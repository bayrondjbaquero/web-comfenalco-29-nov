@extends('layouts.admin.dashboard')

@section('titulo_dash')
@endsection

@section('style')
@endsection

@section('titulo_page')
	Sub Tabs
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-list"></i> Listar Sub tabs / <a style="text-decoration: underline;" href="{{ route('tabs.index') }}">{{$tabs->title}}</a></h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@include('includes.alert')
	      		<div class="row">
		      		<div class="col-xs-12">
		      			<a class="btn btn-success pull-right" href="{{ route('subtabs.create') }}?tab={{ $tabs->id }}"><i class="fa fa-plus"></i> Agregar Sub tabs</a>
		      		</div>
	      		</div>
	          	<div class="col-xs-12">
					<div class="table-responsive">
					    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
					        <thead>
						        <tr>
						        	<th>#</th>
						            <th>Titulo</th>
						            <th>Descripción</th>
						            <th>Slug</th>
						            <th>Acción</th>
						        </tr>
					        </thead>
					        <tbody>
					        @forelse($tabs->sub_tabs as $object)
					            <tr>
					            	<td>{{$loop->iteration}}</td>
									<td>{{$object->title}}</td>
									<td>@if($object->alt) {{$object->alt}} @else No tiene descripción @endif</td>
									<td>{{$object->slug}}</td>
									{{-- <td>
										@if($object->status == 0)
											No publicado
										@else 
											Publicado
										@endif
									</td> --}}
									<td style="width:10%">
										<ul style="list-style: none;padding-left: 0;">
											<li style="display:inline-block;vertical-align: top">
												{!! Form::open(['route'=>['subtabs.destroy',$object->id],'method'=>'DELETE'])!!}
								               		<button onclick="return confirm('¿Está seguro/a de eliminar permanentemente este item {{$object->alt}}?');" class="btn btn-danger btn-block text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i></button>
								                {!! Form::close() !!}
						            		</li>
						            		<li style="display:inline-block">
												<a href="{{URL::route('subtabs.edit',$object->id)}}" class="btn btn-primary btn-block text-left"><i class="fa fa-pencil"></i></a>
											</li>
										</ul>
									</td>
					            </tr>
					        @empty
					            <div class="alert alert-dismissable alert-warning text-white">
					                <button type="button" class="close" data-dismiss="alert">×</button>
					                <h4>Mensaje del sistema!</h4>
					                <p>Ups!, no se encontraron Sub tabs.</p>
					            </div>
					            <td colspan="4">No hay Sub tabs para mostrar</td>
					        @endforelse
					        </tbody>
					    </table>
					</div>		      		    			
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('script')
@endsection