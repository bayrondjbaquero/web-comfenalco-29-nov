@extends('layouts.admin.dashboard')

@section('titulo_dash')Administrador @endsection

@section('styles')
@endsection

@section('titulo_page')
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus-square"></i> Modificar Usuario</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if(count($errors)!= 0)
                    <div class="alert alert-warning text-white" role="alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @include('includes.alert')
                <!-- Formulario update Entrada -->
                <h1 class="text-center">Actualizar Información</h1>
                <br>
                {!! Form::model($object, ['route'=>['usuario.update', $object->id],'method'=>'PUT','class'=>'form-horizontal']) !!}
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Nombre</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{ $object->name }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email" class="col-md-4 control-label">Correo electrónico</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control" name="email" value="{{ $object->email }}" required>

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                        <label for="thumbnail" class="col-md-4 control-label">Cambiar imagen</label>
                        <div class="col-md-6">
                            {!! Form::file('thumbnail',['class'=>'form-control', 'id'=>'file-upload','accept'=>'image/*']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('Previsualizar',null,['class'=>'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <br>
                            <div id="file-preview-zonsse">
                                @if($object->image)
                                <img src="{{asset('img/'.$object->image)}}" alt="" id="file-preview" style="width: 200px;height: 100px;">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12"><h2 class="text-center">¿Quieres cambiar la contraseña?</h2></div>
                    <br>
                    <div class="form-group{{ $errors->has('password_after') ? ' has-error' : '' }}">
                        <label for="password_after" class="col-md-4 control-label">Contraseña Anterior</label>

                        <div class="col-md-6">
                            <input id="password_after" type="password" class="form-control" name="password_after" required>

                            @if ($errors->has('password_after'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_after') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password" class="col-md-4 control-label">Contraseña Nueva</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Guardar cuenta
                            </button>
                        </div>
                    </div>
                {!! Form::close() !!}    
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">  
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.getElementById('file-preview');
                    filePreview.src = e.target.result;
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file-upload');
        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }
    </script> 
@endsection