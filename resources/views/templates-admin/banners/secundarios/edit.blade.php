@extends('layouts.admin.dashboard')

@section('titulo_dash')
    
@endsection

@section('styles')
@endsection

@section('titulo_page')
    Banner
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus-square"></i> Modificar {{$banner['alt']}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if ($errors->any())
                <div class="alert alert-danger text-white" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @include('includes.alert')
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    {!! Form::model($banner, ['route'=>['second_banners.update', $banner->id],'method'=>'PUT','class'=>'form-horizontal', 'files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="title">Título *</label>
                            {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
                        </div>
                        {{-- <div class="form-group">
                            <label for="slug">Slug (<em>Opcional</em>)</label>
                            {!! Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'Título para URL donde se elimnan los espacios vacios', 'required'=>'true']) !!}
                        </div> --}}
                        <div class="form-group">
                            <label for="thumbnail">Cambiar imagen</label>
                            {!! Form::file('image',['class'=>'form-control', 'id'=>'file-upload','accept'=>'image/*']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Previsualizar') !!}
                            <br>
                            <div id="file-preview-zonsse">
                                <img src="{{asset($banner['image'])}}" alt="" id="file-preview" style="width: 200px;height: 100px;">
                            </div>
                        </div>
                        <div class="form-group">
                           <label for="alt">URL de la imagen</label>
                            {!! Form::text('url',null,['class'=>'form-control', 'placeholder'=>'Campo opcional']) !!}
                        </div>
                        {{-- <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('description', 'Descripción') !!}
                            {!! Form::textarea('description',old('description'),['class'=>'form-control my-editor','placeholder'=>'']) !!}
                        </div> --}}
                        <div class="form-group">
                            <label for="type">Sección de banner</label>
                            <br>
                            <select class="form-control" name="menus_id" id="type">
                                @if(isset($menus))
                                    @foreach($menus as $menu)
                                        <option value="{{ $menu['id'] }}" @if($banner['menus_id']== $menu['id']) selected @endif> {{ $menu['title'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> 
                        {{-- <div class="form-group">
                            <label for="condition">Condición</label>
                            <br>
                            <select class="form-control" name="condition" id="condition">
                                <option value="venta" @if($banner->condition=='venta') selected @endif>Venta</option>
                                <option value="arriendo" @if($banner->condition=='arriendo') selected @endif>Arriendo</option>
                            </select>
                        </div> 
                        <div class="form-group">
                            <label for="city">Ciudad</label>
                            {!! Form::text('city',null,['class'=>'form-control', 'placeholder'=>'Digita una Ciudad']) !!}
                        </div>
                        <div class="form-group">
                            <label for="neighborhood">Barrio</label>
                            {!! Form::text('neighborhood',null,['class'=>'form-control', 'placeholder'=>'Digita un Barrio']) !!}
                        </div> --}}
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" @if($banner->status==1) selected @endif>Publicado</option>
                                <option value="0" @if($banner->status==0) selected @endif>No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Actualizar '.$banner['alt'],['class'=>'center-block btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">  
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.getElementById('file-preview');
                    filePreview.src = e.target.result;
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file-upload');
        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }
    </script> 
    @include('includes.tinymce')
    <script>
        $('#boo-resumen').click(function(){
            $('#form-oculto').slideToggle();
        });
    </script>
@endsection
