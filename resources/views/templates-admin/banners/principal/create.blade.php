@extends('layouts.admin.dashboard')

@section('titulo_dash')
@endsection

@section('styles')
@endsection

@section('titulo_page')
	Banner 
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Añadir Banner</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'banners.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
						<div class="form-group">
	                        <label for="title">Nombre</label>
	                        {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'']) !!}
	                    </div>
                        <div class="form-group">
                            <label for="thumbnail">Imagen de proyecto (1110 x 300 pixeles)</label>
                            {!! Form::file('thumbnail', null, ['class'=>'form-control', 'accept'=>'image/*','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                           <label>Url de la imagen</label>
                            {!! Form::text('url',null,['class'=>'form-control', 'placeholder'=>'opcional']) !!}
                        </div>
                        {{-- <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('description', 'Descripción') !!}
                            <textarea name="description" class="form-control my-editor">{{old('description')}}</textarea>
                        </div> --}}
                        <div class="form-group">
                            <label for="type">Sección de banner</label>
                            <br>
                            <select class="form-control" name="menus_id" id="type">
                                <option value="">Selecciona un tipo</option>
                                <option value="0">Inicio</option>
                                @if(isset($menus))
                                    @foreach($menus as $menu)
                                        <option value="{{ $menu['id'] }}">{{ $menu['title'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div> 
                       {{--  <div class="form-group">
                            <label for="condition">Condición</label>
                            <br>
                            <select class="form-control" name="condition" id="condition">
								<option value="">Selecciona una condición</option>
                                <option value="venta">Venta</option>
                                <option value="arriendo">Arriendo</option>
                            </select>
                        </div> 
                        <div class="form-group">
	                        <label for="city">Ciudad</label>
	                        {!! Form::text('city',null,['class'=>'form-control', 'placeholder'=>'Digita una Ciudad']) !!}
	                    </div>
	                    <div class="form-group">
	                        <label for="neighborhood">Barrio</label>
	                        {!! Form::text('neighborhood',null,['class'=>'form-control', 'placeholder'=>'Digita un Barrio']) !!}
	                    </div> --}}
                        {{-- <div class="form-group">
                            <label for="destacado">Nivel de importancia de un proyecto (1 a 5)</label>
                            <br>
                            <select class="form-control" name="destacado" id="destacado_input">
                                <option value="1">1 (Menos importante)</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                                <option value="5">5 (Más importante)</option>
                            </select>
                        </div> --}}
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" selected>Publicado</option>
                                <option value="0">No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Guardar Banner',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
	@include('includes.tinymce')
@endsection
