
    <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Crear Campaña</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'campanas.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
						<div class="form-group">
	                        <label for="title">Nombre</label>
	                        {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'']) !!}
	                    </div>
                        <div class="form-group">
                            <label for="type">Tipo de diseño</label>
                            <br>
                            <div class="row">
                                <div class="col-md-offset-1 col-md-2">
                                    <input type="radio" name="type_design" value="" placeholder="">
                                    <img src="{{ asset('img/diseno1.png') }}" alt="" class="img-responsive" style="height: 150px;">
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="type_design" value="" placeholder="">
                                    <img src="{{ asset('img/diseno2.png') }}" alt="" class="img-responsive" style="height: 150px;">
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="type_design" value="" placeholder="">
                                    <img src="{{ asset('img/diseno3.png') }}" alt="" class="img-responsive" style="height: 150px;">
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="type_design" value="" placeholder="">
                                    <img src="{{ asset('img/diseno4.png') }}" alt="" class="img-responsive" style="height: 150px;">
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="type_design" value="" placeholder="">
                                    <img src="{{ asset('img/diseno5.png') }}" alt="" class="img-responsive" style="height: 150px;">
                                </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="event_start">Fecha de inicio de la campaña</label>
                            <div class="form-group">
                                <div class='input-group date'>
                                    <input name="event_start" id='datetimepicker6' type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="event_end">Fecha de fin de la campaña</label>
                            <div class="form-group">
                                <div class='input-group date'>
                                    <input name="event_end" id='datetimepicker7' type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" selected>Publicado</option>
                                <option value="0">No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Guardar Campaña',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>

