@extends('layouts.admin.dashboard')

@section('titulo_dash')
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('titulo_page')
	Campañas Digitales
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	    	<ul class="nav nav-tabs text-regular" role="tablist">
		        <li role="presentation" class="active">
		            <a href="{{ Request::is('administrador/proyectos/create') ? '/#perfil' : '#perfil' }} " aria-controls="perfil" role="tab" data-toggle="tab">Listar campañas</a>
		        </li>
		        <li role="presentation">
		            <a href="{{ Request::is('administrador/proyectos/create') ? '/#solicitud' : '#solicitud' }}" aria-controls="solicitud" role="tab" data-toggle="tab">Crear nueva campaña</a>
		        </li>
		        <li role="presentation">
		            <a href="{{ Request::is('administrador/proyectos/create') ? '/#contenido' : '#contenido' }}" aria-controls="contenido" role="tab" data-toggle="tab">Crear contenido</a>
		        </li>
		    </ul>
	      	<div class="x_title">
	        	{{-- <h2><i class="fa fa-list"></i> Listar campañas</h2> --}}
	        	<div class="clearfix"></div>
	      	</div>
	      	<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="perfil">
					{{-- @include('cuentas.perfil') --}}
					<div class="x_content row">
			      		@include('includes.alert')
			          	<div class="col-xs-12">
							<div class="table-responsive">
							    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
							        <thead>
								        <tr>
								        	<th>#</th>
								        	<th>Título</th>
								        	<th>Slug</th>
								            <th>Tipo Diseño</th>
								            <th>Acción</th>
								        </tr>
							        </thead>
							        <tbody>
							        @forelse($campanas as $object)
							            <tr>
							            	<td>{{$loop->iteration}}</td>
							            	<td>{{$object->title}}</td>
							            	<td>{{$object->slug}}</td>
							            	<td>{{$object->type_design}}</td>
											<td style="width:10%">
												<ul style="list-style: none;padding-left: 0;">
													<li style="display:inline-block;vertical-align: top">
														{!! Form::open(['route'=>['campanas.destroy',$object->id],'method'=>'DELETE'])!!}
										               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente el banner {{$object->title}}?');" class="btn btn-danger btn-block text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i></button>
										                {!! Form::close() !!}
								            		</li>
													<li style="display:inline-block">
														<a href="{{URL::route('campanas.edit',$object->id)}}" class="btn btn-primary btn-block text-left"><i class="fa fa-pencil"></i></a>
													</li>
												</ul>
											</td>
							            </tr>
							        @empty
							            <div class="alert alert-dismissable alert-warning text-white">
							                <button type="button" class="close" data-dismiss="alert">×</button>
							                <h4>Mensaje del sistema!</h4>
							                <p>Ups!, no se encontraron banner.</p>
							            </div>
							            <td colspan="9">No hay banner para mostrar</td>
							        @endforelse
							        </tbody>
							    </table>
							</div>		      		    			
			          	</div>
			      	</div>
				</div>
				<div role="tabpanel" class="tab-pane " id="solicitud"><br>
					@include('templates-admin.campanas.categorias.create')
				</div>
				<div role="tabpanel" class="tab-pane " id="contenido"><br>
					@include('templates-admin.campanas.contenido.create')
				</div>
			</div>
		</div>
	</div>
@endsection
@section('scripts')
    @include('includes.tinymce')
    {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/es-us.js') }}"></script>

    {{-- <script src="https://reservate.co/dist/libs/bootstrap-datepicker-1.6.1-dist/locales/bootstrap-datepicker.es.min.js"></script> --}}
    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                locale: 'es-us',
                format: 'YYYY-MM-DD hh:mm:ss'
            });
            $('#datetimepicker6').datetimepicker({
                locale: 'es-us',
                useCurrent: false, 
                format: 'YYYY-MM-DD hh:mm:ss'
            });
            $('#datetimepicker7').datetimepicker({
                locale: 'es-us',
                useCurrent: false, 
                format: 'YYYY-MM-DD hh:mm:ss'
            });
            $("#datetimepicker1").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@endsection