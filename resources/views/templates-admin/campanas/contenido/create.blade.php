
    <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Crear Contenido</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'proyects.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="title">Titulo de la web</label>
                            {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'']) !!}
                        </div>
                        <div class="form-group">
                            <label for="banner">Imagen de banner (1920 x 1080 pixeles) no mayor a un tamaño de 2MB</label>
                            {!! Form::file('banner', null, ['class'=>'form-control', 'accept'=>'image/*','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                           <label>Descripción principal</label>
                            <textarea name="description_principal" class="form-control my-editor">{{old('description_principal')}}</textarea>
                        </div>
                        <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('description_secundario', 'Descripción Secundaria') !!}
                            <textarea name="description_secundario" class="form-control my-editor">{{old('description_secundario')}}</textarea>
                        </div> 
                        <div class="form-group">
                            <label for="video">Video (Opcional)</label>
                            {!! Form::text('video',null,['class'=>'form-control', 'placeholder'=>'Video - url de YouTube']) !!}
                        </div>
                        <div class="form-group">
                            <label for="correos">Correos - emails de contacto: ingrese los emails separados por coma(,)</label>
                            {!! Form::text('correos',null,['class'=>'form-control', 'placeholder'=>'ejemplo@gmail.com, ejemplo2@gmail.com']) !!}
                        </div>
                        <div class="form-group">
                            <label for="palabras_claves">Palabras claves</label>
                            {!! Form::text('palabras_claves',null,['class'=>'form-control', 'placeholder'=>'palabra 1, palabra2, palabra3 etc...']) !!}
                        </div>
                        <div class="form-group">
                            <label for="campana_description">Descripción de la campaña</label>
                            {!! Form::text('campana_description',null,['class'=>'form-control', 'placeholder'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum iusto quidem accusantium facere necessitatibus quam delectus. Placeat eaque numquam veritatis? Voluptatum quasi quia unde odio esse quisquam, ad minus pariatur. ']) !!}
                        </div>
                        <div class="form-group">
                            <label for="gogole_analitycs">Google analitics</label>
                            {!! Form::text('gogole_analitycs',null,['class'=>'form-control', 'placeholder'=>'código Google analitycs']) !!}
                        </div>
                        {{-- <div class="form-group">
                            <label for="destacado">Nivel de importancia de un proyecto (1 a 5)</label>
                            <br>
                            <select class="form-control" name="destacado" id="destacado_input">
                                <option value="1">1 (Menos importante)</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                                <option value="5">5 (Más importante)</option>
                            </select>
                        </div> --}}
                    {!! Form::submit('Guardar Campaña',['class'=>'center-block btn btn-success']) !!}
                    {!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>

