@extends('layouts.admin.dashboard')

@section('titulo_dash')
@endsection

@section('styles')
@endsection

@section('titulo_page')
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus-square"></i> Modificar {{ $menus['title']}}</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if ($errors->any())
                <div class="alert alert-danger text-white" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @include('includes.alert')
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    {!! Form::model($menus, ['route'=>['menus.update', $menus->id],'method'=>'PUT','class'=>'form-horizontal', 'files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="title">Título *</label>
                            {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug (<em>Opcional</em>)</label>
                            {!! Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'Título para URL donde se elimnan los espacios vacios', 'required'=>'true']) !!}
                        </div>
                        <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('description', 'Descripción') !!}
                            {!! Form::textarea('description',old('description'),['class'=>'form-control my-editor','placeholder'=>'']) !!}
                        </div>
                        <div class="form-group">
                            <label for="type">Tipo de contacto</label>
                            <br>
                            <select class="form-control" name="type" id="type">
                                <option value="0" @if($menus->type_contact == 0) selected @endif>Tipo 1</option>
                                <option value="1" @if($menus->type_contact == 1) selected @endif>Tipo 2</option>
                            </select>
                        </div> 
                        <div class="form-group">
                            <label for="type">Tipo de contacto</label>
                            <br>
                            <select class="form-control" name="type_menu" id="type_menu">
                                <option value="1" @if($menus->type_menu == 1) selected @endif>Menú principal</option>
                                <option value="2" @if($menus->type_menu == 2) selected @endif>Top Menú</option>
                            </select>
                        </div> 
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" @if($menus->status==1) selected @endif>Publicado</option>
                                <option value="0" @if($menus->status==0) selected @endif>No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Actualizar '.$menus['title'],['class'=>'center-block btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts') 
    @include('includes.tinymce')
    <script>
        $('#boo-resumen').click(function(){
            $('#form-oculto').slideToggle();
        });
    </script>
@endsection
