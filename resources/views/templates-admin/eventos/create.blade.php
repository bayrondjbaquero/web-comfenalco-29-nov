@extends('layouts.admin.dashboard')

@section('titulo_dash')
    Eventos
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('titulo_page')
    Eventos
@endsection

@section('content')
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus-square"></i> Añadir Evento</h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content row">
                @if(count($errors)!= 0)
                    <div class="alert alert-warning text-white" role="alert">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @include('includes.alert')
                <!-- Formulario ADD Eventos -->
                <div class="col-md-10 col-md-offset-1 col-xs-12">
                    {!! Form::open(['route'=>'events.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="title">Nombre</label>
                            {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'']) !!}
                        </div>
                        <div class="form-group table-responsive" id="footer-id">
                            {!! Form::label('body', 'Descripción') !!}
                            <textarea name="body" class="form-control my-editor">{{old('body')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Imagen del evento</label>
                            {!! Form::file('image', null, ['class'=>'form-control', 'accept'=>'image/*','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                           <label>Descripción de imagen / alt</label>
                            {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'opcional']) !!}
                        </div>
                        <div class="form-group">
                            <label for="date_published">Fecha de publicación</label>
                            <div class='input-group date'>
                                <input name="date_published" id='datetimepicker1' type='text' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="event_start">Fecha de inicio del evento</label>
                            <div class="form-group">
                                <div class='input-group date'>
                                    <input name="event_start" id='datetimepicker6' type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="event_end">Fecha de fin del evento</label>
                            <div class="form-group">
                                <div class='input-group date'>
                                    <input name="event_end" id='datetimepicker7' type='text' class="form-control" />
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" selected>Publicado</option>
                                <option value="0">No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Guardar Evento',['class'=>'center-block btn btn-success']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('includes.tinymce')
    {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/es-us.js') }}"></script>

    {{-- <script src="https://reservate.co/dist/libs/bootstrap-datepicker-1.6.1-dist/locales/bootstrap-datepicker.es.min.js"></script> --}}
    <script>
        $(function () {
            $('#datetimepicker1').datetimepicker({
                locale: 'es-us',
                format: 'YYYY-MM-DD hh:mm:ss'
            });
            $('#datetimepicker6').datetimepicker({
                locale: 'es-us',
                useCurrent: false, 
                format: 'YYYY-MM-DD hh:mm:ss'
            });
            $('#datetimepicker7').datetimepicker({
                locale: 'es-us',
                useCurrent: false, 
                format: 'YYYY-MM-DD hh:mm:ss'
            });
            $("#datetimepicker1").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker6").on("dp.change", function (e) {
                $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
                $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
            });
            $("#datetimepicker7").on("dp.change", function (e) {
                $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script>
@endsection