@extends('layouts.admin.dashboard')

@section('titulo_dash')
	Eventos
@endsection

@section('style')
@endsection

@section('titulo_page')
	Eventos
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-list"></i> Listar Eventos</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@include('includes.alert')
	          	<div class="col-xs-12">
					<div class="table-responsive">
					    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
					        <thead>
						        <tr>
						        	<th>#</th>
						            <th>Título</th>
						            <th style="width: 20%">Descripción</th>
									<th>Imagen</th>
									<th>Fechas</th>
									<th>Estado</th>
						            <th>Acción</th>
						        </tr>
					        </thead>
					        <tbody>
					        @forelse($objects as $object)
					            <tr>
					            	<td>{{$loop->iteration}}</td>
									<td>{{$object->title}}</td>
									<td>{!! \Illuminate\Support\Str::words($object->body,50) !!}</td>
									<td><img width="200" style="height: auto;" src="{{ asset('event_images/'.$object->image)}}" alt="{{$object->alt}}"></td>
									<td>
										<ul class="list-unstyled">
											<li><strong>Fecha de publicación:</strong> {{$object->date_published}}</li>
											<li><strong>Fecha de Inicio:</strong> {{$object->event_start}}</li>
											<li><strong>Fecha de Fin:</strong>  {{$object->event_end}}</li>
										</ul>
									</td>
									<td>
										@if($object->status == 0)
											No publicado
										@else 
											Publicado
										@endif
									</td>
									<td style="width:10%">
										<ul style="list-style: none;padding-left: 0;">
											<li style="display:inline-block;vertical-align: top">
												{!! Form::open(['route'=>['events.destroy',$object->id],'method'=>'DELETE'])!!}
												{{ csrf_field() }}
								               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente un evento?');" class="btn btn-danger btn-block text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i></button>
								                {!! Form::close() !!}
						            		</li>
						            		<li style="display:inline-block">
												<a href="{{URL::route('events.edit',$object->id)}}" class="btn btn-primary btn-block text-left"><i class="fa fa-pencil"></i></a>
											</li>
										</ul>
									</td>
					            </tr>
					        @empty
					            <div class="alert alert-dismissable alert-warning text-white">
					                <button type="button" class="close" data-dismiss="alert">×</button>
					                <h4>Mensaje del sistema!</h4>
					                <p>Ups!, no se encontraron Eventos.</p>
					            </div>
					            <td colspan="9">No hay Eventos para mostrar</td>
					        @endforelse
					        </tbody>
					    </table>
					</div>		      		    			
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('script')
@endsection