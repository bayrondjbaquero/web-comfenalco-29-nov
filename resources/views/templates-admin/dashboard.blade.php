@extends('layouts.admin.dashboard')

@section('titulo_dash')Administrador @endsection

@section('styles')
@endsection

@section('titulo_page')
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="row">
                <br><br>
                <h1 class="text-center">Panel de Administración</h1>
                <br>
                <img class="img-responsive center-block" src="{{ asset('img/logo-principal.png') }}" alt="Comfenalco Cartagena">
                <br><br>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection
