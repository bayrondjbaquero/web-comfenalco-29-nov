@extends('layouts.admin.dashboard')

@section('titulo_dash')
	{{$plural}}
@endsection

@section('style')
@endsection

@section('titulo_page')
	{{$plural}}
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-list"></i> Listar {{$plural}}</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@include('includes.alert')
	          	<div class="col-xs-12">
					<div class="table-responsive">
					    <table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
					        <thead>
						        <tr>
						        	<th>#</th>
						        	<th>Título</th>
						            <th>Imagen</th>
						            <th>Tipo</th>
									<th>Condición</th>
									<th>Ubicación</th>
									<th>Estado</th>
									<th>Gallería</th>
						            <th>Acción</th>
						        </tr>
					        </thead>
					        <tbody>
					        @forelse($objects as $object)
					            <tr>
					            	<td>{{$loop->iteration}}</td>
					            	<td>{{$object->title}}</td>
									<td><img width="100" height="100" src="{{ asset('proyect_images/'.$object->thumbnail)}}" alt="">{{$object->alt}}</td>
									<td>{{$object->type}}</td>
									<td>{{$object->condition}}</td>
									<td>{{$object->city."/".$object->neighborhood}}</td>
									<td>
										@if($object->status == 0)
											No publicado
										@else 
											Publicado
										@endif
									</td>
									<td><a href="{{ route('galleries.show',$object->id) }}" class="btn btn-success">Ver gallería</a> ({{count($object->galleries)}})</td>
									<td style="width:10%">
										<ul style="list-style: none;padding-left: 0;">
											<li style="display:inline-block;vertical-align: top">
												{!! Form::open(['route'=>['proyects.destroy',$object->id],'method'=>'DELETE'])!!}
												{{ csrf_field() }}
								               		<button onclick="return confirm('¿Está seguro de eliminar permanentemente el proyecto {{$object->title}}?');" class="btn btn-danger btn-block text-left" type="submit"><i class="fa fa-remove" aria-hidden="true"></i></button>
								                {!! Form::close() !!}
						            		</li>
											<li style="display:inline-block">
												<a href="{{URL::route('proyects.edit',$object->id)}}" class="btn btn-primary btn-block text-left"><i class="fa fa-pencil"></i></a>
											</li>
										</ul>
									</td>
					            </tr>
					        @empty
					            <div class="alert alert-dismissable alert-warning text-white">
					                <button type="button" class="close" data-dismiss="alert">×</button>
					                <h4>Mensaje del sistema!</h4>
					                <p>Ups!, no se encontraron {{$plural}}.</p>
					            </div>
					            <td colspan="9">No hay {{$plural}} para mostrar</td>
					        @endforelse
					        </tbody>
					    </table>
					</div>		      		    			
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('script')
@endsection