@extends('layouts.admin.dashboard')

@section('titulo_dash')
	{{$singular}}
@endsection

@section('styles')
@endsection

@section('titulo_page')
	{{$singular}}
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Añadir {{$singular. " / " . $proyect->title}}</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
                <div class="col-xs-12" style="height:50px;width:100%;float: none;">
                    <a class="btn btn-success pull-right" href="{{ route('galleries.show',$proyect_id) }}"><i class="fa fa-chevron-left"></i> Regresar</a>
                </div>
		        @include('includes.alert')
		       	<!-- Formulario ADD Proyecto -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'galleries.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
                        {!! Form::hidden('proyect_id', $proyect_id) !!}
                        <div class="form-group">
                            <label for="image">Imagen</label>
                            {!! Form::file('image', null, ['class'=>'form-control', 'accept'=>'image/*','required'=>'true']) !!}
                        </div>
                        <div class="form-group">
                           <label>Descripción de imagen / alt</label>
                            {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'opcional']) !!}
                        </div>
                         {{-- <div class="form-group">
                            <label for="destacado">Nivel de importancia de un proyecto (1 a 5)</label>
                            <br>
                            <select class="form-control" name="destacado" id="destacado_input">
                                <option value="1">1 (Menos importante)</option>
                                <option value="2">2</option>
                                <option value="3" selected>3</option>
                                <option value="4">4</option>
                                <option value="5">5 (Más importante)</option>
                            </select>
                        </div> --}}
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" selected>Publicado</option>
                                <option value="0">No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Guardar Proyecto',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
	@include('includes.tinymce')
@endsection
