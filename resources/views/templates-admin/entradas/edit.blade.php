@extends('layouts.admin.dashboard')

@section('titulo_dash')
	Entrada
@endsection

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('titulo_page')
	Entrada
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Modificar Entrada</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario update Entrada -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::model($object, ['route'=>['entries.update', $object->id],'method'=>'PUT','class'=>'form-horizontal', 'files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
						<div class="form-group">
	                        <label for="title">Título</label>
	                        {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'','required'=>'true']) !!}
	                    </div>
	                    <div class="form-group">
	                        <label for="slug">Slug</label>
	                        {!! Form::text('slug',null,['class'=>'form-control', 'placeholder'=>'','required'=>'true']) !!}
	                    </div>
                        <div class="form-group">
                            <label for="image">Cambiar imagen</label>
                            {!! Form::file('image',['class'=>'form-control', 'id'=>'file-upload','accept'=>'image/*']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('Previsualizar') !!}
                            <br>
                            <div id="file-preview-zonsse">
                                <img src="{{asset('blog_images/'.$object->image)}}" alt="" id="file-preview" style="width: 200px;height: auto;">
                            </div>
                        </div>
                        <div class="form-group">
                           <label>Descripción de imagen / alt</label>
                            {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'opcional']) !!}
                        </div>
                        <div class="form-group table-responsive">
                            {!! Form::label('body', 'Descripción') !!}
                            {!! Form::textarea('body',old('body'),['class'=>'form-control my-editor', 'rows'=>'7']) !!}
                        </div>
                        <div class="form-group">
	                        <label for="summary">Resumen</label>
	                        {!! Form::textarea('summary',null,['class'=>'form-control', 'placeholder'=>'Texto corto para resumen', 'rows'=>'3']) !!}
	                    </div>
                        <div class="form-group row">
                    		<label for="categorys_id">Seleccionar Categorías:</label>
							<select multiple class="form-control" name="categorys_id[]">
								@foreach($categories as $category)
								{{$x = 0}}
								@foreach($object->categorys as $category_valid)
									@if($category_valid->id == $category->id)
										{{$x = 1}}
									@endif
								@endforeach
								@if($x == 1)
									<option value="{{$category->id}}" selected>{{$category->name}}</option>
								@else 
									<option value="{{$category->id}}">{{$category->name}}</option>
								@endif
							@endforeach
							</select>
						</div>
                        <div class="form-group">
                            <label for="date_published">Fecha de publicación</label>
                            <div class='input-group date'>
                                <input value="{{$object->date_published}}" name="date_published" type='text' id='datetimepicker1' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado">
                                <option value="1" @if($object->status==1) selected @endif>Publicado</option>
                                <option value="0" @if($object->status==0) selected @endif>No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Actualizar Entrada',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
	@include('includes.tinymce')
	<script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/es-us.js') }}"></script>

	<script>
		$(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD hh:mm:ss',
            	// daysOfWeekDisabled: [0, 6]
            });
        });

        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.getElementById('file-preview');
                    filePreview.src = e.target.result;
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file-upload');
        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }
	</script>
@endsection
