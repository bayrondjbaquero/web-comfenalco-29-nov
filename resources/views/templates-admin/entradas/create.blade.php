@extends('layouts.admin.dashboard')

@section('titulo_dash')
	Entrada
@endsection

@section('styles')
	<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}">
@endsection

@section('titulo_page')
	Entrada
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Añadir Entrada</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
		       	<!-- Formulario ADD Entrada -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'entries.store','method'=> 'POST','files'=>"true", 'enctype'=>"multipart/form-data"]) !!}
		          		{!! csrf_field() !!}
						<div class="form-group">
	                        <label for="title">Título</label>
	                        {!! Form::text('title',null,['class'=>'form-control', 'placeholder'=>'','required'=>'true']) !!}
	                    </div>
                        <div class="form-group">
                            <label for="imagen">Imagen</label>
                            {!! Form::file('imagen', null, ['class'=>'form-control', 'accept'=>'image/*', 'required'=> 'true']) !!}
                        </div>
                        <div class="form-group">
                           <label>Descripción de imagen / alt</label>
                            {!! Form::text('alt',null,['class'=>'form-control', 'placeholder'=>'opcional']) !!}
                        </div>
                        <div class="form-group table-responsive">
                            {!! Form::label('body', 'Descripción') !!}
                            {!! Form::textarea('body',old('body'),['class'=>'form-control my-editor', 'rows'=>'7']) !!}
                        </div>
                        <div class="form-group">
	                        <label for="summary">Resumen</label>
	                        {!! Form::textarea('summary',null,['class'=>'form-control', 'placeholder'=>'Texto corto con una breve descripción del post', 'rows'=>'3']) !!}
	                    </div>
                        <div class="form-group">
                            <label for="category_id" class="label-control">Categoría(s)</label>
                           <select multiple class="form-control" name="categorys_id[]" required>                                
                                @foreach($objects as $category)
                                    <option value="{{$category->id}}">{{ucfirst(mb_strtolower($category->name))}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="date_published">Fecha de publicación</label>
                            <div class='input-group date'>
                                <input name="date_published" type='text' id='datetimepicker1' class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="status">Estado</label>
                            <select class="form-control" name="status" id="estado" required>
                                <option value="1" selected>Publicado</option>
                                <option value="0">No publicado</option>
                            </select>
                        </div>
                    {!! Form::submit('Guardar Entrada',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
	@include('includes.tinymce')
	<script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/es-us.js') }}"></script>

	<script>
		$(function () {
            $('#datetimepicker1').datetimepicker({
            	format: 'YYYY-MM-DD hh:mm:ss',
            	
            	// daysOfWeekDisabled: [0, 6]
            });
        });
	</script>
@endsection
