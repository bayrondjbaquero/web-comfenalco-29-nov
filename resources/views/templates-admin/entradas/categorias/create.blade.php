@extends('layouts.admin.dashboard')

@section('titulo_dash')
	Categorías
@endsection

@section('styles')
@endsection

@section('titulo_page')
	Categorías
@endsection

@section('content')
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
	      	<div class="x_title">
	        	<h2><i class="fa fa-plus-square"></i> Añadir Categoría</h2>
	        	<div class="clearfix"></div>
	      	</div>
	      	<div class="x_content row">
	      		@if(count($errors)!= 0)
		            <div class="alert alert-warning text-white" role="alert">
						@foreach ($errors->all() as $error)
							<div>{{ $error }}</div>
						@endforeach
		            </div>
		        @endif
		        @include('includes.alert')
	          	<!-- Formulario ADD CATEGORY -->
	          	<div class="col-md-10 col-md-offset-1 col-xs-12">
	          		{!! Form::open(['route'=>'categories.store','method'=> 'POST']) !!}
		          		{!! csrf_field() !!}
		          		<div class="form-group">
	                        <label for="name">Nombre *</label>
	                        {!! Form::text('name',null,['class'=>'form-control', 'placeholder'=>'', 'required'=>'true']) !!}
	                    </div>
                    {!! Form::submit('Guardar Categoría',['class'=>'center-block btn btn-success']) !!}
		          	{!! Form::close() !!}
	          	</div>
	      	</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection
