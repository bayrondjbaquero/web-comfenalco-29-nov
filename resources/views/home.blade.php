@extends('layouts.main')

@section('line')@endsection

@section('title')@endsection

@section('styles')
@endsection

@section('content')
    <div id="main-carousel" class="box-default">
        <div class="container-fluid">
            <div class="row">
                <div id="carouselExampleControls" class="carousel slide mx-auto" data-ride="carousel">
                    @if(isset($banners) && count($banners) != 0)
                    <div class="carousel-inner" role="listbox">
                            @foreach($banners as $index => $slider)
                                @if($index == 0)
                                    <div class="carousel-item active">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                        
                                    </div>
                                @else
                                    <div class="carousel-item ">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                    </div>
                                @endif
                            @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="arrow-left">
                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="arrow-right">
                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
                        </span>
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div id="box-menu" class="box-default mt-4">
        <div class="container">
            <div class="row text-center">
                <div class="col-xs-12 col-sm-4 mb-1 mb-sm-0">
                    <img class="img-fluid mb-1 rounded" src="{{asset('img/box-empresa.jpg')}}" alt="Empresa Comfenalco">
                    <h3 class="bg-blue-2 mb-0 mt-1 p-1 w-100 text-white rounded"><a class="text-white" href="{{ route('empresas') }}">Empresas <i class="hidden-sm-down ball-right"></i></a></h3>
                </div>
                <div class="col-xs-12 col-sm-4 mb-1 mb-sm-0">
                    <img class="img-fluid mb-1 rounded" src="{{asset('img/box-afiliados.jpg')}}" alt="Afiliados Comfenalco">
                    <h3 class="bg-blue-2 mb-0 mt-1 p-1 w-100 text-white rounded"><a class="text-white" href="{{ route('afiliado') }}">Afiliados <i class="hidden-sm-down ball-right"></i></a></h3>
                </div>
                <div class="col-xs-12 col-sm-4 mb-1 mb-sm-0">
                    <img class="img-fluid mb-1 rounded" src="{{asset('img/box-informate.jpg')}}" alt="Infórmate Comfenalco">
                    <h3 class="bg-blue-2 mb-0 mt-1 p-1 w-100 text-white rounded"><a class="text-white" href="{{ route('informate') }}">Infórmate <i class="hidden-sm-down ball-right"></i></a></h3>
                </div>
            </div>
        </div>
    </div>
    <div id="dot" class="d-block mx-auto mt-5 mb-5"></div>
    <div class="container">
        <div class="row">
            @if(count($events)>0)
            <div class="col-sm-12 col-md-4 mb-2 mb-md-0 text-center pb-2">
                <div id="carouselExampleControls0" class="carousel slide mx-auto" data-ride="carousel" style="max-width: 380px;">
                    <div class="carousel-inner" role="listbox">
                        @foreach($events as $event)
                            <div class="carousel-item item-content @if($loop->first)active @endif">
                                <a href="{{route('detalle_de_evento',$event->slug)}}"><div class="mx-auto item-effect" style="background-image: url('{{asset('event_images/'.$event->image)}}');background-repeat: no-repeat;background-size: 100% 100%;height: 362px;width: 378px;"></div></a>
                            </div>
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls0" role="button" data-slide="prev">
                        <span class="arrow-left">
                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls0" role="button" data-slide="next">
                        <span class="arrow-right">
                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
                        </span>
                    </a>
                </div>
            </div>
            @endif
            <div class="col-sm-12 @if(count($events)>0)col-md-8 @else col-md-12 @endif">
                <div class="row">
                    <div class="col-6 col-md-4 mb-3 text-center"><a class="d-block map-style" href="#" target="_blank"><span class="map-style-blue"></span><img class="img-fluid rounded" src="{{asset('img/descargas.jpg')}}" alt=""></a></div>
                    <div class="col-6 col-md-4 mb-3 text-center"><a class="d-block map-style" href="{{ route('mapa_comfenalco') }}"><span class="map-style-blue"></span><img class="map-img img-fluid rounded" src="{{asset('img/comfenalco-mapa.jpg')}}" alt=""></a></div>
                    <div class="col-6 col-md-4 mb-3 text-center"><img class="img-fluid rounded" src="{{asset('img/comfenalco-mail.jpg')}}" alt=""></div>
                    <div class="col-6 col-md-4 mb-3 text-center"><img class="img-fluid rounded" src="{{asset('img/consultas.jpg')}}" alt=""></div>
                    <div class="col-sm-12 col-md-8 mb-3 text-center"><img class="img-fluid rounded" src="{{asset('img/comfenalco-large.jpg')}}" alt=""></div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
