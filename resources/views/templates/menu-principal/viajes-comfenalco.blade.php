@extends('layouts.main')

@section('title')Viajes Comfenalco @endsection

@section('styles')
@endsection

@section('content')
	<div id="main-carousel" class="box-default">
        <div class="container-fluid">
        	<div class="row">
	            <div id="carouselExampleControls" class="carousel slide mx-auto" data-ride="carousel">
                	@if(isset($banners) && count($banners) != 0)
	                <div class="carousel-inner" role="listbox">
				            @foreach($banners as $index => $slider)
				                @if($index == 0)
				                    <div class="carousel-item active">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                    	@if($slider['url'] != NULL) </a> @endif
				                        
				                    </div>
				                @else
				                    <div class="carousel-item ">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                        @if($slider['url'] != NULL) </a> @endif
				                    </div>
				                @endif
				            @endforeach
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	                    <span class="arrow-left">
	                        <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
	                    </span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	                    <span class="arrow-right">
	                        <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
	                    </span>
	                </a>
			        @endif
	            </div>
            </div>
        </div>
    </div>
	<div id="viajes-comfenalco" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-12">
					<ul id="myTab" class="nav flex-column flex-md-row flex-sm-row flex-lg-row flex-xl-row nav-tabs d-sm-flex justify-content-sm-between responsive-tabs" role="tablist">
						<li class="nav-item">
							<a class="justify-content-center text-white nav-link bg-tab-1 active" href="#valojate" role="tab" data-toggle="tab">ALÓJATE EN HOTEL CORALES DE INDIAS</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center text-white nav-link bg-tab-2" href="#vplanea" role="tab" data-toggle="tab">PLANEA TU VIAJE CON NOSOTROS</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade show active" id="valojate">
							@include('templates.menu-principal.viajes-comfenalco.tab-alojate-en-hotel-corales-de-indias')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="vplanea">
							@include('templates.menu-principal.viajes-comfenalco.tab-planea-tu-viaje-con-nosotros')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection
