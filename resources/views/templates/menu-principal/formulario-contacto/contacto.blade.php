<form id="formAfiliaciones" action="" method="POST">
	{{ csrf_field() }}
	<h3 class="text-blue-2 pb-2">Formulario de contacto</h3>
	<div class="form-group row ml-0 mr-0">
		<label for="nombreContacto" class="rounded bg-blue-6 text-white col-md-2 col-form-label">Nombre</label>
		<div class="p-0 col-md-10">
			<input type="text" name="nombreContacto" class="form-control bg-gray-3" id="nombreContacto" placeholder="">
		</div>
	</div>
	<div class="form-group row ml-0 mr-0">
		<label for="correoContacto" class="rounded bg-blue-6 text-white col-md-2 col-form-label">Correo</label>
		<div class="p-0 col-md-10">
			<input type="email" name="correoContacto" class="form-control bg-gray-3" id="correoContacto" placeholder="">
		</div>
	</div>
	<div class="form-group row ml-0 mr-0">
		<label for="telefonoContacto" class="rounded bg-blue-6 text-white col-md-2 col-form-label">Teléfono</label>
		<div class="p-0 col-md-10">
			<input type="text" name="telefonoContacto" class="form-control bg-gray-3" id="telefonoContacto" placeholder="">
		</div>
	</div>
	<div class="form-group row ml-0 mr-0">
		<label for="mensajeContacto" class="rounded bg-blue-6 text-white col-md-2 col-form-label">Mensaje</label>
		<div class="p-0 col-md-10">
			<textarea class="form-control bg-gray-3" name="mensajeContacto" id="mensajeContacto" rows="10"></textarea>
			<button type="submit" class="btn bg-blue-6 text-white">Enviar</button>
		</div>
	</div>			
</form>