@extends('layouts.main')

@section('title')Crédito Social @endsection

@section('styles')
@endsection

@section('content')
	<div id="main-carousel" class="box-default">
        <div class="container-fluid">
        	<div class="row">
	            <div id="carouselExampleControls" class="carousel slide mx-auto" data-ride="carousel">
                	@if(isset($banners) && count($banners) != 0)
	                <div class="carousel-inner" role="listbox">
				            @foreach($banners as $index => $slider)
				                @if($index == 0)
				                    <div class="carousel-item active">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                    	@if($slider['url'] != NULL) </a> @endif
				                        
				                    </div>
				                @else
				                    <div class="carousel-item ">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                        @if($slider['url'] != NULL) </a> @endif
				                    </div>
				                @endif
				            @endforeach
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	                    <span class="arrow-left">
	                        <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
	                    </span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	                    <span class="arrow-right">
	                        <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
	                    </span>
	                </a>
			        @endif
	            </div>
            </div>
        </div>
    </div>
	<div id="credito-social" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-12">
					@if($creditosocial[0]['description'] != NULL)<div class="pl-md-5 pr-md-5 h6 d-block pb-md-4 text-justify">{!! $creditosocial[0]['description'] !!}</div>@endif
					<ul id="myTab" class="nav flex-column flex-md-row flex-sm-row flex-lg-row flex-xl-row nav-tabs d-sm-flex justify-content-sm-between responsive-tabs" role="tablist">
						@if(isset($tabs))
							@foreach($tabs as $index => $tab)
								<li class="nav-item @if($tab['type_tabs'] == 2) dropdown @endif">
									@if($tab['type_tabs'] == 1) 
										<a class="justify-content-center text-white nav-link bg-tab-1 @if($index == 0) active @endif" href="#o{{ htmlentities(str_replace(' ', '', mb_strtolower($tab['title'])), ENT_QUOTES, "UTF-8") }}" role="tab" data-toggle="tab" >{{ $tab['title'] }}</a>
									@endif
									@if($tab['type_tabs'] == 2)
										<a class="justify-content-center text-white nav-link bg-tab-1 dropdown-toggle @if($index == 0) active @endif" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ $tab['title'] }}</a>
										<div class="dropdown-menu">
											@foreach($tab->sub_tabs as $indexs => $tb)
												<a class=" text-uppercase dropdown-item @if($loop->parent->index == 0 && $indexs == 0) active @endif" id="v-pills-{{ $tb['slug'] }}-tab" data-toggle="pill" href="#v-pills-{{ $tb['slug'] }}" role="tab" aria-controls="v-pills-{{ $tb['slug'] }}" aria-expanded="true">{{ $tb['title'] }}</a>
											@endforeach
										</div>
									@endif
								</li>
							@endforeach
						@endif
					</ul>
					<div class="row pb-3">
						<div class="  col-md-12 mx-auto">
							<div class="tab-content" id="v-pills-tabContent">
								@if(isset($tabs))
									@foreach($tabs as $index => $tab)
										@if($tab['type_tabs'] == 1)
											<div role="tabpanel" class="tab-pane fade bg-gray-2  @if($index == 0) show active @endif" id="o{{ htmlentities(str_replace(' ', '', mb_strtolower($tab['title'])), ENT_QUOTES, "UTF-8") }}">
												@include('templates.menu-principal.credito-social.tab-'.$tab['slug'])
											</div>
										@endif
										@if($tab['type_tabs'] == 2)
											@foreach($tab->sub_tabs as $indexs => $tb)
												<div class="tab-pane fade @if($loop->parent->index == 0 && $indexs == 0) active show @endif" id="v-pills-{{ $tb['slug'] }}" role="tabpanel" aria-labelledby="{{ $tb['slug'] }}-tab">
													@include('templates.menu-principal.credito-social.'.$tab['slug'].'.tab-'.$tb['slug'])
												</div>
											@endforeach
										@endif
									@endforeach
								@endif
								{{-- <div role="tabpanel" class="tab-pane fade show active bg-gray-2" id="oempresas">
									<!-- Include -->
									@include('templates.menu-principal.afiliaciones.tab-empresas')
									<div class="row m-0 pt-2 pb-2">
										<div class="col-md-7 mx-auto">
											<br><br>
											@include('templates.menu-principal.formulario-contacto.contacto')
										</div>
									</div>
								</div> --}}
							</div>
						</div>
					</div>
					<!-- Tab panes -->
					{{-- <div class="tab-content">
						<div role="tabpanel" class="tab-pane fade show active" id="ucupocredito">
							@include('templates.menu-principal.credito-social.tab-cupocredito')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="ulibranza">
							@include('templates.menu-principal.credito-social.tab-libranza')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="ulinea">
							@include('templates.menu-principal.credito-social.tab-linea-express')
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection
