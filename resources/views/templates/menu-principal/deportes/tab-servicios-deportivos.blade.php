<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-5 justify-content-md-center">
			<div class="row m-0 pb-2">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/deportes-servicios-deportivos.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<h3 class="text-blue-2 pb-2">SERVICIOS DEPORTIVOS</h3>
					<p class="text-justify">Desarrolle  con nosotros sus Planes de Bienestar Empresarial. Comfenalco Cartagena pone a su disposición su amplia y reconocida experiencia en la planificación, organización, ejecución y control de actividades recreo deportivas.</p>
					<span class="d-block pb-2 h4 text-blue-2">Actividades</span>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Plan Empresa Activa y saludable.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Torneos Internos.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Rumbaterapia.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-orange.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Actividades físicas y recreo deportivas.</p>
                        </div>
                    </div>
				</div>
			</div>
			<div class="row m-0">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>