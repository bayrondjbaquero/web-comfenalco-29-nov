<div class="container bg-gray-2">
    <div class="row pb-3">
        <div class="col-md-11 m-auto pt-4 justify-content-md-center">
            <div class="row m-0 pb-4">
                <div class="col-md-5 text-center pb-3">
                    <img src="{{asset('img/deportes-escuelas-deportivas.jpg')}}" class="img-fluid rounded" alt="Deportes Escuelas Deportivas">
                </div>
                <div class="col-md-7 d-flex justify-content-center flex-column">
                    <h4 class="text-blue-2 pb-2">ESCUELAS DEPORTIVAS</h4>
                    <p class="text-justify">Las Escuelas de Formación Deportiva, ESDECO, buscan contribuir al desarrollo integral de niños y jóvenes en la edad de 4 a 16 años, que comprende los procesos de iniciación, fundamentación y perfeccionamiento deportivo.</p>
                    <p class="text-justify">Son cursos que cuentan con un programa estructurado de procesos pedagógicos, supervisados por un grupo de instructores deportivos, profesionales del área que garantizan el cumplimiento de los objetivos.</p>
                </div>
            </div>
        </div>   
        <div class="col-md-12">
            <div class="d-flex justify-content-center text-center flex-wrap">
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/baloncesto.png')}}" alt="Baloncesto">
                    <figcaption>Baloncesto</figcaption>
                </figure>                
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/beisbol.png')}}" alt="Béisbol">
                    <figcaption>Béisbol</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/futbol.png')}}" alt="Fútbol">
                    <figcaption>Fútbol</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/natacion.png')}}" alt="Natación">
                    <figcaption>Natación</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/patinaje.png')}}" alt="Patinaje">
                    <figcaption>Patinaje</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/atletismo.png')}}" alt="Atletismo">
                    <figcaption>Atletismo</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/ajedrez.png')}}" alt="Ajedrez">
                    <figcaption>Ajedrez</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/voleibol.png')}}" alt="Voleibol">
                    <figcaption>Voleibol</figcaption>
                </figure>
            </div>
        </div>          
        <div class="col-md-11 m-auto pt-4 justify-content-md-center">
            <div class="row m-0">
                <div class="col-md-6 mb-2">
                    <span class="h4 text-blue-2 d-block pb-2">Ventajas</span>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Variedad de modalidades deportivas.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Diversidad de horarios.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Instalaciones cómodas y modernas.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Número determinado de alumnos por profesor.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Instructores especializados por disciplina deportiva.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Retroalimentación pedagógica.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Seguridad.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Acompañamiento permanente.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Asistencia psicosocial.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <span class="h4 text-blue-2 d-block pb-2">Nuestros planes incluyen:</span>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Materiales didácticos.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Escenarios propios.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Instalaciones cómodas y modernas.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Entrega de uniformes.</p>
                        </div>
                    </div>
                    <br>
                    <p>Para más información, llene el siguiente formulario y uno de nuestros asesores se comunicará con usted.</p>
                    <p>Para inscribirte en las escuelas deportivas, <a href="{{ asset('pdf/FORMULARIO INSCRIPCION.pdf') }}" target="_blank" download="Formulario escuelas deportivas">descarga este formulario.</a></p>
                </div>
                <div class="col-md-12 pt-3">
                    <span class="h4 text-blue-2 d-block pb-2">Escuelas deportivas Magangué</span>
                    <div class="row m-0">
                        <div class="col-md-5">
                            <img class="rounded img-fluid d-block mb-2" src="{{ asset('img/deportes-escuelas.jpg') }}" alt="">
                        </div>
                        <div class="col-md-7">
                            <p class="text-justify">Están dirigidas a niños de 4 a 16 años. En caso de adultos, se requiere un grupo de mínimo 10 personas y se ofrece en un horario distinto al de los niños.</p>
                            <figure class="d-inline-block pr-3 text-center">
                                <img class="img-fluid" src="{{asset('img/futbol.png')}}" alt="Fútbol">
                                <figcaption>Fútbol</figcaption>
                            </figure>
                            <figure class="d-inline-block text-center">
                                <img class="img-fluid" src="{{asset('img/natacion.png')}}" alt="Natación">
                                <figcaption>Natación</figcaption>
                            </figure>
                            <p class="mb-2">La matrícula incluye:</p>
                            <p><span class="text-yellow pr-2">•</span>Fútbol: camiseta, pantaloneta, medias y balón.<br>
                            <span class="text-yellow pr-2">•</span>Natación: vestido de baño y gorro.</p>
                        </div>  
                    </div>                  

                    <span class="h4 text-blue-2 d-block pb-2">Requisitos de la inscripción:</span>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Descarga y diligencia este formulario</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Dos fotos tamaño carnet</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Suscribir compromiso de cumplimiento con el reglamento de las escuelas.</p>
                        </div>
                    </div><br>

                    <span class="text-blue-2 h5 d-block pb-2">Horarios:</span>
                    <span class="text-blue-2 h6 d-block">Natación:</span>
                    <p>Martes y Jueves: 4:00 – 6:00 pm<br>
                    Sábado: 9:00 – 10:00 am</p>
                    <span class="text-blue-2 h6 d-block">Fútbol:</span>
                    <p>Lunes, Miércoles y Viernes: 4:00 – 6:00 pm</p>

                    <p>Para inscribirte en las escuelas deportivas, <a href="{{ asset('pdf/FORMULARIO INSCRIPCION.pdf') }}" target="_blank" download="Formulario escuelas deportivas">descarga este formulario.</a></p>

                </div>
                <div class="col-11 mx-auto">
                    <br><br>
                    @include('templates.menu-principal.formulario-contacto.contacto')
                </div>
            </div>
        </div>
    </div>
</div>