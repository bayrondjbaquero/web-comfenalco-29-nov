<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-5 justify-content-md-center">
			<div class="row m-0 pb-2">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/deportes-jhon-murillo.jpg')}}" class="img-fluid rounded" alt="Jhon Murillo">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">CLUB DEPORTIVO</h4>
					<p class="text-justify">El Club Deportivo de Comfenalco Cartagena con más de 40 años de existencia, es el club más reconocido y con mejores resultados deportivos de la ciudad de Cartagena y el departamento de Bolívar.</p>
					<p class="text-justify">Actualmente nuestro Club desarrolla competitivamente las modalidades de Atletismo, Béisbol, Futbol, Natación y Patinaje.</p>
					<p class="text-justify">Comfenalco es cuna de grandes deportistas que han hecho historia en el deporte colombiano y el deporte mundial, como Jolbert y Orlando Cabrera en Béisbol, Cristian Marrugo y John Hernandez en Fútbol, Alvaro Teherán en Baloncesto, John Murillo y Giselly Landazury en Atletismo, Israel Tovío en Ajedrez, Herney Saldarriaga en Voleibol de Playa, Alexandra Vivas en Patinaje, entre otros tantos.</p>
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-7 d-flex flex-column justify-content-center">
					<div class="d-block">
						<p class="text-justify">Comfenalco es la organización que más deportistas ha firmado para las distintas organizaciones de la MLB con más de 40 beisbolistas. Actualmente 5 atletas del club se encuentran adelantando estudios en Puerto Rico, ellos son Maderley Alcázar, Carmen Vergara, Kevin Espinosa, Vanessa Campo y Moisés Ortíz.</p>
						<p class="text-justify">Nuestra atleta Giselly Landázury Parra es considerada la sucesora de la actual campeona mundial de salto triple Katerine Ibargüen, y tiene a su haber el titulo suramericano juvenil y el record juvenil de la modalidad.</p>
					</div>
				</div>
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/deportes-salto-triple.jpg')}}" class="img-fluid rounded" alt="Giselly Landázury">
				</div>
			</div>
			<p>Para inscribirse en el Club Deportivo, <a href="{{ asset('pdf/FORMULARIO INSCRIPCION CLUB.doc') }}" target="_blank">descargue el siguiente formulario.</a></p>
			<div class="row m-0">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>