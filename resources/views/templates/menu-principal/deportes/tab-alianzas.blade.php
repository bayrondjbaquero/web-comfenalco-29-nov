<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-5 justify-content-md-center">
			<div class="row m-0 pb-2">
				<div class="col-md-7 text-center pb-3">
					<img src="{{asset('img/deportes-alianzas.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-5">
					<h3 class="text-blue-2 pb-2">ALIANZAS</h3>
					<p class="text-justify">En la búsqueda permanente de nuevos horizontes  para proyectar el deporte y los deportistas  de Comfenalco Cartagena a nivel local, nacional e internacional; hemos  implementado alianzas  con importantes organizaciones deportivas y no deportivas.</p>
				</div>
			</div>
			<div class="row m-0 pt-4">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>