<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/deportes-gambeta.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">LA GAMBETA</h3>
					<p class="text-justify">Cancha con grama artificial que cuenta con una infraestructura y servicios que le permiten a un grupo de múltiples usuarios la práctica de futbol de salón, ofreciéndoles un escenario cubierto donde las inclemencias del clima no afectaran su práctica, permitiendo además el acceso al deporte organizado.</p>
				</div>
				<div class="col-md-12 mx-auto pt-2">
					<span class="pl-3 h4 text-blue-2 d-block pb-2">Características:</span>
					<div class="d-flex justify-content-center text-center flex-wrap">
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/restroom-y-vestier.png') }}" alt="Baño y vestier">
							<figcaption>Baño y vestier para jugadores</figcaption>
						</figure>
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/restroom-para-mujeres.png') }}" alt="Baño para mujeres">
							<figcaption>Baño para Mujeres</figcaption>
						</figure>
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/mezanine.png') }}" alt="Mezanine">
							<figcaption>Mezanine con mesas (vista a la cancha)</figcaption>
						</figure>
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/tienda-con-surtido.png') }}" alt="Tienda">
							<figcaption>Tienda con surtido de bebidas y alimentos.</figcaption>
						</figure>
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/graderias.png') }}" alt="Graderías">
							<figcaption>Graderías para acompañantes y espectadores.</figcaption>
						</figure>
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/administracion.png') }}" alt="Administración">
							<figcaption>Administración</figcaption>
						</figure>
						<figure class="col">
							<img class="img-fluid" src="{{ asset('img/musica.png') }}" alt="Música">
							<figcaption>Música a volumen moderado</figcaption>
						</figure>
					</div>
				</div>
				<div class="col-12">
					<span class="h4 text-blue-2 d-block pb-2">Ventajas</span>
				</div>
				<div class="col-md-6">
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Disminución de traumas articulares por la flexibilidad que ofrece.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Este tipo de pisos brinda la posibilidad de mayores efectos al balón.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Disminución de contusiones y laceraciones por disminución de la superficie de fricción.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">El lugar donde está ubicado este escenario pues se trata de un espacio que cuenta con muchas más atracciones y zonas de esparcimiento no solo para el jugador adulto sino para el grupo familiar.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Más tiempo efectivo de juego pues los escenarios son completamente cerrados.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 pb-5 pt-3">
                	<div class="row m-0">
		                <div class="col-md-5 text-center">
		                	<div id="carouselExampleControls02" class="carousel slide mx-auto" data-ride="carousel">
			                    <div class="carousel-inner" role="listbox">
			                        <div class="carousel-item active">
			                            <img class="img-fluid rounded d-block m-auto" src="{{asset('img/complejo-deportivo.jpg')}}" alt="">
			                        </div>
			                    </div>
			                    <a class="carousel-control-prev" href="#carouselExampleControls0" role="button" data-slide="prev">
			                        <span class="arrow-left">
			                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
			                        </span>
			                    </a>
			                    <a class="carousel-control-next" href="#carouselExampleControls0" role="button" data-slide="next">
			                        <span class="arrow-right">
			                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
			                        </span>
			                    </a>
			                </div>
		                </div>
		                <div class="col-md-7 justify-content-center d-flex flex-column">
		                	<h4 class="pb-2 text-blue-2">COMPLEJO DEPORTIVO</h4>
		                	<p>Construido en un área aproximada de 8.300 metros cuadrados, este escenario deportivo propio cuenta con:</p>
		                </div>	
	                </div>
                </div>
                <div class="col-md-11 mx-auto">
                	<div class="d-flex justify-content-center text-center flex-wrap">
	                	<figure class="col">
	                		<img class="img-fluid" src="{{ asset('img/cancha-sintetica.png') }}" alt="">
	                		<figcaption>Cancha sintética para Fútbol 9</figcaption>
	                	</figure>
	                	<figure class="col">
	                		<img class="img-fluid" src="{{ asset('img/pista-lineal-atletismo.png') }}" alt="">
	                		<figcaption>Pista lineal de Atletismo de 70 metros</figcaption>
	                	</figure>
	                	<figure class="col">
	                		<img class="img-fluid" src="{{ asset('img/foso-salto.png') }}" alt="">
	                		<figcaption>Foso para salto triple y de longitud</figcaption>
	                	</figure>
	                	<figure class="col">
	                		<img class="img-fluid" src="{{ asset('img/pista-lineal-atletismo.png') }}" alt="">
	                		<figcaption>Un área para iniciación al Patinaje de Carreras.</figcaption>
	                	</figure>
	                	<figure class="col">
	                		<img class="img-fluid" src="{{ asset('img/cancha-polifuncional.png') }}" alt="">
	                		<figcaption>Cancha Polifuncional para la práctica del Baloncesto, Microfútbol y Voleibol.</figcaption>
	                	</figure>
	                	<figure class="col">
	                		<img class="img-fluid" src="{{ asset('img/cancha-para-children.png') }}" alt="">
	                		<figcaption>Cancha Polifuncional para niños entre los 4 y 9 años para Mini Baloncesto, Microfútbol y Mini Voleibol.</figcaption>
	                	</figure>
                	</div>
                	<p class="text-justify pb-2"><em>Además cuenta con servicio de baños, vestieres, cafetería y graderías para 300 Personas aproximadamente, además de los elementos y materiales deportivos necesarios para el desarrollo de las actividades.</em></p>
                	<span class="d-block h4 text-blue-2 pb-2">ACTIVIDADES QUE SE PUEDEN DESARROLLAR</span>
                	<div class="col-md-12">
	                    <div class="media pb-2 d-flex align-items-center">
	                        <img class="d-flex mr-2" src="{{ asset('img/uno-green.png') }}" alt="deportes uno">
	                        <div class="media-body">
	                            <p class="mb-0">AFILIADOS: Juegos informales.</p>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-12">
	                    <div class="media pb-2 d-flex align-items-center">
	                        <img class="d-flex mr-2" src="{{ asset('img/dos-purple.png') }}" alt="deportes dos">
	                        <div class="media-body">
	                            <p class="mb-0">EMPRESAS: Juegos Interempresas, Olimpiadas Interempresas, Torneo Internos, Jornadas Deportivas y Recreativas, Eventos Sociales, Eventos de Capacitación, Acondicionamiento Físico General a Trabajadores.</p>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-12">
	                    <div class="media pb-2 d-flex align-items-center">
	                        <img class="d-flex mr-2" src="{{ asset('img/tres-yellow.png') }}" alt="deportes tres">
	                        <div class="media-body">
	                            <p class="mb-0">ESCUELAS DEPORTIVAS: Festivales Deportivos, Clases por modalidad deportiva de acuerdo con el respectivo programa técnico metodológico, clínicas y exhibiciones deportivas.</p>
	                        </div>
	                    </div>
	                </div>
	                <p class="pt-2">Para más información, llene el siguiente formulario y uno de nuestros asesores se comunicará con usted.</p>
                </div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-10 mx-auto">
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>		
	</div>
</div>





