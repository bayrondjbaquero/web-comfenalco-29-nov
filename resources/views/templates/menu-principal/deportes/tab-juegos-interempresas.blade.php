<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-5 justify-content-md-center">
			<div class="row m-0 pb-2">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/deportes-juegos-interempresas.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<h3 class="text-blue-2 pb-2">JUEGOS INTEREMPRESAS</h3>
					<p class="text-justify">Los Juegos Interempresas y las  Olimpiadas de las empresas afiliadas a Comfenalco Cartagena, los concebimos como una cita deportiva anual, abiertos a la participación de las grandes, medianas y pequeñas empresas de la ciudad de Cartagena.</p>
                    <p class="text-justify pb-2">Se trata de una competición entre empresas, para trasladar el auténtico espíritu “mente sana en cuerpo sano”.</p>
					<span class="d-block pb-2 h4 text-blue-2">Deportes que se desarrollan:</span>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Juegos Interempresas: Fútbol de Salón, Fútbol 9, Softbol y Kickball.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Olimpiadas Empresariales: Ajedrez, Atletismo, Baloncesto, Bolos, Caminata,  Ciclismo , Dominó, Fútbol de Salón, Fútbol 9, Kickball, Natación, Tejo, Tenis, Tenis de Mesa y Voleibol.</p>
                        </div>
                    </div>
				</div>
                <div class="col-md-12 text-center pt-4 pb-4">
                    <a href="{{ asset('pdf/FORMATO DE INSCRIPCION JUEGOS INTEREMPRESAS 2017.docx') }}" target="_blank"><img class="img-fluid" src="{{ asset('img/deportes-descargar-inscripcion.png') }}" alt=""></a>
                </div>
			</div>
			<div class="row m-0">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>