@extends('layouts.main')

@section('title')Educación @endsection

@section('styles')
@endsection

@section('content')
	<div id="main-carousel" class="box-default">
        <div class="container-fluid">
        	<div class="row">
	            <div id="carouselExampleControls" class="carousel slide mx-auto" data-ride="carousel">
                	@if(isset($banners) && count($banners) != 0)
	                <div class="carousel-inner" role="listbox">
				            @foreach($banners as $index => $slider)
				                @if($index == 0)
				                    <div class="carousel-item active">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                    	@if($slider['url'] != NULL) </a> @endif
				                        
				                    </div>
				                @else
				                    <div class="carousel-item ">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                        @if($slider['url'] != NULL) </a> @endif
				                    </div>
				                @endif
				            @endforeach
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	                    <span class="arrow-left">
	                        <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
	                    </span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	                    <span class="arrow-right">
	                        <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
	                    </span>
	                </a>
			        @endif
	            </div>
            </div>
        </div>
    </div>
	<div id="educacion" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-12">
					@if($educacion[0]['description'] != NULL)<div class="pl-md-5 pr-md-5 h6 d-block pb-md-4 text-justify">{!! $educacion[0]['description'] !!}</div>@endif
					<ul id="myTab" class="nav flex-column flex-md-row flex-sm-row flex-lg-row flex-xl-row nav-tabs d-sm-flex justify-content-sm-between responsive-tabs" role="tablist">
						@if(isset($tabs))
							@foreach($tabs as $index => $tab)
								<li class="nav-item @if($tab['type_tabs'] == 2) dropdown @endif">
									@if($tab['type_tabs'] == 1) 
										<a class="justify-content-center text-white nav-link bg-tab-1 @if($index == 0) active @endif" href="#o{{ str_replace(' ', '', strtolower($tab['title'])) }}" role="tab" data-toggle="tab" >{{ $tab['title'] }}</a>
									@endif
									@if($tab['type_tabs'] == 2)
										<a class="justify-content-center text-white nav-link bg-tab-1 dropdown-toggle @if($index == 0) active @endif" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ $tab['title'] }}</a>
										<div class="dropdown-menu">
											@foreach($tab->sub_tabs as $indexs => $tb)
												<a class=" text-uppercase dropdown-item @if($loop->parent->index == 0 && $indexs == 0) active @endif" id="v-pills-{{ $tb['slug'] }}-tab" data-toggle="pill" href="#v-pills-{{ $tb['slug'] }}" role="tab" aria-controls="v-pills-{{ $tb['slug'] }}" aria-expanded="true">{{ $tb['title'] }}</a>
											@endforeach
										</div>
									@endif
								</li>
							@endforeach
						@endif
					</ul>
					<!-- Tab panes -->
					<div class="row pb-3">
						<div class="col-md-12 mx-auto">
							<div class="tab-content" id="v-pills-tabContent">
								@if(isset($tabs))
									@foreach($tabs as $index => $tab)
										@if($tab['type_tabs'] == 1)
											<div role="tabpanel" class="tab-pane fade bg-gray-2  @if($index == 0) show active @endif" id="o{{ str_replace(' ', '', strtolower($tab['title'])) }}">
												@include('templates.menu-principal.educacion.tab-'.$tab['slug'])
											</div>
										@endif
										@if($tab['type_tabs'] == 2)
											@foreach($tab->sub_tabs as $indexs => $tb)
												<div class="tab-pane fade @if($loop->parent->index == 0 && $indexs == 0) active show @endif" id="v-pills-{{ $tb['slug'] }}" role="tabpanel" aria-labelledby="{{ $tb['slug'] }}-tab">
													@include('templates.menu-principal.educacion.'.$tab['slug'].'.nav-'.$tb['slug'])
												</div>
											@endforeach
										@endif
									@endforeach
								@endif
								{{-- @include('templates-admin.menu-principal.educacion.educacion-para-el-trabajo.programas.sistemas') --}}
								{{-- <div role="tabpanel" class="tab-pane fade show active bg-gray-2" id="oempresas">
									<!-- Include -->
									@include('templates.menu-principal.afiliaciones.tab-empresas')
									<div class="row m-0 pt-2 pb-2">
										<div class="col-md-7 mx-auto">
											<br><br>
											@include('templates.menu-principal.formulario-contacto.contacto')
										</div>
									</div>
								</div> --}}
								<!-- Educación continua -->
								<!-- Diplomado en Gerencia de Proyectos -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown27">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-gerencia-de-proyectos')
								</div>
								<!-- Diplomado en Gestion Logistica Empresarial y Seguridad Portuaria-->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown28">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-gestion-logistica-empresarial-y-seguridad-portuaria')
								</div>
								<!-- Diplomado en liquidacion de nomina y presentaciones sociales -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown29">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-liquidacion-de-nomina-y-presentaciones-sociales')
								</div>
								<!-- Diplomado en Marketing Digital y Publicidad Online -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown30">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-marketing-digital-y-publicidad-online')
								</div>
								<!-- Diplomado Sistemas de Gestion en Salud y Seguridad en el Trabajo (SGSST) -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown31">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-de-gestion-en-salud-y-seguridad-en-el-trabajo-sgsst')
								</div>
								<!-- Diplomado en Habilidades Generales -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown32">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-habilidades-generales')
								</div>
								<!-- Diplomado en Gerencia Financiera -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown33">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-gerencia-financiera')
								</div>
								<!-- Diplomado en Sistemas de Gestion Integrados -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown34">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-sistemas-de-gestion-integrados')
								</div>
								<!-- Diplomado en Formacion por Competencia para la educacion -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown35">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-formacion-por-competencia-para-la-educacion')
								</div>
								<!-- Diplomado en Normas Internacionales de Auditoria - NIAS -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown36">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-normas-internacionales-de-auditoria-nias')
								</div>
								<!-- Diplomado en Gestion Humana -->
								<div role="tabpanel" class="tab-pane fade" id="nav-dropdown37">
									@include('templates.menu-principal.educacion.educacion-continua.diplomado-en-gestion-humana')
								</div>
							</div>
						</div>
					</div>
					<!-- Tab panes -->
					
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<!-- History.js -->
<script src="//browserstate.github.io/history.js/scripts/bundled/html4+html5/jquery.history.js"></script>

<script type="text/javascript">
	/* Enlace de doble certificación abre TAB programas */
	$("#myHref").on('click', function() {
		$('#educacion a[href="#nav-dropdown10"]').tab('show');
	});

	/* EDUCACION PARA EL TRABAJO-> PROGRAMAS */
	$("#nav-dropdown10-tab").click(function (e) {
		e.preventDefault();
		reset_tab();
        $("#educacion .tab-content #nav-dropdown10").addClass('show');
        $("#educacion .tab-content #nav-dropdown10").addClass('active');
		$('#educacion a[href="#nav-dropdown10"]').tab('show');
		jumptop();
	});

	/* EDUCACION PARA EL TRABAJO-> PROGRAMAS */
	$("#educacion .tab-content .nav-dropdown10-tab-back").click(function (e) {
		e.preventDefault();
		reset_tab();
        $("#educacion .tab-content #nav-dropdown10").addClass('show');
        $("#educacion .tab-content #nav-dropdown10").addClass('active');
		$('#educacion a[href="#nav-dropdown10"]').tab('show');
		jumptop();
	});

	/* Abrir micrositio programas -> Procesamiento industrial */
	$("#p_procesamiento_industrial").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown14").addClass('show');
    	$("#educacion .tab-content #nav-dropdown14").addClass('active');
    	jumptop();

	});

	$("#p_seguridad_ocupacional").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown15").addClass('show');
    	$("#educacion .tab-content #nav-dropdown15").addClass('active');
    	jumptop();
	});

	$("#p_control_de_calidad").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown16").addClass('show');
    	$("#educacion .tab-content #nav-dropdown16").addClass('active');
    	jumptop();
	});

	$("#p_comercio_exterior").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown17").addClass('show');
    	$("#educacion .tab-content #nav-dropdown17").addClass('active');
    	jumptop();
	});

	$("#p_procesos_logisticos").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown18").addClass('show');
    	$("#educacion .tab-content #nav-dropdown18").addClass('active');
    	jumptop();
	});

	$("#p_auxiliar_contable_y_financiero").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown19").addClass('show');
    	$("#educacion .tab-content #nav-dropdown19").addClass('active');
    	jumptop();
	});

	$("#p_auxiliar_administrativo").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown20").addClass('show');
    	$("#educacion .tab-content #nav-dropdown20").addClass('active');
    	jumptop();
	});
	
	$("#p_mercadeo_y_ventas").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown21").addClass('show');
    	$("#educacion .tab-content #nav-dropdown21").addClass('active');
    	jumptop();
	});

	$("#p_auxiliar_de_talento_humano").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown22").addClass('show');
    	$("#educacion .tab-content #nav-dropdown22").addClass('active');
    	jumptop();
	});

	$("#p_sistemas").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown23").addClass('show');
    	$("#educacion .tab-content #nav-dropdown23").addClass('active');
    	jumptop();
	});

	$("#p_mantenimiento_de_hardware_y_redes").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown24").addClass('show');
    	$("#educacion .tab-content #nav-dropdown24").addClass('active');
    	jumptop();
	});

	$("#p_servicios_hoteleros_y_turisticos").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown25").addClass('show');
    	$("#educacion .tab-content #nav-dropdown25").addClass('active');
    	jumptop();
	});

	$("#p_tecnico_laboral_auxiliar_convenciones_y_eventos").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown26").addClass('show');
    	$("#educacion .tab-content #nav-dropdown26").addClass('active');
    	jumptop();
	});
	/* EDUCACION CONTINUA */

	$(".educacion-continua-back").click(function (e) {
		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #oeducaciÓncontinua").addClass('show');
    	$("#educacion .tab-content #oeducaciÓncontinua").addClass('active');
    	jumptop();
	});

	$("#d_gp").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown27").addClass('show');
    	$("#educacion .tab-content #nav-dropdown27").addClass('active');
    	jumptop();
	});

	$("#d_gleysp").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown28").addClass('show');
    	$("#educacion .tab-content #nav-dropdown28").addClass('active');
    	jumptop();
	});

	$("#d_ldnyps").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown29").addClass('show');
    	$("#educacion .tab-content #nav-dropdown29").addClass('active');
    	jumptop();
	});

	$("#d_mdypo").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown30").addClass('show');
    	$("#educacion .tab-content #nav-dropdown30").addClass('active');
    	jumptop();
	});

	$("#d_sgsst").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown31").addClass('show');
    	$("#educacion .tab-content #nav-dropdown31").addClass('active');
    	jumptop();
	});

	$("#d_hg").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown32").addClass('show');
    	$("#educacion .tab-content #nav-dropdown32").addClass('active');
    	jumptop();
	});

	$("#d_sdgi").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown33").addClass('show');
    	$("#educacion .tab-content #nav-dropdown33").addClass('active');
    	jumptop();
	});

	$("#d_gf").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown34").addClass('show');
    	$("#educacion .tab-content #nav-dropdown34").addClass('active');
    	jumptop();
	});

	$("#d_fpcple").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown35").addClass('show');
    	$("#educacion .tab-content #nav-dropdown35").addClass('active');
    	jumptop();
	});

	$("#d_nias").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown36").addClass('show');
    	$("#educacion .tab-content #nav-dropdown36").addClass('active');
    	jumptop();
	});

	$("#d_gh").click(function (e) {
  		e.preventDefault();
  		reset_tab();
	    $("#educacion .tab-content #nav-dropdown37").addClass('show');
    	$("#educacion .tab-content #nav-dropdown37").addClass('active');
    	jumptop();
	});
	
	/* FUNCION RESET */
	function reset_tab(){
		$("#educacion .tab-content .tab-pane").each(function (index) { 
            $(this).removeClass('show');
            $(this).removeClass('active');
        });
	}
	function jumptop(){
		document.getElementById('educacion').scrollIntoView();
	}
</script>
@endsection
