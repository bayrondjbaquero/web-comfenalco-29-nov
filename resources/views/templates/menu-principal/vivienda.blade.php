@extends('layouts.main')

@section('title')Vivienda @endsection

@section('styles')
@endsection

@section('content')
	<div id="main-carousel" class="box-default">
        <div class="container-fluid">
        	<div class="row">
	            <div id="carouselExampleControls" class="carousel slide mx-auto" data-ride="carousel">
                	@if(isset($banners) && count($banners) != 0)
	                <div class="carousel-inner" role="listbox">
				            @foreach($banners as $index => $slider)
				                @if($index == 0)
				                    <div class="carousel-item active">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                    	@if($slider['url'] != NULL) </a> @endif
				                        
				                    </div>
				                @else
				                    <div class="carousel-item ">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid rounded no-rounded-1110" src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                        @if($slider['url'] != NULL) </a> @endif
				                    </div>
				                @endif
				            @endforeach
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
	                    <span class="arrow-left">
	                        <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
	                    </span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
	                    <span class="arrow-right">
	                        <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
	                    </span>
	                </a>
			        @endif
	            </div>
            </div>
        </div>
    </div>
	<div id="vivienda" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-md-12">
					@if($vivienda[0]['description'] != NULL)<div class="pl-md-5 pr-md-5 h6 d-block pb-md-4 text-justify">{!! $vivienda[0]['description'] !!}</div>@endif
				</div>
				<div id="top_vivienda" class="col-md-12">
					<ul id="myTab" class="nav flex-column flex-md-row flex-sm-row flex-lg-row flex-xl-row nav-tabs d-sm-flex justify-content-sm-between responsive-tabs" role="tablist">
						@if(isset($tabs))
							@foreach($tabs as $index => $tab)
								<li class="nav-item @if($tab['type_tabs'] == 2) dropdown @endif">
									@if($tab['type_tabs'] == 1) 
										<a class="justify-content-center text-white nav-link bg-tab-1 @if($index == 0) active @endif" href="#o{{ htmlentities(str_replace(' ', '', mb_strtolower($tab['title'])), ENT_QUOTES, "UTF-8") }}" role="tab" data-toggle="tab" >{{ $tab['title'] }}</a>
									@endif
									@if($tab['type_tabs'] == 2)
										<a class="justify-content-center text-white nav-link bg-tab-1 dropdown-toggle @if($index == 0) active @endif" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ $tab['title'] }}</a>
										<div class="dropdown-menu">
											@foreach($tab->sub_tabs as $indexs => $tb)
												<a class=" text-uppercase dropdown-item @if($loop->parent->index == 0 && $indexs == 0) active @endif" id="v-pills-{{ $tb['slug'] }}-tab" data-toggle="pill" href="#v-pills-{{ $tb['slug'] }}" role="tab" aria-controls="v-pills-{{ $tb['slug'] }}" aria-expanded="true">{{ $tb['title'] }}</a>
											@endforeach
										</div>
									@endif
								</li>
							@endforeach
						@endif
					</ul>
					<!-- Tab panes -->
					<div class="row pb-3">
						<div class="  col-md-12 mx-auto">
							<div class="tab-content" id="v-pills-tabContent">

								@if(isset($tabs))
									@foreach($tabs as $index => $tab)
										@if($tab['type_tabs'] == 1)
											<div role="tabpanel" class="tab-pane fade bg-gray-2  @if($index == 0) show active @endif" id="o{{ htmlentities(str_replace(' ', '', mb_strtolower($tab['title'])), ENT_QUOTES, "UTF-8") }}">
												{{-- @if(file_exists(app_path().'/../resources/views/templates/menu-principal/vivienda/tab-'.$tab['slug'].'.blade.php')) --}}
												@include('templates.menu-principal.vivienda.tab-'.$tab['slug'])
												{{-- @endif --}}
											</div>
										@endif
										@if($tab['type_tabs'] == 2)
											@foreach($tab->sub_tabs as $indexs => $tb)
												<div class="tab-pane fade @if($loop->parent->index == 0 && $indexs == 0) active show @endif" id="v-pills-{{ $tb['slug'] }}" role="tabpanel" aria-labelledby="{{ $tb['slug'] }}-tab">
													{{-- @if(file_exists(app_path().'/../resources/views/templates.menu-principal.vivienda/tab-'.$tab['slug'].'.nav-'.$tb['slug'].'.blade.php')) --}}
													@include('templates.menu-principal.vivienda.'.$tab['slug'].'.nav-'.$tb['slug'])
													{{-- @endif --}}
												</div>
											@endforeach
										@endif
									@endforeach
								@endif
								<div class="tab-pane fade" id="nav-proyecto-show" role="tabpanel" aria-labelledby="nav-proyecto-show">
									@include('templates.menu-principal.vivienda.proyectos-de-vivienda.nav-proyectos-de-vivienda')
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')

<script>
	$(document).ready(function() {
		load_proyects();
	});

	function hola(slug){
		reset_tab();
		var href = slug;
		$.ajax({
            type: "GET",
            url: '{{ route('cargar_proyectos') }}/'+href,
            success:function(data)
            {
                $("#cargar_proyecto_slug").html(data);
            }
        });
		$("#vivienda .tab-content #nav-proyecto-show").addClass('show');
		$("#vivienda .tab-content #nav-proyecto-show").addClass('active');
		jumptop();
	}

	/* FUNCION RESET */
	function reset_tab(){
		$("#vivienda .tab-content .tab-pane").each(function (index) { 
            $(this).removeClass('show');
            $(this).removeClass('active');
        });
	}

	function jumptop(){
		document.getElementById('top_vivienda').scrollIntoView();
	}

	$(".nav_proyectos_back").click(function (e) {
		e.preventDefault();
		reset_tab();
		load_proyects();
     	$("#vivienda .tab-content #oproyectosdevivienda").addClass('show');
    	$("#vivienda .tab-content #oproyectosdevivienda").addClass('active');
		jumptop();
	});

	function search_proyects()
    {
    	var tipo, condicion, ubicacion;

    	tipo = $('#tipoForm').val();
    	condicion = $('#condicionForm').val();
    	ubicacion = $('#ubicacionForm').val();

    	if(tipo == "" || condicion == "" || ubicacion == ""){
        	alert('Existen campos vacios, intenta otra vez');
        	return false;
        }else{
			$.ajax({
				headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
	            type: "POST",
	            //dataType: 'json',
	            url: '{{ route('busqueda_proyectos') }}',
	            data: $("#search_proyects").serialize(),
	            success:function(data)
	            {
	                $("#cargar_proyectos").html(data);
	            },
	     	});
	     }
    }

    function load_proyects()
    {
		$.ajax({
            type: "GET",
            url: '{{ route('cargar_proyectos') }}',
            success:function(data)
            {
                $("#cargar_proyectos").html(data);
            }
        });
    }

    $(document).on('click','#page-paginate .pagination a', function(e){
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        var route = '{{ route('cargar_proyectos_pagination') }}';
        $.ajax({
            url: route,
            data: {page: page},
            type: 'GET',
            dataType: 'json',
            success: function(data){
                $("#cargar_proyectos").html(data);
            }
        });
    });

    $(document).on('click','#page-paginate-2 .pagination a', function(e){
        e.preventDefault();
        var page = $(this).attr('href').split('pagee=')[1];
        var route = '{{ route('busqueda_proyectos') }}';
        $.ajax({
            url: route,
            data: {page: page},
            type: 'GET',
            dataType: 'json',
            success: function(data){
                $("#cargar_proyectos").html(data);
            }
        });
    });
</script>
@endsection
