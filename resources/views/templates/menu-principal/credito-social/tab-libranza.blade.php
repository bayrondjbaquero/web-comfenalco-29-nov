<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5">
					<img src="{{asset('img/credito-libranza.jpg')}}" class="img-fluid rounded" alt="crédito social libranza">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">LIBRANZA</h4>
					<p class="text-justify">Comfenalco pensando en el mejoramiento de la calidad de vida de nuestros trabajadores afiliados,  ofrece créditos de fácil acceso en distintas modalidades y con las tasas de interés más competitivas del mercado, representando un respaldo económico a los afiliados.<br>
					El sistema de Libranza en Comfenalco tiene como requisito que las empresas afiliadas tengan un convenio de libranza previo con la Caja de Compensación para que sus trabajadores puedan acceder a las diferentes líneas de crédito. </p>
					<span class="d-block h4 text-blue-2 pb-2">¿Qué es el Convenio de Libranza?</span>
					<p>Acuerdo entre la Caja y la Empresa afiliada, en el cual se establece la participación de ambas partes desde el inicio del proceso del crédito hasta la facturación y recaudo del mismo. Este convenio es necesario para que el trabajador afiliado pueda acceder a los beneficios de nuestras líneas de financiación en la modalidad de libranza.</p>
				</div>
				{{-- <div class="col-md-6 p-0"><a href="#"><img class="img-fluid" src="{{ asset('img/credito-convenio-libranza.png') }}" alt="Crédito social Convenio Libranza"></a></div>
				<div class="col-md-6 p-0"><a href="#"><img class="img-fluid" src="{{ asset('img/credito-registro-clientes.png') }}" alt="Crédito social Registro Clientes"></a></div> --}}
			</div>
			<div class="row m-0 pt-2">
				<div class="col-12">
					<span class="d-block h4 text-blue-2 pb-2">Requisitos para acceder al convenio de libranza</span>
					<ul class="list-unstyled pb-2">
						<li><a href="{{ asset('pdf/FORMULARIO_REGISTRO_DE_CLIENTES_2016.pdf') }}" target="_blank">Registro de Cliente</a> y <a href="{{ asset('pdf/CONVENIO DE LIBRANZA 2017.pdf') }}">Convenio (diligenciados y Firmados)</a></li>
						<li>RUT</li>
						<li>Copia de CC Representante Legal</li>
						<li>Estados Financieros (último trimestre y/o semestre)</li>
						<li>Cámara de Comercio Vigente (no mayor a 30 días)</li>
						<li><a href="{{ asset('pdf/FORMATO SOLICITUD DE CLAVE.xlsx') }}" target="_blank">Formato de solicitud de clave</a></li>
					</ul>
					<span class="d-block h4 text-blue-2">¿Para qué puedo usar mi crédito de Libranza?</span>
					<br>
					<span class="d-block h4 font-weight-bold">Libre Inversión:</span>
					<p>Es una línea creada pensando en las múltiples necesidades del afiliado, teniendo en cuenta la capacidad de pago, con un plazo máximo de 72 meses y tasas de interés de acuerdo a la categoría del mismo.</p>
					<span class="d-block h4 font-weight-bold">Educación:</span>
					<p>Crédito destinado para financiar los estudios de educación formal y superior en cualquier Institución Educativa (pregrados, diplomados, seminarios, especializaciones y maestrías). Se financia el 100% del total de la matrícula, hasta un plazo máximo de 48 meses y tasas de interés de acuerdo a la categoría del mismo.</p>
					<span class="d-block h4 font-weight-bold">Vivienda:</span>
					<p>Crédito destinado para todos los afiliados que desean comprar o mejorar su vivienda. Se financia hasta la capacidad de pago del afiliado con un plazo máximo de 72 meses y tasas de interés de acuerdo a la categoría del mismo.</p>
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto-credito-social')
				</div>
			</div>
		</div>			
	</div>
</div>