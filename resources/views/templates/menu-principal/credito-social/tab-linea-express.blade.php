<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-2">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/credito-linea-express.jpg')}}" class="img-fluid rounded" alt="crédito social mercado">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">LÍNEA EXPRESS</h4>
					<p>Es un crédito otorgado al trabajador con un plazo máximo de 3 meses y su medio de pago es la cuota monetaria. Nuestra línea MERCAEXPRESS le permite comprar sus víveres o productos de la canasta familiar en los principales establecimientos en convenio.</p>
					<span class="d-block h4 text-blue-2 pb-2">Requisitos para acceder a Línea express</span>
					<ul class="list-unstyled pb-2">
						<li>Copia de ciudadania 150%.</li>
						<li>Certificado laboral vigente no mayor a 30 días.</li>
						<li>Certificado de número de cuenta.</li>
						<li>Volantes de nómina.</li>
						<li><a target="_blank" href="{{ asset('pdf/FORMULARIO_DE_CRÉDITO.pdf') }}">Formulario de crédito</a>.</li>
					</ul>
				</div>
			</div>
			<div class="row m-0 pt-4 d-flex justify-content-center">
				<div class="col-md-11">
					<div class="row m-0">
						<div class="col-md-12 pb-4">
							<span class="d-block h4 text-blue-2 mb-0 pb-2">Cartagena</span>
							<img class="img-fluid rounded" src="{{ asset('img/credito-almacenes-linea-express.png') }}" alt="Almacenes crédito express">
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4">
							<span class="h4 text-blue-2 d-block">Turbaco</span>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Olímpica</p><br>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Megatiendas</p>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4"><span class="h4 text-blue-2 d-block">Arjona</span>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Megatiendas</p><br>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Olímpica</p>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4"><span class="h4 text-blue-2 d-block">San Juan</span>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Olímpica</p><br>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Megatienda</p>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4"><span class="h4 text-blue-2 d-block">San jacinto</span><p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Autoservicio la Triologia</p></div>
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4"><span class="h4 text-blue-2 d-block">Maria Labaja</span><p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Deposito el Baratón</p></div>
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4"><span class="h4 text-blue-2 d-block">Carmen de Bolívar</span>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Olímpica</p><br>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Autoservicio El Carmen</p>
						</div>						
						<div class="col-md-6 col-lg-6 col-sm-6 pb-4"><span class="h4 text-blue-2 d-block">Magangué</span>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Autoservicio el Rio</p><br>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Olímpica</p><br>
							<p class="pl-2 pr-2 text-center ml-3 bg-yellow-2 rounded p-1 text-white h5 d-inline-block">Éxito</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0">
				<div class="col-12">
					<span class="d-block h4 text-blue-2">Si deseas acceder a esta línea de crédito:</span>
					<p>Acércarte a los puntos de atención al cliente de los establecimientos en convenio donde se le expedirá una orden de servicio, siempre y cuando su subsidio no se encuentre comprometido con otro servicio de la caja. Como documentos soporte debe presentar su identificación en físico.</p>
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto-credito-social')
				</div>
			</div>
		</div>			
	</div>
</div>