<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5">
					<img src="{{asset('img/credito-cupocredito.jpg')}}" class="img-fluid rounded" alt="crédito social libranza">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">CUPOCRÉDITO</h4>
					<p class="text-justify">Crédito rotativo ofrecido a los trabajadores de las empresas afiliadas a la Caja de Compensación a través de la Tarjeta Comfenalco, donde el afiliado puede realizar sus compras en más de 600 <a href="{{ asset('pdf/ESTABLECIMIENTOS PAGINA.pdf') }}" target="_blank">establecimientos en convenio</a>. Puede cancelar sus extractos, de manera cómoda, en todas las oficinas a nivel nacional del Banco Davivienda, Banco Bogotá y AV Villas, así como en las Supertiendas y Droguerías Olímpica - SAO.</p>
					<span class="d-block h4 text-blue-2 pb-2">Beneficios</span>
					<div class="media">
						<img class="d-flex mr-2" src="{{asset('img/credito-uno.png')}}" alt="credito uno">
						<div class="media-body">
					    	<p>Se manejan dos ciclos de facturación:</p>
					    	<ul class="list-unstyled">
					    		<li><strong class="text-blue-2">·</strong> Ciclo 1 se facturará del 1 al 5 de cada mes.</li>
					    		<li><strong class="text-blue-2">·</strong> Ciclo 2 de facturará del 16 al 20 de cada mes.</li>
					    	</ul>
					  	</div>
					</div>
					<div class="media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/credito-dos.png')}}" alt="credito dos">
						<div class="media-body">
					    	<p class="mb-0">Cuota de manejo de $9,000 mensuales solo si existe saldo.</p>
					  	</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-12">
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/credito-tres.png')}}" alt="credito tres">
						<div class="media-body">
					    	<p class="mb-0">Plazo máximo a diferir las compras hasta 36 meses.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/credito-cuatro.png')}}" alt="credito cuatro">
						<div class="media-body">
					    	<p class="mb-0">Consulta de saldo, cupo disponible, compras realizadas e impresión de extracto a través de la web <a href="http://cupocredito.comfenalco.com:8191/index.php" target="_blank">Click Aquí</a></p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/credito-cinco.png')}}" alt="credito cinco">
						<div class="media-body">
					    	<p class="mb-0">Seguro de Cartera Incluido.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/credito-seis.png')}}" alt="credito seis">
						<div class="media-body">
					    	<p class="mb-0">Tasas de interés según la categoría del afiliado.</p>
					  	</div>
					</div>
					<br>
					<span class="d-block h4 text-blue-2 pb-2">Documentación y Condiciones</span>
					<ul class="list-unstyled">
						<li><strong class="h5 text-blue-2 pr-3">1.</strong>Ser mayor de 21 años.</li>
						<li><strong class="h5 text-blue-2 pr-3">2.</strong>Formulario debidamente diligenciado. <a href="{{ asset('pdf/FORMULARIO_DE_CRÉDITO.pdf') }}" target="_blank"><em>Click Aquí</em></a></li>
						<li><strong class="h5 text-blue-2 pr-3">3.</strong>Fotocopia ampliada a 150%.</li>
						<li><strong class="h5 text-blue-2 pr-3">4.</strong>Mínimo 2 meses de labores en la empresa.</li>
						<li><strong class="h5 text-blue-2 pr-3">5.</strong>No tener reportes negativos en centrales de riesgo.</li>
					</ul>
					<p class="text-justify">Para diligenciar el formulario y Asignar la clave de su Tarjeta Comfenalco, usted debe acercarse a cualquiera de nuestros <a class="font-weight-bold" href="{{ route('contacto') }}">CENTROS INTEGRALES DE SERVICIO (CIS)</a>, o también puede hacerlo en los <a class="font-weight-bold" href="{{ route('contacto') }}">PUNTOS FIJOS DE ATENCIÓN</a>.</p>
					
					<span class="d-block h4 text-blue-2 pb-2">Cupocuota</span>

					<p class="text-justify">Cupo de crédito otorgado al afiliado beneficiario de la Cuota Monetaria que reconoce la Caja mensualmente. El medio de pago es el valor del subsidio que recibe por su grupo familiar activo siempre y cuando no lo tenga comprometido con otro servicio, es otorgado a través de la tarjeta Comfenalco y puede ser utilizado en cualquiera de los establecimientos en convenio:</p>
					<ul class="list-unstyled pb-2">
						<li><span class="text-blue-2 pr-2">•</span>Ser mayor de 21 años.</li>
						<li><span class="text-blue-2 pr-2">•</span>No debe tener comprometida la Cuota Monetaria con algún servicio de la Caja, y debe estar activo al momento de la solicitud.</li>
						<li><span class="text-blue-2 pr-2">•</span>Sin consulta ante Centrales de Riesgo (Datacrédito). Tener más de 2 personas en el grupo familiar beneficiarias de cuota monetaria.</li>
					</ul>
					<span class="d-block h4 text-blue-2 pb-2">Documentación y Condiciones</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="check-yellow">
						<div class="media-body">
					    	<p class="mb-0">Formulario de solicitud diligenciado. <a href="{{ asset('pdf/FORMULARIO_DE_CRÉDITO.pdf') }}" target="_blank"><em>Click Aquí</em></a></p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="check-yellow">
						<div class="media-body">
					    	<p class="mb-0">Copia de Cédula ampliada 150%.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="check-yellow">
						<div class="media-body">
					    	<p class="mb-0">Mínimo 2 meses de labores en la empresa.</p>
					  	</div>
					</div>
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto-credito-social')
				</div>
			</div>
		</div>			
	</div>
</div>