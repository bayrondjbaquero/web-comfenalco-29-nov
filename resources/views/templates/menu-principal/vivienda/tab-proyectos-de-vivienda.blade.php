<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/vivienda-comfenalco.jpg')}}" class="img-fluid rounded" alt="Proyectos de vivienda comfenalco">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">PROYECTOS  DE VIVIENDA</h4>
					<p class="text-justify">Comfenalco Cartagena realizando las veces de corredor e intermediario encargado de facilitar la obtención de vivienda, ha realizado convenios con diferentes constructoras de la ciudad para apoyar en la adquisición de un predio que sea satisfactorio para las familias afiliadas y con facilidades de financiación.</p>
					<p class="text-justify">Entre los proyectos que tenemos actualmente están los que pueden ser adquiridos con Subsidio de Vivienda y los que no tienen esa condición.</p>
				</div>
			</div>
			<div id="vivienda-styles" class="row m-0 pt-2">
				<div class="col-12">
					<div class="bg-white afiliaciones-doc rounded row ml-0 mr-0 pb-3">
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Las imágenes o fotografías son una representación de los espacios y no refleja estrictamente los diseños y acabados del proyecto.</p>
							  	</div>
							</div>
						</div>												
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Los valores, áreas y especificaciones pueden variar sin previo aviso.</p>
							  	</div>
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Aplican condiciones y restricciones de cada constructora y de acuerdo a cada proyecto.</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Comfenalco no asume ninguna responsabilidad derivada de la construcción, calidad, venta y de cualquier acción u omisión que se genere en el proyecto. Nuestra labor es únicamente de carácter informativo.</p>
							  	</div>
							</div>
						</div>
					</div>
					<br><br>
				</div>
				<div class="col-md-12 pb-4">
					<span class="h4 d-block text-blue-2 pb-2">Realiza tu búsqueda aquí</span>
					<form id="search_proyects" class="text-center" method="POST" action="#">
						{{ csrf_field() }}
						<select class="border-0 pl-2 mb-1" name="type" id="tipoForm">
							<option value="">¿Qué buscas?</option>
                            <option value="casa">Casa</option>
                            <option value="apartamento">Apartamento</option>
						</select>
						{{-- <select class="border-0 pl-2 mb-1" name="condition" id="condicionForm">
							<option value="">¿De qué tipo?</option>
                            <option value="arriendo">Arriendo</option>
                            <option value="venta">Venta</option>
						</select> --}}
						<select class="border-0 pl-2 mb-1" name="city" id="ubicacionForm">
							<option value="">¿En dónde?</option>
                            <option value="turbaco">Turbaco</option>
                            <option value="cartagena">Cartagena</option>
						</select>
						<button class="border-0 pl-0 pr-0 mb-1" type="button" onclick="search_proyects()">Buscar <img src="{{ asset('img/search.png') }}" alt=""></button>
					</form>
                    <br>
                    <div id="cargar_proyectos"></div>
				</div>
			</div>
			<div class="col-11 mx-auto">
				<br>
				@include('templates.menu-principal.formulario-contacto.contacto')
			</div>
		</div>			
	</div>
</div>


