<div class="row m-0">
    @foreach($proyects as $proyect)
    <div class="col-md-6">
        <div class="panel panel-default mb-4">
            <div class="panel-body">
                <div class="media bg-marron rounded d-flex flex-wrap">
                    <div class="media-left pr-0 pr-lg-3 pr-xl-3 d-block align-top">
                        <a class="d-block" href="#">
                        @if($proyect->thumbnail != null)
                            <img src="{{ asset('proyect_images/'.$proyect->thumbnail) }}" width="182" height="182" alt="{{$proyect->alt}}" class="img-fluid media-object">
                        @else
                            <img src="{{ asset('proyect_images/proyecto-comfenalco.jpg') }}" width="182" height="182" alt="{{$proyect->alt}}" class="img-fluid media-object">
                        @endif
                        </a>
                    </div>
                    <div class="media-body">
                        <div class="info-proyecto">
                            <p class="mb-1"><strong>{{$proyect->title}}</strong></p>
                            <p>Tipo: {{ucfirst($proyect->type)}}</p>
                            <p>Condición: {{ucfirst($proyect->condition)}}</p>
                            <p>Ciudad: {{ucfirst($proyect->city)}}</p>
                            <p>Barrio: {{ucfirst($proyect->neighborhood)}}</p>
                        </div>
                        <a class="d-inline-block bg-yellow-2 p-1 rounded text-white show_proyect" href="#" onclick="hola('{{$proyect->slug}}')">Ver más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
    <nav id="page-paginate" class="col-md-12" aria-label="Page navigation example">
     {!! $proyects->render() !!}
    </nav>
</div>
<script>
    $('#cargar_proyectos #page-paginate .pagination').addClass('justify-content-center');
    $('#cargar_proyectos #page-paginate li').addClass('page-item');
    $('#cargar_proyectos #page-paginate li a').addClass('page-link');
    $('#cargar_proyectos #page-paginate li span').addClass('page-link');
</script>
