<div class="col-md-6">
	<h4 class="text-capitalize text-blue-2 pb-2">{{strtoupper($proyect->title)}}</h4>
	{!! $proyect->description !!}
</div>
<div class="col-md-6 text-center d-flex align-items-center pb-3 pl-5 pr-5">
	<div id="{{"carousel".$proyect->slug}}" class="carousel slide mx-auto" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
        	@forelse($galleries as $gallery)
            <div class="carousel-item @if($loop->first) active @endif">
                <img class="d-block img-fluid rounded no-rounded-1110 mx-auto" src="{{asset('proyect_images/'.$gallery->image)}}" width="1110" height="331" alt="{{$gallery->alt}}">
            </div>
            @empty
            @if($proyect->thumbnail != null)
            <img class="d-block img-fluid rounded no-rounded-1110 mx-auto" src="{{ asset('proyect_images/'.$proyect->thumbnail) }}" alt="{{$proyect->alt}}">
            @else
            <img class="d-block img-fluid rounded no-rounded-1110 mx-auto" src="{{ asset('proyect_images/proyecto-comfenalco.jpg') }}" alt="{{$proyect->alt}}">
            @endif
            @endforelse
        </div>
        <a class="carousel-control-prev" href="{{"#carousel".$proyect->slug}}" role="button" data-slide="prev">
            <span class="arrow-left">
                <img src="{{asset('img/arrow-left.png')}}" alt="">
            </span>
        </a>
        <a class="carousel-control-next" href="{{"#carousel".$proyect->slug}}" role="button" data-slide="next">
            <span class="arrow-right">
                <img src="{{asset('img/arrow-right.png')}}" alt="">
            </span>
        </a>
    </div>
</div>