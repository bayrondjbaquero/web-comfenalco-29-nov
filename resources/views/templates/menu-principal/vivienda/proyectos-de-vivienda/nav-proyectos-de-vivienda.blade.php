<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-3 justify-content-md-center">
			<div class="row m-0" id="top_vivienda">
				<div class="col-md-12 pb-3">
				    <div class="float-right"><a class="nav_proyectos_back link-hover" href="#"><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A PROYECTOS DE VIVIENDA</a></div>
				</div>
			</div>
			<div id="cargar_proyecto_slug" class="row m-0 pb-4"></div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>







