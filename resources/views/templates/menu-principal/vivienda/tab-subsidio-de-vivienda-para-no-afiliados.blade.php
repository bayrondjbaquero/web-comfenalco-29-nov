	<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/vivienda-subsidio.jpg')}}" class="img-fluid rounded" alt="Vivienda subsidio para no afiliados">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">SUBSIDIO DE VIVIENDA PARA  NO AFILIADOS</h4>
					<p class="text-justify">COMFENALCO Cartagena apoya el trámite para la obtención del subsidio familiar de vivienda a las familias que no cuenten con afiliación a Cajas de Compensación Familiar a través del contrato de encargo de gestión que firmo el Ministerio de Vivienda -Fonvivienda con las cajas de compensación para administrar los subsidios otorgados por el Gobierno Nacional.</p>
					<p class="text-justify">El Gobierno Nacional realiza la convocatoria y asignación de subsidios de vivienda de interés social de acuerdo al presupuesto y cronograma establecido anualmente.</p>
				</div>
				<div class="col-md-12">
					<span class="text-blue-2 d-block h4 pb-2">Convocatoria</span>
					<p class="text-justify">Las Cajas de Compensación realizan la divulgación de la convocatoria y orientación desde la postulación hasta los trámites para hacer efectivo el pago de los subsidios de vivienda. Las Cajas de Compensación Familiar deben realizan los procesos de divulgación, información, recepción de solicitudes, certificación y revisión de la información, digitación, seguimiento y verificación de los documentos para hacer efectivo el pago de los subsidios familiares de vivienda en todas sus modalidades.</p>
					<p class="text-justify pb-2">Los formularios de solicitud del subsidio sólo se entregarán por parte de las Cajas de Compensación Familiar cuando el Gobierno Nacional determine una fecha de apertura de la respectiva convocatoria.</p>
					<a target="_blank" href="{{ asset('pdf/Guia de Postulacion Subsidios de Vivienda Para No Afiliados.pdf') }}" class="d-inline-block mb-2 bg-yellow-2 text-white rounded pt-2 pb-2 pl-4 pr-4">Descargue Aquí Guía de Postulación Subsidios de Vivienda Para No Afiliados.</a>
					<a target="_blank" href="{{ asset('pdf/documentos-postulacion-subsidio-de-viviendas-no-afiliados.zip') }}" class="d-inline-block bg-yellow-2 text-white rounded pt-2 pb-2 pl-4 pr-4">Descarga aquí los documentos para la postulación al subsidio de vivienda para No Afiliados</a>
					<br><br>
					<span class="d-block text-blue-2 pb-2 h4">Desembolso subsidio vivienda No Afiliados</span>
					<p class="text-justify">Para el trámite del cobro del subsidio de vivienda para No afiliados debe seguir los siguientes pasos que se encuentran en la Guía Cobro de Subsidios de Vivienda No Afiliados.</p>
					<a target="_blank" href="{{ asset('pdf/Guia cobro de subsidios familiar de vivienda No Afiliados (2).pdf') }}" class="d-inline-block bg-yellow-2 text-white rounded pt-2 pb-2 pl-4 pr-4">Descargue Aquí Guía de Desembolso de subsidio de vivienda no afiliados.</a>
					<br><br>
					<p class="text-justify pb-2">Si usted va aplicar el subsidio de vivienda en la modalidad de Vivienda usada debe radicar en cualquiera de los Centros Integrales de Servicio (CIS) Comfenalco en Matuna, Mamonal Bosque ó Ejecutivos los siguientes documentos con el fin de que La Caja verifique si la vivienda es apta para aplicar el subsidio:</p>
					<p><strong class="text-orange">•</strong> Copia de la carta de asignación del Subsidio Familiar de vivienda</p>

					<p><strong class="text-orange">•</strong> Copia escritura pública del dueño actual</p>

					<p><strong class="text-orange">•</strong> Original del certificado de tradición y libertad no mayor a 30 días</p>

					<p><strong class="text-orange">•</strong> Certificado de planeación donde diga que la vivienda está:</p>

					<p><strong class="text-blue-2">o</strong> En una zona urbana.</p>
					<p><strong class="text-blue-2">o</strong> Que no está en zona de alto riesgo.</p>
					<p><strong class="text-blue-2">o</strong> Que cuenta con disponibilidad de servicios públicos domiciliarios de acueducto, gas, alcantarillado y energía eléctrica y vías de acceso y que indique que la vivienda cumple con los requisitos mínimos para ser habitada por el hogar beneficiario.</p>
					<p><strong class="text-blue-2">o</strong> Si no tiene disponibilidad de servicios públicos se debe colocar la alternativa que se está utilizando para obtener el servicio.</p>
					<p><strong class="text-orange">•</strong> Desprendible de la apertura de la cuenta CAP-Cuenta de ahorro programado</p>
					<p>Posteriormente Comfenalco le entregará comunicación escrita autorizando la aplicación del subsidio en la vivienda en caso de que cumpla los requisitos, si no cumple se le detalla las inconsistencias para que proceda a subsanarlas de ser posible</p>
				</div>
			</div>
		</div>	
		<div class="col-11 mx-auto">
			<br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>
	</div>
</div>