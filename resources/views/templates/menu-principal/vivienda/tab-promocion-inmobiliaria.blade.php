<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/promocion inmobiliaria.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">PROMOCIÓN INMOBILIARIA</h4>
					<p class="text-justify">Comfenalco Cartagena a través de la unidad de vivienda se complace en poner a su disposición su servicio de promoción inmobiliaria Comfenalco a través del cual podrá utilizar diferentes medios de comunicación para la comercialización de su proyecto entre los afiliados y comunidad en general.</p>
					<p class="text-justify">El objetivo es brindar un espacio a los promotores inmobiliarios para facilitar la comercialización de sus proyectos, acercarse a sus clientes actuales y potenciales con el ánimo de seguir apoyando e impulsando el sector inmobiliario y de la construcción de Bolívar.</p>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-12">
					<span class="d-block h4 text-blue-2 pb-2">Publicidad:</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Mailing a la base de datos  de afiliados. Redes sociales, Fan page, Twitter, instagram, facebook. Difusión en redes sociales una vez a la semana por tres meses.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Banner por tres meses rotativo en el link de vivienda.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Volanteo en el CIC - Centro Integral de Servicios Comfenalco La Matuna durante un día.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Espacio por 30 días en el CIS - Centro Integral de Servicios Comfenalco Los Ejecutivos, se podría modificar 15 días en Lobby del Centro Empresarial Comfenalco Mamonal y 15 días en CIS Ejecutivos o el tiempo que prefiera, siempre y cuando sea en total 30 días.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Ubicación por un mes, todos los domingos en el Centro Recreacional Takurika.</p>
					  	</div>
					</div>
					<br>
					<a class="d-table pl-4 ml-2" href="#"><img class="img-fluid" src="{{ asset('img/vivienda-tarifa-promocion.png') }}" alt=""></a>
					<br>
					<span class="d-block h4 text-blue-2 pb-2">FORMA DE PAGO</span>
					<p class="text-justify">Transferencia electrónica de cuenta de ahorros Davivienda No. 056670167891</p>
					<br>
					<span class="d-block h4 text-blue-2 pb-2">DOCUMENTACIÓN REQUERIDA PARA INSCRIPCIÓN</span>
					<p class="text-justify">Enviar la siguiente documentación a la siguiente dirección de correo: <a href="mailto:bzuniga@comfenalco.com">bzuniga@comfenalco.com</a></p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Copia soporte de pago (volante de consignación o pantallazo de transferencia electrónica)</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Formato de vinculación diligenciado</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Certificación de existencia y representación Legal</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Copia del RUT</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Fotocopia de la cédula del representante legal</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Referencias comerciales </p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Referencias bancarias</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Referencias de experiencia en otros proyectos</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Brochure del proyecto con valores</p>
					<p class="mb-0"><strong class="text-orange pr-2">•</strong> Licencia de construcción</p>
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>





