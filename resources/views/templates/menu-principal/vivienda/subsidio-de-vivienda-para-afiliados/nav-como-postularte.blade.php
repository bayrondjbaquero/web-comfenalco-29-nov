<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6">
					<h4 class="text-blue-2 pb-2">Es muy fácil postularte aquí te regalamos los pasos para que puedas hacerlo:</h4><br>
					<div class="media pb-3 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/uno-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Consulta aquí las fechas de postulación al subsidio de vivienda año 2017. – <a target="_blank" href="{{ asset('pdf/vivienda-calendario-vigencia.jpg') }}">Se pone el calendario vigente</a>.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/dos-purple.png')}}" alt="">
						<div class="media-body">
					    	<p class="text-justify mb-0"><a target="_blank" href="{{ asset('pdf/FORMULARIO_VIVIENDA_.pdf') }}">Descarga tu formulario aquí</a> o solicítalo en los <a href="{{ route('contacto') }}">puntos de atención</a>. Diligéncialo sin tachones ni enmendaduras. <a target="_blank" href="{{ asset('pdf/GUIA DILIGENCIAMIENTO-DEL-FORMULARIO-FOVIS.pdf') }}">Descargue aquí su Guía de diligenciamiento.</a></p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/tres-yellow.png')}}" alt="">
						<div class="media-body">
							<p class="mb-0">Verifica si cumples con los siguientes requisitos:.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/como-postularte.jpg')}}" class="img-fluid rounded" alt="Como postularte">
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-12">
					<div class="d-block ml-3 pl-4">
				    	<p class="text-justify"><strong class="text-orange">•</strong> Estar afiliado a COMFENALCO CARTAGENA por lo menos uno de los integrantes del grupo familiar a postularse.</p>
						<p class="text-justify"><strong class="text-orange">•</strong> Un hogar conformado por los cónyuges, las uniones maritales de hecho, incluyendo las parejas del mismo sexo, y/o el grupo de personas unidas por vínculos de parentesco hasta tercer grado de consanguinidad, segundo de afinidad y primero civil, que compartan un mismo espacio habitacional.</p>
						<p class="text-justify"><strong class="text-orange">•</strong> Ahorro:  Tener un ahorro en cesantías, ahorro programado y/o abonos a constructoras de proyectos de vivienda de interés social.</p>
						<p class="text-justify"><strong class="text-orange">•</strong> Si te vas a postular al subsidio para vivienda nueva, no puedes tener vivienda propia.</p>
						<p class="text-justify"><strong class="text-orange">•</strong> No haber recibido antes subsidio familiar de vivienda.</p>
						<p class="text-justify"><strong class="text-orange">•</strong> Tener financiación complementaria (Capacidad de crédito o crédito aprobado expedido por entidades financieras) para completar el valor de la vivienda.</p>
						<p class="text-justify"><strong class="text-orange">•</strong> Uno de los integrantes del grupo familiar debe ser el propietario del lote o terraza legalizados, cuando se trate de construcción en sitio propio, o de la vivienda a la cual se le aplicará el mejoramiento cuando se trate de mejoramiento de vivienda.</p>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/cuatro-blue.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Entrega el formulario firmado con los documentos solicitados en los <a href="{{ route('contacto') }}">puntos de atención</a>: Centros Integrales de Servicio (CIS) en Cartagena Comfenalco centro de atención CEDESARROLLO, Matuna, Bocagrande, Mamonal, Bosque y Ejecutivos, también puedes entregar en las regionales ubicadas en Turbaco, Magangué, y Carmen de Bolívar. Recuerda reclamar el desprendible del formulario firmado.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/cinco-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Espera las fechas de asignación y si sales asignado, Comfenalco te contactará para citarte a una reunión donde se hará entrega de tu carta de asignación al subsidio.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/seis-rosa.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Si llegada la fecha de asignación del subsidio no te hemos contactado debes acercarte a cualquiera de los CIS Comfenalco donde estarán publicados los listados de las personas que quedan en estado Calificado o Negados.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/siete-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Si transcurre el tiempo de vencimiento o de aplicación del subsidio y no has encontrado un proyecto de vivienda, debes renunciar por escrito al subsidio de vivienda y postularte para una próxima asignación.</p>
					  	</div>
					</div>
					<br>
					<p class="text-blue-2 text-justify"><em>Nota: El estado Calificado es cuando el postulante ha cumplido con todos los requisitos para postularte pero el puntaje arrojado en la postulación no alcanzó para llegar al tope de recursos que tiene la Caja para asignar, la persona puede seguir postulándose en las siguientes convocatorias programadas por la Caja de Compensación. El estado Negado es cuando la persona no cumplió alguno de los requisitos exigidos en la postulación, en ese caso el postulante debe acercarse a nuestros CIS Comfenalco, consultar la causal de negación y si es posible subsanar dicha causal para continuar con su proceso de postulación en otras próximas convocatorias.</em></p>
					<br>
					<p class="h5 bg-marron text-white text-center pt-3 pb-3 pl-5 pr-5 rounded"><strong>Tenga en cuenta que no se puede firmar escritura antes de la adjudicacion del subsidio.</strong></p>
					<br><br>
					<span class="pb-2 d-block h4 text-blue-2">¿Quiénes no pueden recibirlo?</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/x-red.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Quienes hayan sido beneficiarios de una vivienda o de un crédito otorgado por el Instituto de Crédito Territorial.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/x-red.png')}}" alt="">
						<div class="media-body">
					    	<p class="text-justify mb-0">Quienes como beneficiarios hayan recibido subsidios familiares de vivienda, o quienes siendo favorecidos con la asignación no hubieren presentado antes del vencimiento del subsidio su renuncia a la utilización. Lo anterior cobija los subsidios otorgados por FONVIVIENDA, INURBE, CAJA AGRARIA, BANCO AGRARIO, FOREC, FOCAFE, las Cajas de Compensación Familiar o cualquier entidad pública de cualquier orden en casos de calamidades naturales.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/x-red.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Quienes hubieren presentado información que no corresponda a la verdad en cualquiera de los procesos de acceso al subsidio.</p>
					  	</div>
					</div>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/x-red.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Los afiliados a la Caja Promotora de Vivienda Militar.</p>
					  	</div>
					</div>
					<br><br>
					<span class="d-block text-blue-2 pb-2 h4">¿Qué documentos debo anexar al formulario?</span>
					<p class="text-blue-2 pb-2"><em>Nota: Todos los documentos deben ser originales y vigentes en el periodo de postulación</em></p>
					<p class="text-justify"><strong class="text-green">•</strong> Fotocopia de la cédula de ciudadanía (mayores de edad)</p>
					<p class="text-justify"><strong class="text-green">•</strong> Certificado laboral vigente</p>
					<p class="text-justify"><strong class="text-green">•</strong> Certificado de ahorro programado</p>
					<p class="text-justify"><strong class="text-green">•</strong> Certificado de Cesantías Inmovilizadas</p>
					<p class="text-justify"><strong class="text-green">•</strong> Certificado de capacidad de crédito o crédito aprobado</p>
					<p class="text-justify"><strong class="text-green">•</strong> Registro civil de matrimonio o Declaración juramentada para los que conviven en unión libre, el cual puedes <a target="_blank" href="pdf/Formato_Declaracion_Juramentada_De_Dependencia_Economica.pdf">descargar aquí</a>.</p>
					<p class="text-justify"><strong class="text-green">•</strong> Registro civil de nacimiento para los menores.</p>
					<p class="text-justify"><strong class="text-green">•</strong> Declaración ante notario que acredite la condición de mujer u hombre cabeza de hogar(la condición de hombre cabeza de hogar aplica si tiene hijos discapacitados que estén a su cuidado y si la conyugue o compañera se encuentra discapacitada física, mentalmente o moralmente), cuando fuere del caso, de conformidad con lo establecido en el ordenamiento jurídico vigente. <a target="_blank" href="{{ asset('pdf/FORMATO DECLARACIONES POSTULACION AL SUBSIDIO DE VIVIENDA.pdf') }}">Descárguelo aquí</a>.</p>
					<p class="text-justify"><strong class="text-green">•</strong> Certificado médico que acredite la discapacidad física o mental de alguno de los miembros  del hogar.</p>
					<p class="text-justify"><strong class="text-green">•</strong> Certificado de la constructora por el valor pagado, firmado por el representante legal y el revisor fiscal, anexando copia de las consignaciones y/o recibos de caja (solo aplica cuando el ahorro es en cuotas iniciales).</p>
					<br>
					<span class="h4 text-blue-2 pb-3 d-block">Documentos adicionales a presentar para la modalidad de Mejoramiento de Vivienda o Construcción en sitio propio:</span>
					<p class="text-justify"><strong class="text-orange">•</strong> Certificado de Tradición y Libertad donde conste la propiedad de la vivienda o lote (en cabeza de cualquiera de los postulantes)(No mayor a 30 días).</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Escritura pública donde conste la propiedad de la vivienda o lote (en cabeza de cualquiera de los postulantes).</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Certificado de Planeación Municipal o de quien haga las veces donde diga: Que la vivienda o lote está en una zona urbana, -Que no está en zona de alto riego-Que cuenta con disponibilidad de servicios públicos domiciliarios de acueducto, gas, alcantarillado, energía eléctrica y vías de acceso que la vivienda cumple con los requisitos mínimos para ser habitada por el hogar beneficiario.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Recibo de impuesto predial (Que incluya avalúo catastral, nombre del propietario y estar al día).</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Presupuesto de obra de lo que va a mejorar o construir.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Certificado de existencia de la vivienda o lote (Fotos Ing. asesor externo Comfenalco).</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Licencia de construcción de lo que va a mejorar o construir en aquellos municipios con categoría especial, 1 o 2 caso de Cartagena.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Certificado de elegibilidad del proyecto si es en municipios con categorías, 3,4, 5 y 6.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> La propiedad a construir o mejorar debe ser la única propiedad que posea el hogar postulante.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> La propiedad no pueden tener hipoteca ni embargo, debe estar libre de todo gravamen salvo la hipoteca constituida a favor de la entidad que financiará su ejecución.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> El lote o vivienda debe estar en un desarrollo legal, su título de propiedad debe encontrarse inscrito en la Oficina de Registro de Instrumentos Públicos a nombre de uno cualquiera de los miembros del hogar postulante, quienes deben habitar en la vivienda.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> El subsidio no se gira al beneficiario sino al Ingeniero una vez se realice la mejora o construcción verificada contra </p>escritura registrada ante la oficina de instrumentos públicos.
					<p class="text-justify"><strong class="text-orange">•</strong> Para desarrollar cualquier obra de construcción, ampliación, demolición o remodelación dentro del territorio Nacional, se requiere tener permiso o licencia de construcción expedido por las respectivas administraciones municipales. (Ley No.9 de 1989). Este trámite se realiza ante una entidad denominada CURADURIA URBANA.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Tramite la licencia de construcción una vez usted haya sido asignado.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> No necesitan solicitar una licencia para las reparaciones locativas que son obras que tienen como finalidad mantener el inmueble en las debidas condiciones de higiene y ornato sin afectar su estructura, ni su distribución interna, ni sus características funcionales y/o volumétricas (Ver Artículo 8 de la Ley 810 de 2003.</p>
					<p class="text-justify"><strong class="text-orange">•</strong> Usted debe cancelar lo estipulado para la Visita Técnica con el fin de que se le expida el acta de existencia de la vivienda o el lote, solicite el recibo de consignación en los CIS de Comfenalco.</p>
					<br>
					<span class="h4 text-blue-2 d-block pb-3">RECUERDE:</span>
					<p class="text-justify"><strong class="text-blue-2">•</strong> Para la postulación al Subsidio de vivienda no necesita intermediarios, hágalo directamente en COMFENALCO CARTAGENA.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> Los subsidios de vivienda se desembolsan al constructor de la vivienda, no al beneficiario del subsidio.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> Los subsidios de vivienda tienen una vigencia, es decir, usted deberá aplicar el subsidio dentro de dicha vigencia, de lo contrario lo perderá.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> Los proyectos de vivienda promocionados por Comfenalco no son los únicos proyectos en los que se puede aplicar el subsidio de vivienda.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> Lo importante en la escogencia de la vivienda, es que el proyecto esté debidamente elegible y registrado ante el Ministerio de Ambiente, Vivienda y Desarrollo territorial.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> El valor del subsidio es un aporte o complemento para la adquisición de su solución de vivienda. Si el valor de la vivienda es mayor al valor del subsidio, deberá completar el costo de la vivienda con sus propios recursos que pueden estar representados en ahorros, créditos u otros.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> La Ley 1537 de junio de 2012, contempla la restricción de no poder vender, ni arrendar la vivienda con subsidio en un periodo de 10 años, contados a partir del registro de la escritura en la oficina de Instrumentos Públicos.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> No incluir personas propietarias de vivienda o beneficiadas con el S.F.V.Los datos contenidos en el formulario se verifican y en caso de falsedad, se aplican las sanciones contempladas en la Ley 3 de 1991: "La persona que presente documentos o información falsos, con el objeto de que le sea adjudicado un subsidio familiar de vivienda, quedará inhabilitada por el término de diez (10) años para volver a solicitarlo".</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> No se recibirán formularios sin los documentos solicitados.</p>
					<p class="text-justify"><strong class="text-blue-2">•</strong> Los subsidios de vivienda se obtienen por calificación: se califican los ingresos del hogar, monto del ahorro, antigüedad de la cuenta programada o cesantías inmovilizadas, avalúo del lote de terreno o vivienda a mejorar, condiciones especiales de los miembros del hogar, tales como certificación por discapacidad física o mental, mayores de 65 años y mujer u hombre cabeza de familia, Número de veces que el hogar postulante ha participado en el proceso de asignación del subsidio sin haber resultado beneficiario, cumpliendo con todos los requisitos para la calificación.</p>
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>