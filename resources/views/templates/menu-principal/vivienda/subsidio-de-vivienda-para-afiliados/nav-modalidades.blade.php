<div id="nav-dropdown2" class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 m-auto pt-4 justify-content-md-center">
			<h3 class="text-blue-2 pb-3 pt-1">Modalidades</h3>
			<ul class="nav nav-pills pb-4" id="pills-tab" role="tablist">
				<li class="nav-item col">
					<a class="nav-link active d-flex justify-content-around align-items-center flex-column bg-marron rounded" id="pills-nueva-tab" data-toggle="pill" href="#pills-nueva" role="tab" aria-controls="pills-nueva" aria-expanded="true">
						<img class="img-fluid" src="{{asset('img/vivienda-nueva.png')}}" alt="">
						<span class="text-white text-center">VIVIENDA NUEVA</span>
					</a>
				</li>
				<li class="nav-item col">
					<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-marron rounded p-3" id="pills-mejoramiento-tab" data-toggle="pill" href="#pills-mejoramiento" role="tab" aria-controls="pills-mejoramiento" aria-expanded="true">
						<img class="img-fluid" src="{{asset('img/vivienda-mejoramiento-de-vivienda.png')}}" alt="">
						<span class="text-white text-center">MEJORAMIENTO DE VIVIENDA</span>
					</a>
				</li>
				<li class="nav-item col">
					<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-marron rounded p-3" id="pills-construccion-tab" data-toggle="pill" href="#pills-construccion" role="tab" aria-controls="pills-construccion" aria-expanded="true">
						<img class="img-fluid" src="{{asset('img/vivienda-construccion-en-sitio-propia.png')}}" alt="">
						<span class="text-white text-center">CONSTRUCCIÓN EN SITIO PROPIO</span>
					</a>
				</li>
			</ul>
			<div class="tab-content pt-3" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-nueva" role="tabpanel" aria-labelledby="pills-nueva-tab">
					<span class="h3 text-blue-2 d-block pb-2">Vivienda nueva</span>
					<p>Subsidio para adquirir vivienda en cualquier municipio del País, en un proyecto elegible para Vivienda de Interés Social (VIS) que no supere los 135 SMLMV, o en proyecto elegible para Vivienda de Interés Prioritaria (VIP), que no supere los 70 SMLMV.</p>
				</div>
				<div class="tab-pane fade" id="pills-mejoramiento" role="tabpanel" aria-labelledby="pills-mejoramiento-tab">
					<span class="h3 text-blue-2 d-block pb-2">Mejoramiento de Vivienda</span>
					<p class="text-justify pb-2">Modalidad por la cual el beneficiario del subsidio supera una o varias de las carencias básicas de la vivienda. La vivienda a mejorar debe presentar al menos una de las siguientes situaciones:</p>
					<br>
					<div class="bg-white afiliaciones-doc rounded row ml-0 mr-0 pb-3">
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Deficiencias en la estructura principal, cimientos, muros o cubierta.</p>
							  	</div>
							</div>
						</div>												
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Carencia o vetustez de redes secundarias y acometidas domiciliarias de acueducto y alcantarillado.</p>
							  	</div>
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Construcción en materiales provisionales tales como latas, tela asfáltica y madera de desecho.</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Carencia o vetustez de baños y/o cocina.</p>
							  	</div>
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-lg-12 col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Existencia de pisos en tierra o en materiales inapropiados.</p>
							  	</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-6 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Existencia de hacinamiento crítico, cuando el hogar habita en una vivienda con más de tres personas por cuarto, incluyendo sala, comedor y dormitorios.</p>
							  	</div>
							</div>
						</div>
					</div>
					<br>
					<p class="text-blue-2"><em>Nota: En aquellos casos en que la totalidad de la vivienda se encuentre construida en materiales provisionales, se considerará objeto de un programa de construcción en sitio propio.</em></p>
				</div>
				<div class="tab-pane fade" id="pills-construccion" role="tabpanel" aria-labelledby="pills-construccion-tab">
					<span class="h3 text-blue-2 d-block pb-2">Construcción en sitio propio</span>
					<p>Modalidad en la cual el beneficiario del subsidio accede a una vivienda de interés social, mediante la edificación de la misma en un lote de su propiedad que puede ser un lote de terreno, una terraza o una cubierta de losa.</p>
				</div>
			</div>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>
	</div>
</div>