<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6">
					<h3 class="text-blue-2 pb-2 text-uppercase">Generalidades</h3>
					<p class="pb-2">Los valores del subsidio están sujetos a los ingresos del hogar.</p>
					<a class="rounded d-inline-block pl-4 pr-4 pt-2 pb-2 bg-yellow-2 text-white" target="_blank" href="{{ asset('pdf/TABLA VALORES SUBSIDIO DE VIVIENDA 2017.pdf') }}">Descargue aquí valores subsidio de vivienda</a>
					<span class="d-block pt-2 text-blue-2 h4 pb-2">Vigencia del  Subsidio de Vivienda</span>
					<p class="text-justify">Los subsidios de vivienda de interés social asignados por las Cajas de Compensación Familiar tendrán una vigencia de doce (12) meses calendario, contados desde el primer día del mes siguiente a la fecha de la publicación de su asignación.</p>
					<p class="text-justify pb-2">Articulo 51 Decreto 2190 de 2009: Parágrafo 4: Las Cajas de Compensación Familiar podrán prorrogar, mediante acuerdo expedido por su respectivo Consejo Directivo, la vigencia de los subsidios familiares de vivienda asignados a sus afiliados por un plazo no superior a doce (12) meses, prorrogable máximo por doce (12) meses más.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/como-postularte.jpg')}}" class="img-fluid rounded" alt="Como postularste">
				</div>
				<div class="col-md-12">
					{{-- <span class="d-block text-blue-2 h4 pb-2">Postulantes</span>
					<p class="text-justify pb-2">Los inscritos en el Registro de Postulantes, que no fueren beneficiarios en una asignación de subsidios, podrán continuar como postulantes hábiles para las asignaciones de la totalidad del año calendario.<br>Si no fueren beneficiarios en las demás asignaciones de dicho año, para continuar siendo postulantes en las asignaciones del año siguiente deberán manifestar tal interés, solicitando el formato de continuidad en la postulación en los CIS de Comfenalco.</p> --}}
					<span class="d-block text-blue-2 h4 pb-2">Desembolso del subsidio</span>
					<p class="text-justify pb-2">El constructor u oferente debe el iniciar el proceso de solicitud de desembolso radicando los documentos requeridos ante la Caja de Compensación Comfenalco Cartagena en cualquiera de los Centros Integrales de Servicios (CIS).</p>
					<a class="rounded d-inline-block pl-4 pr-4 pt-2 pb-2 bg-yellow-2 text-white" target="_blank" href="{{ asset('pdf/INSTRUCTIVO_DESEMBOLSO_SUBSIDIOS_DE_VIVIENDA.pdf') }}">Descargue Aquí Instructivo Para El Desembolso De Los Subsidios De Vivienda-Afiliados</a>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>