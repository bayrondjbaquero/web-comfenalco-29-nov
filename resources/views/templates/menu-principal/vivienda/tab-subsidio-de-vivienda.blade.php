<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/vivienda-subsidio.jpg')}}" class="img-fluid rounded" alt="Vivienda imagen">
				</div>
				<div class="col-md-7">
					<h4 class="text-blue-2 pb-2">SUBSIDIO DE VIVIENDA</h4>
					<p class="text-justify">El subsidio de vivienda es un aporte en dinero que se otorga al beneficiario por una sola vez   y que constituye un complemento al esfuerzo del ahorro para facilitar la adquisición, mejora  o construcción de una solución de vivienda.</p>
					<p class="text-justify">El subsidio de vivienda no es un préstamo ni un crédito, es un aporte que les facilita a los hogares mejorar su calidad de vida.</p>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>





