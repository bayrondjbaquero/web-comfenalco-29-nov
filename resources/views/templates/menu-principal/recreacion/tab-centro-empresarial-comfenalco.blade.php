<div class="container-fluid bg-gray-2 pt-2">
	<div class="row pb-3">
		<div class="col-md-12 p-0">
			<div id="carouselExampleControls3" class="carousel carousel-fade mx-auto" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                            @foreach($tab->second_banners as $index => $slider)
                                @if($index == 0)
                                    <div class="carousel-item active">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                        
                                    </div>
                                @else
                                    <div class="carousel-item ">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                    </div>
                                @endif
                            @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls3" role="button" data-slide="prev">
                        <span class="arrow-left">
                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls3" role="button" data-slide="next">
                        <span class="arrow-right">
                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
                        </span>
                    </a>
            </div>
		</div>
	</div>
</div>
<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">CENTRO EMPRESARIAL COMFENALCO</h3>
			<p class="text-justify">Comfenalco se complace en poner a su disposición su moderno Centro Empresarial donde podrá encontrar tecnológicos e innovadores espacios para sus actividades en el corazón de la Zona Industrial de Mamonal Km 6.</p>
			<span class="d-block pb-2 text-blue-2 h4">Contamos con:</span><br>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<img  class="img-fluid rounded" src="{{ asset('img/recreacion-oficinas.jpg') }}" alt="Recreación Oficinas">
					<p class="pl-md-3 pr-md-3">4 Oficinas con capacidad para 3 personas cada una.</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<img  class="img-fluid rounded" src="{{ asset('img/recreacion-salones.jpg') }}" alt="Recreación Salones">
					<p class="pl-md-3 pr-md-3">10 Salones con capacidad* de 25 a 75 personas.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<img  class="img-fluid rounded" src="{{ asset('img/recreacion-salas-de-juntas.jpg') }}" alt="Recreación Salas de juntas">
					<p class="pl-md-3 pr-md-3">5 Salas de juntas con capacidad de 5 a 10 personas ( Mesa principal, sillas con brazo y sillas especiales).</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<img  class="img-fluid rounded" src="{{ asset('img/recreacion-amplio-restaurante.jpg') }}" alt="Recreación Amplio restaurante">
					<p class="pl-md-3 pr-md-3">Amplio restaurante al mejor estilo Da’Tonys.</p>
				</div>
			</div>
			<span class="d-block pb-2 text-blue-2 h4">Ofrecemos los siguientes servicios:</span><br>
			<div class="d-flex justify-content-center text-center flex-wrap">
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/agua-y-cafe.png')}}" alt="Baloncesto">
                    <figcaption>Estación de agua y café</figcaption>
                </figure>                
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/almuerzo-empresarial.png')}}" alt="Béisbol">
                    <figcaption>Almuerzo empresarial</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/almuerzo-ejecutivo.png')}}" alt="Fútbol">
                    <figcaption>Almuerzo Ejecutivo</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/variedad-de-comidas.png')}}" alt="Natación">
                    <figcaption>Variedad de comidas a la carta</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/tecnologia-de-punta.png')}}" alt="Patinaje">
                    <figcaption>Tecnología de punta</figcaption>
                </figure>
            </div>

			<p class="text-justify mb-0"><strong class="text-blue-2">Horario de atención:</strong> Lunes a Viernes de 8:00Am - 5:00Pm y Sábados de 8:00Am- 12:00Pm</p>
			<p class="text-justify"><strong class="text-blue-2">No se permite el ingreso de alimentos y bebidas.</strong></p>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





