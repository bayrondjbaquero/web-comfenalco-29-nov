<div class="container-fluid bg-gray-2 pt-2">
	<div class="row pb-3">
		<div class="col-md-12 p-0">
			<div id="carouselExampleControls2" class="carousel carousel-fade mx-auto" data-ride="carousel">
                @if(isset($tab->second_banners) && count($tab->second_banners) != 0)
	                <div class="carousel-inner" role="listbox">
				            @foreach($tab->second_banners as $index => $slider)
				                @if($index == 0)
				                    <div class="carousel-item active">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                    	@if($slider['url'] != NULL) </a> @endif
				                        
				                    </div>
				                @else
				                    <div class="carousel-item ">
				                    	@if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
				                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
				                        @if($slider['url'] != NULL) </a> @endif
				                    </div>
				                @endif
				            @endforeach
	                </div>
	                <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
	                    <span class="arrow-left">
	                        <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
	                    </span>
	                </a>
	                <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
	                    <span class="arrow-right">
	                        <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
	                    </span>
	                </a>
			        @endif
            </div>
		</div>
	</div>
</div>
<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">SEDE RECREACIONAL TAKURIKA</h3>
			<p class="text-justify">La Sede Recreacional TAKURIKA, se encuentra ubicada entre las poblaciones de Pontezuela y Bayunca a 15 minutos de Cartagena. Es el espacio ideal para  que empresas, familias y amigos disfruten de  la recreación, el deporte y el sano esparcimiento.</p>
			<p class="text-justify">Contamos con excelentes áreas para el esparcimiento y la formación, así como para la realización de sus eventos corporativos y sociales:</p><br>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-kiosco-principal.jpg') }}" alt="Recreación Kiosco principal">
					<p class="pl-md-3 pr-md-3">Kiosco principal, kioscos alternos y terraza recreativa</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-atracciones-opcionales.png') }}" alt="Recreación Atracciones opcionales">
					<p class="pl-md-3 pr-md-3">11 Atracciones: bicicletas acuáticas, muro de escalar, campo de mini golf, rio lento, toboganes (Tornadito, Loop y Kamikaze), Parque infantil.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-piscina-de-adultos.jpg') }}" alt="Recreación Piscina de adultos">
					<p class="pl-md-3 pr-md-3">Piscina de adultos (tobogán, microclima, hidromasajes)</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-ping-pong.jpg') }}" alt="Recreación Mesas de ping pong">
					<p class="pl-md-3 pr-md-3">Mesas de ping pong , Juegos de mesa</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-parque-infantil.jpg') }}" alt="Recreación Parque acuático infantil">
					<p class="pl-md-3 pr-md-3">Parque acuático infantil</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-zona-deportiva.jpg') }}" alt="Recreación Zona deportiva y canchas">
					<p class="pl-md-3 pr-md-3">Zona deportiva y canchas (microfútbol, voleibol, béisbol, fútbol)</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-zonas-verdes.jpg') }}" alt="Recreación Zonas verdes con parques infantiles">
					<p class="pl-md-3 pr-md-3">Zonas verdes con parques infantiles</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-servicio-de-enfermeria.jpg') }}" alt="Recreación Servicio de enfermería y salvavidas certificados">
					<p class="pl-md-3 pr-md-3">Servicio de enfermería y salvavidas certificados</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/recreacion-amplios-parqueaderos.jpg') }}" alt="Recreación Amplios parqueaderos">
					<p class="pl-md-3 pr-md-3">Amplios parqueaderos</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-xl-left text-lg-left">
					<img class="img-fluid rounded" src="{{ asset('img/cicloruta-walk.jpg') }}" alt="Recreación Cicloruta">
					<p class="pl-md-3 pr-md-3">Cicloruta</p>
				</div>
			</div>
			<p class="text-justify">Comfenalco ofrece el servicio de transporte de ida y regreso a la sede recreacional Takurika los fines de semanas y días festivos en los horarios de 8am a 9am. Para los afiliados categoría A Y B el transporte se brinda de manera Subsidiada.</p>
			<p class="text-justify mb-0"><strong class="text-blue-2">Días de servicio:</strong> martes a domingo (se exceptúa el primer día hábil de la semana)</p>
			<p class="text-justify"><strong class="text-blue-2">Horario de apertura:</strong> 9:00 a.m. Horario de Cierre: 4:00 p.m.</p>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





