<div class="container-fluid bg-gray-2 pt-2">
	<div class="row pb-3">
		<div class="col-md-12 p-0">
			<div id="carouselExampleControls5" class="carousel carousel-fade mx-auto" data-ride="carousel">
                @if(count($tab->second_banners) > 0)
                 <div class="carousel-inner" role="listbox">
                            @foreach($tab->second_banners as $index => $slider)
                                @if($index == 0)
                                    <div class="carousel-item active">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                        
                                    </div>
                                @else
                                    <div class="carousel-item ">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                    </div>
                                @endif
                            @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls5" role="button" data-slide="prev">
                        <span class="arrow-left">
                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls5" role="button" data-slide="next">
                        <span class="arrow-right">
                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
                        </span>
                    </a>
                    @endif
            </div>
		</div>
	</div>
</div>
<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">SEDE RECREACIONAL MAGANGUÉ</h3>		
            <p>
                Comfenalco se complace en poner a su disposición su nueva Sede Recreacional Magangué donde podrá encontrar confortables espacios para sus 
                actividades recreativas y académicas. A continuación presentamos nuestro portafolio de servicios:
            </p>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">1 Salón con capacidad* hasta 100 personas, ideal para eventos académicos y sociales </strong></p>
                </div>
            </div>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">1 Salón con capacidad* hasta 50 personas</strong> </p>
                </div>
            </div>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">4 Salones para capacitación con capacidad* hasta 30 personas</strong> </p>
                </div>
            </div>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">Servicio de cafería</strong> </p>
                </div>
            </div>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">Cancha Sintética</strong> </p>
                </div>
            </div>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">Piscina para adultos y niños</strong> </p>
                </div>
            </div>
            <div class="media pb-2 d-flex">
                <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                <div class="media-body">
                    <p class="mb-0"><strong class="text-blue-3">Alimentos y bebidas para eventos </strong> </p>
                </div>
            </div><br>
            <div class="media pb-2 d-flex">
                <p class="text-blue-2">
                    <strong>* Montaje tpo auditorio con una mesa principal y sillas para asistentes. La variación del montaje puede alterar la capacidad ﬁnal del espacio.</strong>
                </p>
            </div>
            <br>
            <h3 class="text-blue-2 pb-3 d-block">PLANES ESPECIALES</h3> 
            <div class="row">
                <div class="col-md-3 text-center">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/pasadias.png') }}" alt="">
                    <p class="text-blue-3 h5">Pasadía</p>
                    <p class="text-blue-3">
                        Incluye: <br>
                        Entrada a la sede <br>
                        Refrigerio básico <br>
                        Almuerzo Ejecutivo
                    </p>
                </div>
                <div class="col-md-3 text-center">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/plan-para-eventos.png') }}" alt="">
                    <p class="text-blue-3 h5">Alquiler Salones</p>
                    <p class="text-blue-3">
                        Incluye: <br>
                        Mesa principal vestida <br>
                        Sillas con brazo o sillas rimax o  <br>
                        Sonido básico <br>
                        Video beam y/o tv
                    </p>
                </div>
            </div><br>
            <div class="row m-0">
                <div class="col-md-10">
                    <div class="d-inline-block p-2 bg-blue-7 h5 rounded"><a href="{{ asset('pdf/Cotizacion estandar Magangue V-2 (1).pdf') }}" target="_blank" class="text-white">Descargar aquí cotización estándar</a></div>
                    <br><br>
                    <p><strong class="text-blue-2">Horario de atención: </strong>Lunes a viernes de 9am a 4pm, Sábado, domingo y festivos de 10am a 6pm.</p>
                </div>
            </div>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





