<div class="container-fluid bg-gray-2 pt-2">
	<div class="row pb-3">
		<div class="col-md-12 p-0">
			<div id="carouselExampleControls55" class="carousel carousel-fade mx-auto" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                            @foreach($tab->second_banners as $index => $slider)
                                @if($index == 0)
                                    <div class="carousel-item active">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="278" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                        
                                    </div>
                                @else
                                    <div class="carousel-item ">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid " src="{{asset($slider['image'])}}" width="1110" height="278" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                    </div>
                                @endif
                            @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls55" role="button" data-slide="prev">
                        <span class="arrow-left">
                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls55" role="button" data-slide="next">
                        <span class="arrow-right">
                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
                        </span>
                    </a>
            </div>
		</div>
	</div>
</div>
<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
            <h3 class="text-blue-2 pb-3">HOTEL CORALES DE INDIAS</h3>
            <div class="row m-0">
                <div class="col-md-6 text-center">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/recreacion-hotel-1.jpg') }}" alt="">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/recreacion-hotel-2.jpg') }}" alt="">
                    <h6 class="text-blue-2">Carrera 1 No 62- 198, Crespo, Cartagena de Indias.</h6>
                </div>
                <div class="col-md-6">
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">250 habitaciones todas con vista al mar</strong></p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Restaurante manglares:</strong> un espacio para disfrutar de una deliciosa comida caribeña con un estilo formal y diversa oferta gastronómica.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Cafetería Atarraya:</strong> un espacio al aire libre, descompilado, acogedor y relajante, para disfrutar de un delicioso desayuno, un almuerzo ligero, o una relajante comida, con la refrescante brisa marina que caracteriza al Caribe colombiano.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Bar Alcatraz:</strong> si quiere disfrutar de una noche cartagenera y deleitarse de una bebida o un refrescante coctel, este es el sitio ideal con vista al mar.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Áreas Zonas Húmedas:</strong> espacio que cuenta con baño turco y sauna.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Piscina:</strong> cuenta con jacuzzi y dos piscinas, una para niños con juegos de agua interactivos, y una para adultos. Ambas con una espectacular vista al mar.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Gimnasio</strong></p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex">
                        <img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
                        <div class="media-body">
                            <p class="mb-0"><strong class="text-blue-2">Salones  y salas de juntas</strong></p>
                        </div>
                    </div>
                </div>
            </div>
            <br><br> 
			<h3 class="text-blue-2 pb-3">PLANES ESPECIALES</h3>
            <div class="row m-0">
                <div class="col-md-3 text-center">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/plan-para-eventos.png') }}" alt="">
                    <p class="text-blue-3 h5">Plan para eventos</p>
                    <div class="p-3 bg-blue-7 h5 text-white rounded"><a href="{{ asset('pdf/Cotizacion Estandar Eventos Corales 2017 V-2.pdf') }}" target="_blank" class="text-white">Descargar Ficha Completa</a></div>
                </div>
                <div class="col-md-3 text-center">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/plan-de-alojamiento.png') }}" alt="">
                    <p class="text-blue-3 h5">Plan de Alojamiento</p>
                    <div class="p-3 bg-blue-7 h5 text-white rounded"><a href="{{ asset('pdf/COTIZACION ESTANDAR HOTEL CORALES DE INDIAS - ALOJAMIENTO - 2017 - V05.pdf') }}" target="_blank" class="text-white">Descargar Ficha Completa</a></div>
                </div>
                <div class="col-md-3 text-center">
                    <img class="img-fluid mb-3 rounded" src="{{ asset('img/pasadias.png') }}" alt="">
                    <p class="text-blue-3 h5">Pasadias</p>
                    <div class="p-3 bg-blue-7 h5 text-white rounded"><a href="{{ asset('pdf/PASADIAS HOTEL CORALES 2017 - V2.pdf') }}" target="_blank" class="text-white">Descargar Ficha Completa</a></div>
                </div>
            </div>			
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>