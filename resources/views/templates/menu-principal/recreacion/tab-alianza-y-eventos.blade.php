<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			{{-- <h3 class="text-blue-2 pb-3">NUESTROS SERVICIOS</h3><br> --}}
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/cocoliso.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Cocoliso Isla Resort</p>
					<p class="text-blue-3">
						Guía Permanente , Reseña Histórica, Tour Panorámico por el Archipiélago en Lanchas Rápidas para 50 pasajeros 
						o Yate (200 Personas), parada en la Isla San Martín de 
						Pajarales para visitar el Acuario (Opcional), Café de Bienvenida. Almuerzo típico.
					</p>
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/isla-rosario.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Islas del Rosario y Playa Blanca</p>
					<p class="text-blue-3">
						Guía permanente - Reseña Histórica, Tour panorámico por el 
						Archipiélago de las Islas del Rosario a bordo del Yate ALCATRAZ 
						capacidad 150-300 máx. Llegada al Acuario (Entrada Adicional), Baño de Mar en Playa Blanca, Almuerzo Típico .
						Adicional Tasa Portuaria: $15.500. 
					</p>
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/islas-arena.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Islas Arena Playa Club</p>
					<p class="text-blue-3">
						Pasadía adulto y niño: <br>
						Incluye: Transporte en lancha rápida, ida y regreso del muelle de La Bodeguita a
						Isla Arena Plaza, coctel de bienvenida. Almuerzo tropical, Piscina de 
						agua dulce, Playas privadas, bohíos, Toallas y sillas para la playa, zona de juegos infantiles, polideportivo, mesa de ping-pong, cancha de baloncesto, voleibol y futbol. <br><br>
						Salida : Muelle de la Bodeguita a las 8.00 A.M. Aprox.
						Llegada: Muelle de la Bodeguita a las 4:30 P.M. Aprox.
					</p>
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/isla-encanto.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Hotel Isla del Encanto</p>
					{{-- <p class="text-blue-3">
						Pasadía adulto y niño: <br>
						Incluye: Transporte en lancha rápida, ida y regreso del muelle de La Bodeguita a
						Isla Arena Plaza, coctel de bienvenida. Almuerzo tropical, Piscina de 
						agua dulce, Playas privadas, bohíos, Toallas y sillas para la playa, zona de juegos infantiles, polideportivo, mesa de ping-pong, cancha de baloncesto, voleibol y futbol. <br><br>
						Salida : Muelle de la Bodeguita a las 8.00 A.M. Aprox.
						Llegada: Muelle de la Bodeguita a las 4:30 P.M. Aprox.
					</p> --}}
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/isla-sol.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Hotel Isla del Sol</p>
					<p class="text-blue-3">
						Recogida en los hoteles ubicados en: Bocagrande, Laguito, Centro, 
						Getsemaní y zona norte (No incluye manzanillo). 
						Transporte (Cartagena – Islas del Sol – Cartagena) en cómodas embar-
						caciones para 28 y 50 pasajeros (lanchas rápidas con dos motores). 
						Guía Bilingüe. 
						Frutas tropicales de bienvenida 
						Playa. 
						Piscina con agua de mar. 
						Sillas y camas asoleadoras 
						Recorrido panorámico por el Parque Nacional Natural Corales del Rosa-
						rio con llegada al acuario (entrada opcional - No incluida) 
						Almuerzo típico (sopa de pescado, pescado frito, arroz con coco, yuca 
						frita, patacón, ensalada y bebidas suaves con el almuerzo). 
						Postre típico (Cocadas) 
						Café Colombiano
					</p>
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/recorridos-culturales.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Recorridos Culturales En Cartagena De Indias</p>
					<p class="text-blue-3">
						Los programas que proponemos elevan el sentido de pertenencia e incrementa el conocimiento de la ciudad y de su historia, brindándoles 
						a los estudiantes y aﬁliados de la Caja de Compensación Familiar COM-
						FENALCO, un programa cultural que ponga a su alcance el conocimiento de las obras del escritor Gabriel García Márquez y su relación con Cartagena de Indias, y las historias no contadas de nuestros monumen-
						tos, patrimonio histórico de la humanidad. Cultura, Memoria e Imaginación es la propuesta para disfrutar de estos recorridos que te brindaran: Sensaciones, Diversiones y Conocimiento, una promesa de Tierra Magna con tecnología europea.
					</p>
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/hotel-decameron.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Hotel Decameron</p>
					<p class="text-blue-3">
						Transporte terrestre Cartagena – Barú – Cartagena.
						Alojamiento.
						Desayunos, Almuerzos y Cenas Buffet.
						Snacks entre comidas.
						Bar abierto.
						Impuestos hoteleros.
						TARIFAS DE ACUERDO A TEMPORADA Y 
						DISPONIBILIDAD HOTELERA
					</p>
				</div>
			</div><br>
			<div class="row align-items-center">
				<div class="col-md-5">
					<img  class="img-fluid rounded " src="{{ asset('img/productos-turisticos.png') }}" alt="Recreación Oficinas">
				</div>
				<div class="col-md-6">
					<p class="text-blue-2 h4">Productos Turísticos</p>
					<p class="text-blue-3">
						Pasadía y Receptivos.
						Paquetes Nacionales.
						Paquetes Internacionales.
						Tiquetes Aéreos Nacionales e Internacionales.
						Cruceros.
						Tarjeta De Asistencia Al Viajero.
					</p>
					<p class="text-blue-2 ">
						Modos de ﬁnanciación: Efectvo, Tarjeta crédito, débito, cupocrédito, 
						libranza, póliza Circulo de Viajes.
					</p>
				</div>
			</div>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





