<div class="container-fluid bg-gray-2 pt-2">
	<div class="row pb-3">
		<div class="col-md-12 p-0">
			<div id="carouselExampleControls4" class="carousel carousel-fade mx-auto" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                            @foreach($tab->second_banners as $index => $slider)
                                @if($index == 0)
                                    <div class="carousel-item active">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid  " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                        
                                    </div>
                                @else
                                    <div class="carousel-item ">
                                        @if($slider['url'] != NULL) <a href="{{ $slider['url'] }}" target="_blank"> @endif
                                        <img class="d-block img-fluid  " src="{{asset($slider['image'])}}" width="1110" height="331" alt="{{ $slider['alt'] }}">
                                        @if($slider['url'] != NULL) </a> @endif
                                    </div>
                                @endif
                            @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls4" role="button" data-slide="prev">
                        <span class="arrow-left">
                            <img src="{{asset('img/arrow-left.png')}}" alt="Arrow left Comfenalco">
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls4" role="button" data-slide="next">
                        <span class="arrow-right">
                            <img src="{{asset('img/arrow-right.png')}}" alt="Arrow right Comfenalco">
                        </span>
                    </a>
            </div>
		</div>
	</div>
</div>
<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">JARDÍN BOTÁNICO</h3>
			<p class="text-justify">El Jardín Botánico Guillermo Piñeres, es un lugar con una propuesta ecológica y paisajística especial para los amantes de la naturaleza. Es el sitio ideal para realizar actividades  recreativas, formativas, eventos familiares y empresariales, con un componente ecológico y natural</p>
			<span class="d-block pb-2 text-blue-2 h4">Contamos con los siguientes servicios:</span><br>
			<div class="d-flex justify-content-center text-center flex-wrap pb-2">
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/salones-climatizados.png')}}" alt="Salones climatizados">
                    <figcaption>Salones climatizados con capacidad hasta 50 personas tipo auditorio</figcaption>
                </figure>                
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/alimentos-y-bebidas.png')}}" alt="Servicio de alimentos y bebidas">
                    <figcaption>Servicio de alimentos y bebidas (previa reserva)</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/transporte-climatizado.png')}}" alt="Transporte">
                    <figcaption>Transporte climatizado</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/amplias-zonas-verdes.png')}}" alt="Zonas verdes">
                    <figcaption>Amplias zonas verdes para talleres y ejercicios</figcaption>
                </figure>
                <figure class="col">
                    <img class="img-fluid" src="{{asset('img/telon-y-videobeam.png')}}" alt="Télon y video beam">
                    <figcaption>Telón y video beam para proyección</figcaption>
                </figure>
            </div>
			<span class="d-block pb-3 text-blue-2 h4">Planes especiales:</span>
			<div class="media pb-2 d-flex align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Plan individual con recorrido guiado.</p>
			  	</div>
			</div>
			<div class="media pb-2 d-flex align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Visitas especializadas para grupos.</p>
			  	</div>
			</div>
			<div class="media pb-2 d-flex align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Plan Sembremos un árbol.</p>
			  	</div>
			</div>
			<div class="media pb-2 d-flex align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Plan Rally Ecológico.</p>
			  	</div>
			</div>
			<div class="media pb-2 d-flex align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Paquetes especiales para Colegios y universidades.</p>
			  	</div>
			</div>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





