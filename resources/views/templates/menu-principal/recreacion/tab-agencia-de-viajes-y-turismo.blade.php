<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">NUESTROS SERVICIOS</h3><br>
			<div class="row">
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/asesoria.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">
						Asesoría para desarrollar su programa de viaje de acuerdo a sus expectativas y presupuesto.
					</p>
				</div>
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/servicios.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">Servicios en hoteles, hostales, ﬁncas y centros recreacionales.</p>
				</div>
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/cobertura.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">Cobertura nacional e internacional para la colocación de tiquetes.</p>
				</div>
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/agilidad.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">Asesoría y agilidad en el trámite de visas.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/transporte.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">Transporte privado de tipo 
ejecutivo y para grupos en 
van o buses de turismo para 
que se desplace de manera 
más cómoda, segura y ágil 
en la realización de sus 
visitas de turismo.</p>
				</div>
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/amplia.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">Amplia variedad de 
programas de turismo 
receptivo, excursiones y 
pasadías a destinos de interés de los aﬁliados, y con precios 
preferenciales.</p>
				</div>
				<div class="col-md-3 d-block align-items-center pb-4 text-center">
					<img class="img-fluid rounded" src="{{ asset('img/acompanamiento.png') }}" alt="Recreación Kiosco principal" class="h-25"><br><br>
					<p class="mb-3">Acompañamiento 
permanente a grupos de 20 personas en 
adelante.</p>
				</div>
			</div>
			<span class="d-block pb-2 text-blue-2 h4">PLANEA TU VIAJE CON NOSOTROS:</span><br>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/cartagena.png') }}" alt="Recreación Oficinas">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4">Plan Cartagena</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR RECEPTIVOS CARTAGENA 2017 - V1.pdf') }}" class="btn btn-primary bg-blue-7" target="_blank">Descargar</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/guajira.png') }}" alt="Recreación Salones">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4">Plan Nacional Guajira</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO -  PLAN NACIONAL GUAJIRA - 2017.pdf') }}" class="btn btn-primary bg-blue-7" target="_blank">Descargar</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/medellin.png') }}" alt="Recreación Oficinas">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4 ">Plan Nacional Medellin</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO -  PLAN NACIONAL MEDELLIN - 2017.pdf') }}" class="btn btn-primary bg-blue-7" target="_blank">Descargar</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/sai.png') }}" alt="Recreación Salones">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4 ">Plan Nacional San Andres</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO -  PLAN NACIONAL SAN ANDRES - 2017 -V2.pdf') }}" class="btn btn-primary bg-blue-7" target="_blank">Descargar</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid" src="{{ asset('img/santander.png') }}" alt="Recreación Oficinas">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4 ">Plan Nacional Santander</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO -  PLAN NACIONAL SANTANDER - 2017.pdf') }}" class="btn btn-primary bg-blue-7 " target="_blank">Descargar</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid" src="{{ asset('img/ejecafetero.png') }}" alt="Recreación Salones">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4">Plan Nacional Eje Cafetero</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO -  PLANES NACIONAL EJE CAFETERO- 2017.pdf') }}" class="btn btn-primary bg-blue-7 " target="_blank">Descargar</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/grupales.png') }}" alt="Recreación Oficinas">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4">Plan Nacional Grupales</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO -  PLANES TERRESTRES NACIONALES GRUPALES - 2017- V3.pdf') }}" class="btn btn-primary bg-blue-7 " target="_blank">Descargar</a>
						</div>
					</div>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/santa-marta.png') }}" alt="Recreación Salones">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4">Plan Nacional Santa Marta</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO - PLAN NACIONAL SANTA MARTA - 2017.pdf') }}" class="btn btn-primary bg-blue-7 " target="_blank">Descargar</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column flex-lg-row flex-xl-row text-center text-lg-left text-xl-left">
					<div class="row">
						<div class="col-md-6">
							<img  class="img-fluid rounded" src="{{ asset('img/islas.png') }}" alt="Recreación Oficinas">
						</div>
						<div class="col-md-6">
							<p class="text-blue-2 h4">Plan Nacional Islas</p>
							<a href="{{ asset('pdf/COTIZACION ESTANDAR TURISMO- RECEPTIVOS ISLAS 2017 - V2.pdf') }}" class="btn btn-primary bg-blue-7 " target="_blank">Descargar</a>
						</div>
					</div>
				</div>
			</div>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





