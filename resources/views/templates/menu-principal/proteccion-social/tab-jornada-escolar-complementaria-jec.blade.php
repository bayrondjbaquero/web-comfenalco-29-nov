<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/proteccion-jornada-escolar.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<div class="d-flex justify-content-center flex-column h-100">
						<h4 class="text-blue-2 pb-2">JORNADA ESCOLAR COMPLEMENTARIA - JEC</h4>
						<p class="text-justify">Dirigido a niños, adolescentes y jóvenes  entre los 7 y 15 años, matriculados en Instituciones Educativas Oficiales del Distrito y el Departamento, en las cuales se realizan acciones que orientan pedagógicamente la utilización del tiempo libre.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/uno-green.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Ciencia y Ambiente</p>
					    	<strong class="d-block pb-2">AMBIENTE E INVESTIGACIÓN</strong>
					    	<p class="text-justify">Desarrollo de actividades de estimulación en las áreas comunicativa, cognitiva, psicoafectiva,  motriz y artística generando semilleros de formación deportivos y culturales, proyectos de aula, promoción de hábitos, valores y derechos.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/dos-purple.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Ciencia y Tecnología</p>
					    	<strong class="d-block pb-2">CIENCIA E INVESTIGACIÓN</strong>
					    	<p class="text-justify">Fomentar la Ciencia y la Investigación para la generación de nuevos conocimientos que incrementen la capacidad tecnológica y científica regional, mediante la capacitación de jóvenes promotores que sean pioneros en el desarrollo de nuevos proyectos de investigación.</p>
					  	</div>
					</div>
					<br>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/tres-yellow.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Salud y Nutrición</p>
					    	<p><strong>DANZA Y FOLCLOR</strong><br>
								Valoración de la herencia cultural del estudiante y desarrollo de competencias espirituales a través de la danza.<br><br>

								<strong>FOMENTO CULTURAL AUDIOVISUAL</strong><br>
								Recuperación de la memoria histórica de su entorno, por la promoción de la investigación y uso de los medios audiovisuales que se concretan en la  realización de cortometrajes.<br><br>

								<strong>ARTE ESCENICO TEATRAL</strong><br>
								La creatividad y la expresión artística a través del teatro permiten en niños y adolescentes la integración de los diversos talentos.<br><br>


								<strong>HISTORIA Y PATRIMONIO</strong><br>
								Fomenta el reconocimiento de la identidad cultural de su entorno, valorando y aprendiendo a proteger el patrimonio natural y cultural.<br><br>

								<strong>ARTE ESCENICO DE TITERES</strong><br>
								Genera un espacio sociocultural, estético y artístico que promueve la expresión y la integración social.<br><br>


								<strong>ARTES PLASTICAS</strong><br>
								A través del arte los niños, niñas y jóvenes beneficiados fortalecen sus conocimientos y habilidades utilizando este  como medio de expresión artística.<br><br>

								<strong>MÚSICA</strong><br>
								Se motiva la elaboración de ideas musicales mediante el uso de los instrumentos, con el fin de enriquecer sus posibilidades de expresión.<br>
								Los talleres de la línea de arte y cultura se consolidan con la puesta en escena de los productos y resultados en exposiciones de arte, festival de danza y folclor, Titirifestival, Festival de Cine de Cartagena, Cine en los barrios.</p>
					  	</div>
					</div>
					<br>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/cuatro-blue.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Recreación y Deportes</p>
					    	<strong class="d-block pb-2">ESCUELAS DEPORTIVAS SOCIALES</strong>
					    	<p>Práctica del deporte y la recreación contribuyendo así al adecuado desarrollo integral, físico y cognitivo.</p>
					  	</div>
					</div>
					<br>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/cinco-green.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Procesos Transversales</p>
					    	<p><strong>PSICOSOCIALES</strong><br>
							Identifica y asesora diferentes necesidades en los usuarios  y sus familias, enmarcadas por las dificultades de aprendizajes, el comportamiento, la  violencia de pares, de género e intrafamiliar, la prevención en salud sexual y reproductiva, el trabajo infantil y otras formas de vulnerabilidad,  a través de las siguientes acciones:<br><br>
							· Escuela de padres<br>
							· Talleres con niños, niñas y jóvenes<br>
							· Asesorías a personal docente e institucional<br><br>
							<strong>RECREOFORMATIVOS</strong><br>
							Estrategias lúdico-recreativas para dinamizar e integrar a los miembros de la comunidad educativa, generando espacios para la participación comunitaria y socialización de experiencias.<br><br>
							<strong>PLAN LECTOR</strong><br>
							Fomenta el desarrollo de las competencias comunicativas en lectura y escritura, mediante el mejoramiento del comportamiento lector, la comprensión lectora y la producción textual de los niños y jóvenes.</p>
					  	</div>
					</div>
					<br>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>