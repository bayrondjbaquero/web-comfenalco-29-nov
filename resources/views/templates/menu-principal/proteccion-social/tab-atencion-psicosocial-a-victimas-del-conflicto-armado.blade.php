<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-6">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/proteccion-atencion-psicosocial.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<div class="d-flex justify-content-center flex-column h-100">
						<h4 class="text-blue-2 pb-2">ATENCIÓN PSICOSOCIAL A VICTIMAS DEL CONFLICTO ARMADO</h4>
						<p class="text-justify">Desarrollado bajo la orientación técnica del Ministerio de Salud y Protección Social, la Unidad para la Atención y Reparación Integral a las Víctimas -UNARIV, con cobertura focalizada en 11 municipios del departamento de Bolívar, dirigido a niños y adolescentes víctimas del conflicto armado.</p>
						<p class="text-justify">Contribuye a la ejecución del Programa de Atención Psicosocial a Víctimas del Conflicto (PAPSIVI) con niños y adolescentes, en la mitigación de afectaciones psicosociales y el cuidado emocional del personal de sector salud que atiende a población víctima.</p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/uno-green.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Estrategias de Atención Psicosocial para Niños y  Adolescentes Víctimas del Conflicto Armado:</p>
					    	<p class="text-justify">•  Desarrollo de encuentros grupales con niños entre los 6 a 12 años y adolescentes de 11 a 18 años, en los cuales se incentivan espacios de dignificación y construcción de memoria histórica mediante el uso de herramientas artísticas, comunicativas y simbólicas.</p>
							<p class="text-justify">•  Implementación de Semilleros de Formación, los cuales posibilitan en los niños y adolescentes herramientas para la vida desde el arte, la música, la danza, el deporte y el contacto con el medio ambiente.</p>
					  	</div>
					</div>
					<br>
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/dos-purple.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Estrategias de  Cuidado Emocional a Personal de las Ips, Eps, Ese y otros Actores Sociales:</p>
					    	<p class="text-justify">Herramientas psicosociales para el cuidado emocional, apoyo al apoyo, momentos de expresión, actividades lúdicas dirigidas al personal de las IPS, EPS, ESE y otros actores sociales que tienen una relación permanente con víctimas del conflicto armado.</p>
					  	</div>
					</div>
					<br>
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/tres-yellow.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Proyectos, Redes y Alianzas</p>
					    	<p class="text-justify">La División de Protección Social de Comfenalco Cartagena, cuenta con la experiencia en la consolidación de alianzas y/o convenios en el marco de la cooperación nacional e internacional, logrando la implementación de proyectos orientados al mejoramiento de la calidad de vida en las comunidades donde hacemos presencia.</p>
					  	</div>
					</div>
					<br><br>
					<p class="text-blue-2 text-justify pl-md-4 ml-md-3"><em>Estamos aportando al desarrollo sostenible de la región Caribe y continuamos promoviendo la participación y equidad, por una inclusión social. </em></p>

				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>