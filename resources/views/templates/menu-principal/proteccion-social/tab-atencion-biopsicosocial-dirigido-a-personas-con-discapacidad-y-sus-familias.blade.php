<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/proteccion-social-discapacidad.jpg')}}" class="img-fluid rounded" alt="protección social personas con discapacidad">
				</div>
				<div class="col-md-7">
					<div class="d-flex justify-content-center flex-column h-100">
						<h3 class="text-blue-2 pb-2">ATENCIÓN BIOPSICOSOCIAL DIRIGIDO A PERSONAS CON DISCAPACIDAD Y SUS FAMILIAS</h3>
						<p class="text-justify">Dirigido a personas con discapacidad, sus cuidadores y grupo familiar, los cuales tendrán la oportunidad de vincularse a procesos formativos, recreo formativos y psicosociales, generando espacios para el desarrollo de su autonomía, adaptación familiar y funcionalidad social.</p>
					</div>
				</div>
				<div class="col-md-12">
					<span class="h4 d-block text-blue-2 pb-2">Caracterización Introductoria</span>
					<p class="text-justify">Validación de  la discapacidad del usuario, por medio de una valoración psicosocial que permita conocer el potencial y la funcionalidad del usuario apoyado en una visita domiciliaria que reconoce el contexto social y familiar.</p>
					<span class="h4 d-block text-blue-2 pb-2">Procesos Formativos</span>
					<p class="text-justify">• Talleres de arte y cultura: musical, danza y teatro.<br>
						• Programa de desarrollo psicomotriz, mantenimiento físico y escuelas deportivas.<br>
						• Desarrollo humano hacia un perfil laboral:<br> 
						• Fortalecimiento de proyecto de vida y competencias laborales<br> 
						• Inclusión Laboral<br>
						• Habilidades para vivir (actividades de la vida diaria, talleres de cocina, belleza, informática y logística de eventos)</p>
					<span class="h4 d-block text-blue-2 pb-2">Procesos Psicosociales e Inclusión Socio Laboral</span>
					<p class="text-justify">Acompañamiento psicosocial a usuarios, padres de familia, cuidadores e instituciones, a través de:<br><br>
						• Visitas empresariales<br>
						• Asesorías a familias en temas relacionados con la discapacidad<br>
						• Control y seguimiento a usuarios vinculados<br>
						• Escuela para Padres<br>
						• Encuentros de Familia<br>
						• Formación en hábitos saludables<br>
						• Orientación socio laboral para personas con discapacidad</p>
					<span class="h4 d-block text-blue-2 pb-2">Procesos Recreoformativos</span>
					<p class="text-justify">Favorecer la integración social del usuario, la familia y la comunidad a través de diferentes acciones como:<br><br>
						• Celebraciones especiales<br>
						• Pasadías en centros recreacionales<br>
						• Actividades al aire libre<br>
						• Participación en eventos inclusivos a nivel local y nacional</p>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>