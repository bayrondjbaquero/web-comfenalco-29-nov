<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/proteccion-social-atencion-adultos.jpg')}}" class="img-fluid rounded" alt="protección social atención para adultos">
				</div>
				<div class="col-md-7">
					<div class="d-flex justify-content-center flex-column h-100">
						<h4 class="text-blue-2 pb-2">ATENCIÓN INTEGRAL AL ADULTO MAYOR</h4>
						<p class="text-justify">Orientado al fortalecimiento de la función social, el autocuidado y el rol de las personas mayores de 50 años, en  relación consigo mismo, su familia y comunidad.</p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/uno-green.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Talleres para Empresas Afiliadas</p>
					    	<p>Talleres de  promoción a nivel empresarial, asociaciones de pensionados  y  grupos de adultos mayores.<br><br>
							·  Ciclo Vital (reconocimiento, adaptación, productividad)<br>
							·  Autocuidado<br> 
							·  Calidad de Vida<br>
							· Acciones de prevención a nivel físico, emocional, recreativo, cognitivo y ocupacional.</p>
					  	</div>
					</div>
					<br>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/dos-purple.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Talleres Formativos</p>
					    	<p>Procesos enfocados en la construcción y fortalecimiento de habilidades cognitivas y ocupacionales que favorezcan en los adultos mayores y prepensionados su adaptación y proyección  entre sus pares y familia:<br><br>• Seminario de Preparación al Retiro Laboral:<br><br>Preparación de los prepensionados para tomar decisiones relacionadas con los aspectos de salud, finanzas, legales, y sociales, orientado a la formación de un nuevo estilo de vida; enmarcado en las fases:<br><br>•  Formativa<br>• Recreoformativa<br>• Talleres Ocupacionales<br><br>Actividades como estrategia de ocupación productiva del tiempo libre.<br><br>• Danza<br>• Música<br>• Teatro<br>• Manualidades<br>• Informática</p>
					  	</div>
					</div>
					<br>
				</div>
				<div class="col-md-12">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/tres-yellow.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Actividades Recreoformativas</p>
					    	<p>Actividades orientadas a dinamizar, participar, socializar experiencias para la búsqueda del bienestar social y el disfrute de la vida en forma individual y grupal, enmarcadas en el Club Gerontológico Plenitud Otoñal.<br><br>• Jornada semanal de mantenimiento físico, EJERCÍTATE<br>• Días especiales y celebraciones, viejo tecas, integraciones, paseos<br>• Escuelas deportivas<br>• Participación en eventos a nivel local y nacional</p>
					  	</div>
					</div>
					<br>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>





