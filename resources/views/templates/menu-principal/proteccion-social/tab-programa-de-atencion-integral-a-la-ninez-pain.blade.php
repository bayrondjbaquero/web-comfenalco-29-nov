<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-5 text-center pb-3">
					<img src="{{asset('img/proteccion-programa-de-atencion.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<div class="d-flex justify-content-center flex-column h-100">
						<h4 class="text-blue-2 pb-2">PROGRAMA DE ATENCIÓN INTEGRAL A LA NIÑEZ - PAIN</h4>
						<p class="text-justify">Dirigido a niños desde los cero (0) hasta los seis (6) años de edad beneficiarios del ICBF, a los cuales se les brinda una atención integral en educación, promoción de la salud y nutrición, recreación y atención psicosocial, que incluye a agentes educativos, equipo interdisciplinario y padres de familia.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/uno-green.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Atención y Orientación Psicopedagógica</p>
					    	<p class="text-justify">Desarrollo de actividades de estimulación en las áreas comunicativa, cognitiva, psicoafectiva,  motriz y artística generando semilleros de formación deportivos y culturales, proyectos de aula, promoción de hábitos, valores y derechos.</p>
					  	</div>
					</div>
					<br>
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/dos-purple.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Promoción De Lectura, Lúdica y Recreación</p>
					    	<p class="text-justify">Espacios de lectura, lúdica, recreación  y juego, en los que se implementan estrategias didácticas y pedagógicas en los que niños, padres de familias y agentes educativos fortalecen las normas  y pautas de comportamiento social, valores y actitudes, como procesos de aprendizajes.</p>
					    	<p>· Estrategia Baúl de los Sueños<br>·Actividades lúdicas, recreativas y culturales, cabildos, mes del niño y la recreación, encuentros familiares.</p>
					  	</div>
					</div>
					<br>
				</div>
				<div class="col-md-6">
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/tres-yellow.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Procesos Educativos</p>
					    	<p class="text-justify">Espacios de formación dirigidos a padres de familia y agentes educativos para el logro de habilidades  cognoscitivas y sociales que fortalezcan  los procesos de crianza, educación inicial  y cualificación en los procesos de atención a primera infancia.</p>
					    	<p>· Escuelas de padres<br>· Capacitación para agentes educativos orientadas al desarrollo de herramientas y materiales pedagógicos, diplomados, talleres teórico-prácticos, intervención en aula y prácticas pedagógicas.</p>
					  	</div>
					</div>
					<br>
					<div class="media d-flex justify-content-center">
						<img class="d-flex mr-2" src="{{ asset('img/cuatro-blue.png') }}" alt="">
						<div class="media-body">
					    	<p class="text-blue-2 h4">Salud y Nutrición</p>
					    	<p>Acciones  de promoción y prevención en salud y nutrición que favorecen la  salud física y mental de los niños y sus familias.<br>· Jornadas masivas para la promoción de salud<br>· Valoración y seguimiento nutricional<br>· Actividades lúdicas de promoción de hábitos de vida saludable<br>· Capacitación en AIEPI (Atención integral a enfermedades prevalentes en la primera infancia) e IAMI (Instituciones amigas de la mujer)<br>· Gestión de riesgo en las sedes de atención.<br>· Implementación de buenas prácticas de manufactura (BPM)<br>· Saneamiento básico</p>
					  	</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-11 mx-auto">
					<br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>