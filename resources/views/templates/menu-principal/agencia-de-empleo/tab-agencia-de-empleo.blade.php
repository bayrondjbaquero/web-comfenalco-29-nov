<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4 pt-4">
				<div class="col-md-5">
					<img src="{{asset('img/empleo-agencia-empleo.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<h3 class="text-blue-2 pb-2 pt-3">AGENCIA DE EMPLEO</h3>
					<p class="text-justify">Somos una Agencia de Intermediación laboral, que facilita la inserción laboral ayudando a los trabajadores a encontrar un empleo conveniente y ayudar a los empleadores a contratar trabajadores apropiados a sus necesidades.</p>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-12">
					<span class="d-block h4 text-blue-2 pb-2">Ventajas del Servicio para los buscadores de empleo</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0"><span class="h5 block text-blue-2">Preselección de Personal</span><br>Contamos con un equipo interdisciplinario, de más de 40 profesionales trabajando a su servicio.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0"><span class="h5 block text-blue-2">Caracterización del Mercado Laboral</span><br>Estudio realizado en la ciudad, para identificar brechas entre las necesidades de la demanda y la oferta laboral, con el propósito de estar alineados con la formación asignada</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0"><span class="h5 block text-blue-2">Orientación Vocacional</span><br>Se identifican las debilidades en los   perfiles de los candidatos, posteriormente se diseñan rutas de formación gratuitas con el fin de mejorar sus probabilidades de empleabilidad teniendo en cuenta las exigencias del mercado laboral</p>
					  	</div>
					</div>
					<br>
					<span class="d-block h4 text-blue-2 pb-2">Ventajas del Servicio para los Empresarios</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Registro de la empresa y sus vacantes en nuestro portal de empleos: <a href="http://www.empleoscomfenalco.com" target="_blank">http://www.empleoscomfenalco.com/</a></p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Acompañamiento de psicólogas organizacionales durante el reclutamiento y preselección de candidatos</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Remisión de hojas de vida y aplicación de pruebas psicotécnicas a los candidatos que se ajusten al perfil requerido.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Unidad Móvil para procesos de reclutamiento fuera del casco Urbano de Cartagena </p>
					  	</div>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6">
							<div class="media pb-2 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/uno-purple.png')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Centro Comercial Outlet El Bosque, 1° piso, local# 19.</p>
							  	</div>
							</div>
							<img src="{{ asset('img/empleo-agencia.png') }}" alt="">
						</div>
						<div class="col-md-6">
							<div class="media pb-2 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/dos-purple-2.png')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Sede administrativa 2, diagonal al edificio Cedesarrollo - Zaragocilla</p>
							  	</div>
							</div>
							<img src="{{ asset('img/empleo-sede.png') }}" alt="">
						</div>
						<div class="col-md-12">
							<div class="media pb-2 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/tres-purple.png')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Sede Magangue Bolívar Avenida Colombia  Cll 16 N 13- 25</p>
							  	</div>
							</div>
							<img src="{{ asset('img/empleo-agencia.png') }}" alt="">
							<img class="float-right" src="{{ asset('img/empleo-unidad-movil.png') }}" alt="">
						</div>
						<div class="col-md-12 pt-3 text-center">
							<a  href="http://www.empleoscomfenalco.com" target="_blank"><img class="mt-3" src="{{ asset('img/empleo-vacantes.png') }}" alt=""></a>
						</div>
					</div>	
					
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>

Registro de la empresa y sus vacantes en nuestro portal de empleos: http://www.empleoscomfenalco.com/
Acompañamiento de psicólogas organizacionales durante el reclutamiento y preselección de candidatos
Remisión de hojas de vida y aplicación de pruebas psicotécnicas a los candidatos que se ajusten al perfil requerido.
Unidad Móvil para procesos de reclutamiento fuera del casco Urbano de Cartagena 