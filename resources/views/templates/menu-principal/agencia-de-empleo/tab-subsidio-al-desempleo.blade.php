<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-11 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4 pt-4">
				<div class="col-md-5">
					<img src="{{asset('img/empleo-subsidio-desempleo.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-7">
					<h3 class="text-blue-2 pb-2 pt-3">SUBSIDIO AL DESEMPLEO</h3>
					<p class="text-justify">Es un beneficio que se otorga a las personas cesantes que cumplan con los requisitos establecidos en la Ley 1636 de 2013. Durante el período cesante, los postulantes podrán acceder a los servicios de capacitación laboral, pago a salud, pensión, cuota monetaria (si la recibía cuando era trabajador dependiente), bonos de alimentación, e incentivo adicional por ahorro voluntario de cesantías.</p>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-12">
					<span class="d-block h4 text-blue-2 pb-2">Requisitos para acceder al subsidio de desempleo</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Encontrarse en condición de desempleado por cualquier causa.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Haber realizado aportes a la caja de compensación por lo menos un (1) año continuo o discontinuo en los últimos tres (3) años.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Estar inscrito en el servicio público de empleo (agencia de empleos) o actualizar la hoja de vida en caso de ya estar inscrito.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">No haber recibido el beneficio en los últimos tres (3) años.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">No ser servidor público de elección popular.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">No ser pensionado por jubilación, invalidez, vejez o sobrevivencia.</p>
					  	</div>
					</div>
					<br>
					<span class="d-block h4 text-blue-2 pb-2">Documentos para acceder al subsidio de desempleo</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Formulario Único de Postulación al Mecanismo de Protección al Cesante debidamente diligenciado.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Certificación sobre cesación laboral expedida por el empleador, indicando la fecha exacta de la terminación laboral, última remuneración del trabajador y causa de la terminación.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Certificación de la última EPS y Fondo de Pensiones a los cuales estuvo afiliados, no mayor a 30 días de expedición. </p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Certificado de inscripción en el Servicio Público de Empleo.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Copia de la Cedula de Ciudadanía.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Documentos de nuevas personas a cargo, si las hubiese.</p>
					  	</div>
					</div>
					<br>
					<span class="d-block h4 text-blue-2 pb-2">Causales de terminación del Subsidio al desempleo:</span>
					<div class="media pb-2 d-flex align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">NO acudir y atender los servicios de colocación del servicio público de empleo (agencia de empleos). Si no cumple perderá los beneficios.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">NO Cumplir con los trámites y requisitos exigidos por el servicio público de empleos para participar en procesos de selección a los que sea remitido.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Rechazar sin causa justificada la ocupación que le ofrezca el servicio público de empleo.</p>
					  	</div>
					</div>
					<div class="media d-flex align-items-center pb-2">
						<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">NO asistir o desarrollar el proceso de formación (ruta de capacitación) que le haya diseñado por parte de la agencia de empleos.</p>
					  	</div>
					</div>
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>






