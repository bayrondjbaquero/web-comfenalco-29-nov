<div class="row ml-0 mr-0 pb-3">
	<div class="col-12 mb-3">
		<h3 class="text-blue">Padres Mayores de 60 años</h3>
	</div>
	<div class="col-md-10">
		<div class="pt-4 pb-4 bg-white rounded pl-2 pr-2">
			<div class="flex-column d-flex align-items-center">
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador, si la afiliación es por primera vez.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Fotocopia legible y de buena calidad del Registro Civil de nacimiento del trabajador donde figure el nombre de los padres.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Fotocopia del documento de identidad de los padres, si la afiliación es por primera vez.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Acreditación de la dependencia económica y convivencia del (los) padre(s) con el trabajador, firmado por el trabajador y por el(los) padre(s). (Utilizar y diligenciar el formato proporcionado por la Caja - Mintrabajo).</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Certificado de discapacidad del beneficiario expedido una entidad competente (padres discapacitados).</p>
				  	</div>
				</div>
			</div>
		</div>
	</div>
</div>
