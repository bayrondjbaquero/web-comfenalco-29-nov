<span class="h4 text-blue-2">Documentos</span>
<br><br>
<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
	<div class="media w-100 align-items-center d-flex">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 align-items-center d-flex">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad, si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 align-items-center d-flex">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Certificado de registro civil de matrimonio.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 align-items-center d-flex">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Constancia laboral, si existen hijos de la unión.</p>
	  	</div>
	</div>
</div>