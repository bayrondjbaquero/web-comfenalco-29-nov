<span class="h4 text-blue-2">Documentos</span>
<br><br>
<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador, si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad del trabajador, si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia legible y de buena calidad del registro civil de nacimiento donde figure el nombre de los padres. (Debe tener el NUIP actualizado –formato numérico-).</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia legible y de buena calidad de la tarjeta de identidad (hijos entre los 7 y 17 años), si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia legible y de buena calidad de la cédula de ciudadanía (hijos con 18 años), si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Certificado de estudio original, si es mayor de 12 y hasta los 18 años. Si es educación no formal, se debe especificar el número de horas diarias de estudio en el certificado expedido por institución debidamente aprobada.</p>
	  	</div>
	</div>
</div>
<br>
<span class="d-block pb-2 h3 text-blue-2">Hijos</span>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="check purple">
	<div class="media-body">
    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad del cónyuge o del padre/madre del menor si el trabajador es soltero o separado, siempre que la afiliación sea por primera vez.</p>
  	</div>
</div>
<br>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="check purple">
	<div class="media-body">
    	<p class="mb-0">Documento de la custodia legal emitida por la correspondiente entidad competente (ICBF, comisaría de familia, juzgado de familia, etc.), para trabajador soltero o separado.</p>
  	</div>
</div>
<br><br>
<span class="d-block pb-2 h3 text-blue-2">Hijos adoptivos</span>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-orange.png')}}" alt="check orange">
	<div class="media-body">
    	<p class="mb-0">Sentencia de adopción o acta de entrega del menor, emitido por el Instituto Colombiano de Bienestar Familiar.</p>
  	</div>
</div>
<br>
<span class="d-block pb-2 h3 text-blue-2">Hijastros</span>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
	<div class="media-body">
    	<p class="mb-0">Acreditación como compañero (a) permanente no inferior a 2 años y sociedad conyugal anterior disuelta. Utilizar y diligenciar el formato proporcionado por la Caja - Mintrabajo.</p>
  	</div>
</div>
<br>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
	<div class="media-body">
    	<p class="mb-0">Certificado expedido por la caja en donde se encuentre afiliado el padre o madre biológico(a) del menor que no convive, donde conste que no recibe subsidio familiar en dinero por ese mismo hijo(a). De lo contrario certificado de no afiliación.</p>
  	</div>
</div>
<br>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
	<div class="media-body">
    	<p class="mb-0">Constancia Laboral del cónyuge o declaración de dependencia económica de los hijastros. Utilizar el formato proporcionado por la Caja - Mintrabajo.</p>
  	</div>
</div>
<br>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
	<div class="media-body">
    	<p class="mb-0">Documento de la custodia legal emitida por la correspondiente entidad competente (ICBF, comisaría de familia, juzgado de familia, etc.).</p>
  	</div>
</div>
<br>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
	<div class="media-body">
    	<p class="mb-0">Declaración de dependencia económica. Utilizar el formato proporcionado por la Caja - Mintrabajo.</p>
  	</div>
</div>
<br>
<div class="media w-100 d-flex align-items-center">
	<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
	<div class="media-body">
    	<p class="mb-0">Certificado expedido por la EPS donde conste que el trabajador lo tiene registrado como beneficiario.</p>
  	</div>
</div>
<br>