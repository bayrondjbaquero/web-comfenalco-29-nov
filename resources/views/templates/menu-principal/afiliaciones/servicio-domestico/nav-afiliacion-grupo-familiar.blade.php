<div class="row ml-0 mr-0 pb-3 empresas-tab" id="v-pills-cuarto">
	<div class="col-12">
		<ul class="nav nav-pills pb-4" id="pills-tab" role="tablist">
			<li class="nav-item col">
				<a class="nav-link active d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded" id="pills-hijos-dos-tab" data-toggle="pill" href="#pills-hijos-dos" role="tab" aria-controls="pills-hijos-dos" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-sin-hijos.png')}}" alt="">
					<span class="text-white text-center">AFILIACIÓN CÓNYUGE</span>
				</a>
			</li>
			<li class="nav-item col">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-union-tres-tab" data-toggle="pill" href="#pills-union-tres" role="tab" aria-controls="pills-union-tres" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-e-hijos.png')}}" alt="">
					<span class="text-white text-center">AFILIACIÓN COMPAÑERO PERMANENTE</span>
				</a>
			</li>
			<li class="nav-item col">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-hijastos-cuatro-tab" data-toggle="pill" href="#pills-hijastos-cuatro" role="tab" aria-controls="pills-hijastos-cuatro" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-e-hijastros.png')}}" alt="">
					<span class="text-white text-center">AFILIACIÓN HIJOS, ADOPTIVOS O HIJASTROS</span>
				</a>
			</li>
		</ul>
		<div class="tab-content pt-3" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-hijos-dos" role="tabpanel" aria-labelledby="pills-home-tab">
				@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-afiliacion-conyuge')
			</div>
			<div class="tab-pane fade" id="pills-union-tres" role="tabpanel" aria-labelledby="pills-profile-tab">
				@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-afiliacion-companero-permanente')
			</div>
			<div class="tab-pane fade" id="pills-hijastos-cuatro" role="tabpanel" aria-labelledby="pills-dropdown1-tab">
				@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-afiliacion-hijos-adoptivos-o-hijastros')
			</div>
			<br>
			<!-- servicio domestivo links abajo-->
			<ul class="nav nav-pills pb-4 justify-content-center" id="sd-pills-tabs" role="tablist">
				<li class="nav-item col-md-4">
					<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded" id="hermanos-huerfanos-tab" data-toggle="pill" href="#hermanos-huerfanos" role="tab" aria-controls="hermanos-huerfanos" aria-expanded="true">
						<img class="img-fluid" src="{{asset('img/afiliacion-trabajadora-soltera-con-hijos.png')}}" alt="" style="height: 92px;">
						<span class="text-white text-center">HERMANOS HUÉRFANOS</span>
					</a>
				</li>
				<li class="nav-item col-md-4">
					<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="padres-mayores-tab" data-toggle="pill" href="#padres-mayores" role="tab" aria-controls="padres-mayores" aria-expanded="true">
						<img class="img-fluid" src="{{asset('img/afiliacion-padres.png')}}" alt="" style="height: 92px;">
						<span class="text-white text-center">AFILIACIÓN DE PADRES MAYORES</span>
					</a>
				</li>
			</ul>
			<div class="tab-content pt-3" id="pills-tabContents">
				<div class="tab-pane fade" id="hermanos-huerfanos" role="tabpanel" aria-labelledby="hermanos-huerfanos-tab">
					@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-hermanos-huerfanos')
				</div>
				<div class="tab-pane fade" id="padres-mayores" role="tabpanel" aria-labelledby="padresmayores-tab">
					@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-padres-mayores')
				</div>
			</div>
		</div>
	</div>
	{{-- <div class="pt-4 col-md-10 ml-md-auto">
		@include('templates.menu-principal.formulario-contacto.contacto')											    
	</div> --}}
</div>