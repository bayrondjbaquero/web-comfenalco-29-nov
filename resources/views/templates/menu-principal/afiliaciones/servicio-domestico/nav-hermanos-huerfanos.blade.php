<div class="row ml-0 mr-0 pb-3">
	<div class="col-12 mb-3">
		<h3 class="text-blue">Hermanos Huérfanos</h3>
	</div>
	<div class="col-md-10">
		<div class="pt-4 pb-4 bg-white rounded pl-2 pr-2">
			<div class="flex-column d-flex align-items-center">
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador, si la afiliación es por primera vez.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad del trabajador, si la afiliación es por primera vez.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Declaración juramentada donde conste la convivencia y dependencia económica del hermano huérfano de padres, con el trabajador. Utilizar formato proporcionado por la Caja – Mintrabajo.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Fotocopia legible y de buena calidad del Registro Civil de nacimiento del trabajador y hermano(s) donde figure el nombre de los padres. (Debe tener el NUIP actualizado –formato numérico-).</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Fotocopia legible y de buena calidad de la tarjeta de identidad (hermanos entre los 7 y 17 años) o de la cédula de ciudadanía (hermanos con 18 años).</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Certificado de estudio original, si es mayor de 12 y hasta los 18 años, si es Educación No Formal, se debe especificar el número de horas diarias de estudio en el certificado expedido por institución debidamente aprobada.</p>
				  	</div>
				</div>
				<br>
				<div class="media w-100 d-flex align-items-center">
					<img class="d-flex mr-2" src="{{asset('img/check-yellow.png')}}" alt="">
					<div class="media-body">
				    	<p class="mb-0">Fotocopia de certificado o del registro civil de defunción de los padres.</p>
				  	</div>
				</div>
			</div>
		</div>
	</div>
</div>