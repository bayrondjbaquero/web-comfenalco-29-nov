<span class="h4 text-blue-2">Documentos</span>
<br><br>
<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador, si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad, si la afiliación es por primera vez.</p>
	  	</div>
	</div>
	<br>
	<div class="media w-100 d-flex align-items-center">
		<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
		<div class="media-body">
	    	<p class="mb-0">Acreditación como compañero (a) permanente o manifestación del estado civil no inferior a 2 años. Se debe utilizar y diligenciar el formato proporcionado por la Caja - Mintrabajo, preferiblemente con la firma de la cónyuge, si existen hijos de la unión.</p>
	  	</div>
	</div>
</div>