<div class="row ml-0 mr-0 pb-3">
	<div class="col-md-4 text-center pb-3 pb-lg-0 pb-xl-0">
		<br>
		<img class="rounded img-fluid" src="{{asset('img/afiliaciones-servicio-domestico.jpg')}}" alt="">
	</div>
	<div class="col-md-8">
		<h3 class="text-blue-2">SERVICIO DOMÉSTICO</h3>
		<p class="text-justify">En el Decreto 00721 de 2013, que reglamenta el Numeral 4° del Art. 7° de la Ley 21 de 1982, estipula que todos los empleadores que ocupen uno o más trabajadores permanentes, incluyendo los trabajadores de servicio doméstico, están obligados a pagar el subsidio familiar.</p>
		<span class="h4 text-blue-2">¿A quiénes se consideran trabajadores del servicio doméstico?</span>

		<p class="text-justify">Se considera trabajador del servicio doméstico a la persona natural que, a cambio de una remuneración, presta su servicio personal de manera directa, habitual y bajo continuada subordinación o dependencia, a una o varias personas naturales, para la ejecución de tareas de aseo, cocina, lavado, planchado, cuidado de niños y demás labores propias del hogar del empleador.</p>

		<p class="text-justify">Otro de los beneficios a los que también pueden acceder los trabajadores domésticos afiliados a las Cajas es el de recibir la cuota moderadora o subsidio familiar si se cumple con un mínimo de 96 horas laboradas en el mes, por cada persona a cargo del afiliado, así como los demás servicios que ofrece nuestro portafolio (Educación, Recreación, Deportes, Vivienda, Crédito, entre otros).</p>

		<span class="h4 text-blue-2 pb-2">¿Cómo afiliar a los trabajadores de servicio doméstico?</span>

		<p class="text-justify">Antes de afiliar a los trabajadores del servicio doméstico, la persona natural debe afiliarse como empleador de Servicio Doméstico.<br><br>La afiliación se puede realizar diligenciando el formulario de empleadores y de afiliación de trabajadores anexando fotocopia de cédula del empleador y del empleado.</p>						
		<a class="mb-2 p-1 text-white bg-orange-2 w-100 text-center d-block rounded" href="{{ asset('pdf/VINCULACION DE EMPLEADOR.pdf') }}" target="_blank" download="FORMATO AFILIACION DE EMPLEADOR">Descargar Formato de afiliación de Empleador.</a>
		<a class="p-2 text-white bg-purple w-100 text-center d-block rounded" href="#">Descargar Formato de afiliación de trabajadores de Servicio Doméstico.</a>
		<br>
		<p class="text-justify">La entrega de documentación la puede realizar en todos los Centros Integrales de Servicio al Cliente (CIS) en Mamonal, Matuna, Ejecutivos, Bocagrande y  Bosque (Outlet El Bosque); en los Puntos Fijos de Comfenalco ubicados en SAO Buenos Aires (segundo piso), SAO San Felipe (segundo piso), Olímpica Buenos Aires, Éxito La Castellana, Éxito Los Ejecutivos, o a través de sus ejecutivas de cuentas o enviar al correo <a href="mailto:afiliacionescomfenalco@comfenalco.com">afiliacionescomfenalco@comfenalco.com.</a></p>
		<br>
		<em class="pt-2 text-blue-2 d-block">Nota: Los instructivos los puede descargar en la página web de Comfenalco, en la zona de descarga de la sección de afiliaciones.</em>
	</div>
	{{-- <div class="pt-4 col-md-10 ml-md-auto">
		@include('templates.menu-principal.formulario-contacto.contacto')
	</div> --}}
</div>