<div class="row ml-0 mr-0 pb-3">
	<div class="col-md-4 text-center pb-3 pb-lg-0 pb-xl-0">
		<br>
		<img class="rounded img-fluid" src="{{asset('img/afiliaciones-trabajador.jpg')}}" alt="">
	</div>
	<div class="col-md-8">
		<h3 class="text-blue-2">AFILIACIÓN TRABAJADOR</h3>
		<p class="text-justify">Es importante realizar el trámite de la afiliación de sus trabajadores después de afiliada la empresa para que disfruten de todos nuestros beneficios junto con su grupo familiar.</p>
		<p class="text-justify">Se deben registrar ante la caja los cambio que se presenten de trabajadores para evitar duplicidad de afiliación en los trabajadores y además reportes de mora en el pago de aportes.</p>
		<span class="h4 text-blue-2">Documentos</span>
		<br><br>
		<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
			<div class="media w-100 align-items-center d-flex">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador.</p>
			  	</div>
			</div>
			<br>
			<div class="media w-100 align-items-center d-flex">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad, si la afiliación es por primera vez.</p>
			  	</div>
			</div>
		</div>
	</div>
	{{-- <div class="pt-4 col-md-10 ml-md-auto">
		@include('templates.menu-principal.formulario-contacto.contacto')
	</div> --}}
</div>