<div class="row m-0 pb-3">
	<div class="col-md-3 flex-md-last">
		<div class="pt-4 nav flex-column nav-pills bg-blue-6" id="sv-pills-tab" role="tablist">
			@foreach($tab->sub_tabs as $index => $tb)
				<a class="nav-link text-uppercase @if($index == 0) active @endif" id="sv-pills-{{ $tb['slug'] }}-tab" data-toggle="pill" href="#sv-pills-{{ $tb['slug'] }} " role="tab" aria-controls="sv-pills-{{ $tb['slug'] }}" aria-expanded="true">{{ $tb['title'] }}</a>
			@endforeach
		</div>
	</div>
	<div class="pt-4 col-md-9">
		<div class="tab-content" id="v-pills-tabContent">
			@foreach($tab->sub_tabs as $index => $tb)
				<div class="tab-pane fade @if($index == 0) show active @endif" id="sv-pills-{{ $tb['slug'] }}" role="tabpanel" aria-labelledby="{{ $tb['slug'] }}-tab">
					@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-'.$tb['slug'])
				</div>
			@endforeach
			<!-- 1 -->
			<!-- 3 -->
			{{-- <div class="tab-pane fade" id="v-pills-ten" role="tabpanel" aria-labelledby="v-pills-ten-tab">
				<div class="row ml-0 mr-0 pb-3">
					<div class="col-md-4 text-center pb-3 pb-lg-0 pb-xl-0">
						<br>
						<img class="rounded img-fluid" src="{{asset('img/afiliaciones-gris.jpg')}}" alt="">
					</div>
					<div class="col-md-8">
						<h3 class="text-blue-2 text-uppercase">Afiliación cónyuge</h3>
						<span class="h4 text-blue-2">Documentos</span>
						<br><br>
						<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
							<div class="media w-100 align-items-center d-flex">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 align-items-center d-flex">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad, si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 align-items-center d-flex">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Certificado de registro civil de matrimonio.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 align-items-center d-flex">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Constancia laboral, si existen hijos de la unión.</p>
							  	</div>
							</div>
						</div>
					</div>
					<div class="pt-4 col-md-10 ml-md-auto">
						@include('templates.menu-principal.formulario-contacto.contacto')											    
					</div>
				</div>
			</div> --}}
			<!-- 4 -->
			{{-- <div class="tab-pane fade" id="v-pills-eleven" role="tabpanel" aria-labelledby="v-pills-eleven-tab">
				<div class="row ml-0 mr-0 pb-3">
					<div class="col-md-4 text-center pb-3 pb-lg-0 pb-xl-0">
						<br>
						<img class="rounded img-fluid" src="{{asset('img/afiliaciones-gris.jpg')}}" alt="">
					</div>
					<div class="col-md-8">
						<h3 class="text-blue-2 text-uppercase">Compañero (a) Permanente</h3>
						<span class="h4 text-blue-2">Documentos</span>
						<br><br>
						<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador, si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad, si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Acreditación como compañero (a) permanente o manifestación del estado civil no inferior a 2 años. Se debe utilizar y diligenciar el formato proporcionado por la Caja - Mintrabajo, preferiblemente con la firma de la cónyuge, si existen hijos de la unión.</p>
							  	</div>
							</div>
						</div>
					</div>
					<div class="pt-4 col-md-10 ml-md-auto">
						@include('templates.menu-principal.formulario-contacto.contacto')											    
					</div>
				</div>
			</div> --}}
			<!-- 4 -->
			<div class="tab-pane fade empresas-tab show in active" id="v-pills-cuarto" role="tabpanel" aria-labelledby="v-pills-cuarto-tab">
				<div class="row ml-0 mr-0 pb-3">
					<div class="col-12">
						<ul class="nav nav-pills pb-4" id="pills-tab" role="tablist">
							<li class="nav-item col">
								<a class="nav-link active d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded" id="pills-hijos-dos-tab" data-toggle="pill" href="#pills-hijos-dos" role="tab" aria-controls="pills-hijos-dos" aria-expanded="true">
									<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-sin-hijos.png')}}" alt="">
									<span class="text-white text-center">AFILIACIÓN CÓNYUGE</span>
								</a>
							</li>
							<li class="nav-item col">
								<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-union-tres-tab" data-toggle="pill" href="#pills-union-tres" role="tab" aria-controls="pills-union-tres" aria-expanded="true">
									<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-e-hijos.png')}}" alt="">
									<span class="text-white text-center">AFILIACIÓN COMPAÑERO PERMANENTE</span>
								</a>
							</li>
							<li class="nav-item col">
								<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-hijastos-cuatro-tab" data-toggle="pill" href="#pills-hijastos-cuatro" role="tab" aria-controls="pills-hijastos-cuatro" aria-expanded="true">
									<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-e-hijastros.png')}}" alt="">
									<span class="text-white text-center">AFILIACIÓN HIJOS, ADOPTIVOS O HIJASTROS</span>
								</a>
							</li>
						</ul>
						<div class="tab-content pt-3" id="pills-tabContent">
							<div class="tab-pane fade show active" id="pills-hijos-dos" role="tabpanel" aria-labelledby="pills-home-tab">
								@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-afiliacion-conyuge')
							</div>
							<div class="tab-pane fade" id="pills-union-tres" role="tabpanel" aria-labelledby="pills-profile-tab">
								@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-afiliacion-companero-permanente')
							</div>
							<div class="tab-pane fade" id="pills-hijastos-cuatro" role="tabpanel" aria-labelledby="pills-dropdown1-tab">
								@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-afiliacion-hijos-adoptivos-o-hijastros')
							</div>
							<br>
							<!-- servicio domestivo links abajo-->
							<ul class="nav nav-pills pb-4 justify-content-center" id="sd-pills-tabs" role="tablist">
								<li class="nav-item col-md-4">
									<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded" id="hermanos-huerfanos-tab" data-toggle="pill" href="#hermanos-huerfanos" role="tab" aria-controls="hermanos-huerfanos" aria-expanded="true">
										<img class="img-fluid" src="{{asset('img/afiliacion-trabajadora-soltera-con-hijos.png')}}" alt="" style="height: 92px;">
										<span class="text-white text-center">HERMANOS HUÉRFANOS</span>
									</a>
								</li>
								<li class="nav-item col-md-4">
									<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="padres-mayores-tab" data-toggle="pill" href="#padres-mayores" role="tab" aria-controls="padres-mayores" aria-expanded="true">
										<img class="img-fluid" src="{{asset('img/afiliacion-padres.png')}}" alt="" style="height: 92px;">
										<span class="text-white text-center">AFILIACIÓN DE PADRES MAYORES</span>
									</a>
								</li>
							</ul>
							<div class="tab-content pt-3" id="pills-tabContents">
								<div class="tab-pane fade" id="hermanos-huerfanos" role="tabpanel" aria-labelledby="hermanos-huerfanos-tab">
									@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-hermanos-huerfanos')
								</div>
								<div class="tab-pane fade" id="padres-mayores" role="tabpanel" aria-labelledby="padresmayores-tab">
									@include('templates.menu-principal.afiliaciones.servicio-domestico.nav-padres-mayores')
								</div>
							</div>
						</div>
					</div>
					<div class="pt-4 col-md-10 ml-md-auto">
						@include('templates.menu-principal.formulario-contacto.contacto')											    
					</div>
				</div>
			</div>
			<!-- 5 -->
			<div class="tab-pane fade" id="v-pills-twelve" role="tabpanel" aria-labelledby="v-pills-twelve-tab">
				<div class="row ml-0 mr-0 pb-3">
					<div class="col-md-4 text-center pb-3 pb-lg-0 pb-xl-0">
						<br>
						<img class="rounded img-fluid" src="{{asset('img/afiliaciones-gris.jpg')}}" alt="Información general">
					</div>
					<div class="col-md-8">
						<h3 class="text-blue-2 text-uppercase">Hijos, Adoptivos ó Hijastros</h3>
						<span class="h4 text-blue-2">Documentos</span>
						<br><br>
						<div class="bg-white rounded p-5 flex-column d-flex align-items-center">
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Formulario de afiliación debidamente diligenciado y firmado por el trabajador y el empleador, si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad del trabajador, si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia legible y de buena calidad del registro civil de nacimiento donde figure el nombre de los padres. (Debe tener el NUIP actualizado –formato numérico-).</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia legible y de buena calidad de la tarjeta de identidad (hijos entre los 7 y 17 años), si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia legible y de buena calidad de la cédula de ciudadanía (hijos con 18 años), si la afiliación es por primera vez.</p>
							  	</div>
							</div>
							<br>
							<div class="media w-100 d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Certificado de estudio original, si es mayor de 12 y hasta los 18 años. Si es educación no formal, se debe especificar el número de horas diarias de estudio en el certificado expedido por institución debidamente aprobada.</p>
							  	</div>
							</div>
						</div>
						<br>
						<span class="d-block pb-2 h3 text-blue-2">Hijos</span>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="check purple">
							<div class="media-body">
						    	<p class="mb-0">Fotocopia legible y de buena calidad del documento de identidad del cónyuge o del padre/madre del menor si el trabajador es soltero o separado, siempre que la afiliación sea por primera vez.</p>
						  	</div>
						</div>
						<br>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-purple.png')}}" alt="check purple">
							<div class="media-body">
						    	<p class="mb-0">Documento de la custodia legal emitida por la correspondiente entidad competente (ICBF, comisaría de familia, juzgado de familia, etc.), para trabajador soltero o separado.</p>
						  	</div>
						</div>
						<br><br>
						<span class="d-block pb-2 h3 text-blue-2">Hijos adoptivos</span>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-orange.png')}}" alt="check orange">
							<div class="media-body">
						    	<p class="mb-0">Sentencia de adopción o acta de entrega del menor, emitido por el Instituto Colombiano de Bienestar Familiar.</p>
						  	</div>
						</div>
						<br>
						<span class="d-block pb-2 h3 text-blue-2">Hijastros</span>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
							<div class="media-body">
						    	<p class="mb-0">Acreditación como compañero (a) permanente no inferior a 2 años y sociedad conyugal anterior disuelta. Utilizar y diligenciar el formato proporcionado por la Caja - Mintrabajo.</p>
						  	</div>
						</div>
						<br>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
							<div class="media-body">
						    	<p class="mb-0">Certificado expedido por la caja en donde se encuentre afiliado el padre o madre biológico(a) del menor que no convive, donde conste que no recibe subsidio familiar en dinero por ese mismo hijo(a). De lo contrario certificado de no afiliación.</p>
						  	</div>
						</div>
						<br>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
							<div class="media-body">
						    	<p class="mb-0">Constancia Laboral del cónyuge o declaración de dependencia económica de los hijastros. Utilizar el formato proporcionado por la Caja - Mintrabajo.</p>
						  	</div>
						</div>
						<br>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
							<div class="media-body">
						    	<p class="mb-0">Documento de la custodia legal emitida por la correspondiente entidad competente (ICBF, comisaría de familia, juzgado de familia, etc.).</p>
						  	</div>
						</div>
						<br>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
							<div class="media-body">
						    	<p class="mb-0">Declaración de dependencia económica. Utilizar el formato proporcionado por la Caja - Mintrabajo.</p>
						  	</div>
						</div>
						<br>
						<div class="media w-100 d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-green.png')}}" alt="check green">
							<div class="media-body">
						    	<p class="mb-0">Certificado expedido por la EPS donde conste que el trabajador lo tiene registrado como beneficiario.</p>
						  	</div>
						</div>
						<br>
					</div>
					<div class="pt-4 col-md-10 ml-md-auto">
						@include('templates.menu-principal.formulario-contacto.contacto')											    
					</div>
				</div>
			</div>
			<!-- 6 -->
		</div>
	</div>
</div>