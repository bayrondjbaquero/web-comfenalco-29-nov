<div class="row ml-0 mr-0">
	<div class="col-md-12 mb-3">
		<h3 class="text-blue-2 pt-3">ASOPAGOS</h3>
		<p class="text-justify">La forma más efectiva de realizar sus pagos de PILA.</p>
		<p class="text-justify">Asopagos es una empresa de Comfenalco Cartagena que afianza nuestro compromiso con usted y la región.</p>
		<p class="text-justify">Asesoría personalizada especialmente para usted como afiliado!</p>
		<p><a href="https://www.enlace-apb.com/interssi/.plus" target="_blank">https://www.enlace-apb.com/interssi/.plus</a></p>
		<p><a href="/pdf/PORTAFOLIO_ASOPAGOS_S.A.pdf" target="_blank">Descargar Portafolio de servicios</a></p>
	</div>
</div>