<div class="row ml-0 mr-0 pb-3">
	<div class="col-12 mb-3">
		<h3 class="text-blue-2">AFILIACIÓN TRABAJADOR</h3>
		<p class="text-justify">Es importante realizar el trámite de la afiliación de sus trabajadores después de afiliada la empresa para que disfruten de todos nuestros beneficios junto con su grupo familiar.</p>
		<p class="text-justify">Se deben registrar ante la caja los cambio que se presenten de trabajadores para evitar duplicidad de afiliación en los trabajadores y además  retrasos en el pago de aportes.</p>
		<h3 class="text-blue-2">Documentación</h3>
	</div>
	<div class="col-md-5 pt-4 pb-4 bg-white rounded">
		<div class="media d-flex justify-content-center align-items-center">
			<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
			<div class="media-body">
		    	<p class="mb-0"><a href="{{ asset('pdf/FORMATO DE AFILIACION DE EMPLEADOS.pdf') }}" target="_blank">Formulario de afiliación</a></p>
		  	</div>
		</div>
		<br>
		<div class="media d-flex justify-content-center align-items-center">
			<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
			<div class="media-body">
		    	<p class="mb-0">Cédula de Ciudadanía del trabajador</p>
		  	</div>
		</div>
		<br>
		<div class="media d-flex justify-content-center align-items-center">
			<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
			<div class="media-body">
		    	<p class="mb-0">Documentos de su grupo familiar</p>
		  	</div>
		</div>
	</div>
</div>