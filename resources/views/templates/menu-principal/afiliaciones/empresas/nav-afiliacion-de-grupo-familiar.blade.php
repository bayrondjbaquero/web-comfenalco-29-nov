<div class="row ml-0 mr-0 pb-3 empresas-tab" id="v-pills-three">
	<div class="col-12">
		<ul class="nav nav-pills pb-4" id="pills-tab" role="tablist">
			<li class="nav-item col">
				<a class="nav-link active d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded" id="pills-hijos-tab" data-toggle="pill" href="#pills-hijos" role="tab" aria-controls="pills-hijos" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-sin-hijos.png')}}" alt="">
					<span class="text-white text-center">AFILIACIÓN CON CÓNYUGE SIN HIJOS</span>
				</a>
			</li>
			<li class="nav-item col">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-union-tab" data-toggle="pill" href="#pills-union" role="tab" aria-controls="pills-union" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-e-hijos.png')}}" alt="">
					<span class="text-white text-center">AFILIACIÓN CON CÓNYUGE E HIJOS DE LA UNIÓN</span>
				</a>
			</li>
			<li class="nav-item col">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-hijastos-tab" data-toggle="pill" href="#pills-hijastos" role="tab" aria-controls="pills-hijastos" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliaciones-conyuge-e-hijastros.png')}}" alt="">
					<span class="text-white text-center">AFILIACIÓN CON CÓNYUGE E HIJASTROS</span>
				</a>
			</li>
			<li class="nav-item col">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-trabajador-separado-tab" data-toggle="pill" href="#pills-trabajador-separado" role="tab" aria-controls="pills-trabajador-separado" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliacion-trabajador-separado-con-hijos.png')}}" alt="" style="height: 92px;">
					<span class="text-white text-center">AFILIACIÓN SEPARADO CON HIJOS</span>
				</a>
			</li>
		</ul>
		<div class="tab-content pt-3" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-hijos" role="tabpanel" aria-labelledby="pills-home-tab">
				<h3 class="text-blue">Documentación</h3>
				<div class="col-md-10 pt-4 pb-4 bg-white rounded">
					<div class="media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Documento de identificación del cónyuge.</p>
					  	</div>
					</div>
					<br>
					<div class="media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Fotocopia de registro civil de matrimonio o declaración juramentada de convivencia vigente.</p>
					  	</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-union" role="tabpanel" aria-labelledby="pills-profile-tab">
				<h3 class="text-blue">Documentación</h3>
				<div class="bg-white afiliaciones-doc rounded row ml-0 mr-0">
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Documento de identificación del cónyuge.</p>
						  	</div>
						</div>
					</div>												
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Certificado de estudio para hijos mayores de 12 años.</p>
						  	</div>
						</div>
					</div>
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Fotocopia de registro civil de matrimonio o declaración juramentada de convivencia vigente.</p>
						  	</div>
						</div>
					</div>
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Fotocopia de registro civil de nacimiento de los hijos.</p>
						  	</div>
						</div>
					</div>
					<br>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-hijastos" role="tabpanel" aria-labelledby="pills-dropdown1-tab">
				<h3 class="text-blue">Documentación</h3>
				<div class="bg-white afiliaciones-doc rounded row ml-0 mr-0">
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Documento de identificación del cónyuge.</p>
						  	</div>
						</div>
					</div>												
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Certificado de estudio de los hijastros mayores de 12 años.</p>
						  	</div>
						</div>
					</div>
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Certificado de afiliación a EPS de los hijastros.</p>
						  	</div>
						</div>
					</div>
					<div class="col-md-6 pt-2 pb-2 d-flex align-items-center">
						<div class="media d-flex align-items-center">
							<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Fotocopia de registro civil de nacimientos de hijastros.</p>
						    	<br>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-trabajador-separado" role="tabpanel" aria-labelledby="pills-dropdown1-tab">
				@include('templates.menu-principal.afiliaciones.empresas.nav-afiliacion-trabajador-separado-con-hijos')
			</div>
		</div>
		<br>
		<ul class="nav nav-pills pb-4 justify-content-center" id="pills-tabs" role="tablist">
			<li class="nav-item col-md-3">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded" id="pills-trabajadora-soltera-tab" data-toggle="pill" href="#v-trabajadora-soltera" role="tab" aria-controls="pills-trabajadora-soltera" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliacion-trabajadora-soltera-con-hijos.png')}}" alt="" style="height: 92px;">
					<span class="text-white text-center">AFILIACIÓN TRABAJADORA SOLTERA CON HIJOS</span>
				</a>
			</li>
			<li class="nav-item col-md-3">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-padres-tab" data-toggle="pill" href="#pills-padres" role="tab" aria-controls="pills-padres" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliacion-padres.png')}}" alt="" style="height: 92px;">
					<span class="text-white text-center">AFILIACIÓN DE PADRES</span>
				</a>
			</li>
			<li class="nav-item col-md-3">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-huerfanos-de-padres-tab" data-toggle="pill" href="#pills-huerfanos-de-padres" role="tab" aria-controls="pills-huerfanos-de-padres" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/afiliacion-huerfanos-de-padres.png')}}" alt="" style="height: 92px;">
					<span class="text-white text-center text-uppercase">Afiliación hermanos huérfanos de padres</span>
				</a>
			</li>
			<li class="nav-item col-md-3">
				<a class="nav-link d-flex justify-content-around align-items-center flex-column bg-blue-6 rounded p-3" id="pills-huerfanos-de-padres-tab" data-toggle="pill" href="#pills-discapacitados" role="tab" aria-controls="pills-huerfanos-de-padres" aria-expanded="true">
					<img class="img-fluid" src="{{asset('img/discapacitados.png')}}" alt="" style="height: 92px;">
					<span class="text-white text-center text-uppercase">Afiliación discapacitados</span>
				</a>
			</li>
		</ul>
		<div class="tab-content pt-3" id="pills-tabContents">
			<div class="tab-pane fade" id="v-trabajadora-soltera" role="tabpanel" aria-labelledby="v-pills-five-tab">
				@include('templates.menu-principal.afiliaciones.empresas.nav-afiliacion-trabajadora-soltera-con-hijos')
			</div>
			<div class="tab-pane fade" id="pills-padres" role="tabpanel" aria-labelledby="v-pills-six-tab">
				@include('templates.menu-principal.afiliaciones.empresas.nav-afiliacion-padres')
			</div>
			<div class="tab-pane fade" id="pills-huerfanos-de-padres" role="tabpanel" aria-labelledby="v-pills-seven-tab">
				@include('templates.menu-principal.afiliaciones.empresas.nav-afiliacion-huerfanos-de-padres')
			</div>
			<div class="tab-pane fade" id="pills-discapacitados" role="tabpanel" aria-labelledby="v-pills-eight-tab">
				@include('templates.menu-principal.afiliaciones.empresas.nav-discapacitados')
			</div>
		</div>
	</div>
</div>