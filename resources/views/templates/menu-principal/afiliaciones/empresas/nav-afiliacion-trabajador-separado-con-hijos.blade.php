<div class="row ml-0 mr-0 pb-3">
	<div class="col-12 mb-3">
		<h3 class="text-blue">Documentación</h3>
	</div>
	<div class="col-md-10">
		<div class="pt-4 pb-4 bg-white rounded pl-2 pr-2">
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
				<div class="media-body">
			    	<p class="mb-0">Cédula de ciudadanía de la madre o padre del menor.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
				<div class="media-body">
			    	<p class="mb-0">Registro civil de nacimiento de los hijos para acreditar parentesco: sin autenticar, legible y no interesa la vigencia.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
				<div class="media-body">
			    	<p class="mb-0">Certificado de estudio de los hijos mayores de 12 años.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
				<div class="media-body">
			    	<p class="mb-0">Fotocopia de acta de divorcio o declaración juramentada donde certifica estado civil separado.</p>
			  	</div>
			</div>
		</div>
	</div>
</div>