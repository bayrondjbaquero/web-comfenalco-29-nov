<div class="row ml-0 mr-0">
	<div class="col-md-12 mb-3">
		<br>
		<h3 class="text-blue-2">SUBSIDIO FAMILIAR</h3>
		<p class="text-justify">Es una prestación social que se entrega en dinero, especie y/o servicio, a los trabajadores de medianos y menores recursos, en proporción al número de personas a cargo, que cumplan los requisitos establecidos por la Ley.</p>
		<p class="text-justify">Según Ley 789 de 2002, Artículo 3, tienen derecho al subsidio familiar en dinero los trabajadores:</p>
	</div>
	<div class="col-md-12 pt-3 pb-4 bg-white rounded">
		<div class="media d-flex justify-content-center align-items-center">
			<img class="d-flex mr-2" src="/img/check-yellow.jpg" alt="">
			<div class="media-body">
		    	<p class="mb-0">Cuya remuneración mensual, fija o variable no sobrepase los cuatro (4) salarios mínimos legales mensuales vigentes (smlmv) siempre y cuando laboren al menos 96 horas al mes.</p>
		  	</div>
		</div>
		<br>
		<div class="media d-flex justify-content-center align-items-center">
			<img class="d-flex mr-2" src="/img/check-yellow.jpg" alt="">
			<div class="media-body">
		    	<p class="mb-0">Que sumados sus ingresos con el cónyuge o compañero (a), no sobrepasen seis (6) salarios mínimos legales mensuales vigentes (smlmv). En este caso recibirá el subsidio uno de los dos afiliados.</p>
		  	</div>
		</div>
		<br>
		<div class="media d-flex justify-content-center align-items-center">
			<img class="d-flex mr-2" src="/img/check-yellow.jpg" alt="">
			<div class="media-body">
		    	<p class="mb-0">Podrán cobrar simultáneamente el subsidio familiar por los mismos hijos el padre y la madre, cuyas remuneraciones sumadas no excedan de cuatro (4) salarios mínimos legales mensuales vigentes (smlmv).</p>
		  	</div>
		</div>
	</div>
	<div class="col-md-12">
		<br>
		<p class="text-justify">Recibirá un subsidio extraordinario en caso de muerte de una persona a cargo, por la cual el trabajador estuviere recibiendo subsidio familiar, se pagará un subsidio extraordinario en el mes en que esto ocurra, equivalente a 12 mensualidades del subsidio en dinero que viniere recibiendo por el fallecido.<br><br>En caso de que fallezca el trabajador, podrá administrar la prestación social durante máximo 12 meses (según el análisis previo de la solicitud), quien demuestre haber quedado con la custodia de los beneficiarios que estuvieren a cargo.</p>
	</div>
</div>
 


