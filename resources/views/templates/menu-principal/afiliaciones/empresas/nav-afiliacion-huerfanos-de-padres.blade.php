<div class="row ml-0 mr-0 pb-3">
	<div class="col-md-10">
		<p class="text-justify">Se pueden afiliar al grupo familiar a los hermanos menores de 18 años que sean huérfanos de padre y madre.</p>
	</div>
	<div class="col-12 mb-3">
		<h3 class="text-blue">Documentación</h3>
	</div>
	<div class="col-md-10">
		<div class="pt-4 pb-4 bg-white rounded pl-2 pr-2">
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Fotocopia de cedula de trabajador.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Fotocopia de registro civil de nacimiento del trabajador.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Fotocopia de registro civil de nacimiento de hermano(a).</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Registro civil de defunción de los padres.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Certificado de estudio de hermanos mayores de 12 años.</p>
			  	</div>
			</div>
		</div>
	</div>
</div>