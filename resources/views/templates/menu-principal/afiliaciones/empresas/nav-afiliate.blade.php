<div class="row ml-0 mr-0">
	<div class="col-md-12 mb-3">
		<h3 class="text-blue-2 pt-3 text-uppercase">Usuario Plataforma Afiliate</h3>
		<p class="text-justify">Registre todas las novedades de sus trabajadores desde nuestra plataforma sin documentación impresa y con una mayor capacidad en tiempo de respuesta.</p>
		<p class="text-justify">Es el canal más actualizado para conocer la información de los trabajadores.  Es una plataforma con ancho de banda de 150 MB lo que permite tener un servidor con mejor capacidad para el cargue de la información.</p>
		<p class="text-justify">Solicite su clave en cualquiera de nuestros puntos deatención o con su ejecutiva de cuentas. <a href="http://afiliate.comfenalco.com/" target="_blank">http://afiliate.comfenalco.com/</a></p>
		<p><a href="/pdf/Formato_de_Autorizacion_de_Usuarios_Afiliate_Ver_4.0.pdf" target="_blank">Descargar formato</a> y <a href="/pdf/MANUAL DE USUARIO EMPLEADOR.pdf" target="_blank">manual</a>.</p>
	</div>
</div>