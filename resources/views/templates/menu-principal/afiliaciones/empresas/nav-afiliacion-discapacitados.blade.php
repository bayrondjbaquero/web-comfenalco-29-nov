<div class="col-md-12 mb-3">
	<h3 class="text-blue-2 pt-3">AFILIACIÓN DISCAPACITADOS</h3>
	<p class="text-justify">Los beneficiarios discapacitados del grupo familiar recibirán doble cuota monetaria de manera permanente</p>
	<h3 class="text-blue-2">Documentación</h3>
</div>
<div class="col-md-12 pt-4 pb-4 bg-white rounded">
	<div class="media d-flex justify-content-center align-items-center">
		<img class="d-flex mr-2" src="/img/check-yellow.jpg" alt="">
		<div class="media-body">
	    	<p class="mb-0">Fotocopia de cedula de trabajador.</p>
	  	</div>
	</div>
	<br>
	<div class="media d-flex justify-content-center align-items-center">
		<img class="d-flex mr-2" src="/img/check-yellow.jpg" alt="">
		<div class="media-body">
	    	<p class="mb-0">Documentación requerida para beneficiarios (hijos, hermanos o padres).</p>
	  	</div>
	</div>
	<br>
	<div class="media d-flex justify-content-center align-items-center">
		<img class="d-flex mr-2" src="/img/check-yellow.jpg" alt="">
		<div class="media-body">
	    	<p class="mb-0">Certificado de discapacidad expedido por una entidad competente</p>
	  	</div>
	</div>
</div>