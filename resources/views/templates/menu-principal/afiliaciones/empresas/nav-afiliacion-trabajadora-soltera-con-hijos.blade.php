<div class="row ml-0 mr-0 pb-3">
	<div class="col-12 mb-3">
		<h3 class="text-blue">Documentación</h3>
	</div>
	<div class="col-md-10">
		<div class="pt-4 pb-4 bg-white rounded pl-2 pr-2">
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Registro civil de nacimiento de los hijos para acreditar parentesco: sin autenticar, legible y no interesa la vigencia.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Certificado de estudio para mayores de 12 años o boletín de calificaciones de establecimiento docente debidamente aprobado.</p>
			  	</div>
			</div>
			<br>
			<div class="media d-flex justify-content-center align-items-center">
				<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="">
				<div class="media-body">
			    	<p class="mb-0">Certificado del médico de la EPS o entidad competente, donde conste la capacidad física disminuida que impida trabajar, indicando tipo de discapacidad.</p>
			  	</div>
			</div>
		</div>
	</div>
</div>