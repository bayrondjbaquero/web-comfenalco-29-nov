<div class="row pb-3">
	<div class="col-md-3 text-center pt-4">
		<img class="img-fluid rounded" src="{{asset('img/afiliaciones-pensionados.jpg')}}" alt="">
	</div>
	<div class="pt-4 col-md-9">
		<div class="row ml-0 mr-0 pb-3">
			<div class="col-md-12">
				<h3 class="text-blue">PENSIONADOS</h3>
				<p class="text-justify">Comfenalco le ofrece  a los pensionados, el derecho a disfrutar todos los servicios que ofrece la Caja de Compensación, de acuerdo a su tipo de afiliación, basado en el siguiente cuadro.</p>
				<a href="{{ asset('img/tabla-pensionados.png') }}" target="_blank"><img class="img-fluid d-block m-auto" src="{{ asset('img/tabla-pensionados.png') }}" alt=""></a><br><br>
				<span class="text-blue h3">Documentos</span>
				<p class="text-justify mb-0 pt-2">Para la afiliación presenta la siguiente documentación en nuestros Puntos de Atención o envíala al correo afiliacionesempresariales@comfenalco.com</p>
				<br><br>
			</div>
		</div>
		<div class="row ml-0 mr-0">
			<div class="col-md-8">
				<div class="bg-white afiliaciones-doc rounded">
					<div class="row ml-0 mr-0">
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0"><a href="{{ asset('pdf/Pensionados con aportes.pdf') }}" target="_blank" download="FORMULARIO DE PENSIONADOS">Formulario de afiliación</a></p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia de CC</p>
							  	</div>
							</div>
						</div>												
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Carta de compromiso de pago de aporte</p>
							  	</div>
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia de Resolución de pensión</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Últimos dos (2) comprobantes de pago</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Documentos de identificación del grupo familiar</p>
							  	</div>
							</div>
						</div>
					</div>
				</div>
				{{-- <br>
				<div class="col-12 pt-2 pb-2">
					<em class="text-blue-2">NOTA: Si los documentos enumerados son presentados en fotocopia deben estar debidamente autenticados.</em>
				</div> --}}
			</div>
		</div> 	
		<br>
		{{-- <div class="col-md-10">
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div> --}}
	</div>
</div>