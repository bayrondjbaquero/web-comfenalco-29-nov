<div class="row m-0 pb-3">
	<div class="col-md-3 text-center pt-4">
		<img class="img-fluid rounded" src="{{asset('img/afiliaciones-independientes.jpg')}}" alt="afiliaciones independientes">
	</div>
	<div class="pt-4 col-md-9">
		<div class="row ml-0 mr-0 pb-3">
			<div class="col-md-12">
				<h3 class="text-blue">INDEPENDIENTES</h3>
				<p class="text-justify">Comfenalco le ofrece a los independientes la oportunidad de afiliarse a la caja de compensación para disfrutar de todos los servicios que ofrecemos.<br> La afiliación voluntaria y tiene 2 opciones de pago de aportes:</p>
				<p class="text-justify">Aquellos independientes que aportes el 0.6% de sus ingresos mensuales tendrán derecho a nuestros servicios de Recreación, Capacitación y Turismo.</p>
				<p class="text-justify">Aquellos independientes que voluntariamente aporten el 2% de sus ingresos mensuales tendrán derecho a disfrutar de todos nuestros servicios, excepto subsidio familiar y crédito social.</p>
				<span class="text-blue h4">Documentos</span>
				<p class="text-justify mb-0 pt-2">Para la afiliación presenta la siguiente documentación en nuestros Puntos de Atención o envíala al correo afiliacionesempresariales@comfenalco.com</p>
				<br>
			</div>
		</div>
		<div class="row ml-0 mr-0">
			<div class="col-md-8">
				<div class="bg-white afiliaciones-doc rounded">
					<div class="row ml-0 mr-0">
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0"><a href="{{ asset('pdf/formulario_de_trabajador_independiente_con_firmas_atras.pdf') }}" target="_blank" download="FORMULARIO DE TRABAJADOR INDEPENDIENTE">Formulario de afiliación</a></p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Fotocopia de CC</p>
							  	</div>
							</div>
						</div>												
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Carta de compromiso de pago de aporte</p>
							  	</div>
							</div>
						</div>
						<div class="w-100"></div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Certificado de EPS</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Últimos dos (2) pagos de aporte a salud.</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 pt-2 pb-2 d-flex align-items-center">
							<div class="media d-flex align-items-center">
								<img class="d-flex mr-2" src="{{asset('img/check-yellow.jpg')}}" alt="check yellow">
								<div class="media-body">
							    	<p class="mb-0">Documentos de identificación del grupo familiar</p>
							  	</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			{{-- <div class="col-12 pt-2 pb-2">
				<br>
				<em class="text-blue-2">NOTA: Si los documentos enumerados son presentados en fotocopia deben estar debidamente autenticados.</em>
			</div> --}}
		</div> 	
		{{-- <br>
		<div class="col-md-10">
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div> --}}
	</div>
</div>