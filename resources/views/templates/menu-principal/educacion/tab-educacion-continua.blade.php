<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-2">		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<br><br>
					<h3 class="text-blue-2 pb-2">DIPLOMADOS</h3>
				</div>
			</div>
			<div class="row m-0 pb-2">	
				<div class="col-md-10 m-auto justify-content-md-center">
					<div class="row m-0">	
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_gp" href="#"><img src="{{asset('img/educacion-diplomado-gerencia.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_gleysp" href="#"><img src="{{asset('img/educacion-diplomado-gestion.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_ldnyps" href="#"><img src="{{asset('img/educacion-diplomado-liquidacion.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_mdypo" href="#"><img src="{{asset('img/educacion-diplomado-marketing.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_sgsst" href="#"><img src="{{asset('img/educacion-diplomado-sistemas.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_hg" href="#"><img src="{{asset('img/educacion-diplomado-habilidades.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_gf" href="#"><img src="{{asset('img/educacion-diplomado-financiera.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_sdgi" href="#"><img src="{{asset('img/educacion-diplomado-integrados.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_fpcple" href="#"><img src="{{asset('img/educacion-diplomado-formacion.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_nias" href="#"><img src="{{asset('img/educacion-diplomado-nias.png')}}" alt="" class="img-fluid"></a></div>
						<div class="col-md-6 col-lg-4 mb-3 text-center"><a id="d_gh" href="#"><img src="{{asset('img/educacion-diplomado-humana.png')}}" alt="" class="img-fluid"></a></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

