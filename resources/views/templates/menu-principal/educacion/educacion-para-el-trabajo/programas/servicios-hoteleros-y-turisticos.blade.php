<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12">
					<div class="float-right"><a class="nav-dropdown10-tab-back link-hover" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A PROGRAMAS</a></div>
				</div>			
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-servicios-hoteleros.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h4 class="align-self-start text-white bg-yellow-2 pb-3 mb-3 pt-3 pl-5 pr-5 rounded text-uppercase">Servicios Hoteleros y Turísticos</h4>
					<span class="text-blue-2 pb-2 d-block h4">PERFIL POR COMPETENCIA</span>
					<p class="text-justify">1. Apoya los Servicios Turísticos orientados a Hoteles, de Agencias de Viajes, y Cruceros.</p>
					<p class="text-justify">2. Maneja las relaciones organizacionales y de seguimiento con clientes en Hoteles, Agencias de Viajes, y Cruceros.</p>
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL OCUPACIONAL</span>
					<p>El Técnico Laboral en Servicios Hoteleros y Turísticos puede ubicarse laboralmente de acuerdo a la clasificación laboral de ocupaciones en empresas públicas o privadas como:</p>
					<p><span class="text-yellow">•</span> Auxiliar asesor de Proyectos Turísticos en Agencias de Viajes<br>
					<span class="text-yellow">•</span> Auxiliar asesor de Proyectos Turísticos en el Sector Hotelero<br>
					<span class="text-yellow">•</span> Auxiliar asesor de Proyectos Turísticos en Cruceros.</p>
				</div>
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL EMPRENDEDOR</span>
			    	<p class="mb-0"><span class="text-yellow">•</span> Generación de ideas de negocios relacionadas con Proyectos Turísticos orientados a Hoteles, Agencias de Viajes, y Cruceros</p>
				</div>
			</div>
			<div class="row m-0 mt-2 mb-3">
				<span class="text-bold text-blue-2 pl-3 pb-2 d-block h4">PLAN DE ESTUDIO</span>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Primer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Introducción al Servicio Hotelero, de Agencias de Viaje y Cruceros</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Protocolo Personal y Organizacional</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Inglés - A1</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Comunicación Oral y Escrita</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Herramientas Informática</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Segundo semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Servicios Hoteleros</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Comunicación Organizacional</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Inglés - A2</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">tercer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Servicios en Agencias De Viajes</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Servicios en Cruceros</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Atención, Servicio y Seguimiento al Cliente</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Visión del Ser</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Emprendimiento</p>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>