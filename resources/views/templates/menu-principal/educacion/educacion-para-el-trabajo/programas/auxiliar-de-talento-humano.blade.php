<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-12">
					<div class="float-right"><a class="nav-dropdown10-tab-back link-hover" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A PROGRAMAS</a></div>
				</div>				
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-talento-humano.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h4 class="align-self-start text-white bg-blue-7 pb-3 mb-3 pt-3 pl-5 pr-5 rounded text-uppercase">Auxiliar de Talento Humano</h4>
					<span class="text-blue-2 pb-2 d-block h4">PERFIL POR COMPETENCIA</span>
					<p class="text-justify">1. Apoya el proceso de liquidación de nómina, SGSS y prestaciones sociales.</p>
					<p class="text-justify">2. Apoya el proceso de preselección, selección de candidatos y vinculación a las personas seleccionadas</p>
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL OCUPACIONAL</span>
					<p>El Técnico Laboral Auxiliar de Talento Humano puede ubicarse laboralmente de acuerdo a la clasificación laboral de ocupaciones en empresas públicas o privadas como:</p>
					<p><span class="text-yellow">•</span> Auxiliar de Personal<br>
					<span class="text-yellow">•</span> Auxiliar de Selección de Personal<br>
					<span class="text-yellow">•</span> Auxiliar de Contratación<br>
					<span class="text-yellow">•</span> Auxiliar de Nómina</p>
				</div>
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL EMPRENDEDOR</span>
			    	<p class="mb-0"><span class="text-yellow">•</span> Generación de Ideas de negocios relacionadas con Asesoría laboral y de Selección Laboral.</p>
				</div>
			</div>
			<div class="row m-0 mt-2 mb-3">
				<span class="text-bold text-blue-2 pl-3 pb-2 d-block h4">PLAN DE ESTUDIO</span>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Primer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Selección Laboral I</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Legislación Laboral</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Comunicación Oral y Escrita</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Herramientas Informática</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Segundo semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Selección Laboral II</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Protocolo de Atención al Cliente</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seguridad Social, Prestaciones e Indemnizaciones</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">tercer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Vinculación Laboral</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Liquidación De Nómina Aplicado</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Visión Del Ser</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Emprendimiento</p>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>