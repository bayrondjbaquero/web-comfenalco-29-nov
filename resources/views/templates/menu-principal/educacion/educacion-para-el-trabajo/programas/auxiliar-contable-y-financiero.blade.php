<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12">
					<div class="float-right"><a class="nav-dropdown10-tab-back link-hover" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A PROGRAMAS</a></div>
				</div>				
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-auxiliar-contable.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h4 class="align-self-start text-white bg-blue-7 pb-3 mb-3 pt-3 pl-5 pr-5 rounded text-uppercase">Auxiliar Contable y Financiero</h4>
					<span class="text-blue-2 pb-2 d-block h4">PERFIL POR COMPETENCIA</span>
					<p class="text-justify">1. Contabiliza los recursos de operación, inversión y financiación de acuerdo con las normas y políticas organizacionales.</p>
					<p class="text-justify">2. Elabora declaraciones tributarias de acuerdo con Guía técnica y normativa fiscal</p>
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL OCUPACIONAL</span>
					<p>El Técnico Laboral en Auxiliar Contable y Financiero puede ubicarse laboralmente de acuerdo a la clasificación laboral de ocupaciones en empresas públicas o privadas como:</p>
					<p><span class="text-yellow">•</span> Auxiliar de Contabilidad<br>
						<span class="text-yellow">•</span> Auxiliar de Cuentas por cobrar y pagar<br>
						<span class="text-yellow">•</span> Auxiliar de Nómina<br>
						<span class="text-yellow">•</span> Auxiliar Financiero<br>
						<span class="text-yellow">•</span> Auxiliar de Impuestos</p>
				</div>
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL EMPRENDEDOR</span>
			    	<p class="mb-0"><span class="text-yellow">•</span> Generación de ideas de negocios relacionadas con la consultoría y asesoría contable, financiera, laboral y fiscal.</p>
				</div>
			</div>
			<div class="row m-0 mt-2 mb-3">
				<span class="text-bold text-blue-2 pl-3 pb-2 d-block h4">PLAN DE ESTUDIO</span>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Primer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Principios De Contabilidad Aplicada</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Comunicación Oral y Escrita</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Herramientas Informática</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Segundo semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Legislación Comercial</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Contabilidad De Pasivo Y Patrimonio</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Taller Tributario Aplicado</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">tercer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Gestión y Liquidación De Nómina</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Contabilidad de Costos</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Análisis Contable y Financiero</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Visión del Ser</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Emprendimiento</p>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

