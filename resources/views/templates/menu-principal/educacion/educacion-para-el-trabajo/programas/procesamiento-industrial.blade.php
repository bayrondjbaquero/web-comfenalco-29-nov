<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12">
					<div class="float-right"><a class="nav-dropdown10-tab-back link-hover" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A PROGRAMAS</a></div>
				</div>				
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-facilidades.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h4 class="align-self-start text-white bg-green-2 pb-3 mb-3 pt-3 pl-5 pr-5 rounded text-uppercase">Procesamiento Industrial</h4>
					<span class="text-blue-2 pb-2 d-block h4">PERFIL POR COMPETENCIA</span>
					<p class="text-justify">1. Apoya la fabricación de productos industriales de acuerdo con el procedimiento establecido y la orden de producción.</p>
					<p class="text-justify">2. Elabora e interpreta planos industriales</p>
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL OCUPACIONAL</span>
					<p>El técnico Laboral en Procesamiento industrial puede ubicarse laboralmente de acuerdo a la clasificación Laboral de ocupaciones en empresas públicas o privadas como:</p>
					<p><span class="text-yellow">•</span> Auxiliar en procesamiento, fabricación y ensamble de materiales, y productos.<br>
					<span class="text-yellow">•</span> Operador en Procesos de fabricación de productos industriales.<br>
					<span class="text-yellow">•</span> Asesor en elaboración e interpretación de planos industriales.</p>
				</div>
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL EMPRENDEDOR</span>
			    	<p class="mb-0"><span class="text-yellow">•</span> Generación de ideas de negocios relacionadas con asesoría en elaboración e interpretación de planos industriales.</p>
				</div>
			</div>
			<div class="row m-0 mt-2 mb-3">
				<span class="text-bold text-blue-2 pl-3 pb-2 d-block h4">PLAN DE ESTUDIO</span>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Primer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Procesos Industriales y de Transformación</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Metrología Industrial</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Comunicación Oral y Escrita</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal I Herramientas Informática</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Segundo semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Dibujo Mecánico Aplicado I</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Instrumentación y Automatización Básica</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Equipos Industriales</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">tercer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seguridad Industrial</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Manejo de Materiales</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Procesos de Manufactura</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Dibujo Mecánico Aplicado II</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II Visión del Ser</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Seminario Transversal II</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Emprendimiento</p>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

