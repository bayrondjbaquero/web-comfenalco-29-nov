<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12">
					<div class="float-right"><a class="nav-dropdown10-tab-back link-hover" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A PROGRAMAS</a></div>
				</div>			
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-tecnico-laboral.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h4 class="align-self-start text-white bg-yellow-2 pb-3 mb-3 pt-3 pl-5 pr-5 rounded text-uppercase">Técnico laboral auxiliar convenciones y eventos</h4>
					<span class="text-blue-2 pb-2 d-block h4">PERFIL POR COMPETENCIA</span>
					<p class="text-justify">El Técnico Laboral Auxiliar de Convenciones y Eventos puede crear su propia empresa o ubicarse laboralmente de acuerdo a la clasificación laboral de ocupaciones en empresas públicas o privadas como:</p>
					<p class="text-justify">1. Auxiliar, organizadores de eventos.</br>
					2. Auxiliar, logística de eventos.</p>
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL OCUPACIONAL</span>
					<p>El Técnico Laboral en Mantenimiento de Hardware y Redes puede ubicarse laboralmente de acuerdo a la clasificación laboral de ocupaciones en empresas públicas o privadas como:</p>
					<p><span class="text-yellow">•</span> Técnico en Centros de Cómputo.<br>
					<span class="text-yellow">•</span> Técnico en Soportes y Reparación de equipos<br>
					<span class="text-yellow">•</span> Técnico en sistemas de instalación y Mantenimiento de Redes de Computación</p>
				</div>
				<div class="col-md-6 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">PERFIL EMPRENDEDOR</span>
			    	<p class="mb-0"><span class="text-yellow">•</span> Generación de ideas de negocios relacionadas con Soportes y Reparación de equipos en instalación y Mantenimiento de Redes de Computación.</p>
				</div>
			</div>
			<div class="row m-0 mt-2 mb-3">
				<span class="text-bold text-blue-2 pl-3 pb-2 d-block h4">PLAN DE ESTUDIO</span>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Primer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Servicio al Cliente</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Teoría Turística</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Estadística</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Herramientas Informáticas</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Comunicación Oral y Escrita</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">Segundo semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Técnicas de Ventas</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Transacciones Comerciales</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Técnicas de Recreación</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Fundamentos de Administración</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Contabilidad General</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 d-block text-blue-2 pb-2 text-uppercase">tercer semestre</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Control de Inventarios</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Manejo de Valores e Ingresos</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Organización de Convenciones y Eventos</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Competencias del Ser</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-purple.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Emprendimiento</p>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>