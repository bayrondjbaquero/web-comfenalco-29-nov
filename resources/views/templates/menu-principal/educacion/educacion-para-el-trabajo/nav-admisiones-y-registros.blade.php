<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4 mt-2">		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-uppercase text-blue-2 pb-2">Admisiones y Registros</h3>
					<p class="text-justify">Todas las personas interesadas en Ingresar a Cedesarrollo Comfenalco deberán cumplir con siguientes requisitos y con el procedimiento de acuerdo al tipo de formación de su preferencia.</p>
					<span class="d-block h4 text-blue-2 pb-2 text-uppercase">Formación Técnica</span>
					<p class="text-blue-2"><em>Requisitos y Procedimientos de Inscripción para los aspirantes PROGRAMA DOBLE CERTIFICACIÓN:</em></p>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Haber aprobado 9º grado</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Presentar certificado del plantel educativo donde aprobó el 9º grado</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Fotocopia de cédula de ciudadanía o Tarjeta de identidad</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Diligenciar formulario de inscripción</p>
					  	</div>
					</div>
					<br>
					<p class="text-blue-2"><em>Requisitos y Procedimientos de Inscripción para los aspirantes programa para personas que han cursado hasta 9° y trabajadores:</em></p>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Tener 16 años cumplidos</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Presentar certificado del plantel educativo donde aprobó el 9º grado o diploma de grado de bachiller.</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Fotocopia de cédula de ciudadanía o Tarjeta de identidad</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Diligenciar formulario de inscripción</p>
					  	</div>
					</div>
					
				</div>
				<div class="col-md-6 text-center pb-3 d-flex justify-content-center flex-column">
					<img src="{{asset('img/educacion-admisiones-registros.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

