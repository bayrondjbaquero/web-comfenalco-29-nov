<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">CEDESARROLLO</h3>
					<p class="text-justify">El Centro de Educación para el Trabajo y el Desarrollo humano, CEDESARROLLO, es un Centro cuyo propósito principal es ofrecer a la comunidad y al sector empresarial, una gama de servicios educativos, con énfasis en los requerimientos y necesidades del entorno, apoyados en nuevas tecnologías.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-cedesarrollo.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-12">
					<span class="d-block h4 text-blue-2 pb-2">CEDESARROLLO COMFENALCO, SINÓNIMO DE CALIDAD</span>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Certificación bajo la norma ISO 9001</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">La primera institución en Cartagena y Bolívar en contar con el reconocimiento de Alta Calidad de sus programas, certificados bajo las normas NTC 5555, NTC 558 y NTC 5666 otorgados por ICONTEC.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Preparación para comportarse adecuadamente en ambientes de trabajo</p>
					  	</div>
					</div>
				</div>
				<div class="col-12 mt-3">
					<span class="d-block h4 text-blue-2 pb-2 text-capitalize">¿POR QUÉ ESTUDIAR EN CEDESARROLLO?</span>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Preparación para realizar entrevistas de trabajo efectivas.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Desarrollo de competencias y habilidades comunicativas y de emprendimiento en alto nivel.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Inscripción  en la Agencia de Empleos de Comfenalco, al concluir su formación, lo que permite que pueda emplearse y pagar su formación de educación superior.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Excelente nivel académico, sus profesores altamente capacitados, conformado por profesionales, especialistas y magísteres expertos en el desarrollo de competencias laborales y del ser.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Acceso a excelentes recursos tecnológicos y de infraestructura, contando con 20 Laboratorios, 87 Salones de última tecnología, destinadas para clases de pregrado y diplomados, biblioteca de dos niveles dotada con amplias zonas de consulta y lectura con puestos en grupos e individuales, zona de hemeroteca y semilleros de investigación, que  harán que tu formación cuente con la excelencia de nuestra institución.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Posibilidad de desarrollar tu formación de manera acorde a tus intereses por la flexibilidad de horario que tiene la Institución, así mismo podrás hacer doble programa y contarás con apoyo en la inserción laboral a través de nuestra agencia de empleo.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">A través de nuestro modelo de educación, como egresado puedes homologar y continuar la tecnología y profesionalización con las universidades en convenio.</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Ambiente dedicado al desarrollo de competencias, con actividades culturales y deportivas, como elemento fundamental de la educación integral de nuestros estudiantes.</p>
					  	</div>
					</div>
				</div>
				<!-- mid -->
				<div class="col-12 p-3 mb-2">
					<p class="text-justify">Así mismo Cedesarrollo Comfenalco, buscando el mejoramiento de la calidad de vida de sus estudiantes, ha consolidado una estrategia que permite su vinculación laboral. De igual forma, con esta estrategia se busca propiciar una alianza académica entre Cedesarrollo Comfenalco y el sector productivo, que permita fortalecer las competencias laborales de los estudiantes mediante el proyecto de formación práctica:</p>
				</div>
				<div class="col-md-6">
					<img class="rounded img-fluid pb-3" src="{{ asset('img/educacion-practicas-empresariales.jpg') }}" alt="">
					<span class="d-block h4 text-blue-2 pb-2">Prácticas empresariales:</span>
					<p class="text-justify">Es una modalidad en la cual el estudiante tiene espacio y las herramientas para desarrollar su práctica en una empresa la cual le permite el fortalecimiento de las competencias adquiridas dentro del aula de clase. Estas prácticas se encaminan dentro de la modalidad de bajo la modalidad de pasantías  o contrato de aprendizaje SENA.<br>Hasta la fecha Cedesarrollo cuenta con 94 convenios firmados con el sector productivo. Esta alianza permitirá fidelizar las plazas de prácticas para los estudiantes de la institución.</p>
				</div>
				<div class="col-md-6">
					<img class="rounded img-fluid pb-3" src="{{ asset('img/educacion-taller-de-entrenamiento.jpg') }}" alt="">
					<span class="d-block h4 text-blue-2 pb-2">Taller de entrenamiento:</span>
					<p class="text-justify">Son prácticas en ambientes reales o simulados que permiten afianzar claramente las competencias que se están desarrollando.</p>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

