<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-11 m-auto justify-content-md-center">
					<div class="row m-0 mb-3">
						<div class="col-md-6 d-flex justify-content-center flex-column">
							<h3 class="text-blue-2 pb-2">PROGRAMAS TÉCNICO LABORALES</h3>
							<p class="text-justify">Los programas técnicos laborales se ofertan bajo la modalidad de formación progresiva articulados en armonía total con programa tecnológicos, posibilitando así que las competencias sean reconocidas en la instancia de la educación superior. <a href="#p_programas" class=""><em>Programas</em></a></p>
							<p class="text-justify pb-2">Cedesarrollo agrupa sus programas en ESCUELAS de acuerdo al área de formación, ofertándolos a:</p>
						</div>
						<div class="col-md-6 text-center pb-3">
							<img src="{{asset('img/educacion-programas-tecnico-laborales.jpg')}}" class="img-fluid rounded" alt="">
						</div>
					</div>
				</div>	
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-6">
					<img class="rounded img-fluid mb-3" src="{{ asset('img/educacion-bachilleres-educacion.jpg') }}" alt="">
					<span class="d-block h5 text-blue-2 pb-2">a) Bachilleres y Trabajadores</span>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Duración: 3 semestres</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Jornadas: Diurna y Nocturna y sábados.</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 pt bgmedia d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Niveles de inglés exigido: Competencia en Nivel A1</p>
					  	</div>
					</div>
				</div>
				<div class="col-md-6">
					<img class="rounded img-fluid mb-3" src="{{ asset('img/educacion-estudiantes.jpg') }}" alt="">
					<span class="d-block h5 text-blue-2 pb-2">b) Estudiantes de grado 10° de colegios de la ciudad</span>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Duración: 4 semestres. Los estudiantes Realizan, 2 semestres en grado 10° y 2 semestres en grado 11°</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Jornadas: Un día de la semana en contra jornada y sábados.</p>
					  	</div>
					</div>
					<br>
					<div class="pl-2 pr-2 media d-flex justify-content-center align-items-center">
						<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
						<div class="media-body">
					    	<p class="mb-0">Niveles de inglés exigido: Competencia en Nivel A1.</p>
					  	</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pb-4 pt-4">		
				<div class="col-md-12" id="p_programas">
					<br>
					<h3 class="d-inline-block text-white bg-blue-2 pb-3 pt-3 pl-5 pr-5 rounded">PROGRAMAS</h3>
					<br><br>
					<span class="pl-2 d-block text-blue-2 h4 mb-3">ESCUELA INDUSTRIAL</span>
					<div class="row m-0">
						<div class="col-lg-4 col-md-6 mb-3">
							<a data-target="#educacion" id="p_procesamiento_industrial" href="#nav-dropdown14"><img class="img-fluid rounded" src="{{ asset('img/educacion-industrial.png') }}" alt=""></a>
						</div>	
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_seguridad_ocupacional" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-seguridad-ocupacional.png') }}" alt=""></a>
						</div>
						<div id="p_control_de_calidad" class="col-lg-4 col-md-6 mb-3">
							<a href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-control-calidad.png') }}" alt=""></a>
						</div>
					</div>
					<br><br>
					<span class="text-uppercase pl-2 d-block text-blue-2 h4 mb-3">Escuela de Administración y Finanzas</span>
					<div class="row m-0">
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_comercio_exterior" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-comercio-exterior.png') }}" alt=""></a>
						</div>	
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_procesos_logisticos" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-procesos-logisticos.png') }}" alt=""></a>
						</div>
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_auxiliar_contable_y_financiero" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-auxiliar-contable.png') }}" alt=""></a>
						</div>
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_auxiliar_administrativo" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-auxiliar-administrativo.png') }}" alt=""></a>
						</div>	
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_mercadeo_y_ventas" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-mercadeo-ventas.png') }}" alt=""></a>
						</div>
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_auxiliar_de_talento_humano" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-talento-humano.png') }}" alt=""></a>
						</div>
					</div>
					<br><br>
					<span class="text-uppercase pl-2 d-block text-blue-2 h4 mb-3">Escuela de Informática y Tecnología</span>
					<div class="row m-0">
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_sistemas" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-sistemas.png') }}" alt=""></a>
						</div>	
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_mantenimiento_de_hardware_y_redes" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-hardware-redes.png') }}" alt=""></a>
						</div>
					</div>
					<br><br>
					<span class="text-uppercase pl-2 d-block text-blue-2 h4 mb-3">Escuela de Hotelería y Turismo</span>
					<div class="row m-0">
						<div class="col-lg-4 col-md-6 mb-3">
							<a id="p_servicios_hoteleros_y_turisticos" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-servicios-hoteleros.png') }}" alt=""></a>
						</div>	
						{{-- <div class="col-lg-4 col-md-6 mb-3">
							<a id="p_tecnico_laboral_auxiliar_convenciones_y_eventos" href="#"><img class="img-fluid rounded" src="{{ asset('img/educacion-tecnico-laboral.png') }}" alt=""></a>
						</div> --}}
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

