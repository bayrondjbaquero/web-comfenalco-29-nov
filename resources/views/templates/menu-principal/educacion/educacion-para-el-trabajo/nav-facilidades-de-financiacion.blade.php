<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-12">
					<h3 class="text-uppercase text-blue-2 pb-2 rounded">Facilidades de Financiación</h3>
					<img src="{{asset('img/educacion-financiacion.png')}}" class="img-fluid rounded mb-4" alt="">
					<div class="col-md-8 bg-gray-3 p-0">
						<div class="d-flex justify-content-around rounded align-items-center bg-orange-2 pt-3 pb-3 pl-5 pr-5">
							<img class="img-fluid" src="{{ asset('img/peso.png') }}" alt="">
							<p class="text-justify text-white">El valor de su matrícula: Cedesarrollo Comfenalco brinda a sus estudiantes diferentes<br> opciones de financiación y formas de pago. A continuación enumeramos las diferentes<br> opciones:</p>
						</div>
						<div class="pl-3 pr-3 pt-2 pb-2">
							<p class="text-justify">Pago en efectivo: Si elige esta opción debe dirigirse al Banco Davivienda, BBVA, O AV Villas con el volante de matrícula, enviado al correo, impreso en láser.</p>
							<p class="text-justify">Tarjetas de crédito o débito: Se reciben en los puntos de atención de Comfenalco: Centro de atención al estudiante CAE, CIS Matuna, bocagrande, Centro empresarial, y almacenes de cadena; Éxito ejecutivos y Sao san Felipe. Aceptamos todas las tarjetas que pertenezcan a las franquicias: TARJETA CUPOCRÉDITO DE COMFENALCO, Visa, Mastercard, Maestro y American Express</p>
						</div>
					</div>
					<br>
					<div class="col-md-10 bg-gray-3 p-0 mb-2">
						<div class="d-flex justify-content-around rounded align-items-center bg-green-2 pt-3 pb-3 pl-5 pr-5">
							<img class="d-inline-block img-fluid" src="{{ asset('img/tarjeta.png') }}" alt="">
							<p class="text-justify text-white">Acceso a crédito con instituciones financieras: El estudiante puede hacer créditos directos con la entidad<br>financiera con la cual tenga relación, entre las cuales tenemos: </p>
						</div>
						<div class="pl-3 pr-3 pt-2 pb-2">
							<div class="media">
								<img class="mr-3" src="{{ asset('img/check-yellow.png') }}" alt="">
								<div class="media-body">
									<p class="text-justify">Crédito Educativo por libranza Comfenalco: Línea dirigida para financiar los estudios de Educación Formal y Superior (Pregrados, Diplomados, Seminarios, Especializaciones y Maestrías).</p>
									<p>1. Financiación del 100% del valor de la matrícula.<br>
									2. Plazo: Hasta 48, según el programa.<br>
									3. Monto Sujeto a capacidad de pago del afiliado<br>
									4. Tasa de Interés de acuerdo a la categoría del afiliado<br>
									Requisitos:<br>
									1. Fotocopia del documento de identidad ampliado al 150%<br>
									2. Carta laboral con fecha de expedición no mayor a 30 días<br>
									3. Los dos últimos desprendibles de nómina.<br>
									4. Formulario de crédito totalmente diligenciado<br>
									5. Copia de recibo de matrícula.</p>
								</div>
							</div>
							<div class="media">
								<img class="mr-3" src="{{ asset('img/check-yellow.png') }}" alt="">
								<div class="media-body">
									<p class="text-justify">Itaú Educativo: Anteriormente banco Helm Bank; Cuenta con una oficina en las instalaciones de Cedesarrollo ubicada en el centro de atención al estudiante y ofrece las siguientes opciones:</p>
									<p>· Financiación hasta el 100% del valor de la matrícula<br>
										· Financiación entre 3 y 12 meses dependiendo del valor a financiar<br>
										· Resultado del estudio inmediato</p>
								</div>
							</div>
							<div class="media">
								<img class="mr-3" src="{{ asset('img/check-yellow.png') }}" alt="">
								<div class="media-body">
									<p>Banco Pichincha: Cuenta con una oficina en las instalaciones de Cedesarrollo ubicada en el centro de atención al estudiante y ofrece las siguientes opciones:
										- Aprobación inmediata
										- Plazo de 3 a 12 meses dependiendo del valor a financiar.
										- Solicitud de crédito.
										- Fotocopia de cédula ampliada al 150%.
										- Volante de matrícula</p>
								</div>
							</div>
							<div class="media">
								<img class="mr-3" src="{{ asset('img/check-yellow.png') }}" alt="">
								<div class="media-body">
									<p class="text-justify">Crédito Brilla: Sin estudios de crédito, sin fiadores, sin cuota inicial, en 6 cómodas cuotas. Solo debes presentar tu cédula como titular del servicio de gas, con 2 facturas recientes de SURTIGAS. Para mayor información marcar 164 o al 01 8000 910164 e ingresar www.surtigas.com.co y seguir las instrucciones. La cual cuenta con una oficina en las instalaciones de Cedesarrollo ubicada en admisiones</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

