<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-logros-y-reconocimientos.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex mb-3 justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">LOGROS Y RECONOCIMIENTOS</h3>
					<p class="pb-2 text-justify">La Ciudad Escolar Comfenalco, gradúa anualmente un promedio de 320 estudiantes. Actualmente se cuenta con la categoría A+, otorgada por el ICFES gracias a los resultados de las Pruebas Saber 11. Universidades a nivel local, nacional e internacional, destacan el rendimiento de los egresados de la CEC, reconociendo su alto nivel académico y competitivo:</p>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Concurso Nacional del Cuento RCN 2016-2017, Gerónimo Díaz Castilla, estudiante de cuarto primaria obtuvo el 1° puesto a nivel nacional en la categoría 1 (primero a tercero de primaria).</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Menciones de Honor "Norma Concurso 2016 de Literatura Infantil y Juvenil", Transición, 22 Distinciones; Primaria, 16 Distinciones; Bachillerato 33 Distinciones.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Juegos Intercolegiados - Supérate Distrital 2016; 50 medallas de oro, 42 de plata y 27 de bronce.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Medallería Juegos Supérate Intercolegiados 2016; 24 medallas de oro en levantamiento de pesas, 11 en ajedrez y 10 en atletismo.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Concurso Internacional Mercosur 2016, 3 ganadoras.</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Obtención de becas, Roble Amarillo Universidad del Norte, Talento Caribe U. Tecnológica Bolívar, Ser Pilo Paga y Universidad de los Andes.</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Distinción medalla de plata Universidad del Norte, Facultad de Ingenierías por la excelencia académica de los egresados 2014, 2015, 2016.</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Distinción medalla de plata Universidad Tecnológica de Bolívar, por la excelencia académica de los egresados 2014, 2015, 2016.</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">83 Admitidos en la Universidad de Cartagena en el 2017</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">32 Distinciones Andrés Bello.</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">La distinción SIMÓN BOLÍVAR, que otorga el Ministerio de Educación nacional a las mejores instituciones educativas del país. Esta distinción se la otorgó el MEN a la CEC, en dos oportunidades.</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Ruta Quetzal BBVA: representación por Colombia a nivel internacional dos veces consecutivas.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Campeones de los Juegos Intercolegiados en 31 años consecutivos.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Orquesta Filarmónica Juvenil de Comfenalco acompañó en concierto al gran Intérprete tenor Placido Domingo en presentación en la ciudad de Cartagena.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Orquesta Big Band acompañó a Justo Almario en la conmemoración de los 100 años de Lucho Bermúdez.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Concurso de Artes Plásticas realizados en la Ciudad de Troyes, Francia, auspiciado por la UNESCO. En este importante evento quedó seleccionado la obra artística de un estudiante quién ocupó el octavo puesto en su categoría y fue el único Latinoamericano en recibir este reconocimiento.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Participación en el concurso de la NASA, Aventura Espacial Barranquilla 2009, merecedores de representar a Colombia, en un Evento Internacional que se celebra en TAILANDIA.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Campeones de Patinaje en el Centroamericano.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Mundial patinaje de Cartagena, campeones de la válida de la World Cup en Cartagena, seleccionados posteriormente para representar a Colombia para el mundial de CHINA.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Reconocimiento Nacional de Ministerio de Cultura a la Escuela de Música de Comfenalco por su extraordinaria labor de una década.</p>
						  	</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>