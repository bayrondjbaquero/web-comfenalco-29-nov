<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-doble-certificacion.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">DOBLE CERTIFICACIÓN</h3>
					<p class="text-justify">Comfenalco, a través de su Centro para el Trabajo y el Desarrollo Humano, CEDESARROLLO, desarrolló un programa para que los estudiantes en los grados 9 y 10, de los colegios públicos y privados, pudieran graduarse con dos títulos: Bachiller y técnico Laboral.</p>
					<p class="text-justify">La Ciudad Escolar pensando en la deserción actual de los colegios y garantizando mejores resultados en las pruebas de Saberes ICFES, emprendió desde el año 2010, la socialización del modelo de doble certificación que cuenta con los programas técnicos laborales.</p>
					<p class="text-justify">La duración de cada uno de estos programas es de cuatro (4) semestres, que son articulados con carreras tecnológicas y profesionales, lo que garantiza la obtención de 3 títulos en menos tiempo y la posibilidad de tener un mejor futuro.</p>
					<p class="text-justify">En la actualidad se cuenta con convenio con Universidades para la articulación en las carreras técnico laborales.</p>
					<p class="text-justify">Comfenalco financia la educación de los hijos de los trabajadores a través de las líneas de crédito en libre Inversión, Educación o Cupocrédito. Igual que con las formas de pago reconocidas en el mercado.</p>
				</div>
				<div class="col-md-12 mt-2">
					<span class="d-block text-blue-2 h4 pb-2 pl-3">Beneficios del modelo educativo</span>
					<div class="row m-0">
						<div class="col-md-12 mb-3">
							<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
								<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Mejor nivel académico para la presentación de pruebas de estado, pruebas en universidades públicas y ganar becas de prestigiosas universidades.</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 mb-3">
							<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
								<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Ambiente de estudio de nivel universitario, lo que permite al estudiante estar adaptado al hacer su transición hacia la universidad</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 mb-3">
							<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
								<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Contar con conocimiento previo de la línea de formación de su preferencia, lo que permite disminuir la posibilidad de deserción universitaria.</p>
							  	</div>
							</div>
						</div>
						<div class="col-md-12 mb-3">
							<div class="pl-2 pr-2 pt-4 pb-4 bg-gray-3 rounded media d-flex justify-content-center align-items-center">
								<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
								<div class="media-body">
							    	<p class="mb-0">Mejora de los niveles de responsabilidad de los estudiantes</p>
							  	</div>
							</div>
						</div>
					</div>
					<a href="#" id="myHref"><img class="img-fluid rounded d-block m-auto" src="{{ asset('img/educacion-consulta-aqui.jpg') }}" alt=""></a>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>


