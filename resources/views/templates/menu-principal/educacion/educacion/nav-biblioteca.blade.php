<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-biblioteca.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 d-flex mb-3 justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">BIBLIOTECA</h3>
					<p class="text-justify">La Biblioteca Luis Carlos López, es una moderna edificación de 4 niveles ubicada en la Ciudad Escolar Comfenalco – Zaragocilla, que ofrece a todos los afiliados espacios cómodos y especializados para la consulta y la investigación. En ella, podrá encontrar los servicios de sala de junta, sala virtual y de lectura.</p>
					<p class="text-justify">Las colecciones están organizadas en estantes estratégicamente ubicados para el fácil acceso de los usuarios debidamente catalogadas y clasificadas de acuerdo a normas internacionales.<br>Para la población estudiantil se cuenta con 13.000 libros entre obras literales de autores locales, nacionales e internacionales, clásicos o contemporáneos, textos escolares, obras ágiles de consulta, enciclopedias, diccionarios, manuales, libros de recetas, autoayuda, material audiovisual, entre otros.</p>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="h4 text-blue-2 d-block pb-2 pl-3">INFRAESTRUCTURA</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">4 puestos de consulta</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-fuxia.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Biblioteca Virtual (20 equipos)</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green-3.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">2 equipos para ver películas y escuchar música</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-red-2.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Sala de lectura para acceso libre y directo con los libros, circulación y préstamo con capacidad para 80 personas.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-orange-2.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Áreas de lectura en grupo</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-pink.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Puesto de lectura individual</p>
						  	</div>
						</div>
						<br>	
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-blue.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Cubículos para lecturas</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-green-2.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Rincón literario</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow-2.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Internet inalámbrico</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-blue-2.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Hemeroteca: períodicos y revistas locales, regionales y nacionales</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-red.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Acerbo bibliográfico: ofrece acceso a las base de datos PRO, Legiscomex, Ebsco, igualmente a video, CD y DVD.</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-fuxia.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Un espacio de recepción, sistema de seguridad bibliográfica y plazoleta de la cultura.</p>
						  	</div>
						</div>		
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>