<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-informacion.jpg')}}" class="img-fluid rounded" alt="">
				</div>
				<div class="col-md-6 mb-3">
					<h3 class="text-blue-2 pb-2">Modelo Pedagógico Holístico, Investigativo Y Transformador (MHIT)</h3>
					<p class="text-justify">Nuestra institución educativa forma bajo el modelo holístico investigativo transformador, pedagógico definido para educar de manera especial a los estudiantes de la CEC, como acción de mejoramiento continuo de nuestro proceso educativo.</p>
					<p class="text-justify">El modelo fundamenta su proceso de desarrollo en la investigación como eje central, acompañado de los Núcleos inciertos y las preguntas ambiguas que se complementan con los estándares de competencias y el desarrollo de una forma intencionada de habilidades y destrezas de pensamientos, que le dan al modelo una estructura contemporánea, acorde con la generación que se está formando. De igual forma el modelo direcciona el proceso formativo del estudiante al aprendizaje autónomo, puesto que el desarrollo de los procesos investigativos los lleva a tomar decisiones en todo momento.</p>
				</div>
				<div class="col-12 text-center mb-5">
					<span class="d-block pb-2 h4 text-blue-2 text-center">Criterios Pedagógicos del Modelo MHIT</span>
					<img src="{{ asset('img/educacion-programas.png') }}" alt="" class="img-fluid">
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<span class="text-blue-2 pb-2 d-block h4">DESARROLLANDO COMPETENCIAS INVESTIGATIVAS</span>
					<p class="text-justify">Actualmente se adelantan 14 proyectos de aula, desde pre-jardín hasta grado 11, en donde todas las asignaturas, de forma integrada, juegan un papel importante para el cumplimiento de los objetivos del proyecto, presentando al final del año los resultados del grado en los diferentes niveles.</p>
					<p class="text-justify">Como apoyo para mejorar la enseñanza y promover mejores ambientes de aprendizaje, en la CEC se han tenido importantes adquisiciones en tecnología educativa, entre las que se cuentan:</p>
					<p class="text-justify"><span class="text-yellow">•</span> 3 laboratorios Lego (Robótica), un adelanto en innovación educativa. Mejora el desarrollo del pensamiento lógico matemático, desarrollo de la inteligencia espacial, habilidades comunicativas, pensamiento científico y tecnológico.</p>
					<p class="text-justify"><span class="text-yellow">•</span> 25 aulas amigas dotadas con tecnología que permite hacer del proceso de enseñanza un ambiente interactivo en el cual el estudiante tiene la oportunidad de aprender de una manera más didáctica y más amigable con la tecnología.</p>
				</div>
				<div class="col-md-6 mb-3 text-center mt-2">
					<img src="{{ asset('img/educacion-competencias.jpg') }}" alt="" class="img-fluid rounded">
				</div>
				<div class="col-md-6 mb-3 text-center mt-2">
					<img src="{{ asset('img/educacion-bienestar-estudiantil.jpg') }}" alt="" class="img-fluid rounded">
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<span class="text-blue-2 pb-2 d-block h4">COORDINACIÓN DE PSICOLOGÍA Y BIENESTAR ESTUDIANTIL</span>
					<p class="text-justify">Esta coordinación apoya los procesos pedagógicos y tiene como objetivo principal, proporcionar bienestar integral al estudiante o comunidad educativa, padres de familia y docentes. Cuenta con 3 especialistas en psicología clínica y una especialista en psicología social para la atención de los casos que presenten dificultad comportamental, académico o social, que puedan afectar el ambiente académico del estudiante.</p>
					<p class="pb-2 text-justify">Así mismo dentro del programa de bienestar estudiantil se destacan las áreas de:</p>
					<div class="d-flex justify-content-center text-center flex-wrap pb-2">
						<figure class="col"><img src="{{ asset('img/enfermeria.png') }}" alt=""><figcaption>Enfermería</figcaption></figure>
						<figure class="col"><img src="{{ asset('img/primeros-auxilios.png') }}" alt=""><figcaption>Primeros auxilios</figcaption></figure>
						<figure class="col"><img src="{{ asset('img/capellania.png') }}" alt=""><figcaption>Capellanía</figcaption></figure>
					</div>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="text-bold h4 text-blue-2 d-block pb-2 pl-3">PROGRAMAS DE DESARROLLO HUMANO, CON LA PARTICIPACIÓN DE ESTUDIANTES LÍDERES</span>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Proyecto de Educación sexual y competencias ciudadanas</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Proyecto prevención consumo de drogas y alcohol</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Proyecto prevención del maltrato y abuso sexual infantil</p>
						  	</div>
						</div>
						<br>
						<div class="media d-flex justify-content-center align-items-center">
							<img class="d-flex mr-2" src="{{ asset('img/check-yellow-3.png') }}" alt="">
							<div class="media-body">
						    	<p class="mb-0">Proyecto Prevención de accidentes</p>
						  	</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<div class="pl-2 pr-2 pt-4 pb-4 bg-white rounded">
						<span class="text-bold h4 text-blue-2 d-block pb-2 pl-3">PROGRAMAS DE DESARROLLO HUMANO, CON LA PARTICIPACIÓN DE ESTUDIANTES LÍDERES</span>
						<p class="text-justify">Este servicio beneficia a los estudiantes nuevos y antiguos ingresando a tiempo a las instalaciones de la institución sin riesgo alguno y facilitando al padre de familia el transporte diario. Cuenta con 29 rutas que cubre gran parte de la zona de la ciudad.</p>
						<p class="text-justify">El servicio de Transporte Escolar es opcional y se otorga de acuerdo a la disponibilidad de cupos y zonas donde regularmente se preste el servicio.</p>
					</div>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<img class="pb-3 rounded img-fluid" src="{{ asset('img/educacion-deportes.jpg') }}" alt="">
					<span class="h4 d-block pb-2 text-blue-2">DEPORTES</span>
					<p class="text-justify">El colegio, a través del grupo de profesores especializados en el área de educación física, ha obtenido grandes logros que incluyen campeonatos a nivel local, nacional e internacional, en disciplinas como el fútbol y el béisbol, lo que ha llevado a la Institución a que desarrolle programas especiales dentro de las aulas que son complementados con las Escuelas de Formación Deportiva y Club Deportivo en el área de Deportes. Contamos con pequeñas ligas de Béisbol.</p>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<img class="pb-3 rounded img-fluid" src="{{ asset('img/educacion-artes-plasticas.jpg') }}" alt="">
					<span class="h4 d-block pb-2 text-blue-2">ARTES PLÁSTICAS</span>
					<p class="pb-2 text-justify">La Ciudad Escolar dentro de su formación académica, destaca las artes plásticas como elemento fundamental de la educación integral del estudiante, contando con:</p>
					<p><strong class="text-yellow">·</strong> Orquesta Filarmónica<br>
						<strong class="text-yellow">·</strong> Tuna y Coral<br>
						<strong class="text-yellow">·</strong> Banda Marcial<br>
						<strong class="text-yellow">·</strong> Orquesta Tropical<br>
						<strong class="text-yellow">·</strong> Grupo de Danzas Folklóricas<br>
						<strong class="text-yellow">·</strong> Sala especializada en artes plásticas y escénicas.</p>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<img class="pb-3 rounded img-fluid" src="{{ asset('img/educacion-gobierno-escolar.jpg') }}" alt="">
					<span class="h4 d-block pb-2 text-blue-2">GOBIERNO ESCOLAR</span>
					<p class="text-justify">El Gobierno Nacional a través del artículo 142 de la ley general de educación (ley 115) y el decreto reglamentario 1860 en su artículo 29, establece la organización del gobierno escolar en todas las Instituciones, permitiendo la participación democrática de todos los estamentos; estudiantes, profesores, directivos, padres de familia y sectores productivos de la comunidad en la dirección de las instituciones educativas.</p>
					<p class="text-justify">El Gobierno Escolar de la C.E.C., articula los procesos administrativos, académicos y de proyección comunitaria, dirime conflictos, gestiona y decide lo pertinente al "Ser y al quehacer" educativo de la Institución. La conformación del gobierno escolar en la C.E.C., permite generar procesos participativos, democráticos y autónomos. Los artículos 143, 144, 145 de la ley General de Educación y los artículos 18,19, 20, 21 del decreto 1860 son la guía que nos ha permitido direccional este proceso.</p>
				</div>
				<div class="col-md-6 mb-3 mt-2">
					<img class="pb-3 rounded img-fluid" src="{{ asset('img/educacion-pei.jpg') }}" alt="">
					<span class="h4 d-block pb-2 text-blue-2">PEI</span>
					<p class="text-justify">El P.E.I. de la CIUDAD ESCOLAR COMFENALCO, es un instrumento que provee y organiza acciones administrativas, de planeación y técnico – pedagógicas para resolver necesidades o expectativas de la comunidad educativa enmarcadas en la política y disposiciones legales nacionales, departamentales y municipales y de la Caja de Compensación Familiar COMFENALCO.</p>
				</div>
			</div>	
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>			
	</div>
</div>