<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado en Gerencia de Proyectos</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Generar en los participantes el fortalecimiento de competencias necesarias para el desempeño como gerentes o integrantes de un equipo de desarrollo de proyectos aplicando herramientas y habilidades a partir de una base teórica de conocimientos, con el propósito de incrementar la eficiencia en su gestión y en la de la organización.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Todo público y en especial a profesionales de diferentes disciplinas que estén interesadas y relacionadas con el tema, preferiblemente con conocimientos y experiencias básicas en el área de proyectos.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_gp.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-content-start flex-wrap">
						<img class="mr-2 mt-2" src="{{ asset('img/educacion-d_fdp.png') }}" alt="">
						<img class="mr-2 mt-2" src="{{ asset('img/educacion-d_fdp1.png') }}" alt="">
						<img class="mr-2 mt-2" src="{{ asset('img/educacion-d_gdp2.png') }}" alt="">
						<img class="mr-2 mt-2" src="{{ asset('img/educacion-d_cdp.png') }}" alt="">
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>