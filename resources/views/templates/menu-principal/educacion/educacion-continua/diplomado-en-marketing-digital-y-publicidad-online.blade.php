<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado en Marketing Digital y Publicidad OnLine</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Formar personas innovadoras y altamente capacitadas para planificar, desarrollar y ejecutar estrategias de publicidad on line y marketing digital, que permitan posicionar a las organizaciones en audiencias segmentadas.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Profesionales o futuros profesionales que desean fortalecer su actividad dentro del marketing digital y nuevas estrategias de comunicación. Directores de las áreas de Mercadeo y Comunicación. Personal responsable de las áreas de internet, tecnología o e- marketing, responsables de publicidad on line, ventas y negocios, comercio electrónico Profesionales de agencias y consultoras interactivas. Gerentes de empresas, y emprendedores.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_mdypo.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-items-start flex-column">
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m1.png') }}" alt=""><p class="mb-0">Entorno Digital Y Tendencias En Marketing</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m2.png') }}" alt=""><p class="mb-0">Contexto Ético Y Legal Del Comercio Electrónico Y El Marketing Digital</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m3.png') }}" alt=""><p class="mb-0">Plan De Marketing Digital</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m4.png') }}" alt=""><p class="mb-0">Marketing Digital Y Publicidad On Line</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m5.png') }}" alt=""><p class="mb-0">Las Redes Sociales Y Herramientas De Medición Y Análisis De Resultados</p></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>