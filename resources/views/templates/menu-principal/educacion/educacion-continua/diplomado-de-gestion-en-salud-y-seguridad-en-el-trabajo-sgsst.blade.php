<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>	
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado Sistemas de Gestión en Salud y Seguridad en el Trabajo (SGSST)</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Proporcionar las bases suficientes a los participantes en lo relacionado al Sistema HSEQ, para que puedan hacer abordaje, análisis y brindar soluciones integrales a las situaciones presentadas en sus sitios de trabajo.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Profesionales de diferentes disciplinas, estudiantes de último semestre de carreras afines, técnicos, tecnólogos y público en general interesados en profundizar estos conocimientos.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_sgsst.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-items-start flex-column">
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m1.png') }}" alt=""><p class="mb-0">Sistema de gestión de seguridad y salud en el trabajo</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m2.png') }}" alt=""><p class="mb-0">Salud en el trabajo</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m3.png') }}" alt=""><p class="mb-0">Seguridad en el trabajo y medio ambiente</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m4.png') }}" alt=""><p class="mb-0">Higiene en el trabajo</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m5.png') }}" alt=""><p class="mb-0">Epidemiologia y vigilancia</p></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>