<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado en Liquidación de Nómina y Prestaciones Sociales</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Desarrollar en los participantes completo conocimiento en torno a toda la normatividad vigente del Código Sustantivo del Trabajo que regula los procesos de liquidación de nómina y prestaciones sociales en Colombia, a través de talleres prácticos que permitan consolidar competencia en la materia.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Directores, coordinadores, auxiliares, asistentes de personal que se desempeñan o tienen relación con el área de nómina y prestaciones sociales en las organizaciones, profesionales de diferentes áreas y público en general interesadas en el tema.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_ldnyps.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-items-start flex-column">
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m1.png') }}" alt=""><p class="mb-0">Los contratos laborales</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m2.png') }}" alt=""><p class="mb-0">Liquidación de nómina</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m3.png') }}" alt=""><p class="mb-0">Retención en la fuente sobre ingresos laborales</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m4.png') }}" alt=""><p class="mb-0">Obligaciones de seguridad social</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m5.png') }}" alt=""><p class="mb-0">La liquidación de prestaciones sociales</p></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>