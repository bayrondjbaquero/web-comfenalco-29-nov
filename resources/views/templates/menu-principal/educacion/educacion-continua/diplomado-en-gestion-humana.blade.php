<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado en Gestión Humana</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Conocer y comprender los procesos que desde la creatividad e innovación pueden generar un desarrollo organizacional.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Brindar a los participantes los elementos necesarios que les permita tener una visión general de la Gestión de Personal en la Organización, teniendo en cuenta los cambios del entorno; fortaleciendo el profesionalismo e idoneidad del personal de los procesos de Talento Humano en el presente y hacia el futuro, y promoviendo el liderazgo para influir en el alcance de los resultados propuestos.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_gh.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-items-start flex-column">
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m1.png') }}" alt=""><p class="mb-0">Direccionamiento estratégico y tendencias de la gestión del talento humano</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m2.png') }}" alt=""><p class="mb-0">Selección y vinculación de las personas bajo el modelo de competencias</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m3.png') }}" alt=""><p class="mb-0">Formación y desarrollo de las personas en el modelo de competencias</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m4.png') }}" alt=""><p class="mb-0">Valoración de cargos y administración de salarios</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m5.png') }}" alt=""><p class="mb-0">Gestión de las relaciones laborales y legales</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m6.png') }}" alt=""><p class="mb-0">Valoración del desempeño y evaluación del impacto de los procesos de gestión humana</p></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>