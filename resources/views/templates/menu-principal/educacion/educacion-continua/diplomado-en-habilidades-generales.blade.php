<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado en Habilidades Gerenciales</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Suministrar a los participantes los fundamentos administrativos y conceptos básicos gerenciales que les permitan desarrollar sus habilidades necesarias para mejorar en los procesos de toma de decisiones y afrontar los diferentes retos y situaciones dentro y fuera de sus organizaciones, en búsqueda de la creación de valor mediante la toma de decisiones acertadas en alineación con la visión estratégica de sus empresa.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Profesionales de las diferentes áreas del conocimiento que desean ampliar su perfil. Gerentes, jefes de área, líderes y sus correspondientes equipos en las diferentes unidades estratégicas de las organizaciones. Gerentes, directores y líderes de procesos que tengan bajo su responsabilidad procesos de toma de decisiones que impacten de manera significativa en los resultados organizacionales.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_hg.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-items-start flex-column">
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m1.png') }}" alt=""><p class="mb-0">Desarrollo de personal e imagen profesional</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m2.png') }}" alt=""><p class="mb-0">Negociación y manejo de conflictos</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m3.png') }}" alt=""><p class="mb-0">Liderazgo y creatividad en la toma de decisiones</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m4.png') }}" alt=""><p class="mb-0">Finanzas para no financieros</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m5.png') }}" alt=""><p class="mb-0">Balanced scorecard</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m6.png') }}" alt=""><p class="mb-0">Integración y desarrollo de equipos de trabajo</p></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>