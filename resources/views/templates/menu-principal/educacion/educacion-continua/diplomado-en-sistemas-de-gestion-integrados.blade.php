<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">
				<div class="col-md-12 pb-3">
					<div class="float-right"><a class="educacion-continua-back link-hover pr-3" href=""><img class="pr-1" src="{{ asset('img/back.png') }}" alt="">REGRESAR A EDUCACIÓN CONTINUA</a></div>
				</div>
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-3 text-capitalize">Diplomado en Sistemas de Gestión Integrados</h3>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Objetivo</span>
					<p class="text-justify">Capacitar a los participantes en los diferentes conceptos generales y particulares correspondientes a los sistemas integrales de gestión relacionados con su diseño, implementación, evaluación y mejora, enfocados a la adquisición de las metas organizacionales acordes con las políticas corporativas, de acuerdo con los lineamientos de las normas NTC ICONTEC ISO 9001:2015, ISO 14001:2015, OHSAS 18001:2007.</p>
					<span class="text-blue-2 pb-2 d-block h4 mb-0">Dirigido a</span>
					<p class="text-justify">Profesionales de diferentes disciplinas estudiantes de último semestre de carreras afines, técnicos, tecnólogos y público en general interesados en profundizar estos conocimientos.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/d_sdgi.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>
			<div class="row m-0 pb-2">
				<div class="col-md-12 mb-3">
					<span class="text-blue-2 pb-2 d-block h4">Temas a desarrollar</span>
					<div class="d-flex align-items-start flex-column">
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m1.png') }}" alt=""><p class="mb-0">Introducción Sistemas Integrados de Gestión</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m2.png') }}" alt=""><p class="mb-0">Sistema de Gestión de Calidad</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m3.png') }}" alt=""><p class="mb-0">Sistema de Gestión Ambiental</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m4.png') }}" alt=""><p class="mb-0">Sistema de Gestión en Salud y Seguridad en el Trabajo – Auditorías Sistemas de Gestión</p></div>
						<div class="d-flex align-items-center pb-3"><img class="pr-3" src="{{ asset('img/educacion_d_m5.png') }}" alt=""><p class="mb-0">Prácticas de Auditoria</p></div>
					</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>