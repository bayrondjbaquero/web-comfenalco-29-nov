<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">CAPACITACIONES EMPRESARIALES</h3>
					<p class="text-justify">Capacitaciones diseñadas de acuerdo a las necesidades establecidas en el plan de formación o de desarrollo de personal estructurado por la empresa, brindando así, un apoyo para el cumplimiento del mismo, ya que se diseñan a la medida de las necesidades de cada empresa, con capacitadores altamente entrenados en cada línea de formación.</p>
					<p class="text-justify">Estos son algunos de los beneficios de la capacitación empresarial:</p>
					<p class="text-justify"><span class="text-yellow">•</span> Aumenta el rendimiento laboral de los empleados.<br>
						<span class="text-yellow">•</span> Empleados felices y eficientes<br>
						<span class="text-yellow">•</span> Incentiva la labor de todas las áreas de la compañía.<br>
						<span class="text-yellow">•</span> Permite alinear los objetivos de la empresa con todas las áreas.<br>
						<span class="text-yellow">•</span> Mejora el clima organizacional.<br>
						<span class="text-yellow">•</span> Favorece la competitividad empresarial.<br>
						<span class="text-yellow">•</span> Aumenta la satisfacción de los empleados.</p>
					<p class="text-justify">El recurso humano es el motor de las empresas. Pensar en el desarrollo personal y laboral de un equipo de trabajo, es pensar en el bienestar de la compañía y en el futuro del negocio.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-capacidades-empresariales.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>	
			<div class="row m-0 pb-4">	
				<div class="col-md-12 pb-3">
					<span class="d-block h4 text-blue-2 pb-2 text-uppercase">Nuestra Oferta</span>
			
					<div class="w-100 collapse-down" id="a_capacidades_empresariales" role="tablist">
					  <div class="card">
					    <div class="card-header" role="tab" id="headingOne">
					      <h5 class="mb-0">
					        <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Capacitaciones Blandas<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>

					    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#a_capacidades_empresariales">
					      <div class="card-body p-4">
					      		<p class="text-bold">Capacitaciones Blandas</p>
						        <div class="d-flex justify-content-start">
									<p class="pr-3">Compromiso<br> 
									Autoconfianza<br>
									Autocontrol<br> 
									Autoaprendizaje<br>
									Organización en el Trabajo<br>
									Relaciones Interpersonales<br>
									Adaptación al Cambio<br>
									Actitud de servicio<br>
									Comunicación<br>
									Creatividad e Innovación.<br>
									Liderazgo</p>
								<p>Trabajo en equipo.<br>
									Aceptación de normas.<br>
									Resolución de problemas.<br>
									Toma de Decisiones.<br>
									Tolerancia al trabajo bajo presión.<br>
									Aprendiendo a manejar adecuadamente el síndrome de Burnout (Manejo del Estrés).<br>
									Coaching.<br>
									Programación Neurolingüística PNL.</p>
							</div>
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="headingTwo">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Cursos Especializados<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#a_capacidades_empresariales">
					      <div class="card-body p-4">
							<p><span class="text-yellow pr-2">•</span>	Técnicas de ventas - No es vender por vender<br> 
								<span class="text-yellow pr-2">•</span>	Cómo dirigir un equipo de ventas campeón<br> 
								<span class="text-yellow pr-2">•</span>	Cómo Realizar un cierre de venta exitoso<br>
								<span class="text-yellow pr-2">•</span>	Orden y aseo (Filosofía de las 5 s)<br> 
								<span class="text-yellow pr-2">•</span>	Manejo integral de residuos<br> 
								<span class="text-yellow pr-2">•</span>	Gestión de ventas efectivas<br> 
								<span class="text-yellow pr-2">•</span>	Estrategias para mantener un buen clima laboral<br> 
								<span class="text-yellow pr-2">•</span>	Administración estratégica del Talento Humano<br> 
								<span class="text-yellow pr-2">•</span>	Actualización de pruebas Psicotécnicas<br> 
								<span class="text-yellow pr-2">•</span>	Cultura de responsabilidad tributaria<br> 
								<span class="text-yellow pr-2">•</span>	Últimas tendencias en Centros de entrenamientos (Assesment Center)<br> 
								<span class="text-yellow pr-2">•</span>	Como Realizar un cierre de venta exitoso<br> 
								<span class="text-yellow pr-2">•</span>	Seminario taller en control y seguimiento de proveedores<br> 
								<span class="text-yellow pr-2">•</span>	Formación e implementación de políticas y gestión efectiva de facturación<br> 
								<span class="text-yellow pr-2">•</span>	MECI GP 1000<br> 
								<span class="text-yellow pr-2">•</span>	Gestión efectiva de Quejas y reclamos<br> 
								<span class="text-yellow pr-2">•</span>	Seminario taller en gestión y herramientas contables<br> 
								<span class="text-yellow pr-2">•</span>	Actualización en políticas contables<br> 
								<span class="text-yellow pr-2">•</span>	Seminario taller en control y seguimiento de proveedores<br> 
								<span class="text-yellow pr-2">•</span>	Manejo básico de extintores<br> 
								<span class="text-yellow pr-2">•</span>	Actualización nuevos requisitos normativos 9001: 2015<br> 
								<span class="text-yellow pr-2">•</span>	Curso - Taller Práctico Implementación NIIF<br> 
								<span class="text-yellow pr-2">•</span>	Curso en Atención al Cliente<br> 
								<span class="text-yellow pr-2">•</span>	Curso Básico en BPM<br> 
								<span class="text-yellow pr-2">•</span>	Curso Básico en Trabajo en Altura <br>
								<span class="text-yellow pr-2">•</span>	Curso Avanzado en Trabajo en Altura<br> 
								<span class="text-yellow pr-2">•</span>	Rescate en Altura<br> 
								<span class="text-yellow pr-2">•</span>	Curso Básico Obligatorio para Conductores de Vehículos de Carga que Transportan Mercancías Peligrosas<br> 
								<span class="text-yellow pr-2">•</span>	Actualización e Reforma Tributaria<br> 
								<span class="text-yellow pr-2">•</span>	Fidelización de clientes y atención a PQR<br> 
								<span class="text-yellow pr-2">•</span>	Legislación básica de seguridad industrial <br>
								<span class="text-yellow pr-2">•</span>	Logística en organización de almacenes o inventarios<br> 
								<span class="text-yellow pr-2">•</span>	Finanzas para no financieros<br> 
								<span class="text-yellow pr-2">•</span>	Técnicas de ventas - No es vender por vender<br> 
								<span class="text-yellow pr-2">•</span>	Gestión eficiente de compras<br> 
								<span class="text-yellow pr-2">•</span>	Capacitación en deportes<br> 
								<span class="text-yellow pr-2">•</span>	Control y seguimiento de clientes<br> 
								<span class="text-yellow pr-2">•</span>	Legislación básica en gestión ambiental<br> 
								<span class="text-yellow pr-2">•</span>	Manejo de estrategias y medios de comunicación interna<br> 
								<span class="text-yellow pr-2">•</span>	Curso de Liquidación de Nómina y Prestaciones Sociales<br> 
								<span class="text-yellow pr-2">•</span>	Entrevistas efectivas bajo el modelo de competencias<br> 
								<span class="text-yellow pr-2">•</span>	Servicio al cliente como factor diferenciador en una organización<br> 
								<span class="text-yellow pr-2">•</span>	Manejo De Inventarios Y Almacenes<br> 
								<span class="text-yellow pr-2">•</span>	Elaboración de indicadores de calidad<br> 
								<span class="text-yellow pr-2">•</span>	Mantenga su flujo de caja al día<br> 
								<span class="text-yellow pr-2">•</span>	Evaluación de alternativas de inversión.</p>			        
					      </div>
					    </div>
					  </div>
				  	</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

