<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">DIAGNÓSTICO EMPRESARIAL Y CONSULTORÍAS</h3>
					<p class="text-justify">Realizar estudios de diagnóstico que permitan determinar las necesidades de formación del personal de la empresa, como las oportunidades de mejora de los procesos de las diferentes áreas de la organización, así como la implementación de los mismos; enfocada al mejoramiento de procesos en las áreas Gerencial, Administrativa, Comercial, Financiera, Talento Humano, y Servicios; brindando soluciones a sus necesidades de fortalecimiento corporativo en direccionamiento Estratégico, Financiero, Organizacional y Comercial. Además de efectuar acompañamiento en la implementación de las soluciones y en la evolución de logros.</p>
					<p class="text-justify">Contamos con consultoría en:</p>					
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-diagnostico-empresarial.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>	
			<div class="row m-0 pb-4">	
				<div class="col-md-12 pb-3">
					<div class="w-100 collapse-down" id="diag1" role="tablist">
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc1">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc01" aria-expanded="false" aria-controls="deyc01">NORMA ISO 9001:2015 – 14001:2015 - OSHA 18001<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc01" class="collapse show" role="tabpanel" aria-labelledby="deyc1" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">La gestión eficaz de un sistema de calidad permite conseguir unos clientes fidelizados; el cumplimiento de la legislación laboral y de prevención de riesgos laborales, así como una gestión justa y ética de los recursos humanos propiciará a unos trabajadores cualificados y motivados; mientras que la calidad medioambiental permitirá que la sociedad se vea favorecida por un trato de las personas como responsables con el medio ambiente, Todo ello, conjuntamente, permite una innovación, mejora y aprendizaje continuos, de modo que se produce un desarrollo empresarial sostenible.</p>        
					      </div>
					    </div>
					  </div>
						<div class="card">
					    <div class="card-header" role="tab" id="deyc2">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc02" aria-expanded="false" aria-controls="deyc02">NORMAS TÉCNICAS SECTORIALES<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc02" class="collapse" role="tabpanel" aria-labelledby="deyc2" data-parent="#diag1">
					      <div class="card-body p-4"> 
					      	<p class="text-justify">La implementación consiste en que el prestador analice cada uno de los requisitos que establece la Norma Técnica Sectorial de Turismo Sostenible que le corresponde, le aplique y documente cada una de las evidencias  para dar cumplimiento. El prestador debe cumplir con todos los requisitos establecidos en la Norma que le aplique para renovar su registro nacional de turismo (RNT).</p>      
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc3">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc03" aria-expanded="false" aria-controls="deyc03">BUSINESS ALLIANCE FOR SECURE COMMERCE - BASC<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc03" class="collapse" role="tabpanel" aria-labelledby="deyc3" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">Esta norma está destinada a ayudar a las organizaciones en el desarrollo de una propuesta de Gestión en Control y Seguridad en el Comercio Internacional, que proteja a las empresas, a sus empleados y otras personas cuya seguridad puedan verse afectadas por sus actividades. Muchas de las características de una administración efectiva no se pueden distinguir de las prácticas propuestas de administración de calidad y excelencia empresarial.</p>    
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc4">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc04" aria-expanded="false" aria-controls="deyc04">SISTEMA DE GESTIÓN DE SEGURIDAD Y SALUD EN EL TRABAJO<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc04" class="collapse" role="tabpanel" aria-labelledby="deyc4" data-parent="#diag1">
					      <div class="card-body p-4">        
					      	<p class="text-justify">Un sistema de gestión de la salud y la seguridad en el trabajo (SGSST) fomenta los entornos de trabajo seguros y saludables al ofrecer un marco que permite a la organización identificar y controlar coherentemente sus riesgos de salud y seguridad, reducir el potencial de accidentes, apoyar el cumplimiento de las leyes y mejorar el rendimiento en general.<br>Actualmente la implementación de este sistema esta soportado bajo el decreto 1072 de 2015 requisito legal y obligatorio para todas las empresas.</p>
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc5">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc05" aria-expanded="false" aria-controls="deyc05">REGISTRO ÚNICO PARA CONTRATISTA – RUC<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc05" class="collapse" role="tabpanel" aria-labelledby="deyc5" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">Lograr el desarrollo armónico de los programas de Seguridad Industrial, Salud Ocupacional y Ambiente, a través de un proceso de evaluación que permita dinamizar e impulsar el mejoramiento continuo en la gestión, desarrollada por las empresas contratistas, para asegurar el cumplimiento de los requisitos legales y de otra índole.</p>
			      	       	<p class="text-justify">El RUC, fue diseñado para ser implementado en empresas contratistas del sector hidrocarburos y de otros sectores contratantes, con el objetivo principal de impulsar el desempeño de sus contratistas</p>       
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc6">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc06" aria-expanded="false" aria-controls="deyc06">BUENAS PRÁCTICAS DE MANUFACTURA – BPM<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc06" class="collapse" role="tabpanel" aria-labelledby="deyc6" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">Las buenas prácticas de manufactura tienen por principal objetivo evitar la ocurrencia o recurrencia de errores. Se trata de prever situaciones que pueden causar problemas en las operaciones estableciendo una dinámica operacional consistente mediante el uso de procedimientos estándares de operación.<br>La gerencia es responsable de asegurar, adoptar e implementar todas las medidas razonables y precauciones necesarias para el cumplimiento de las BPM.<br>Esta responsabilidad incluye:</p> 
					      	<p class="text-justify">
					      		<span class="text-yellow pr-2">•</span>El Control de Enfermedades de transmisión Alimentaría (ETA), aseo e higiene del personal.<br>
								<span class="text-yellow pr-2">•</span>Educación y capacitación del personal en manipulación de alimentos y otras áreas de interés para mantener la salud pública<br>
								<span class="text-yellow pr-2">•</span>Asignación de los recursos económicos, materiales y humanos<br>
								<span class="text-yellow pr-2">•</span>Supervisión y cumplimiento</p>
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc7">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc07" aria-expanded="false" aria-controls="deyc07">PLAN ESTRATÉGICO VIAL LEY 1503 DE 2011<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc07" class="collapse" role="tabpanel" aria-labelledby="deyc7" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">Es un documento de planificación, que contiene las acciones, mecanismos, estrategias y medidas que debe realizar una empresa, de forma que incida efectivamente en la accidentalidad vial. Es decir, que la obliga a pensar, planear y trazar el mapa de acciones, incluyendo tiempos, recursos y actores concretos con el fin de reducir las tasas de accidentalidad de tránsito.<br>Este documento debe registrarse ante el organismo de tránsito que corresponde a la jurisdicción donde se encuentra la empresa, la Alcaldía Municipal o si son de orden nacional, ante la Superintendencia de Puertos y Transportes.<br><br><em>El Artículo 12 de la Ley 1503 de 2011, establece que "Toda entidad, organización o empresa del sector público o privado que para cumplir sus fines misionales a en el desarrollo de sus actividades posea, fabrique, ensamble, comercialice, contrate, o administre flotas de vehículos automotores a no automotores superiores a diez (10) unidades, o contrate o administre personal de conductores, contribuirán al objeto de la presente Ley".</em></p>    
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc8">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc08" aria-expanded="false" aria-controls="deyc08">CLIMA ORGANIZACIONAL<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc08" class="collapse" role="tabpanel" aria-labelledby="deyc8" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">Teniendo en cuenta que el clima laboral es el medio en el que se desarrolla el trabajo cotidiano, y que la calidad de este clima influye directamente en la satisfacción de los trabajadores y por lo tanto en la productividad, informamos que nuestro proceso implica la medición de los siguientes aspectos:</p>      
					      	<p class="text-justify"> Comunicación entre sus miembros<br>
								<span class="text-yellow pr-2">•</span>Relaciones interpersonales<br>
								<span class="text-yellow pr-2">•</span>Liderazgo y estilos de mando<br>
								<span class="text-yellow pr-2">•</span>Trabajo en equipo (Cooperación/colaboración)<br> 
								<span class="text-yellow pr-2">•</span>Manejo del estrés o trabajo bajo presión<br>
								<span class="text-yellow pr-2">•</span>Manejo de conflicto y conflicto de intereses<br>
								<span class="text-yellow pr-2">•</span>Solución de problemas<br>
								<span class="text-yellow pr-2">•</span>Condiciones de trabajo e instalaciones<br>
								<span class="text-yellow pr-2">•</span>Actitud ante los cambios<br>
								<span class="text-yellow pr-2">•</span>Orientación hacia el cliente<br>
								<span class="text-yellow pr-2">•</span>Satisfacción en el puesto de trabajo</p>
								<p class="text-justify">Entre otros aspectos generales. Estos aspectos se medirán desde la Gerencia, mandos medios y hasta los cargos de base u operativos.<br>Todo este proceso arrojara un diagnóstico de cómo se está comportando la cultura empresarial y gracias a esto se podrá cotejar con los directivos que aspectos están facilitando o detrimentando los resultados generales de la empresa.</p>
								<p class="text-justify">Con base en esto se diseñara toda una estrategia de intervención basada en diferentes metodologías de desarrollo, entre tantas se pueden mencionar las más usadas:</p>
								<p class="text-justify">
								<span class="text-yellow pr-2">•</span>Formación grupal<br>
								<span class="text-yellow pr-2">•</span>Formación individual<br> 
								<span class="text-yellow pr-2">•</span>Coaching individual<br>
								<span class="text-yellow pr-2">•</span>Tutoriales ya sea de forma indoor (en aula tradicional) o outdoor (fuera o al aire libre).</p>
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc9">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc09" aria-expanded="false" aria-controls="deyc09">PLANEACIÓN ESTRATÉGICA<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc09" class="collapse" role="tabpanel" aria-labelledby="deyc9" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">Comprende el análisis de clientes, análisis del sector competitivo, tendencias y poder de negociación de proveedores del sector, tendencias entorno socioeconómico, análisis FODA, análisis de las políticas de  financieras de inversión, apalancamiento, endeudamiento y asesoría de planeación estratégica enfocado al desarrollo financiero, mercadeo y clientes.<br><strong>Actividades de asesoría</strong><br>
							<span class="text-yellow pr-2">•</span>Asesoría en Análisis Organizacional<br>
							<span class="text-yellow pr-2">•</span>Asesoría Planeación Estratégica</p>    
					      </div>
					    </div>
					  </div>
					  <div class="card">
					    <div class="card-header" role="tab" id="deyc10">
					      <h5 class="mb-0">
					        <a class="collapsed" data-toggle="collapse" href="#deyc010" aria-expanded="false" aria-controls="deyc010">AUDITORIA INTERNA<img class="float-right" src="{{ asset('img/arrow-down.png') }}" alt=""></a>
					      </h5>
					    </div>
					    <div id="deyc010" class="collapse" role="tabpanel" aria-labelledby="deyc10" data-parent="#diag1">
					      <div class="card-body p-4">
					      	<p class="text-justify">La auditoría se trata de un examen metódico que se realiza para determinar si las actividades y resultados relativos satisfacen las disposiciones previamente establecidas y que realmente se llevan a cabo, además de comprobar que son adecuadas para alcanzar los objetivos propuestos.<br><strong>Objetivo de la Auditoria Interna:</strong><br><span class="text-yellow pr-2">•</span>Comprobar la adecuación de los elementos del Sistema de Gestión a auditar con los requisitos especificados en la norma de referencia<br><span class="text-yellow pr-2">•</span>Aportar información para la mejora continua del Sistema de Gestión Integrado.</p>
					      </div>
					    </div>
					  </div>
				  	</div>
				</div>
			</div>
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

