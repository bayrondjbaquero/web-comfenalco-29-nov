<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-12 m-auto pt-4 justify-content-md-center">
			<div class="row m-0 pb-4">		
				<div class="col-md-6 d-flex justify-content-center flex-column">
					<h3 class="text-blue-2 pb-2">FOMENTO EMPRESARIAL</h3>
					<p class="text-justify">Su caja de Compensación Familiar COMFENALCO – CARTAGENA cuenta con el Programa de fortalecimiento profesional y empresarial, el cual se encarga de desarrollar actividades dirigidas al fortalecimiento de las empresas afiliadas y no afiliadas de la región, mediante la oferta de servicios que contribuyen al mejoramiento de su productividad y competitividad.</p>
					<p class="text-justify">Nuestro portafolio de servicios, se fundamenta en ofrecer servicios empresariales y diagnósticos y consultorías, soportados por un equipo humano altamente capacitado, competente y comprometido.</p>
				</div>
				<div class="col-md-6 text-center pb-3">
					<img src="{{asset('img/educacion-fomento-empresarial.jpg')}}" class="img-fluid rounded" alt="">
				</div>
			</div>		
			<div class="row m-0 pt-2">
				<div class="col-md-11 mx-auto">
					<br><br>
					@include('templates.menu-principal.formulario-contacto.contacto')
				</div>
			</div>
		</div>
	</div>
</div>

