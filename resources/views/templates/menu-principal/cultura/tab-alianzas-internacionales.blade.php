<div class="container bg-gray-2">
    <div class="row pb-3 pt-3">
        <div class="col-md-11 m-auto pt-4 justify-content-md-center">
            <div class="row m-0 pb-4">
                <div class="col-md-6">
                    <div class="d-flex justify-content-center flex-column h-100">
                        <h3 class="text-blue-2 pb-2">ALIANZAS INTERNACIONALES</h3>
                        <p class="text-justify text-bold">Northwestern State University of Louisiana. USA. (NSULA)</p>
                        <p class="text-justify">Ubicada en Natchitoches, Louisiana, la Universidad Northwestern State mantiene, desde 2012, una estrecha relación con el Servicio de Cultura, permitiendo, mediante un sistema de becas, que más de 110 estudiantes de la Escuela de Música y Danza de Comfenalco, se encuentren actualmente adelantando estudios profesionales en música, danza y/o en la carrera que desean.</p>
                        <p class="text-justify">Los requisitos para ser candidato a beca son los siguientes:<br>
                            <span class="text-yellow pr-2 pb-2">•</span>Ser afiliado a Comfenalco<br>
                            <span class="text-yellow pr-2 pb-2">•</span>Hacer parte de la Escuela de Música o Danza de Comfenalco<br>
                            <span class="text-yellow pr-2 pb-2">•</span>Tener un dominio instrumental o dancístico de alto nivel<br>
                            <span class="text-yellow pr-2 pb-2">•</span>Compromiso con las convocatorias y presentaciones organizadas por la Gerencia de Cultura<br>
                            <span class="text-yellow pr-2 pb-2">•</span>Cumplir con los deberes contemplados en el reglamento y manual de convivencia de las Escuelas</p>
                    </div>
                </div>
                <div class="col-md-6 text-center pb-3">
                    <img src="{{asset('img/cultura-alianzas-internacionales.jpg')}}" class="img-fluid rounded" alt="Alianzas internacionales">
                </div>                
                <div class="col-md-11 mx-auto">
                    <br><br>
                    @include('templates.menu-principal.formulario-contacto.contacto')
                </div>     
            </div>
        </div>          
    </div>
</div>