<div class="container bg-gray-2">
    <div class="row pb-3 pt-3">
        <div class="col-md-11 m-auto pt-4 justify-content-md-center">
            <div class="row m-0 pb-4">
                <div class="col-md-6">
                    <div class="d-flex justify-content-center flex-column h-100">
                        <h3 class="text-blue-2 pb-2">FOMENTO A LA CULTURA</h3>
                        <p class="text-justify">La Coordinación de Fomento Cultural tiene como objetivo gestionar, garantizar, promover y divulgar programas, proyectos, eventos y alianzas que permitan el desarrollo de las artes, el patrimonio y la cultura en Cartagena y la Región. Los servicios a los que puede tener acceso desde esta coordinación, afiliados y no afiliados a la caja, son los siguientes:</p>
                    </div>
                </div>
                <div class="col-md-6 text-center pb-3">
                    <img src="{{asset('img/fomento a la cultura.jpg')}}" class="img-fluid rounded" alt="Fomento a la Cultura">
                </div>
                <div class="col-md-12 pt-3">
                    <span class="h4 text-blue-2 d-block pb-2">1. Programa de Danza</span>
                    <p class="pb-3">El programa busca desarrollar aptitudes dancísticas y rítmicas que estimulen la expresión corporal y el manejo de la amplitud del movimiento y el rango articular, que permitan generar expresiones culturales que garanticen el mejoramiento de la calidad de vida de afiliados y no afiliados.<br>El programa cuenta con una amplia sede, ubicada en Zaragocilla, la cual tiene abierta sus puertas para ofrecerle cursos de:</p>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Bailes De Salón: chachachá, tango, merengue, bachata, salsa, samba, mambo</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Bailes Tradicionales: cumbia, mapale, currulao, joropo, puya, bambuco.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Danza Urbana: break dance, hip-hop, reggaeton, zumba, entre otras.</p>
                        </div>
                    </div>
                    <div class="media pb-2 d-flex align-items-center">
                        <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">Comparsas: Folclóricas y modernas</p>
                        </div>
                    </div>
                    <p class="text-justify">De igual forma, ofrecemos cursos In Situ, para aquellas empresas y trabajadores que no puedan trasladarse hasta nuestras instalaciones y deseen recibir sus clases en la misma empresa, el horario será determinado por ustedes.</p>
                </div>
                <div class="col-md-12">
                    <div class="row m-0">
                        <div class="col-md-6 pb-md-4 mb-2">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <span class="h4 text-blue-2 d-block pb-2">2. Programa de Danza y Folclor para Niños y Jóvenes en condición de discapacidad cognitiva</span>
                                <p class="text-justify">El programa busca desarrollar habilidades musicales y dancísticas en la población de niños y jóvenes en condición de discapacidad cognitiva, quienes formarán grupos de Comparsas y de música folclórica, que sin duda ayudarán al fortalecimiento psicomotriz.</p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center pb-4 mb-2">
                            <img src="{{asset('img/cultura-danza-folclor.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-6 text-center pb-3">
                            <img src="{{asset('img/cultura-eventos.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <span class="h4 text-blue-2 d-block pb-2">3. Organización de Eventos Culturales</span>
                                <p class="text-justify">Con el fin de brindar a nuestra población objeto un amplio portafolio, la unidad de cultura ofrece la organización, asesoría, promoción, gestión y realización de eventos culturales, los cuales podrán servir a nuestras empresas afiliadas y no afiliadas para darle a sus trabajadores y familias una oportunidad de conocer el mundo del arte, la música y la cultura.</p>
                            </div>
                        </div>
                        <div class="col-md-6 pb-md-4 mb-2">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <span class="h4 text-blue-2 d-block pb-2">4. Danza, teatro y Cultura</span>
                                <p class="text-justify">Ofrecemos espectáculos de danza y puesta en escena de diferentes ritmos y obras teatrales, con carácter tradicional caribeño, internacional, clásico y popular, todo para que las familias, trabajadores y empresas vean en la cultura una buena forma de invertir su tiempo libre, procurando nutrir el alma a través del arte.</p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center pb-4 mb-2">
                            <img src="{{asset('img/cultura-danza-teatro.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-6 text-center pb-4 mb-2">
                            <img src="{{asset('img/cultura-asesoria.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-6 pb-md-4 mb-2">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <span class="h4 text-blue-2 d-block pb-2">5. Asesoría y montajes de espectáculos</span>
                                <p class="text-justify">Ofrecemos un acompañamiento permanente en el desarrollo artístico de grupos musicales y de danzas, conformados por los equipos de trabajo de cada una de las empresas afiliadas, de la mano de nuestros colaboradores con el estándar de calidad reconocido.</p>
                            </div>
                        </div>
                         <div class="col-md-6 pb-md-4 mb-2">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <span class="h4 text-blue-2 d-block pb-2">6. Cultura y Empresa</span>
                                <p class="text-justify">En el marco de la RSE ofrecemos a las empresas afiliadas este producto que busca sensibilizar a los empresarios para que vean en la cultura un elemento generador de valor, fundamental para el crecimiento y el desarrollo sostenible, en temas como la innovación, creatividad y bases de las manifestaciones culturales.</p>
                                <p>Nuestros programas son:<br><span class="text-yellow pr-2">•</span>Historia Empresarial de Cartagena<br><span class="text-yellow pr-2">•</span>Cultura y RSE</p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center pb-4 mb-2">
                            <img src="{{asset('img/cultura y empresa.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-11 mx-auto">
                            <br><br>
                            @include('templates.menu-principal.formulario-contacto.contacto')
                        </div>
                    </div>
                </div>     
            </div>
        </div>          
    </div>
</div>