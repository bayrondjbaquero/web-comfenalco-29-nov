<div class="container bg-gray-2">
    <div class="row pb-3 pt-3">
        <div class="col-md-11 m-auto pt-4 justify-content-md-center">
            <div class="row m-0 pb-4">
                <div class="col-md-6">
                    <div class="d-flex justify-content-center flex-column h-100">
                        <h3 class="text-blue-2 pb-2">ESCUELA DE MÚSICA</h3>
                        <p class="text-justify">La Escuela de Música de Comfenalco es un laboratorio idóneo para desarrollar las habilidades musicales de afiliados, no afiliados y población vulnerable, a través de un programa sistemático; una infraestructura de punta, con salones insonorizados y salas de ensayos; y un talento humano altamente calificado y especializado, que busca desarrollar destrezas auditivas (melódicas y rítmicas) y motoras, a través de la ejecución e interpretación de un instrumento musical de la mano de la experiencia colectiva con el ensamble en una agrupación, desde las primeras etapas del proceso formativo artístico.</p>
                    </div>
                </div>
                <div class="col-md-6 text-center pb-3">
                    <img src="{{asset('img/cultura-escuela-musica.jpg')}}" class="img-fluid rounded" alt="Escuela de música">
                </div>
                <div class="col-md-12 pt-3">
                    <span class="h4 text-blue-2 d-block pb-2">Cursos de Instrumento</span>
                    <p class="pb-3">Ofrecemos una amplia variedad de cursos, con capacidad para 6 u 8 personas por clase, lo que permite una atención personalizada.</p>
                </div>
                <div class="col-md-12 p-0 pb-3">
                    <div class="d-flex justify-content-xs-center justify-content-md-start text-center flex-wrap">
                        <div class="row m-0">
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-flauta.png')}}" alt="Flauta">
                                <figcaption>Flauta</figcaption>
                            </figure>                
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-oboe.png')}}" alt="Oboe">
                                <figcaption>Oboe</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-clarinete.png')}}" alt="Clarinete">
                                <figcaption>Clarinete</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-fagot.png')}}" alt="Fagot">
                                <figcaption>Fagot</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-corno.png')}}" alt="Corno">
                                <figcaption>Corno</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-trompeta.png')}}" alt="Trompeta">
                                <figcaption>Trompeta</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-trombon.png')}}" alt="Trombón">
                                <figcaption>Trombón</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-percusion.png')}}" alt="Percusión">
                                <figcaption>Percusión</figcaption>
                            </figure>
                             <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-guitarra.png')}}" alt="Guitarra">
                                <figcaption>Guitarra</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-piano.png')}}" alt="Piano">
                                <figcaption>Piano</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-violin.png')}}" alt="Violín">
                                <figcaption>Violín</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-viola.png')}}" alt="Viola">
                                <figcaption>Viola</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-violonchelo.png')}}" alt="Violonchelo">
                                <figcaption>Violonchelo</figcaption>
                            </figure>
                            <figure class="col-md-2 col-sm-4 col-xs-6">
                                <img class="img-fluid" src="{{asset('img/cultura-contrabajo.png')}}" alt="Contrabajo">
                                <figcaption>Contrabajo</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>     
                <div class="col-md-12">
                    <span class="h4 text-blue-2 d-block pb-2">Horarios de atención</span>
                    <p><span class="text-yellow pr-2">•</span>Jóvenes de 14 en adelante - Martes y Jueves: de 6:00 a 06:45 pm<br><span class="text-yellow pr-2">•</span>Niños de 7 a 13 años -  Sábado: de 08:00 a 09:30 am y de 09:30 am - 11:00 am</p>
                    <br>
                    <p class="text-blue-2 h5 pb-3">La escuela de Música cuenta con 10 agrupaciones de alto nivel, formadas, en su mayoría, por niños y jóvenes entre los 5 y 17 años, a quienes se les ha inculcado el amor por la cultura, las artes y la música, estas son:</em></p>
                    <div class="row m-0">
                        <div class="col-md-6 pb-3">
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Orquesta Filarmónica</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Juvenil Orquesta</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Filarmónica Prejuvenil</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Big Band</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Banda Sinfónica</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pb-3">
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Folclor Tropical</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Charanga</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Cross Over</p>
                                </div>
                            </div>
                            <div class="media pb-2 d-flex align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-yellow.png') }}" alt="">
                                <div class="media-body">
                                    <p class="mb-0">Coro Infantil</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pb-md-4 mb-2">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <h4 class="text-blue-2 pb-2">PROGRAMA DE ADULTO MAYOR “ESCUCHA, TOCA, CANTA Y BAILA”</h4>
                                <p class="text-justify">El programa busca desarrollar en los adultos mayores habilidades musicales que permitan el esparcimiento cultural, iniciación artística, apreciación musical y dancística mediante una metodología teórico - práctica en el marco de Aprender – Haciendo.</p>
                                <p class="text-justify">Duración: Sesiones de dos horas semanales durante 12 semanas, es decir, 24 horas trimestrales, luego de los cuales podrán interpretar y danzar en grupo varios arreglos musicales.</p>
                            </div>
                        </div>
                        <div class="col-md-6 text-center pb-4 mb-2">
                            <img src="{{asset('img/cultura-adulto-mayor.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-6 text-center pb-3">
                            <img src="{{asset('img/cultura-jornada-escolar.jpg')}}" class="img-fluid rounded" alt="">
                        </div>
                        <div class="col-md-6">
                            <div class="d-flex justify-content-center flex-column h-100">
                                <h4 class="text-blue-2 pb-2">JORNADA ESCOLAR COMPLEMENTARIA (JEC)</h4>
                                <p class="text-justify">La implementación de este programa se justifica en la importancia que tienen las actividades de formación complementaria, como un medio para resaltar el sentido de la convivencia sana y el aprovechamiento del tiempo libre, desarrollando elementos culturares que agrupan y crean el sentido de pertenencia colectiva, como un espacio de creatividad y expresión en beneficio del enriquecimiento personal y el amor por la vida, en forma individual y colectiva. La Escuela de Música ofrece en contra jornada un completo programa de música basados en una metodología lúdica, de investigación introspectiva vivencial, con un diseño de enseñanza holístico, humanístico y axiológico, sustentado en las neurociencias, las artes musicales, la nueva antropología histórica y la semiótica.</p>
                            </div>
                        </div>
                        <div class="col-md-11 mx-auto">
                            <br><br>
                            @include('templates.menu-principal.formulario-contacto.contacto')
                        </div>
                    </div>
                </div>     
            </div>
        </div>          
    </div>
</div>