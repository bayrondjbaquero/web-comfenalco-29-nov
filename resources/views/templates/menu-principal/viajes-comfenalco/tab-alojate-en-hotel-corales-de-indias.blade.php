<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">ALÓJATE EN HOTEL CORALES DE INDIAS</h3>
			<br>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column text-center">
					<img class="pb-1 img-fluid rounded" src="{{ asset('img/viajes-restaurante-manglares.jpg') }}" alt="Viajes comfenalco">
					<span class="h4 d-inline-block text-blue-2 pb-0">Restaurante Manglares:</span>
					<p class="pl-md-3 pr-md-3">un espacio para disfrutar de una deliciosa comida caribeña con un estilo formal y diversa oferta gastronómica.</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column text-center">
					<img class="pb-1 img-fluid rounded" src="{{ asset('img/viajes-cafeteria-atarraya.jpg') }}" alt="Viajes comfenalco">
					<span class="h4 d-inline-block text-blue-2 pb-0">Cafetería Atarraya:</span>
					<p class="pl-md-3 pr-md-3"> un espacio al aire libre, descompilado, acogedor y relajante, para disfrutar de un delicioso desayuno en la mañana, un almuerzo ligero hacia el mediodía, o una relajante comida, con la refrescante brisa marina que caracteriza al Caribe colombiano.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column text-center">
					<img class="pb-1 img-fluid rounded" src="{{ asset('img/viajes-bar-alcatraz.jpg') }}" alt="Viajes comfenalco">
					<span class="h4 d-inline-block text-blue-2 pb-0">Bar Alcatraz: </span>
					<p class="pl-md-3 pr-md-3">si quiere disfrutar de una noche cartagenera y deleitarse de una bebida o un refrescante coctel, este es el sitio ideal con vista al mar.</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column text-center">
					<img class="pb-1 img-fluid rounded" src="{{ asset('img/viajes-zonas-humedas.jpg') }}" alt="Viajes comfenalco">
					<span class="h4 d-inline-block text-blue-2 pb-0">Áreas Zonas Húmedas: </span>
					<p class="pl-md-3 pr-md-3">espacio que cuenta con baño turco y sauna.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column text-center">
					<img class="pb-1 img-fluid rounded" src="{{ asset('img/viajes-piscina.jpg') }}" alt="Viajes comfenalco">
					<span class="h4 d-inline-block text-blue-2 pb-0">Piscina: </span>
					<p class="pl-md-3 pr-md-3">cuenta con jacuzzi y dos piscinas, una para niños con juegos de agua interactivos, y una para adultos. Ambas con una espectacular vista al mar.</p>
				</div>
				<div class="col-md-6 d-flex align-items-center pb-4 flex-column text-center">
					<img class="pb-1 img-fluid rounded" src="{{ asset('img/viajes-gym.jpg') }}" alt="Viajes comfenalco">
					<span class="h4 d-inline-block text-blue-2 pb-0">Gimnasio</span>
					<p class="pl-md-3 pr-md-3"></p>
				</div>
			</div>
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





