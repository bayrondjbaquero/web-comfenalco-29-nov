<div class="container bg-gray-2">
	<div class="row pb-3">
		<div class="col-md-10 mx-auto pt-4">
			<h3 class="text-blue-2 pb-3">PLANEA TU VIAJE CON NOSOTROS</h3>
			<p class="text-justify">Plan todo incluido a destinos nacionales e internacionales <br>con precios preferenciales. Planes de crucero y tours.</p>
			<br>
			<div class="d-flex justify-content-center text-center flex-wrap pb-2">
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-asesorias.png')}}" alt="viajes comfenalco">
                    <figcaption>Asesoría para desarrollar su programa de viaje de acuerdo a sus expectativas y presupuesto.</figcaption>
                </figure>                
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-servicios-en-hoteles.png')}}" alt="viajes comfenalco">
                    <figcaption>Servicios en hoteles, hostales, fincas y centros recreacionales.</figcaption>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-cobertura-nacional.png')}}" alt="viajes comfenalco">
                    <figcaption>Cobertura nacional e internacional para la colocación de tiquetes.</figcaption>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-asesoria-y-agilidad.png')}}" alt="viajes comfenalco">
                    <figcaption>Asesoría y agilidad en el trámite de visas.</figcaption>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-transporte-privado.png')}}" alt="viajes comfenalco">
                    <figcaption>Transporte privado de tipo ejecutivo y para grupos en van o buses de turismo para que se desplace de manera más cómoda, segura y ágil en la realización de sus visitas de turismo.</figcaption>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-programas-de-turismo.png')}}" alt="viajes comfenalco">
                    <figcaption>Amplia variedad de programas de turismo receptivo, excursiones y pasadías a destinos de interés de los afiliados, y con precios preferenciales.</figcaption>
                </figure>
                <figure class="col-lg-3 col-md-6">
                    <img class="img-fluid" src="{{asset('img/viajes-acompanamiento.png')}}" alt="viajes comfenalco">
                    <figcaption>Acompañamiento permanente a grupos de 20 personas en adelante.</figcaption>
                </figure>
                <figure class="col-lg-3 col-md-6">
                </figure>
            </div>			
			<br><br>
			@include('templates.menu-principal.formulario-contacto.contacto')
		</div>			
	</div>
</div>





