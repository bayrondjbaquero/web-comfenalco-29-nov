@extends('layouts.main')

@section('title')Empresas @endsection

@section('styles')
@endsection

@section('content')
	<div id="afiliado" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-12 pb-2 pt-2">
					<h2 class="text-blue text-center">EMPRESAS</h2>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-orange text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/empresas-afiliate.png')}}" alt="afiliado money">
							<figcaption class="pt-2">Afilia a los trabajadores de tu<br> empresa y conoce todos los<br> beneficios que obtienen.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('afiliaciones') }}">Haz clic aquí</a>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-orange-2 text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/empresas-subsidio-familiar.png')}}" alt="afiliado deporte">
							<figcaption class="pt-2">Consulta el subsidio familiar<br> que reciben tus empleados<br> afiliados a comfenalco.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('deportes') }}">Haz clic aquí</a>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-green text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/empresas-credito-libranza.png')}}" alt="afiliado escenarios">
							<figcaption class="pt-2">Encuentra cómo tus<br> empleados pueden acceder a<br> los créditos de libranza.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('deportes') }}">Haz clic aquí</a>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-purple text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/empresas-consultas.png')}}" alt="afiliado home">
							<figcaption class="pt-2">Consulta toda la información<br> sobre tu empresa afiliada a<br> comfenalco.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('vivienda') }}">Haz clic aquí</a>
					</div>
				</div>
				<div class="col-xl-4 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-fucsia text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/empresas-proyectos-vivienda.png')}}" alt="afiliado musica">
							<figcaption class="pt-2">Descubre todos los proyectos de<br> vivienda ofrecidos Comfenalco y<br> postula a tus empleados al subsidio<br> de vivienda.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('cultura') }}">Haz clic aquí</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection