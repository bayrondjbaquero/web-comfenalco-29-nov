@extends('layouts.main')

@section('title')Mapa @endsection

@section('styles')
<style>
	iframe.i4ewOd-pzNkMb-haAclf {
	    background-color: #085ba4 !important;
	}
</style>
@endsection

@section('content')
    <div id="mymap" class="box-default pt-4 pb-4">
        <div class="container">
            <div class="row">
            	<div class="col-md-10 m-auto pb-4">
            		<h3 class="text-blue-2 pb-3 pt-2">MAPA COMFENALCO</h3>
        			<iframe src="https://www.google.com/maps/d/embed?mid=1aRavxi-XCRBGOaRn0T2OZSBoA5I" width="100%" height="480"></iframe>
            	</div>	
			</div>
        </div>
    </div>
@endsection

@section('scripts')
	<script>
		$('iframe .i4ewOd-pzNkMb-haAclf').css('background-color','#085ba4');
	</script>
@endsection

