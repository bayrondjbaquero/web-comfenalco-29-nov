@extends('layouts.main')

@section('title'){{$object->title}} @endsection

@section('styles')
@endsection

@section('content')
    <div id="informate" class="box-default pt-5 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('informate')}}" class="text-blue-2 h1 cursor-pointer"><i class="fa fa-tags" aria-hidden="true"></i> INFÓRMATE</a>
                </div>
                <div class="col-sm-9 col-xs-12">
                    <br>
                @forelse($objects as $blog)
                   <article class="category-article mb-3">
                        <div class="row m-0 border">
                            <div class="col-xs-12 col-md-3">
                                <img src="{{asset('blog_images/'.$blog->image)}}" style="max-height: 240px;" class="img-fluid rounded rounded-0" alt="{{$blog->alt}}">
                            </div>
                            <div class="col-xs-12 col-md-9">
                                <div class="metas">
                                    <img class="img-fluid d-inline-block text-red" src="{{ asset('img/calendar-icon.png') }}" alt=""> <span itemprop="datePublished" class="date d-inline-block">{{ ucfirst($blog->date_published->format('l, j \d\e F \d\e Y'))}}</span>
                                </div>
                                <a class="text-black" href="{{ route('detalle_de_post', $blog->slug) }}"><h4 class="text-bold pt-2">{{ ucfirst(mb_strtolower($blog->title))}}</h4></a>
                                @foreach($blog->categorys as $category)
                                    <p class="mb-0 h5 d-inline-block"><span class="badge bg-red h4">{{$category->name}}</span></p>
                                @endforeach
                                <div class="descripcion">
                                    @if($blog->resumen != null) {!! \Illuminate\Support\Str::words($blog->summmary,30) !!} @else <p> {!! \Illuminate\Support\Str::words(strip_tags($blog->body),30) !!}</p> @endif <a class="text-black text-bold float-left" href="{{ route('detalle_de_post', $blog->slug) }}">Leer más</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    <hr>
                @empty
                    <article class="category-article">
                    <br>
                    <div class="col-xs-12">
                        <span class="d-block h4">Lo sentimos, no encontramos entradas que figuren con la categoría: <strong>{{$object->name}}</strong>.</span>
                    </div>
                @endforelse
                </div>
                <div class="col-xs-12 col-sm-3">
                    <br> 
                    <span class="text-bold text-blue h4 d-block">CATEGORÍAS</span>
                    <ul class="list-unstyled pl-4">
                    @forelse($categories as $cat)
                        <li>
                            <div class="media d-flex justify-content-center align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-blue.png') }}" width="16" height="16" alt="">
                                <div class="media-body pt-1 pb-1">
                                    <p class="mb-0"><a class="text-blue text-black" href="{{route('posts_por_categoria',$cat->slug)}}">{{ucfirst(mb_strtolower($cat->name))}}</a></p>
                                </div>
                            </div>
                        </li>
                    @empty
                        <li>No hemos encontraron categorías</li>
                    @endforelse
                    </ul>
                </div>
			</div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection