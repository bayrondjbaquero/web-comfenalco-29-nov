@extends('layouts.main')

@section('title'){{$object->title}} @endsection

@section('styles')
<!-- twitter -->
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@ctg_comfenalco" />
<meta name="twitter:title" content="{{$object->title}}" />
<meta name="twitter:description" content="@if($object->summary != null) {!! str_limit($object->summary),100 !!} @else {!! \Illuminate\Support\Str::words(strip_tags($object->body),100) !!}@endif" />
<meta name="twitter:image" content="{{asset('blog_images/'.$object->image)}}" />
@endsection

@section('content')
    <div id="informate" class="box-default pt-5 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <article itemscope itemtype="http://schema.org/Article">
                        <h1 itemprop="name" class="text-bold text-blue-2">{{ ucfirst(mb_strtolower($object->title))}}</h1>
                        <div class="metas">
                            <img class="img-fluid d-inline-block text-red" src="{{ asset('img/calendar-icon.png') }}" alt=""> <span itemprop="datePublished" class="date d-inline-block">{{ ucfirst($object->date_published->format('l, j \d\e F \d\e Y'))}}</span>
                        </div>
                        {{-- <img itemprop="image" src="{{asset('blog_images/colombia-es-logistica.jpg')}}" class="img-responsive" alt="Centro informático"> --}}
                        <br>
                        <div itemprop="description" class="descripcion">{!! $object->body !!}</div>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false" data-size="large">Tweet</a>
                    </article>
                    <!-- comments -->
                    {{-- <div class="box-contact-white">
                        <i class="fa fa-comments d-inline-block color-orange" aria-hidden="true"></i> <span class="d-inline-block date h4">Comentarios</span>
                        <br>
                        @if($validate_commt != 0)  
                            @foreach($comments as $comment)
                                @if($comment->estado == App\Models\Comment::COMENTARIO_APROBADO)
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span class="card-title text-bold d-inline-block">{{$comment->name}}</span>  <span class="d-inline-block pull-right"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$comment->created_at->ago()}}</span>
                                        </div>
                                        <div class="panel-body">
                                            <p>{{$comment->comment}}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <p><em>Sin comentarios</em></p><br>
                        @endif
                            <hr>
                        @if(count($errors)!= 0)
                            <div class="alert alert-danger text-black" role="alert">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                            </div>
                        @endif
                        <br>
                        <span class="h4">Agrega un comentario</span>
                        <form action="{{ route('commentPost', $object->blog_id) }}" method="POST" id="myForm">
                            {{ csrf_field() }}
                            <div class="row m-0">
                                <div class="col-md-6 form-group">
                                    <input class="form-control" type="text" name="name" placeholder="Nombre">
                                </div>
                                <div class="col-md-6 form-group">
                                    <input class="form-control"  type="email" name="email" placeholder="Correo electrónico">
                                </div>
                                <div class="col-md-12 form-group">
                                    <textarea class="form-control"  name="comment" id="comentarios" cols="10" rows="7" placeholder="Comentario"></textarea>
                                    <div class="control-submit">
                                        <button class="btn-orange text-white" type="submit">Enviar <i class="fa fa-arrow-right text-white"></i></button>
                                    </div>
                                </div>
                            </div>  
                        </form>
                    </div> --}}
                </div>
                <div class="col-xs-12 col-xs-offset-1 col-sm-3">
                    <br> 
                    <span class="text-bold h4 d-block">CATEGORÍAS</span>
                    <ul class="list-unstyled pl-4">
                    @foreach($object->categorys as $cat)
                        <li>
                            <div class="media d-flex justify-content-center align-items-center">
                                <img class="d-flex mr-2" src="{{ asset('img/check-blue.png') }}" width="16" height="16" alt="">
                                <div class="media-body pt-1 pb-1">
                                    <p class="mb-0"><a class="text-blue" href="{{route('posts_por_categoria',$cat->slug)}}">{{ucfirst(mb_strtolower($cat->name))}}</a></p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                    <br>
                    <span class="text-bold h4 d-block">OTRAS ENTRADAS</span>
                    @forelse($recients as $recient)
                    @if($loop->iteration < 4)
                        <div class="recient">
                            <span class="text-white w-100 text-right pr-2 float-right bg-red _news-date text-bold">{{ ucfirst($recient->created_at->format('j \d\e F \d\e Y'))}}</span>
                            <img class="img-fluid" src="{{asset('blog_images/'.$recient->image)}}" alt="{{$recient->alt}}">
                            <a class="text-light text-blue h5" href="{{route('detalle_de_post', $recient->slug)}}">{{ucfirst(mb_strtolower($recient->title))}}</a>
                        </div>
                        <br>
                    @else
                        <p><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <a class="text-light text-black" href="{{route('detalle_de_post', $recient->slug)}}">{{ucfirst(mb_strtolower($recient->title))}}</a></p>
                    @endif
                    @empty
                        <p>No hay entradas para mostrar.</p>
                    @endforelse
                </div>
			</div>
        </div>
    </div>
@endsection

@section('scripts')
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
@endsection