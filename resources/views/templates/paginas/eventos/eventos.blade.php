@extends('layouts.main')

@section('title')Eventos @endsection

@section('styles')
@endsection

@section('content')
    <div id="informate" class="box-default pt-5 pb-4">
        <div class="container">
            <div class="row">
            	<div class="col-md-12">
                    <h1 class="text-blue-2 text-bold pb-1">EVENTOS</h1>
                    <br>
                    <div class="row m-0">
                    @forelse($objects as $event)
                        <div class="col-md-4">
                            <article class="card mb-3">
                                <div class="mx-auto" style="background-image: url('{{asset('event_images/'.$event->image)}}');background-repeat: no-repeat;background-size: 100% 100%;height:300px;width: 340px;"></div>
                                <div class="card-body p-3">
                                    <h4 class="card-title font-weight-bold text-blue-2">{{$event->title}}</h4>
                                    <p class="card-text">{!! \Illuminate\Support\Str::words(strip_tags($event->body),30) !!}</p>
                                    <p class="card-text"><small class="text-muted h7">{{ ucfirst($event->date_published->format('l, j \d\e F \d\e Y'))}}</small></p>
                                    <a class="badge badge-danger" href="{{ route('detalle_de_evento', $event->slug) }}"><span class="d-inline-block h6 mb-1 pl-2 pr-2">Ver más</span></a>
                                </div>
                            </article>
                        </div>
                    @empty
                        <div class="col-xs-12">
                            <span class="d-block h4">Lo sentimos, no encontramos eventos para mostrar.</span>
                        </div>
                    @endforelse
                    </div>
                </div>
			</div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection

