@extends('layouts.main')

@section('title')Servicios en línea @endsection

@section('styles')
@endsection

@section('content')
    <div id="servicios-en-linea" class="box-default pt-4 pb-4">
        <div class="container">
            <div class="row pb-4">
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue text-white">
						<img src="{{asset('img/servicios-afiliate.png')}}" alt="Servicios Afíliate Comfenalco">
						<div class="group-line d-flex flex-column">
							<a class="opacity-22 text-white" href="http://afiliate.comfenalco.com/" target="_blank">Afíliate Comfenalco</a>
							<a class="opacity-23 text-white d-flex justify-content-center align-items-center" href="http://afiliate.comfenalco.com/" target="_blank">Asamblea <br>(Estado de su empresa)</a>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue-5 text-white">
						<img src="{{asset('img/servicios-crediweb.png')}}" alt="Servicios Credi Web">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="https://crediweb.comfenalco.com/" target="_blank">Credi Web</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue text-white">
						<img src="{{asset('img/servicios-empleos.png')}}" alt="Servicios Empleos Comfenalco">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="http://www.empleoscomfenalco.com/" target="_blank">Empleos<br>Comfenalco</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue-5 text-white">
						<img src="{{asset('img/servicios-biblioweb.png')}}" alt="Servicios BiblioWeb">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="http://colegio.comfenalco.com:8444/Schoolweb/hweb300.aspx" target="_blank">BiblioWeb</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue text-white">
						<img src="{{asset('img/servicios-pago-proveedores.png')}}" alt="Servicios Consulta Pago de Proveedores">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="https://certificadotributario.comfenalco.com:8443/" target="_blank">Consulta Pago<br>de Proveedores</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue-5 text-white">
						<img src="{{asset('img/servicios-aportes.png')}}" alt="Servicios Pago de aportes planilla única">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="https://www.enlace-apb.com/interssi/.plus" target="_blank">Pago de aportes<br>planilla única</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue text-white">
						<img src="{{asset('img/servicios-politica-tratamiento.png')}}" alt="Servicios Política de tratamiento de pagos proveedores">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="http://www.comfenalco.com/mantenimiento/uploaded/PTD%20COMFENALCO%20%20PUBLICADA%20II.pdf" target="_blank">Política de tratamiento de datos</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue-5 text-white">
						<img src="{{asset('img/servicios-consulta.png')}}" alt="Servicios Consulta de afíliados">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="#" target="_blank">Datos de<br>afíliación</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue text-white">
						<img src="{{asset('img/serivicios-cec.png')}}" alt="Servicios Adminisiones CEC">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="#" target="_blank">Admisiones<br>CEC</a>
					</div>
				</div>
				<div class="col-sm-6 col-xl-3 col-lg-3 col-md-4 pb-3 pl-2 pr-2">
					<div class="serv align-items-center d-flex flex-column justify-content-between rounded text-center box-default bg-blue-5 text-white">
						<img src="{{asset('img/servicios-inscripcion.png')}}" alt="Servicios Inscripción Cedesarrollo">
						<a class="align-self-stretch d-flex justify-content-center align-items-center servicios-enlace opacity-2 text-white" href="https://www.q10academico.com/Preinscripcion?aplentId=8fa60f3a-1a89-4048-a798-afd5cda72549" target="_blank">Inscripción<br>Cedesarrollo</a>
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
