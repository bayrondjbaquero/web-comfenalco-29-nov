@extends('layouts.main')

@section('title')Organización @endsection

@section('styles')
@endsection

@section('content')
	<div id="organizacion" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-md-6 pb-2">
					<img class="img-fluid rounded" src="{{asset('img/deleted/organizacion-group.jpg')}}" alt="">
				</div>
				<div class="col-md-6">
					<h3 class="text-red">ORGANIZACIÓN</h3>
					<p class="text-blue text-justify font-weight-bold"><em>Nuestro nombre completo es el de Caja de Compensación Familiar de Fenalco – Andi COMFENALCO CARTAGENA.</em></p>
					<p class="text-justify">Somos una corporación civil de carácter privado, sin ánimo de lucro, de duración indefinida que cumple funciones de Seguridad Social.<br>
					Pertenecemos al Sistema de Subsidio Familiar, que hace parte del Sistema de Seguridad Social de Colombia y es concebido por el Estado como mecanismo de redistribución de los ingresos en un país que exhibe grandes desigualdades desde el punto de vista socioeconómico.</p>
				</div>
			</div>
			<div class="row pb-4">
				<div class="col-12 pb-4">
					<div class="line-gray"></div>
				</div>
			</div>
			<div class="row pb-4">
				<div class="col-md-3 col-12 col-sm-6 pb-2 text-center">
					<img class="rounded img-fluid" src="{{asset('img/deleted/mision.jpg')}}" alt="">
				</div>
				<div class="col-md-3 col-12 col-sm-6 pb-2">
					<h3 class="text-red">Misión</h3>
					<p class="text-justify">Mejorar la calidad de vida de sus afiliados y de la población vulnerable mediante la prestación de servicios sociales que contribuyan al desarrollo de la región en el marco del sistema de compensación familiar.</p>
				</div>
				<div class="col-md-3 col-12 col-sm-6 pb-2 text-center">
					<img class="rounded img-fluid" src="{{asset('img/deleted/vision.jpg')}}" alt="">
				</div>
				<div class="col-md-3 col-12 col-sm-6 pb-2">
					<h3 class="text-red">Visión</h3>
					<p class="text-justify">En 2021 Comfenalco Cartagena estará a la vanguardia en la prestación de servicios sociales en Colombia, sustentada en eficientes prácticas de actualización tecnológica integrada, en alianzas estratégicas, en el desarrollo del talento humano y con infraestructura moderna y adecuada, bajo criterios orientadores de innovación.</p>
				</div>
			</div>
			<div class="row pb-4">
				<div class="col-12 pb-4">
					<div class="line-gray"></div>
				</div>
			</div>
			<div class="row pb-4">
				<div class="col-12">
					<h3 class="text-red">Reseña Histórica</h3>
				</div>
				<div class="col-md-6 text-justify mb-3">
					<p class="text-blue font-weight-bold"><em>La historia de COMFENALCO Cartagena comienza el 20 de enero de 1961 cuando en las oficinas de FENALCO (Federación Nacional de Comerciantes) en el piso 612 del edificio Andian se reunieron un grupo de comerciantes de la ciudad con el propósito de crear una Caja de Compensación Familiar.</em></p>
					<p>En esta primera asamblea se aprobaron los estatutos de La Caja y se nombró como presidente al Sr. Antonio Araujo y como secretario al Dr. Ciro Castilla.Pocos días después, el día 3 de febrero, el Consejo Directivo nombra la primera mesa directiva y nombra como Presidente al Sr. Teófilo Barbur, Vicepresidente Napoleón Coronel y como Director General al Dr. Ciro Castilla; en esta reunión se afiliaron las primeras empresas a nuestra Caja, las cuales fueron:</p>
					<div class="row">
					<div class="col-sm-6">
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Panadería Imperial</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Alfonso Diaz Granados Y CIA Ltda.</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Olarte Y CIA.</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Hotel Quinta Avenida</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Hotel Playa</span>
					</div>
					<div class="col-sm-6">
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Acueductos Y Alcantarillados De Bolívar</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Grasa La Casera.</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Transportes Rosario.</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Méndez Y Gomez Ltda.</span>
						<span class="d-block pb-2"><img class="pr-2" src="{{asset('img/deleted/check.png')}}" alt="">Espa Ltda.</span>
					</div>
					</div>			
				</div>
				<div class="col-md-6 mb-3"><img class="img-fluid rounded" src="{{asset('img/deleted/reseña-historica.jpg')}}" alt=""></div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1961</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">El 14 de febrero de 1961 con resolución No. 92, la Gobernación de Bolívar resuelve <<...reconocer la personería jurídica a la entidad denominada Caja de Compensación Familiar de FENALCO de Cartagena…>> haciendo de esta fecha algo histórico, ya que con ello inicia el funcionamiento de nuestra Caja como tal.</p></div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1962</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">En el año 1962 se creó el decreto 3151 por medio del cual<<...las Cajas de Compensación podrán invertir en obras de beneficio social...>. De ésta forma el Consejo Directivo dio inicio a mediados de este mismo año a los programas de tipo social que son hoy la razón de ser nuestra Caja.</p></div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1968</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">En el año 1968 tuvo lugar uno de los más importantes sucesos en la historia de COMFENALCO, la Asociación Nacional de Industriales ANDI se unió a La Caja contribuyendo así a impulsarlos objetivos de COMFENALCO.</p></div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1970</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">En 1970 se propuso la creación de la Ciudad Escolar Comfenalco para los hijos de los trabajadores, para lo cual se aprueba la compra de los lotes ubicados en Zaragocilla.</p></div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1971</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">Entre los años de 1971 y 1972 se contrata al Ingeniero Felipe Juan para la construcción de la Ciudad Escolar Comfenalco CEC y ASPAEN asesora a COMFENALCO para establecer en primera instancia los programas de educación. Dándose inicio al proceso de apertura de matrículas en 1973.</p></div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1973</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">El 31 de diciembre de 1973 se constituye la Ley 56, por medio de la cual se establece que el sector laboral debe entrar a participar en el Consejo Directivo de Las Cajas de Compensación Familiar, teniendo derecho a nombrar dos (2) representantes por parte de los trabajadores ante dicho Consejo.</p></div>
					</div>
				</div>
				<div class="col-md-12 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica d-flex justify-content-center flex">
						<div class="col-12 col-md-3 d-flex justify-content-center align-items-center w-133">
							<span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1981</span>
						</div>
						<div class="col pl-md-0 d-flex align-items-center">
							<p class="text-justify mb-0">Con la Ley 25 de 1981 el Gobierno creó la Superintendencia del Subsidio Familiar, cuya función es vigilar las actividades de las cajas de compensación en el país.</p>
						</div>
					</div>
				</div>
				<div class="col-md-12 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica d-flex justify-content-center flex">
						<div class="col-12 col-md-3 d-flex justify-content-center align-items-center w-133">
							<span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1982</span>
						</div>
						<div class="col pl-md-0 d-flex align-items-center">
							<p class="text-justify mb-0">Con la Ley 21 de 1982 el Gobierno modifica el régimen de subsidio familiar, entre las modificaciones más importantes que se hicieron están las siguientes:<br>· Límite del pago del subsidio en dinero y los servicios subsidiados, a los trabajadores que devenguen hasta cuatro (4) veces el salario mínimo legal mensual vigente. Para este efecto, solo se limitaba el pago del subsidio en dinero, el resto del servicio podía prestarse a los afiliados sin límite de sueldo.<br>· Pago del subsidio familiar por hijos hasta de 23 años, solo si después de los 18 se demostrase estudiosde educación superior, intermedios o técnicos.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1982</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">Otra de las modificaciones que se hicieron al tenor de la Ley 21 de 1982 dieron para aprobar nuevos estatutos en COMFENALCO, entre estos fue el establecimiento y conformación de más miembros en el Consejo Directivo, el cual quedó constituido por cinco (5) miembros principales(con sus respectivos suplentes) nombrados por los empleadores y otros cuatro (4) miembros (con suplentes) nombrados por los trabajadores representando a las centrales obreras.</p></div>
					</div>
				</div>
				<div class="col-md-6 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-min-height">
						<div class="col-lg-3 d-flex justify-content-center align-items-center"><span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1984</span></div>
						<div class="col-lg-9 pl-lg-0 d-flex align-items-center"><p class="text-justify mb-0">Con la Ley 31 de 1984 se establece la paridad en el Consejo Directivo a cinco (5) miembros entre empleadores y trabajadores beneficiarios.</p></div>
					</div>
				</div>
				<div class="col-md-12 mb-3">
					<div class="row bg-gray-2 rounded pt-3 pb-3 ml-1 mr-1 date-historica d-flex justify-content-center flex">
						<div class="col-12 col-md-3 d-flex justify-content-center align-items-center w-133">
							<span class="p-3 pl-4 pr-4 bg-red text-white rounded h5">1982</span>
						</div>
						<div class="col pl-md-0 d-flex align-items-center">
							<p class="text-justify mb-0">En el año 1982 el Dr. Ciro Castilla deja la Dirección de La Caja, reemplazándolo en el cargo el hasta entonces Jefe de la División Administrativa, el Dr. Mario Ramos Vélez; quien hasta el mes de noviembre de 1997 desarrolló de manera eficiente y eficaz toda una labor de carácter social, fomentando por intermedio de nuestra Caja en el Distrito de Cartagena y demás poblaciones del departamento de Bolívar un sin número de actividades y programas.<br><span class="text-blue font-weight-bold"><em>A partir de ese entonces toma la Dirección el Dr. Ricardo Segovia Brid quien se encargaba de dirigir la División Salud, ahora convertida en la IPS de COMFENALCO Cartagena.</em><span></p>
						</div>
					</div>
				</div>
				<div class="col-12">
					<ul id="myTab" class="nav flex-column flex-md-row flex-sm-row flex-lg-row flex-xl-row nav-tabs d-sm-flex justify-content-sm-between responsive-tabs" role="tablist">
						<li class="nav-item">
							<a class="text-white nav-link bg-tab-1 active" href="#osistema" role="tab" data-toggle="tab">SISTEMA DE <br>GESTIÓN DE CALIDAD</a>
						</li>
						<li class="nav-item">
							<a class="text-white nav-link bg-tab-2" href="#ocobertura" role="tab" data-toggle="tab">COBERTURA</a>
						</li>
						<li class="nav-item">
							<a class="text-white nav-link bg-tab-3" href="#onormatividad" role="tab" data-toggle="tab">NORMATIVIDAD</a>
						</li>
						<li class="nav-item">
							<a class="text-white nav-link bg-tab-4" href="#oorganigrama" role="tab" data-toggle="tab">ORGANIGRAMA</a>
						</li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane fade show active" id="osistema">
							<div class="row pt-3 pb-3 text-justify">
								<div class="col-md-6 pb-2 pb-md-0 text-center">
									<img class="img-fluid rounded" src="{{asset('img/deleted/sistema-gestion-calidad.jpg')}}" alt="">
								</div>
								<div class="col-md-6">
									<p>Este proceso se inició en el año 2004 ,por ser uno de los objetivos estratégicos de La Caja, el certificado de Gestión de la Calidad, por parte del ICONTEC, bajo las normas internacionales ISO 9001:2000, fue otorgado en septiembre del 2005 en el proceso de Vinculación, Recaudo de Aportes y Pago de la cuota monetaria y los procesos de apoyo como Recurso Humano, Gestión financiera, Infraestructura física, Infraestructura tecnológica, Compras, Almacén, Archivo y Correspondencia, Gestión Jurídica, Gestión Comercial y Comunicaciones. Política  de Calidad</p>
								</div>
								<div class="col-12">
									<p>En COMFENALCO CARTAGENA nos comprometemos a proporcionar eficientemente servicios de calidad, en el marco del sistema de compensación familiar, que mejoren la calidad de vida de nuestros afiliados y población vulnerable acordes con los requisitos de cada unidad de servicio, para lograr su máxima satisfacción, mediante la mejora continua de nuestros procesos con un talento humano motivado y competente.</p>
									<span class="text-blue">Objetivos de Calidad</span>
									<ul>
										<li>Garantizar la calidad en la prestación de servicios a nuestros clientes cumpliendo con los requisitos especificados en los procesos del Sistema de Gestión de Calidad</li>
										<li>Aumentar el nivel de satisfacción de los clientes en los procesos del Sistema de Gestión de Calidad</li>
										<li>Mejorar continuamente la eficacia de nuestros procesos para contribuir la satisfacción de nuestros clientes.</li>
										<li>Estructurar un proceso de formación para desarrollar en el recurso humano las competencias y motivación necesarias para lograr el mejoramiento continuo en su desempeño</li>
										<li>Garantizar el uso eficiente de los recursos que conlleven a la optimización de nuestros procesos, contribuyendo a la sostenibilidad del ambiente.</li>
									</ul>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="ocobertura">
							<div class="row pt-3 pb-3 text-justify">
								<div class="col-12">
									<p class="text-justify">Somos una Caja de Compensación con cobertura en todo el Departamento de Bolívar, nuestra organización es de carácter privado, sin ánimo de lucro, que busca desde un punto de vista social compensar la distribución de los ingresos mediante el pago del Subsidio Familiar a los trabajadores de medianos y menores ingresos, de acuerdo al número de beneficiarios a cargo; buscando aliviar las cargas económicas que representa el sostenimiento de una familia como núcleo básico de la sociedad.</p>
								</div>
								<div class="mb-3 col-md-3 pb-2 pb-md-0 text-center">
									<img class="img-fluid rounded" src="{{asset('img/organizacion-cobertura-1.jpg')}}" alt="">
								</div>
								<div class="col-md-9">
									<span class="h4 d-block pb-2 text-blue-2">Regional Magangué</span>
									<p>En el municipio de Magangué, desde el corazón del Departamento de Bolívar, Comfenalco, tiene el gusto de facilitarles a todas las empresas, afiliados y comunidad en general la posibilidad de acercarse a nuestra agencia regional para disfrutar de nuestros variados servicios o realizar todas sus operaciones y gestiones con la Caja sin necesidad de trasladarse físicamente al distrito capital en Cartagena de Indias.<br> Es nuestra voluntad que los municipios de Magangué y Mompox, en compañía de sus pueblos, vecindades, corregimientos ribereños y de la sabana, cuenten con los recursos e infraestructura apropiada para demandar en sitio los diferentes servicios previstos para nuestra gran nube de afiliados.</p>
								</div>
								<div class="mb-3 col-md-3 pb-2 pb-md-0 text-center">
									<img class="img-fluid rounded" src="{{asset('img/organizacion-corbertura-2.jpg')}}" alt="">
								</div>
								<div class="col-md-9">
									<span class="h4 d-block pb-2 text-blue-2">Regional Carmen de Bolívar</span>
									<p>Desde el Municipio de El Carmen Centro del Departamento de Bolívar, en la subregión de los Montes de María, Comfenalco les ofrece a todas las empresas afiliadas y comunidad en general la oportunidad de acercarse a nuestra Agencia para disfrutar de todos nuestros servicios y realizar gestiones sin tener que trasladarse a la Ciudad de Cartagena.<br> Contamos con una gama de empresas afiliadas de San Juan, San Jacinto, El Guamo, Córdoba, Zambrano y El Carmen, todas ellas reciben una magnífica atención en lo que se refiere a entrega de subsidio familiar monetario, recaudo de aportes, recepción de documentos, educación, recreación y deportes, crédito social, vivienda, subsidio al desempleo y resolver oportunamente las inquietudes.</p>
								</div>
								<div class="mb-3 col-md-3 pb-2 pb-md-0 text-center">
									<img class="img-fluid rounded" src="{{asset('img/organizacion-corbertura-3.jpg')}}" alt="">
								</div>
								<div class="col-md-9">
									<span class="h4 d-block pb-2 text-blue-2">Regional Santa Rosa del Sur</span>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="onormatividad">
							<div class="row pt-3 pb-3 text-justify">
								<div class="col-md-5 pb-2 mb-3 pb-md-0 text-center">
									<img class="img-fluid rounded" src="{{asset('img/organizacion-normativa-1.jpg')}}" alt="">
								</div>
								<div class="col-md-7">
									<span class="text-blue-2 d-block pb-2 h4">Código del Buen Gobierno y Ética</span>
									<p>COMFENALCO CARTAGENA en cumplimiento de la obligación legal establecida en el parágrafo 2° del artículo 21 de la Ley 789 de 2002 decidió adoptar el presente Código de Buen Gobierno y Ética, con el cual busca encausar todo la actividad de la Caja dentro del régimen de transparencia pretendido por la sociedad, en atención a los recursos que ésta ha tenido a bien darle a su cargo, en cumplimiento de la función social que le es propia a las Cajas de Compensación Familiar en general.</p>
								</div>
								<div class="col-12">
									<p>El objetivo de nuestro Código de Buen Gobierno y Ética es compilar las políticas, normas, sistemas y principios éticos que orientan las actuaciones de COMFENALCO CARTAGENA y de todas las personas vinculadas con ella, con el fin de preservar la integridad, ética institucional, así como asegurar la adecuada administración de sus asuntos, el respeto por los empleadores afiliados y todos afiliados a la Corporación; al igual que propender por el respeto a la comunidad, asegurando una adecuada prestación de los servicios de la Corporación y el conocimiento público de su gestión.</p>
									<p>En la búsqueda por generar confianza de los grupos de referencia y de interés, la Caja ha decidido implementar procesos y prácticas propias del Buen Gobierno Corporativo organizando la distribución de derechos y responsabilidades entre todo el equipo, recogiendo la visión, misión y valores corporativos que se han determinado.</p>
									<p>El presente Código, aplica a todas las actuaciones del Consejo Directivo, Director Administrativo, directivos y demás empleados, contratistas, proveedores, trabajadores afiliados y empleadores afiliados a la Caja, con el fin que la actuación de la Corporación se ajuste a los principios éticos y a las prácticas de buen manejo corporativo.</p>
									<span class="text-blue-2 d-block pb-2 h4">Estatutos de Comfenalco</span>
									<p>Aprobados por la Superintendencia del Subsidio Familiar Mediante la resolución 570 del 2011.<br>En el panel de descargas pude obtener el documento con los Estatutos de Comfenalco.<br><br>Aportes Obligatorios de los Empresarios, Provistos en La Ley 21 de 1982<br>Todos los empleadores con uno o más trabajadores permanentes, es decir, trabajadores que ejecuten labores propias de las actividades normales del empleador y no realicen trabajos ocasionales, accidentales o transitorios, están obligados a aportar a las Cajas de Compensación Familiar.<br>Este aporte es del 4% del total de los salarios pagados, sin deducciones, estos aportes deben pagarse, en el caso de nuestra Caja de Compensación Familiar, Comfenalco Cartagena, mediante la PLANILLA ÚNICA DE APORTES (PILA).</p>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane fade" id="oorganigrama">
							<div class="row pb-3 text-justify">
								<div class="col-12">
									<img class="img-fluid" src="{{ asset('img/organizacion-organigrama.jpg') }}" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{asset('js/jquery.bootstrap-responsive-tabs.min.js')}}"></script>
	<script>
		$('a[data-toggle="tab"]').click(function (e) {
			e.preventDefault();
			if(e.target.hash == '#osistema'){
				$('.nav-tabs').css('border-color','#FFCB05');
			}else if(e.target.hash == '#ocobertura'){
				$('.nav-tabs').css('border-color','#E84E1B');
			}else if(e.target.hash == '#onormatividad'){
				$('.nav-tabs').css('border-color','#3AA935');
			}else if(e.target.hash == '#oorganigrama'){
				$('.nav-tabs').css('border-color','#36A8E0');
			}
		});

		// $('.responsive-tabs').responsiveTabs({
		// 	accordionOn: ['xs'] // xs, sm, md, lg
		// });
	</script>
@endsection
