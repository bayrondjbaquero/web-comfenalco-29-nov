@extends('layouts.main')

@section('title')Contáctanos @endsection

@section('styles')
@endsection

@section('content')
    <div id="contacto" class="box-default pt-4 pb-4">
        <div class="container">
            <div class="row">
            	<div class="col-md-6 pb-4">
            		<img class="img-fluid rounded" src="{{asset('img/img-contacto.jpg')}}" alt="">
            	</div>	
            	<div class="col-md-6 pb-4">
            		<br>
            		<h3 class="text-red"><strong>CONTÁCTANOS</strong></h3>
            		<br>
					<p class="text-blue text-justify"><em><strong>En Comfenalco Cartagena estamos a su disposición para atender sus inquietudes o solicitudes, comuníquese con nosotros o diríjase a nuestros Centros Integrales de Servicios (CIS).</strong></em></p>
					<ul>
						<li><p>Envíe sus inquietudes o solicitudes a <strong><a class="text-red" href="mailto:servicioalcliente@comfenalco.com">servicioalcliente@comfenalco.com</a></strong></p></li>
						<li><p>Reciba atención personalizada en nuestro Call Center 6723800 - línea gratuita 01 8000 95 8080.</p></li>
					</ul>
            	</div>
            	<div class="col-md-6 pb-4"><a href="#"><img class="img-fluid rounded" src="{{asset('img/cont-pqrs.jpg')}}" alt=""></a></div>
            	<div class="col-md-6 pb-4"><a href="{{ route('contacto_preguntas_frecuentes') }}"><img class="img-fluid rounded" src="{{asset('img/cont-preguntar.jpg')}}" alt=""></a></div>
            </div>
            <div class="row pb-4">
				<div class="col-12 pb-4">
					<div class="line-gray"></div>
				</div>
			</div>
			<div class="row pb-4">
				<div class="col-md-6">
					<h6 class="text-red"><strong>CENTROS INTEGRALES DE SERVICIOS AL CLIENTE (CIS)</strong></h6>
					<p class="mb-0"><strong class="text-blue">CIS EJECUTIVOS:</strong> Centro Comercial Ejecutivos Bloque B - Local 26</p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes  de 7:30 am. a 5:30pm. -  Jornada continua</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 am. -11:00 am.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS MATUNA:</strong> Centro Avenida Venezuela, Centro Comercial La Cascada Local 1</p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 am. a 5:30pm - Jornada continua</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 am. -11:00 am.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS BOSQUE:</strong> Centro Comercial Outlet del Bosque</p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 am. a 5:30pm - Jornada continua</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 am. -11:00 am.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS CEDESARROLLO:</strong> Edificio Cedesarrollo Zaragocilla</p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 a.m. a 12:00 m. - 1:30 p.m. a 5:30 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 a.m. a 11:00 a.m.</li>
					</ul>

					<p class="mb-0"><strong class="text-blue">CIS MAMONAL:</strong> CENTRO EMPRESARIAL COMFENALCO</p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 a.m. a 12:00 m. - 1:30 p.m. a 5:30 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 a.m. a 11:00 a.m.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS BOCAGRANDE:</strong> Cra 3° N° 8-128 (Frente al centro comercial Bocagrande)</p>
					<ul class="mb-4 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 am. a 5:30pm - Jornada continua</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 am. -11:00 am..</li>
					</ul>
					<h6 class="text-red"><strong>HORARIOS DE ATENCIÓN (PUNTOS FIJOS)</strong></h6>
					<p class="mb-0"><strong class="text-blue">SAO Plazuela</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 am a 12:30 pm – 2:00 Pm a 6:30 Pm</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 am a 12:30 pm – 1:30 Pm a 4:00 Pm</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">SAO San Felipe</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 am a 12:30 pm – 2:00 Pm a 6:30 Pm</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 am a 12:30 pm – 1:30 Pm a 4:00 Pm</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">Olímpica Buenos Aires</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 a.m. a 12:30 p.m. – 2:00 Pm a 6:30 p.m</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 am a 12:30 p.m. – 1:30 Pm a 4:00 P.m.</li>
					</ul>
				</div>
				<div class="col-md-6">
					<p class="mb-0"><strong class="text-blue">Olímpica San Fernando</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 a.m. a 12:30 pm – 2:00 p.m .a 6:30 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 a.m. a 12:30 p.m. – 1:30 Pm a 4:00 p.m.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">Olímpica Turbaco</strong> Centro Comercial Outlet del Bosque</p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 7:30 a.m. a 12:30 p.m. – 2:00 p.m. a 5:30 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 a.m. a 12:30 p.m. – 1:30 p.m. a 4:00 p.m.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">Éxito Castellana</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 am a 12:30 pm – 2:00 Pm a 6:30 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 am a 12:30 p.m – 1:30 p.m a 4:00 p.m</li>
					</ul>

					<p class="mb-0"><strong class="text-blue">Éxito Cartagena</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 am a 12:30 pm – 2:00 Pm a 6:30 Pm</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 am a 12:30 p.m. – 1:30 Pm a 4:00 Pm</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">Éxito Ejecutivos</strong></p>
					<ul class="mb-4 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a Viernes: 8:30 am a 12:30 pm – 2:00 Pm a 6:30 Pm</li>
						<li><strong class="text-red">·</strong> Sábados: 8:30 am a 12:30 p.m. – 1:30 Pm a 4:00 p.m</li>
					</ul>
					<h6 class="text-red"><strong>CENTROS INTEGRALES DE SERVICIOS AL CLIENTE (CIS) - REGIONALES</strong></h6>
					<p class="mb-0"><strong class="text-blue">CIS TURBACO</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 a.m – 12:00 m a 1:30 p.m. – 5:00 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 a.m. a 11:00 a.m.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS CARMEN DE BOLIVAR</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 a.m – 12:00 m a 1:30 p.m. – 5:00 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 a.m. a 11:00 a.m.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS MAGANGUÉ</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 a.m – 12:00 m a 1:30 p.m. – 5:00 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 a.m. a 11:00 a.m.</li>
					</ul>
					<p class="mb-0"><strong class="text-blue">CIS SANTA ROSA DEL SUR</strong></p>
					<ul class="mb-2 list-unstyled pl-3">
						<li><strong class="text-red">·</strong> Lunes a viernes de 7:30 a.m – 12:00 m a 1:30 p.m. – 5:30 p.m.</li>
						<li><strong class="text-red">·</strong> Sábados de 8:00 a.m. a 11:00 a.m.</li>
					</ul>
				</div>
			</div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection

