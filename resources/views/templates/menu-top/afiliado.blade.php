@extends('layouts.main')

@section('title')Afiliado @endsection

@section('styles')
@endsection

@section('content')
	<div id="afiliado" class="box-default pt-4 pb-4">
		<div class="container">
			<div class="row pb-4">
				<div class="col-12 pb-2 pt-2">
					<h2 class="text-blue">BENEFICIOS DE NUESTROS AFILIADOS</h2>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-orange text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-money.png')}}" alt="afiliado money">
							<figcaption class="pt-2">Recibir la cuota monetaria de<br>$29.106 si cumple con las<br>condiciones establecidas del<br>año 2017.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('afiliaciones') }}">Conócelos aquí</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-orange-2 text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-deporte.png')}}" alt="afiliado deporte">
							<figcaption class="pt-2">Sus hijos podrán fomentar sus<br>habilidades deportivas en<br>nuestras escuelas.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('deportes') }}">Conócelos aquí</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-green text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-escenario.png')}}" alt="afiliado escenarios">
							<figcaption class="pt-2">Podrá usar nuestros<br>escenarios deportivos.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('deportes') }}">Conócelos aquí</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-blue-4 text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-home.png')}}" alt="afiliado home">
							<figcaption class="pt-2">Podrá postularse al<br> Subsidio de Vivienda.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('vivienda') }}">Conoce las fechas aquí</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-purple text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-musica.png')}}" alt="afiliado musica">
							<figcaption class="pt-2">Participar de expresiones<br>artísticas como la danza y la<br>música en nuestas escuelas y<br>grupos culturales.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded" href="{{ route('cultura') }}">Conócelos aquí</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="d-flex flex-column justify-content-around rounded afi text-center box-default bg-fucsia text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-educacion.png')}}" alt="afiliado educación">
							<figcaption>Podrá acceder a excelentes<br>alternativas educativas en primaria,<br>secundaria, superior y educación<br>continuada.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded ml-3 mr-3" href="{{ route('educacion') }}">Conoce nuestras instituciones<br>y programas aquí.</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-blue text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-marca.png')}}" alt="afiliado marca">
							<figcaption>Accede de forma fácil a nuestro<br>portafolio de crédito social a través<br>de nuestros 3 servicios principales<br>Mercaexpress, créditos por libranza<br>y Cupocrédito.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded ml-3 mr-3" href="{{ route('educacion') }}">Conócelos aquí</a>
					</div>
				</div>
				<div class="col-xl-3 col-lg-4 col-md-6 pb-3 pl-2 pr-2">
					<div class="align-items-center d-flex flex-column justify-content-around rounded afi text-center box-default bg-red-3 text-white p-2">
						<figure class="mb-0">
							<img src="{{asset('img/afiliado-recreacion.png')}}" alt="afiliado recreación">
							<figcaption>Contarás con servicios de recreación y<br>desarrollo social en nuestras 5 sedes<br>propias: Centro recreacional Takurika,<br>Jardín Botánico Guillermo Piñeres, Hotel<br>Corales de Indias, Centro Empresarial<br>Comfenalco y Centro Recreacional y de<br>Servicios Magangué.</figcaption>
						</figure>
						<a class="afiliado-enlace opacity-1 text-white rounded ml-3 mr-3" href="{{ route('recreacion') }}">Conócelos aquí</a>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
@endsection
