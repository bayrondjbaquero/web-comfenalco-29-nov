@extends('layouts.main')

@section('title')Preguntas frecuentes @endsection

@section('styles')
@endsection

@section('content')
    <div id="contacto-preguntas-frecuentes" class="box-default pt-4 pb-4">
        <div class="container">
            <div class="row">
            	<div class="col-md-12 pb-4">
            		<h3 class="text-red"><strong>Preguntas frecuentes</strong></h3>
            		<br>
            		<ul id="myTab" class="nav flex-column flex-md-row flex-lg-row flex-xl-row nav-tabs d-sm-flex justify-content-sm-between responsive-tabs flex-md-wrap" role="tablist">
						{{-- <li class="nav-item">
							<a class="justify-content-center nav-link bg-tab active text-uppercase" href="#pf1" role="tab" data-toggle="tab">Afiliaciones</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf2" role="tab" data-toggle="tab">Aportes</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf3" role="tab" data-toggle="tab">Ciudad Escolar Comfenalco</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf4" role="tab" data-toggle="tab">Crédito</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf5" role="tab" data-toggle="tab">Recreación</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf6" role="tab" data-toggle="tab">Viajes Comfenalco</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf7" role="tab" data-toggle="tab">Subsidio Familiar</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf8" role="tab" data-toggle="tab">Tarjeta Comfenalco</a>
						</li>
						<li class="nav-item">
							<a class="justify-content-center nav-link bg-tab text-uppercase" href="#pf9" role="tab" data-toggle="tab">Vivienda</a>
						</li> --}}
						@foreach($question_categories as $category)
						<li class="nav-item pb-1">
							<a class="justify-content-center nav-link bg-tab @if($loop->first) active @endif text-uppercase" href="#pf{{$loop->iteration}}" role="tab" data-toggle="tab">{{$category->name}}</a>
						</li>
						@endforeach
					</ul>
					<!-- Tab panes -->
					<div class="tab-content" id="nav-tabContent">
						@foreach($question_categories as $category)
						<div role="tabpanel" class="tab-pane fade @if($loop->first) show active @endif" id="pf{{$loop->iteration}}">
							<div id="accordion{{$loop->iteration}}" role="tablist">
							  @foreach($category->questions as $question)
							    <div class="card">
							      <div class="card-header" role="tab" id="h{{$loop->parent->iteration . $loop->iteration}}">
							        <h5 class="mb-0">
							          <a data-toggle="collapse" href="#c{{$loop->parent->iteration . $loop->iteration}}" aria-expanded="true" aria-controls="c{{$loop->parent->iteration . $loop->iteration}}">{{$question->question}}</a>
							        </h5>
							      </div>
							      <div id="c{{$loop->parent->iteration . $loop->iteration}}" class="collapse @if($loop->first) show @endif" role="tabpanel" aria-labelledby="h{{$loop->parent->iteration . $loop->iteration}}" data-parent="#accordion{{$loop->parent->iteration}}">
							        <div class="card-body pt-3 pb-3 pl-4 pr-4">
							          {!! $question->answer !!}
							        </div>
							      </div>
							  </div>
							  @endforeach
							  </div>
						  </div>
						@endforeach
					</div>

					{{-- <div class="tab-content" id="nav-tabContent">
						<div role="tabpanel" class="tab-pane fade show active" id="pf1">
							@include('templates.menu-top.contacto.preguntas-frecuentes.afiliaciones')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf2">
							@include('templates.menu-top.contacto.preguntas-frecuentes.aportes')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf3">
							@include('templates.menu-top.contacto.preguntas-frecuentes.ciudad-escolar-comfenalco')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf4">
							@include('templates.menu-top.contacto.preguntas-frecuentes.credito')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf5">
							@include('templates.menu-top.contacto.preguntas-frecuentes.recreacion')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf6">
							@include('templates.menu-top.contacto.preguntas-frecuentes.viajes-comfenalco')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf7">
							@include('templates.menu-top.contacto.preguntas-frecuentes.subsidio-familiar')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf8">
							@include('templates.menu-top.contacto.preguntas-frecuentes.tarjeta-comfenalco')
						</div>
						<div role="tabpanel" class="tab-pane fade" id="pf9">
							@include('templates.menu-top.contacto.preguntas-frecuentes.vivienda')
						</div>
					</div> --}}
				</div>
			</div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection