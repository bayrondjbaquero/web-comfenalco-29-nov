<div id="accordion4" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="h411">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c411" aria-expanded="false" aria-controls="c411">¿Hasta qué cantidad de dinero me prestan por la línea adquisición de vivienda?</a>
      </h5>
    </div>
    <div id="c411" class="collapse show" role="tabpanel" aria-labelledby="h411" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Los montos aprobados están sujetos a la capacidad de endeudamiento del trabajador y condiciones laborales.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h412">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c412" aria-expanded="false" aria-controls="c412">¿Cuáles son los requisitos para el crédito educativo por libranza?</a>
      </h5>
    </div>
    <div id="c412" class="collapse" role="tabpanel" aria-labelledby="h412" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>• Formulario de solicitud de Crédito, debidamente diligenciado.<br>
			• Certificado Laboral donde especifique antigüedad, salario, cargo, tipo de contrato<br>
			• Ultimas colillas de pago<br>
			• Copia de la cédula de ciudadanía ampliada al 150.<br>
			• Orden de matrícula.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h413">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c413" aria-expanded="false" aria-controls="c413">¿Para qué tipo de estudios me prestan por la línea de crédito para estudio?</a>
      </h5>
    </div>
    <div id="c413" class="collapse" role="tabpanel" aria-labelledby="h413" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>• Colegios<br>
			• Carreras técnicas<br>
			• Pregrado<br>
			• Postgrado<br>
			• Maestría</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h415">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c415" aria-expanded="false" aria-controls="c415">¿Cuáles son los requisitos para solicitar un crédito de libre inversión?</a>
      </h5>
    </div>
    <div id="c415" class="collapse" role="tabpanel" aria-labelledby="h415" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
	 	<p>• Formulario de solicitud de Crédito, debidamente diligenciado.<br>
			• Certificado Laboral donde especifique antigüedad, salario, cargo, tipo de contrato<br>
			• Certificación cuenta de ahorros.<br>
			• Ultimas colillas de pago<br>
			• Copia de la cédula de ciudadanía ampliada al 150.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h414">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c414" aria-expanded="false" aria-controls="c414">¿Hasta qué cantidad de dinero me prestan por libre inversión?</a>
      </h5>
    </div>
    <div id="c414" class="collapse" role="tabpanel" aria-labelledby="h414" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Los montos aprobados están sujetos a la capacidad de endeudamiento del trabajador y condiciones laborales.</p>
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" role="tab" id="h416">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c416" aria-expanded="false" aria-controls="c416">¿Cuáles son las líneas de crédito a las que puedo acceder por libranza?</a>
      </h5>
    </div>
    <div id="c416" class="collapse" role="tabpanel" aria-labelledby="h416" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
	 	<p>Las líneas de crédito que ofrece Comfenalco Cartagena son:<br>
			• Libre inversión<br>
			• Educación<br>
			• Vivienda</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h417">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c417" aria-expanded="false" aria-controls="c417">¿Cómo puedo obtener el Formato de Convenio de Libranza y el Formato de Registro de Cliente?</a>
      </h5>
    </div>
    <div id="c417" class="collapse" role="tabpanel" aria-labelledby="h417" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
	 	<p>A través de la ejecutiva de cuentas asignada para la empresa, visitando nuestros Centros Integrales de Servicios (CIS) o Puntos fijos de atención, o bajarlos a través de la página www.comfenalco.com</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h418">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c418" aria-expanded="false" aria-controls="c418">¿Cuáles son los requisitos y que documentos necesita la empresa para acceder al convenio de libranza?</a>
      </h5>
    </div>
    <div id="c418" class="collapse" role="tabpanel" aria-labelledby="h418" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
	 	<p>•	La empresa debe tener mínimo 2 meses de estar afiliada con Comfenalco<br>
			•	La empresa debe estar al día con los pagos de aportes<br>
			•	Cámara de Comercio Vigente (no mayor a 30 días)<br>
			•	Fotocopia de cedula del representante legal<br>
			•	Copia del RUT.<br>
			•	Estados financieros (firmados por el Rep. Legal y /o Revisor fiscal, del año anterior y/o corte del trimestre más reciente)<br>
			•	Formato de Convenio de libranza firmado por el representante legal<br>
			•	Formato de Registro de Cliente, firmado por el Representante Legal de la Empresa y firmas que autoricen las libranzas.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h420">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c420" aria-expanded="false" aria-controls="c420">¿Cómo puedo acceder a las líneas de crédito de Comfenalco?</a>
      </h5>
    </div>
    <div id="c420" class="collapse" role="tabpanel" aria-labelledby="h420" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
	 	<p>Para poder acceder a las líneas de crédito de Comfenalco, es necesario que la empresa con la que labora tenga suscrito Convenio de Libranza con la caja.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h419">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c419" aria-expanded="false" aria-controls="c419">¿Hasta qué cantidad de dinero me prestan por la línea de estudio?</a>
      </h5>
    </div>
    <div id="c419" class="collapse" role="tabpanel" aria-labelledby="h419" data-parent="#accordion4">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
	 	<p>Para la línea de estudio se presta el valor del semestre. Sujeto a la capacidad de endeudamiento del trabajador y condiciones laborales.</p>
      </div>
    </div>
  </div>
</div>