<div id="accordion5" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="h51">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c51" aria-expanded="true" aria-controls="c51">¿Cómo puedo realizar una reserva o solicitar cotización de servicios del Jardín Botánico?</a>
      </h5>
    </div>
    <div id="c51" class="collapse show" role="tabpanel" aria-labelledby="h51" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Usted podrá contactarse al teléfono 672 38 00 ext. 2360 o al celular 315 893 50 02 o a los correos electrónicos eventos@comfenalco.com o ntovar@comfenalco.com.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h52">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c52" aria-expanded="true" aria-controls="c52">¿Si soy afiliado puedo ingresar a toda mi familia al Centro Recreativo y Empresarial Corales de Indias, con mi tarifa de afiliación?</a>
      </h5>
    </div>
    <div id="c52" class="collapse" role="tabpanel" aria-labelledby="h52" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>En su caja de compensación Familiar Comfenalco, los beneficios de los afiliados son los siguientes:<br>
			• Conyugue<br>
			• Hijos Menores de 18 años<br>
			• Padres mayores de 60 años que dependan económicamente del afiliado.</p>
			<p>Estas personas pueden ingresar bajo la categoría de afiliación del afiliado siempre y cuando estén registrados en la base de datos del núcleo familiar y en estado ACTIVO.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h53">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c53" aria-expanded="true" aria-controls="c53">¿Dónde puedo adquirir los servicios del Centro Recreativo y Empresarial Corales de Indias?</a>
      </h5>
    </div>
    <div id="c53" class="collapse" role="tabpanel" aria-labelledby="h53" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>• En el Centro Recreativo y Empresarial Corales encontrara un funcionario de la Caja de Compensación Familiar y podrá adquirir el servicio que desee.<br>
			• En los CIS (Centros integrales de servicios, como Los Ejecutivos, Bloc Port,
			Outlet el Bosque, Matuna, puntos fijos de atención de los almacenes de cadena (Éxito Cartagena, Éxito Castellana, Éxito Ejecutivos, SAO Plazuela, Olímpica San Felipe, Olímpica Buenos Aires, Cámara de Comercio Ronda Real y Cámara de Comercio Centro).<br>
			• Centro de Atención al estudiante ubicado en Cedesarrollo Zaragocilla.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h54">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c54" aria-expanded="true" aria-controls="c54">¿Qué días está abierto el Centro Recreativo y Empresarial Corales de Indias?</a>
      </h5>
    </div>
    <div id="c54" class="collapse" role="tabpanel" aria-labelledby="h54" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Todos los días de la semana, las 24 horas del día.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h55">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c55" aria-expanded="true" aria-controls="c55">¿Qué categoría de afiliación soy?</a>
      </h5>
    </div>
    <div id="c55" class="collapse" role="tabpanel" aria-labelledby="h55" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Si su Salario es de 1 a 2 SMLV es: Categoria A<br>
			Si su Salario es de 3 a 4 SMLV es: Categoria B<br>
			Si su Salario es de 4 más SMLV es: Categoria C<br>
			Si usted no es afiliado a nuestra Caja de Compensación Familiar, su categoria es D o No afiliado.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h56">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c56" aria-expanded="true" aria-controls="c56">¿Por ser afiliado a la caja obtengo descuentos especiales de este servicio?</a>
      </h5>
    </div>
    <div id="c56" class="collapse" role="tabpanel" aria-labelledby="h56" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Sí. De acuerdo a su categoría de afiliación obtiene tarifas especiales en el Centro Recreativo y Empresarial Corales de Indias.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h57">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c57" aria-expanded="true" aria-controls="c57">¿Qué tipo de servicios puedo encontrar en el Centro Recreativo y Empresarial Corales de Indias?</a>
      </h5>
    </div>
    <div id="c57" class="collapse" role="tabpanel" aria-labelledby="h57" data-parent="#accordion5">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Puede acceder a los siguientes servicios:<br>
		• Alojamiento<br>
		• Pasadías<br>
		• Eventos empresariales y sociales<br>
		• Servicios de SPA</p>
      </div>
    </div>
  </div>
</div>