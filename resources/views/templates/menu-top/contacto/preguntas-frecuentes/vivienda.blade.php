<div id="accordion9" role="tablist">
	<div class="card">
		<div class="card-header" role="tab" id="h91">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c91" aria-expanded="false" aria-controls="c91">¿Cuales son los requisitos para mejora de vivienda?</a>
		  </h5>
		</div>
		<div id="c91" class="collapse show" role="tabpanel" aria-labelledby="h91" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Escritura pública del lote a nombre del afiliado que se va a postular.<br>
			Certificado de Tradición y Libertad donde conste la propiedad del terreno actualizada a nombre del postulante afiliado.<br>
			Paz y salvo catastro.<br>
			Certificado de Planeación Municipal o de quien haga las veces donde diga: Que la vivienda está en una zona urbana, que no está en zona de alto riesgo, que cuenta con disponibilidad de servicios públicos domiciliarios de acueducto, gas, alcantarillado, energía eléctrica y vías de acceso de la vivienda cumple con los requisitos mínimos para ser habitada por el hogar beneficiario.<br>
			Presupuesto del ing. o arquitecto, debe anexar tarjeta profesional.<br>
			Capacidad de endeudamiento<br>
			Fotocopia de la cédula de ciudadanía de los mayores de edad.<br>
			Certificado de ahorro previo. (si lo posee, no es un requisito esencial para esta modalidad)<br>
			Registro civil de matrimonio o prueba de la unión marital de hecho.<br>
			Registro civil de nacimiento para los menores de 18 años y prueba de la discapacidad de los mayores que formen el hogar, según sea el caso.<br>
			Carnets o certificación del SISBEN, si lo posee.<br>
			Certificado laboral vigente con sueldo no mayor a 30 días.<br>
			Valor del subsidio $9.640.800 (18 smlv)</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h92">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c92" aria-expanded="false" aria-controls="c92">¿Qué requisitos debo cumplir para postularme al subsidio de vivienda?</a>
		  </h5>
		</div>
		<div id="c92" class="collapse" role="tabpanel" aria-labelledby="h92" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Ser afiliado a Comfenalco.<br>
			Conformar un hogar con 2 o más personas.<br>
			Presentar certificación de financiación complementaria.<br>
			Seleccionar un proyecto de vivienda previamente declarado elegible.<br>
			No poseer vivienda propia.<br>
			No recibir ingresos mayores a 4 SMLV.<br>
			Tener un ahorro previo que puede ser en:<br>
			Cuenta de ahorro programado.<br>
			Cesantías Inmovilizadas.<br>
			Cuota Inicial.<br>
			Fondos Mutuos de Inversión.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h93">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c93" aria-expanded="false" aria-controls="c93">¿Quiénes se pueden postular al subsidio de vivienda?</a>
		  </h5>
		</div>
		<div id="c93" class="collapse" role="tabpanel" aria-labelledby="h93" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Todos los trabajadores afiliados a la caja de compensación, que no posea vivienda propia y que sus ingresos familiares no sean mayores a 4 smmlv.<p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h94">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c94" aria-expanded="false" aria-controls="c94">¿Cuales son los documentos que debo presentar para subsidio de vivienda nueva?</a>
		  </h5>
		</div>
		<div id="c94" class="collapse" role="tabpanel" aria-labelledby="h94" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Fotocopia de la cédula de ciudadanía de los mayores de edad.<br>
			Certificado de ahorro previo.<br>
			Certificado de capacidad de crédito.<br>
			Registro civil de matrimonio o prueba de la unión marital de hecho.<br>
			Registro civil de nacimiento para los menores de 18 años y prueba de la discapacidad de los mayores que formen el hogar, según sea el caso.<br>
			Carnets o certificación del SISBEN, si lo posee.<br>
			Certificado laboral vigente con sueldo no mayor a 30 días.<br>
			Si el ahorro esta representado en cuotas iniciales el afiliado debe 
			presentar copia autenticada de la promesa de compraventa, copia
			de los recibos de caja o volantes de consignación legibles de los
			pagos le desembolso a la constructora y una certificación emitida
			por la constructora donde conste que el afiliado realizo los pagos 
			y esta debe de venir firmada por el representante legal y el revisor
			fiscal.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h95">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c95" aria-expanded="false" aria-controls="c95">¿Cuáles son los requisitos para construcción en sitio propio?</a>
		  </h5>
		</div>
		<div id="c95" class="collapse" role="tabpanel" aria-labelledby="h95" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Se aplica este subsidio cuando el beneficiario supera una o varias de las carencias básicas de la vivienda perteneciente a un desarrollo legalizado o a una edificación siempre y cuando su título de propiedad se encuentre inscrito en la Oficina de Registro de Instrumentos Públicos a nombre del postulante y a su vez, este debe habitar la vivienda.</p>

			<p>La vivienda a mejorar debe presentar al menos una de las siguientes situaciones:</p>

			<p>Deficiencias en la estructura principal, cimientos, muros o cubierta.<br>
			Carencia o vetustez de redes secundarias y acometidas domiciliarias de acueducto y alcantarillado.<br>
			Carencia o vetustez de baños y/o cocina.<br>
			Existencia de pisos en tierra o en materiales inapropiados.<br>
			Construcción en materiales provisionales tales como latas, tela asfáltica y madera de desecho.<br>
			Existencia de hacinamiento crítico, cuando el hogar habita en una vivienda con más de tres personas por cuarto, incluyendo sala,  comedor y dormitorios.<br>
			En aquellos casos en que la totalidad de la vivienda se encuentre construida en materiales provisionales, se considerará objeto de un programa de mejoramiento de vivienda.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h96">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c96" aria-expanded="false" aria-controls="c96">¿Cuáles son los requisitos para desmovilización de mi cuenta de ahorro programada?</a>
		  </h5>
		</div>
		<div id="c96" class="collapse" role="tabpanel" aria-labelledby="h96" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Debe presentar el certificado de inmovilización de la cuenta, fotocopia de la cedula y una carta solicitando dicho certificado.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h97">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c97" aria-expanded="false" aria-controls="c97">¿Cuáles son los requisitos para obtener el subsidio de vivienda?</a>
		  </h5>
		</div>
		<div id="c97" class="collapse" role="tabpanel" aria-labelledby="h97" data-parent="#accordion9">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>No poseer vivienda propia, tener un ahorro programado, no superar los 4 Salarios mínimos legales vigentes, etc.</p>
		  </div>
		</div>
	</div>
</div>