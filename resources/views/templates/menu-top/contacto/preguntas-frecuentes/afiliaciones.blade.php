<div id="accordion" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="h1">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c1" aria-expanded="true" aria-controls="c1">¿Cómo hago para afiliar a mis padres?</a>
      </h5>
    </div>
    <div id="c1" class="collapse show" role="tabpanel" aria-labelledby="h1" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>“Se podrán afiliar los padres del trabajador beneficiario mayores de 60 años, siempre y cuando ninguno de los dos reciba salario, renta o pensión alguna. No podrán cobrar simultáneamente este subsidio más de uno de los hijos trabajadores y que dependan económicamente del trabajador"</p>
		<p>Documentos:</p>
		<p>Formulario de afiliación debidamente diligenciado.
		Registro civil del trabajador
		Fotocopia del documento de identidad del trabajador y los padres
		Declaración juramentada (Formato expedido por la Caja).</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h2">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c2" aria-expanded="false" aria-controls="c2">¿Cómo hago para afiliar a mis Hijos?</a>
      </h5>
    </div>
    <div id="c2" class="collapse" role="tabpanel" aria-labelledby="h2" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Formulario de afiliación debidamente diligenciado.</p>
		<p>Registro Civil de Nacimiento del menor.</p>
		<p>Certificado escolar (si es mayor a 12 años).</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h3">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c3" aria-expanded="false" aria-controls="c3">¿Cómo hago para afiliar a mi Hijo Discapacitado?</a>
      </h5>
    </div>
    <div id="c3" class="collapse" role="tabpanel" aria-labelledby="h3" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Formulario de afiliación debidamente diligenciado.</p>
		<p>Registro civil.</p>
		<p>Certificado de discapacidad expedido por EPS</p>
		<p>Certificado Valoración expedido por COMFENALCO</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h4">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c4" aria-expanded="false" aria-controls="c4">¿Cómo hago para afiliar a mi compañera (o)?</a>
      </h5>
    </div>
    <div id="c4" class="collapse" role="tabpanel" aria-labelledby="h4" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Formulario de afiliación debidamente diligenciado.</p>
		<p>Copia Cédula de Ciudadanía de ambos.</p>
		<p>Declaración Juramentada. (Formato expedido por la Caja).</p>
		<p>Certificado laboral, si el cónyuge trabaja.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h5">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c5" aria-expanded="false" aria-controls="c5">¿Cómo hago para afiliar a un hijastro?</a>
      </h5>
    </div>
    <div id="c5" class="collapse" role="tabpanel" aria-labelledby="h5" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Formulario de afiliación debidamente diligenciado.</p>
		<p>Diligenciar Formato de Declaración juramentada expedido por la Caja.</p>
		<p>Certificado expedido por la Caja en donde se encuentre afiliado el padre/madre sanguínea de menor que no convive, donde conste que no recibe subsidio familiar en dinero por ese mismo hijo (a). O certificado de NO afiliación.</p>
		<p>Registro civil del hijastro</p>
		<p>Certificado escolar (si es mayor a 12 años).</p>
		<p>Certificado de la EPS donde conste que el trabajador lo tiene como beneficiario.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h6">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c6" aria-expanded="false" aria-controls="c6">¿Quiénes son las personas que se pueden afiliar en el grupo familiar?</a>
      </h5>
    </div>
    <div id="c6" class="collapse" role="tabpanel" aria-labelledby="h6" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Hijos, Hijastros, Hijos adoptivos menores a 18 años 11 meses.</p>
		<p>Padres mayores de 60 años.</p>
		<p>Hermanos huérfanos de padre y madre menores a 18 años 11 meses.</p>
		<p>Personas a cargo del trabajador que estén en situación de discapacidad física ó mental, causarán doble cuota de Subsidio Familiar, sin limitaciones en razón de su edad.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h7">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c7" aria-expanded="false" aria-controls="c7">¿Cuáles son los requisitos para afiliar a los trabajadores?</a>
      </h5>
    </div>
    <div id="c7" class="collapse" role="tabpanel" aria-labelledby="h7" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Formulario de afiliación debidamente diligenciado.</p>
		<p>Copia de Cédula del trabajador legible</p>
      </div>
    </div>
  </div>
  <!-- -->
  <div class="card">
    <div class="card-header" role="tab" id="h8">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c8" aria-expanded="false" aria-controls="c8">¿Puedo recibir cuota monetaria por mis padres?</a>
      </h5>
    </div>
    <div id="c8" class="collapse" role="tabpanel" aria-labelledby="h8" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
        <p>Se recibe cuota monetaria por padres afiliados dentro del grupo familiar, si cumplen con los siguientes requisitos:</p>
        <p>Ser mayores de 60 años.</p>
        <p>No recibir ningún tipo de pensión.</p>
        <p>No estar afiliados a EPS como cotizantes.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h9">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c9" aria-expanded="false" aria-controls="c9">¿Cuando un trabajador cambia de empleador es necesario actualizar toda la documentación de éste en la Caja y solicitar afiliación nuevamente?</a>
      </h5>
    </div>
    <div id="c9" class="collapse" role="tabpanel" aria-labelledby="h9" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
        <p>Sí, la afiliación la debe solicitar el nuevo empleador anexando la fotocopia de cedula del trabajador.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h10">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c10" aria-expanded="false" aria-controls="c10">¿Los trabajadores quedan afiliados a la Caja de Compensación de manera automática cuando se afilia la Empresa?</a>
      </h5>
    </div>
    <div id="c10" class="collapse" role="tabpanel" aria-labelledby="h10" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
        <p>No, la afiliación de la empresa debe estar seguida del trámite de solicitud de afiliación de los trabajadores. Recuerde que puede realizar este procedimiento desde nuestro portal Afíliate con su usuario.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h11">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c11" aria-expanded="false" aria-controls="c11">¿Como conozco la categoría a la que pertenezco en la Caja?</a>
      </h5>
    </div>
    <div id="c11" class="collapse" role="tabpanel" aria-labelledby="h11" data-parent="#accordion">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
        <p>La categoría de los afiliados depende del nivel de ingresos:</p>
        <p>Si su Salario es de 1 a 2 SMLV es Categoría A</p>
        <p>Si su Salario es de 3 a 4 SMLV es Categoría B</p>
        <p>Si su Salario es de 4 más SMLV es Categoría C</p>
        <p>Si usted no es afiliado a nuestra Caja de Compensación Familiar su Categoría es D</p>
      </div>
    </div>
  </div>
</div>