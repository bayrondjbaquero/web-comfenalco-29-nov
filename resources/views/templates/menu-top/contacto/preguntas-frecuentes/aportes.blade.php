<div id="accordion2" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="h21">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c21" aria-expanded="true" aria-controls="c21">¿Qué debo hacer en caso de receso de actividades?</a>
      </h5>
    </div>
    <div id="c21" class="collapse show" role="tabpanel" aria-labelledby="h21" data-parent="#accordion2">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>La empresa que por alguna razón (paradas, suspensión de actividades, etc.) no genere gastos de nómina deberá notificar a la caja sobre dichos periodos. De la misma forma que debe notificar el reinicio de su actividad comercial.</p>
      	<img src="{{asset('img/contacto-tablas.jpg')}}" alt="">
      	<p></p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h22">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c22" aria-expanded="true" aria-controls="c22">¿Qué debe hacer la empresa para solicitar convenio de pago?</a>
      </h5>
    </div>
    <div id="c22" class="collapse" role="tabpanel" aria-labelledby="h22" data-parent="#accordion2">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Los convenios de pago se le ofrecen a las empresas que están en mora en el pago de los aportes parafiscales.<br/> La finalidad de los convenios de pago es permitirle a las empresas que paguen vigencias anteriores y vigencias actuales.</p>
		<p><strong>Requisitos para los convenios</strong></p>

		<p>Solicitar el convenio directamente en la oficina de nuestro centro Integral de Servicios CIS Ejecutivos ubicado en el Centro Comercial Ejecutivos Bloque B - Local 26.<br/>
		Nómina de los periodos que tienen en mora.<br/>
		Cámara de comercio actualizada no mayor a 30 días.<br/>
		Carta firmada por el representante legal donde solicite el convenio de pago.</p>
      </div>
    </div>
  </div>
</div>