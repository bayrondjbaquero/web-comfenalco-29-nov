<div id="accordion6" role="tablist">
	<div class="card">
	  <div class="card-header" role="tab" id="h61">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c61" aria-expanded="true" aria-controls="c61">¿Qué información se debe enviar a la agencia de viajes para solicitar una cotización de turismo?</a>
	    </h5>
	  </div>
	  <div id="c61" class="collapse show" role="tabpanel" aria-labelledby="h61" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>El cliente interesado en cotizar un servicio turístico con la Agencia de Viajes, deberá suministrar a través del correo electrónico turismo@comfenalco.com la siguiente información:</p>
	  	  	<p>Número de identificación<br>
			Número de personas (adultos y niños)<br>
			Destino<br>
			Fecha específica o tentativa<br>
			Número de teléfono fijo o celular</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h62">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c62" aria-expanded="true" aria-controls="c62">¿Quién puede suministrar información de los servicios de turismo?</a>
	    </h5>
	  </div>
	  <div id="c62" class="collapse" role="tabpanel" aria-labelledby="h62" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>El área comercial, el call center, la Unidad de recreación y Turismo o su ejecutiva comercial asignada.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h63">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c63" aria-expanded="true" aria-controls="c63">¿Quiénes pueden utilizar los servicios de turismo?</a>
	    </h5>
	  </div>
	  <div id="c63" class="collapse" role="tabpanel" aria-labelledby="h63" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Todas aquellas personas y/o empresas interesadas en los planes promocionados por la Caja de Compensación (afiliados, no afiliados y sus Familias, empresas afiliadas y empresas no afiliadas).</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h64">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c64" aria-expanded="true" aria-controls="c64">¿Los afiliados tienen algún descuento en los planes turísticos promocionados por la caja de compensación?</a>
	    </h5>
	  </div>
	  <div id="c64" class="collapse" role="tabpanel" aria-labelledby="h64" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Sí. Los planes turísticos promocionados por la Caja de Compensación otorgan un descuento al afiliado. Las tarifas informadas en la cotización indican el precio para personas y/o empresas afiliadas y para personas y/o empresas no afiliadas.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h65">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c65" aria-expanded="true" aria-controls="c65">¿Se permite el ingreso de alimentos al Jardín Botánico?</a>
	    </h5>
	  </div>
	  <div id="c65" class="collapse" role="tabpanel" aria-labelledby="h65" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>No se permite el ingreso de alimentos y bebidas. Únicamente se permite el consumo de alimentos y bebidas en el área de Cafetería.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h66">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c66" aria-expanded="true" aria-controls="c66">¿Puedo llevar mascotas al Jardín Botánico Guillermo Piñeres?</a>
	    </h5>
	  </div>
	  <div id="c66" class="collapse" role="tabpanel" aria-labelledby="h66" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>El ingreso de mascotas al Jardín Botánico no está permitido.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h67">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c67" aria-expanded="true" aria-controls="c67">¿Con cuántas horas de anticipación debo estar en el aeropuerto?</a>
	    </h5>
	  </div>
	  <div id="c67" class="collapse" role="tabpanel" aria-labelledby="h67" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Para los trayectos nacionales se debe presentar hora y media antes y para los trayectos internacionales debe presentarse tres horas antes de la salida del vuelo, Es importante verificar la documentación antes de viajar.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h68">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c68" aria-expanded="true" aria-controls="c68">¿Dónde puedo tramitar mi reembolso?</a>
	    </h5>
	  </div>
	  <div id="c68" class="collapse" role="tabpanel" aria-labelledby="h68" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Puede llamar al teléfono 6723800 Ext. 2100 o visitar nuestra Agencia de Viajes con el fin de diligenciar su solicitud.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h69">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c69" aria-expanded="true" aria-controls="c69">Los motivos por los cuales puede solicitar un reembolso son:</a>
	    </h5>
	  </div>
	  <div id="c69" class="collapse" role="tabpanel" aria-labelledby="h69" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Por voluntad del Pasajero<br>
			Documentos extraviados<br>
			Personas privadas de la libertad<br>
			Personas fallecidas<br>
			Enfermedad (se deberá anexar certificado médico)<br>
			Cancelación de vuelo<br>
			Demora en la salida de vuelo<br>
			Cambio de itinerarios<br>
			Sobreventa<br>
			Pérdida de una conexión causada por la aerolinea<br>
			Negación de visa<br>
			Boletos duplicados<br>
			Doble cobro de impuestos de salida y tasas aeroportuarias</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h610">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c610" aria-expanded="true" aria-controls="c610">¿Si tengo un tiquete no reembolsable y no puedo realizar mi viaje. ¿puedo solicitar un reembolso?</a>
	    </h5>
	  </div>
	  <div id="c610" class="collapse" role="tabpanel" aria-labelledby="h610" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Sí, puede solicitar el reembolso de los impuestos que correspondan a los trayectos no utilizados del boleto. Debe tener en cuenta que algunos impuestos no son reembolsables por las aerolíneas y para su devolución es necesario que se dirija a las oficinas de la Agencia de Viajes Comfenalco. Si tiene un boleto no reembolsable, pero permite cambios de fecha, puede reprogramar su viaje sin perder el valor total del boleto. Se realizarán los cobros de los cargos por cambios aplicables.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h611">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c611" aria-expanded="true" aria-controls="c611">¿Es reembolsable el valor del tiquete en caso de no hacer uso del servicio?</a>
	    </h5>
	  </div>
	  <div id="c611" class="collapse" role="tabpanel" aria-labelledby="h611" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Lo primero que debe hacer es verificar en la confirmación de su compra el tipo de opción tarifaria que ha adquirido. En la mayoría de los casos, se genera un cargo adicional por el trámite, el cual se le descontará del monto a reintegrar. Es importante mencionar que no se puede reembolsar algunos impuestos.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h612">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c612" aria-expanded="true" aria-controls="c612">¿Qué es un equipaje de mano?</a>
	    </h5>
	  </div>
	  <div id="c612" class="collapse" role="tabpanel" aria-labelledby="h612" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Las aerolíneas consideran equipaje de mano a los objetos personales que pueden ser llevados en la cabina del avión y son de responsabilidad de cada pasajero. Los equipajes de mano tienen un peso y medida de maleta estipulado por cada una de las aerolíneas. Tenga en cuenta que no es permitido llevar en este tipo de equipaje mercancías peligrosas, balones y/o pelotas infladas, objetos corto punzantes, instrumentos quirúrgicos, vidrios, espejos o cualquier elemento que pueda generar algún tipo de peligro para los demás pasajeros y el equipo de la aerolínea. Se recomienda comunicarse con la Agencia de Viajes Comfenalco o la aerolínea para saber las políticas del equipaje de mano.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h613">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c613" aria-expanded="true" aria-controls="c613">¿De qué manera puedo saber si la reserva aérea esta confirmada?</a>
	    </h5>
	  </div>
	  <div id="c613" class="collapse" role="tabpanel" aria-labelledby="h613" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Una vez se emita una clave de confirmación y se cumplan los plazos de vigencia estipulados por la aerolínea para el pago se procede a la emisión del tiquete.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h614">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c614" aria-expanded="true" aria-controls="c614">¿Se puede adquirir tiquetes aéreos nacionales e internacionales en la agencia de viajes comfenalco?</a>
	    </h5>
	  </div>
	  <div id="c614" class="collapse" role="tabpanel" aria-labelledby="h614" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Sí.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h615">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c615" aria-expanded="true" aria-controls="c615">¿La agencia de viajes entrega un soporte para presentar al momento de acceder al servicio de turismo?</a>
	    </h5>
	  </div>
	  <div id="c615" class="collapse" role="tabpanel" aria-labelledby="h615" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Sí. Es entregado un voucher o constancia firmada por el aérea de turismo, la cual debe ser presentada al momento de encontrarse en el lugar donde se tomará el servicio.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h616">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c616" aria-expanded="true" aria-controls="c616">¿Hay una edad específica para asignar precio por niño o adulto?</a>
	    </h5>
	  </div>
	  <div id="c616" class="collapse" role="tabpanel" aria-labelledby="h616" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Sí, pero varía de acuerdo al servicio a tomar y el destino.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h617">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c617" aria-expanded="true" aria-controls="c617">¿Hay un plazo establecido para el pago total del servicio solicitado?</a>
	    </h5>
	  </div>
	  <div id="c617" class="collapse" role="tabpanel" aria-labelledby="h617" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, el valor total debe ser cancelado antes de hacer uso del servicio.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h618">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c618" aria-expanded="true" aria-controls="c618">¿Se puede realizar uno o varios depósitos con el fin de confirmar el servicio?</a>
	    </h5>
	  </div>
	  <div id="c618" class="collapse" role="tabpanel" aria-labelledby="h618" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, el valor del depósito se establece con la Agencia de Viajes.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h619">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c619" aria-expanded="true" aria-controls="c619">¿En caso de requerir un servicio de turismo ¿puedo realizar transferencias bancarias?</a>
	    </h5>
	  </div>
	  <div id="c619" class="collapse" role="tabpanel" aria-labelledby="h619" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, en los bancos autorizados. Para este caso deberá contactarse a través del correo electrónico con la Agencia de Viajes y solicitar este medio de pago. (turismo@comfenalco.com).</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h620">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c620" aria-expanded="true" aria-controls="c620">¿A través de que medios puedo pagar los servicios de turismo?</a>
	    </h5>
	  </div>
	  <div id="c620" class="collapse" role="tabpanel" aria-labelledby="h620" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>El pago puede realizarse en efectivo, tarjeta crédito o débito, cupo crédito, libranza y póliza Circulo de Viajes Universal.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h621">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c621" aria-expanded="true" aria-controls="c621">¿Dónde puedo adquirir y pagar los servicios de turismo?</a>
	    </h5>
	  </div>
	  <div id="c621" class="collapse" role="tabpanel" aria-labelledby="h621" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>En los CIS (Centros integrales de Servicios: Los Ejecutivos, Bloc Port, Outlet el Bosque y Matuna), puntos fijos de atención de los almacenes de cadena (Éxito Cartagena, Éxito Castellana, Éxito Ejecutivos, SAO Plazuela, Olímpica San Felipe, Olímpica Buenos Aires, Cámara de Comercio Ronda Real y Cámara de Comercio Centro) y Centro de Atención al estudiante ubicado en Cedesarrollo Zaragocilla.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h622">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c622" aria-expanded="true" aria-controls="c622">¿La agencia cuenta con destinos específicos para los planes internacionales?</a>
	    </h5>
	  </div>
	  <div id="c622" class="collapse" role="tabpanel" aria-labelledby="h622" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>La agencia de Comfenalco cuenta con un gran portafolio de planes a nivel internacional permitiendo que sus afiliados disfruten de los lugares que deseen.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h623">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c623" aria-expanded="true" aria-controls="c623">¿Puedo comprar planes nacionales e internacionales o cruceros en la agencia de viajes?</a>
	    </h5>
	  </div>
	  <div id="c623" class="collapse" role="tabpanel" aria-labelledby="h623" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, la agencia cuenta con convenios con proveedores mayoristas a nivel internacional y de cruceros.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h624">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c624" aria-expanded="true" aria-controls="c624">¿Hay algún convenio con otras cajas de compensación?</a>
	    </h5>
	  </div>
	  <div id="c624" class="collapse" role="tabpanel" aria-labelledby="h624" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, la Caja de Compensación Comfenalco tiene convenios con Cafam (Bogotá y Melgar), Colsubsidio (Bogotá, Girardot y Los Llanos Orientales), Comfandi (Cali, Lago Calima y departamento del Cauca) y Combarranquilla (Atlántico) lo cual permite hacer homologación para todos los afiliados a la categoría a la cual pertenecen en su Caja de Compensación de origen. Es indispensable informar antes de su viaje y presentar su carnet de afiliación acompañado de la tarjeta de identidad. En cuanto a las otras Cajas de Compensación existen convenios intercajas que nos permiten utilizar sus servicios pero bajo otras condiciones tarifarias.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h625">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c625" aria-expanded="true" aria-controls="c625">¿La agencia cuenta con destinos específicos para los planes internacionales?</a>
	    </h5>
	  </div>
	  <div id="c625" class="collapse" role="tabpanel" aria-labelledby="h625" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>La agencia de Comfenalco cuenta con un gran portafolio de planes a nivel internacional permitiendo que sus afiliados disfruten de los lugares que deseen.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h626">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c626" aria-expanded="true" aria-controls="c626">¿Puedo comprar planes nacionales e internacionales o cruceros en la agencia de viajes?</a>
	    </h5>
	  </div>
	  <div id="c626" class="collapse" role="tabpanel" aria-labelledby="h626" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, la agencia cuenta con convenios con proveedores mayoristas a nivel internacional y de cruceros.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h627">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c627" aria-expanded="true" aria-controls="c627">¿Puedo tener una variedad de opciones para realizar pasadías en islas?</a>
	    </h5>
	  </div>
	  <div id="c627" class="collapse" role="tabpanel" aria-labelledby="h627" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si, contamos con 6 opciones diferentes:<br>
			Tropical Inn – Tierra Bomba<br>
			Isla Arena – Tierrabomba<br>
			Cocoliso - Isla del Rosario<br>
			Isla del Encanto – Isla de Rosario<br>
			Isla del Sol – Isla del Rosario<br>
			Playa Blanca – Barú</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h628">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c628" aria-expanded="true" aria-controls="c628">¿Los cambios de horarios, destinos y fechas son permitidos?</a>
	    </h5>
	  </div>
	  <div id="c628" class="collapse" role="tabpanel" aria-labelledby="h628" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Sí, siempre y cuando la aerolinea presente disponibilidad en el vuelo. Están sujetos a las condiciones y restricciones dadas por la aerolinea.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h629">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c629" aria-expanded="true" aria-controls="c629">¿Si no me presento en el aeropuerto pierdo el tiquete?</a>
	    </h5>
	  </div>
	  <div id="c629" class="collapse" role="tabpanel" aria-labelledby="h629" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>Si no se presenta está sujeto al pago de cargos que corresponda de acuerdo a las condiciones aplicadas a la tarifa por parte de la aerolinea. Se le notificara al momento de realizar una nueva reserva.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h630">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c630" aria-expanded="true" aria-controls="c630">¿Se puede solicitar cambio de nombre del pasajero?</a>
	    </h5>
	  </div>
	  <div id="c630" class="collapse" role="tabpanel" aria-labelledby="h630" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>No, dentro de las políticas establecidas por cada aerolínea no es permitido el cambio de nombre.</p>
	    </div>
	  </div>
	</div>
	<div class="card">
	  <div class="card-header" role="tab" id="h631">
	    <h5 class="mb-0">
	      <a data-toggle="collapse" href="#c631" aria-expanded="true" aria-controls="c631">¿Cuál es el procedimiento en el caso que el equipaje llegue averiado o se pierda?</a>
	    </h5>
	  </div>
	  <div id="c631" class="collapse" role="tabpanel" aria-labelledby="h631" data-parent="#accordion6">
	    <div class="card-body pt-3 pb-3 pl-4 pr-4">
	  	  <p>La aerolínea debe responder por el daño o pérdida de equipaje, una vez llegue a su destino y se le presente cualquiera de estas dos posibilidades debe hacer el reclamo inmediato y directo a la aerolínea, ellos tomaran un reporte y con base en ello realizan las acciones pertinentes.</p>
	    </div>
	  </div>
	</div>
</div>