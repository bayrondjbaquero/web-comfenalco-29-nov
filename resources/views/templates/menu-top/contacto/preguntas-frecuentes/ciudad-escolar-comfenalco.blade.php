<div id="accordion3" role="tablist">
  <div class="card">
    <div class="card-header" role="tab" id="h31">
      <h5 class="mb-0">
        <a data-toggle="collapse" href="#c31" aria-expanded="true" aria-controls="c31">¿Dónde se puede realizar el pago de la Matricula y servicios Educativos?</a>
      </h5>
    </div>
    <div id="c31" class="collapse show" role="tabpanel" aria-labelledby="h31" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>El pago en efectivo se puede realizar en: Banco Davivienda, BBVA y AV Villas. Con tarjetas débito y/o crédito en CIS Cedesarrollo.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h32">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c32" aria-expanded="false" aria-controls="c32">¿Cuál es el objetivo del diplomado? ESCUELA DE FORMACION PARA PADRES DE FAMILIA?</a>
      </h5>
    </div>
    <div id="c32" class="collapse" role="tabpanel" aria-labelledby="h32" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>La Escuela de Formación para PADRES DE FAMILIA tiene como objetivo proporcionar a los padres de familia diversas estrategias para entender, apoyar, comprender y dar respuestas a los cambios propios del proceso de desarrollo por el cual está pasando sus hijos, tanto en el ámbito emocional, afectivo, académico, como social. Así mismo pretende dar herramientas para fortalecer el núcleo familiar y enfrentar los retos que le impone la sociedad actual.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h33">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c33" aria-expanded="false" aria-controls="c33">¿Quiénes deben asistir al diplomado?</a>
      </h5>
    </div>
    <div id="c33" class="collapse" role="tabpanel" aria-labelledby="h33" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Deben asistir al diplomado los padres de familia que matriculan a sus hijos por primera vez en la Ciudad Escolar Comfenalco.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h34">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c34" aria-expanded="false" aria-controls="c34">¿Cuáles son documentos que se debe anexar al formulario de inscripción?</a>
      </h5>
    </div>
    <div id="c34" class="collapse" role="tabpanel" aria-labelledby="h34" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Registro civil (Para todos los grados)<br>
			Fotocopia del Carnet de Vacunas (Para los grados de Preescolar)<br>
			Una fotografía de 3 x 4 cms Fondo Azul (Para todos los grados)<br>
			Para preescolar y Básica Primaria: Certificados de valoraciones del año inmediatamente anterior.<br>
			Certificación de buen comportamiento de la institución de la cual proviene.<br>
			Certificado de Paz y Salvo de la institución de la cual proviene.<br>
			Fotocopia de la tarjeta identidad (aplica para secundaria).</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h35">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c35" aria-expanded="false" aria-controls="c35">¿Dónde se pueden conseguir los formularios de inscripción?</a>
      </h5>
    </div>
    <div id="c35" class="collapse" role="tabpanel" aria-labelledby="h35" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Para adquirir el formulario de inscripción deben acercarse a cualquier de nuestros puntos de atención por el recibo de consignación para cancelar en el banco, con el recibo cancelado, acercarse a CIS Cedesarrollo a buscar el formulario y la información anexa.</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h36">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c36" aria-expanded="false" aria-controls="c36">¿Cuáles son documentos que se debe anexar al formulario de inscripción?</a>
      </h5>
    </div>
    <div id="c36" class="collapse" role="tabpanel" aria-labelledby="h36" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Registro civil (Para todos los grados)<br>
			Fotocopia del Carnet de Vacunas (Para los grados de Preescolar)<br>
			Una fotografía de 3 x 4 cms Fondo Azul (Para todos los grados)<br>
			Para preescolar y Básica Primaria: Certificados de valoraciones del año inmediatamente anterior.<br>
			Certificación de buen comportamiento de la institución de la cual proviene.<br>
			Certificado de Paz y Salvo de la institución de la cual proviene.<br>
			Fotocopia de la tarjeta identidad (aplica para secundaria)</p>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="h37">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#c37" aria-expanded="false" aria-controls="c37">¿Cuáles son los temas a tratar?</a>
      </h5>
    </div>
    <div id="c37" class="collapse" role="tabpanel" aria-labelledby="h37" data-parent="#accordion3">
      <div class="card-body pt-3 pb-3 pl-4 pr-4">
      	<p>Los temas a tratar son: Pautas de crianza, derecho de familia, presupuesto familiar, violencia intrafamiliar, prevención del consumo de drogas, educación sexual, los peligros de Internet, manejo de duelo, nueva ley de infancia y adolescencia, desarrollo evolutivo, depresión y suicidio, nutrición, problemas de aprendizaje, cómo abordar problemas de comportamiento, entre otros. Este programa académico tendrá una inversión de 10 cuotas que serán facturadas con cada pensión, por este valor cada nueva familia tendrá derecho a 2 cupos (Padre y Madre preferiblemente).</p>
      </div>
    </div>
  </div>
</div>