<div id="accordion7" role="tablist">
	<div class="card">
		<div class="card-header" role="tab" id="h71">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c71" aria-expanded="false" aria-controls="c71">¿Se puede utilizar el plan de ahorro depositado en la cuenta de Circulo de Viajes Universal S.A para adquirir los servicios de la agencia de viajes de Comfenalco?</a>
		  </h5>
		</div>
		<div id="c71" class="collapse show" role="tabpanel" aria-labelledby="h71" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Sí, debe solicitar su estado de cuenta a Círculo de Viajes Universal.<br>Una vez obtenga la información, deberá adjuntar carta autorizando a la Agencia de Viajes Comfenalco el uso de la cantidad a disponer para el servicio de turismo y fotocopia de su cedula para proceder con el servicio.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h72">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c72" aria-expanded="false" aria-controls="c72">¿Hay un plazo establecido para el pago total del servicio solicitado?</a>
		  </h5>
		</div>
		<div id="c72" class="collapse" role="tabpanel" aria-labelledby="h72" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Si, el valor total debe ser cancelado antes de hacer uso del servicio.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h73">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c73" aria-expanded="false" aria-controls="c73">¿La agencia de viajes entrega un soporte para presentar al momento de acceder al servicio de turismo?</a>
		  </h5>
		</div>
		<div id="c73" class="collapse" role="tabpanel" aria-labelledby="h73" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Sí. Es entregado un voucher o constancia firmada por el aérea de turismo, la cual debe ser presentada al momento de encontrarse en el lugar donde se tomará el servicio.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h74">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c74" aria-expanded="false" aria-controls="c74">¿Dónde consigo mi Carné o Tarjeta Comfenalco?</a>
		  </h5>
		</div>
		<div id="c74" class="collapse" role="tabpanel" aria-labelledby="h74" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Cuando el trabajador se encuentra afiliado por la empresa, su carné o Tarjeta Comfenalco, es enviada dentro de los 15 días siguientes a la empresa o a los CIS de acuerdo a la Zona donde esta se encuentre ubicada.</p>
		 	<p>Si el caso es de reexpedición por pérdida o robo de la Tarjeta Comfenalco, las tarjeta será enviada al CIS donde se presentó la solicitud.</p>
		 	<p>Si pasados los 15 día no ha recibido su Tarjeta Comfenalco, puede llamar a la línea call center, 6723888 y le indicaran la ubicación de esta.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h75">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c75" aria-expanded="false" aria-controls="c75">¿Quiénes tienen derecho a recibir el subsidio?</a>
		  </h5>
		</div>
		<div id="c75" class="collapse" role="tabpanel" aria-labelledby="h75" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Son beneficiarios del régimen del Subsidio Familiar y de nuestros servicios los trabajadores al servicio de los empleadores afiliados a nuestra Caja de Compensación Familiar que cumplan con los requisitos señalados en el artículo 7° de la Ley 21 de 1982, que además para recibir el subsidio familiar en dinero reúnan los siguientes requisitos:</p>
		 	<p>Tener remuneración mensual fija o variable que no sobrepase el límite de cuatro (4) salarios mínimos legales vigentes S.M.L.M.V.<br>
			Que sumados sus ingresos con los de su cónyuge o compañero (a), no sobrepasen seis (6) salarios mínimos legales mensuales vigentes S.M.L.M.V<br>
			Que laboren diariamente más de la mitad de la jornada máxima legal ordinaria o totalicen un mínimo de noventa y seis horas de labor durante el respectivo mes.<br>
			Tener personas a cargo que den derecho a recibir el subsidio familiar en dinero.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h76">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c76" aria-expanded="false" aria-controls="c76">¿Comfenalco me entrega el formato de los certificados?</a>
		  </h5>
		</div>
		<div id="c76" class="collapse" role="tabpanel" aria-labelledby="h76" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Anualmente Comfenalco, teniendo una cortesía con nuestros afiliados, a partir del mes de febrero, hace llegar a sus empresas afiliadas, los formatos para los certificados de escolaridad y supervivencia de los trabajadores con los hijos mayores de 12 años y padres afiliados.<br>Si el formato no llega a la empresa, el trabajador puede reclamarlo a partir del mes de Febrero en los Centros Integrales de Servicios CIS.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h77">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c77" aria-expanded="false" aria-controls="c77">¿Cuál es el saldo de mi tarjeta?</a>
		  </h5>
		</div>
		<div id="c77" class="collapse" role="tabpanel" aria-labelledby="h77" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>La consulta del saldo de los subsidios consignados en la TIPS, es personal e intransferible.<br>Para consultar el saldo de su tarjeta, debe dirigirse a los Centros Integrales de Servicios CIS, La Matuna y Ejecutivos y en las Oficinas regionales de Turbaco, Carmen de Bolívar y Magangué.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h78">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c78" aria-expanded="false" aria-controls="c78">¿Cómo me puedo identificar como afiliado a Comfenalco?</a>
		  </h5>
		</div>
		<div id="c78" class="collapse" role="tabpanel" aria-labelledby="h78" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Luego de tramitada la afiliación del trabajador por la empresa, hace entrega de un carnet o Tarjeta Integral de Pagos y Servicios (TIPS), que lo identifica como afiliado a Comfenalco Cartagena.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h79">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c79" aria-expanded="false" aria-controls="c79">¿Cada cuánto tengo que actualizar los documentos de mi grupo familiar?</a>
		  </h5>
		</div>
		<div id="c79" class="collapse" role="tabpanel" aria-labelledby="h79" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Anualmente, el mes de marzo de deben actualizar los certificados de escolaridad de los niños mayores de 12 años y los Certificados de Supervivencia de los padres, el cual debe ser expedido en una notaría.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h710">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c710" aria-expanded="false" aria-controls="c710">A los niños discapacitados le cancelan doble subsidio?</a>
		  </h5>
		</div>
		<div id="c710" class="collapse" role="tabpanel" aria-labelledby="h710" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Si, pero para tal efecto se debe cumplir con los siguientes requisitos:<br>
			1. Tener formulario Diligenciado<br>
			2. registro Civil original<br>
			3. Certificado de la EPS original y la valoración de la IPS COMFENALCO.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h711">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c711" aria-expanded="false" aria-controls="c711">¿Deseo saber si puedo realizar un anticipo de subsidio?</a>
		  </h5>
		</div>
		<div id="c711" class="collapse" role="tabpanel" aria-labelledby="h711" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Todas las personas que tengan personas a su cargo pueden realizar anticipo de subsidios en compra en algunos de almacenes en convenio y este se puede realizar hasta tres máximos.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h712">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c712" aria-expanded="false" aria-controls="c712">¿No llegó su Subsidio familiar Comfenalco?</a>
		  </h5>
		</div>
		<div id="c712" class="collapse" role="tabpanel" aria-labelledby="h712" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Tenga en cuenta las siguientes consideraciones para recibir su subsidio mensualmente:<br>• La empresa debe tener a los trabajadores inscritos y el trabajador haber afiliado a su grupo familiar con la documentación requerida.<br>• Cancelar oportunamente y de acuerdo a la ley sus aportes dentro de los primeros diez días de cada mes (si paga después del día estipulado deberá esperar hasta el próximo pago)</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h713">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c713" aria-expanded="false" aria-controls="c713">¿Cuándo consignan los subsidios?</a>
		  </h5>
		</div>
		<div id="c713" class="collapse" role="tabpanel" aria-labelledby="h713" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Del 24 al 25 de cada mes cuando la empresa registra puntual sus aportes.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h714">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c714" aria-expanded="false" aria-controls="c714">¿Por qué no me ha llegado el subsidio?</a>
		  </h5>
		</div>
		<div id="c714" class="collapse" role="tabpanel" aria-labelledby="h714" data-parent="#accordion7">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>1. Porque no ha presentado la documentación requerida<br>2. Porque no está reflejado el pago del aporte por parte de la empresa.</p>
		  </div>
		</div>
	</div>
</div>