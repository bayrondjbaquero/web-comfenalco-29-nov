<div id="accordion8" role="tablist">
	<div class="card">
		<div class="card-header" role="tab" id="h81">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c81" aria-expanded="false" aria-controls="c81">¿Cuáles son los puntos de activación?</a>
		  </h5>
		</div>
		<div id="c81" class="collapse show" role="tabpanel" aria-labelledby="h81" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Centros Integrales De Servicios Cis<br>
			Los Ejecutivos, La Matuna, Bosque, Mamonal y Bocagrande.<br>
			Regionales<br>
			Carmen de Bolívar, Turbaco y Magangué</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h82">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c82" aria-expanded="false" aria-controls="c82">¿Qué se debe hacer en caso de pérdida o robo de la tarjeta?</a>
		  </h5>
		</div>
		<div id="c82" class="collapse" role="tabpanel" aria-labelledby="h82" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>El afiliado debe llamar a línea de Atención al cliente, acercarse a un Centro Integral de Servicio Al Cliente (CIS) o a un punto fijo de atención, para realizar el BLOQUEO.<br>
			Se debe consignar $ 7.500 por la Reexpedición de la Tarjeta.<br>
			Presentar copia del denuncio por la Pérdida de la Tarjeta, soporte de consignación y copia de la Cédula de Ciudadanía del Afiliado ampliada a 150 en el CIS más cercano.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h83">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c83" aria-expanded="false" aria-controls="c83">¿Cómo puedo solicitar reexpedición de mi Tarjeta si esta deteriorada?</a>
		  </h5>
		</div>
		<div id="c83" class="collapse" role="tabpanel" aria-labelledby="h83" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Si su tarjeta está deteriorada debe acercarse a un Centro Integral de Servicio Al Cliente (CIS) o a un punto fijo de atención, con su documento de identidad y la tarjeta Comfenalco.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h84">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c84" aria-expanded="false" aria-controls="c84">¿Dónde localizo mi Tarjeta?</a>
		  </h5>
		</div>
		<div id="c84" class="collapse" role="tabpanel" aria-labelledby="h84" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Cuando el trabajador se encuentra afiliado por la empresa, su tarjeta, es enviada dentro de los 15 días siguientes a la empresa o a los CIS de acuerdo a la Zona donde esta se encuentre ubicada.<br>
			Si el caso es de reexpedición por pérdida o robo, la tarjeta será enviada al sector que registra la empresa en nuestro sistema (CIS o dirección de la misma)<br>
			Dado el caso que la tarjeta se encuentre en custodia será enviada a donde lo solicita el afiliado (CIS donde presentó la solicitud o a la empresa).<br>
			Si pasados los 15 día no ha recibido su Tarjeta, puede llamar a la línea call center, 6723800, o 01 8000 95 80 80 y le indicaran la ubicación de esta.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h85">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c85" aria-expanded="false" aria-controls="c85">¿Qué debo hacer en caso de bloqueo de la tarjeta por clave invalida?</a>
		  </h5>
		</div>
		<div id="c85" class="collapse" role="tabpanel" aria-labelledby="h85" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Si se ingresa 3 veces un PIN inválido, el afiliado debe esperar al día siguiente para volver a intentarlo o acercarse a un Centro Integral de Servicio al Cliente CIS, más cercano para realizar cambio de clave.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h86">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c86" aria-expanded="false" aria-controls="c86">¿Cuál es el procedimiento para redimir mi subsidio?</a>
		  </h5>
		</div>
		<div id="c86" class="collapse" role="tabpanel" aria-labelledby="h86" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>El trabajador afiliado presenta la tarjeta y el documento de identidad en el cajero con DATAFONO Redeban, de los establecimientos en convenio.<br>Si el afiliado desea la redención de la cuota monetaria, el cajero debe entregarle el 100% del valor en efectivo.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h87">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c87" aria-expanded="false" aria-controls="c87">¿Cómo consulto el saldo de mi tarjeta?</a>
		  </h5>
		</div>
		<div id="c87" class="collapse" role="tabpanel" aria-labelledby="h87" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>Para consultar el saldo de su tarjeta, debe dirigirse a los Centros Integrales de Servicio al Cliente CIS y/oPuntos Fijos de Atención o llamando a la línea de atención al cliente 6723800, línea 018000 95 80 80.</p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h88">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c88" aria-expanded="false" aria-controls="c88">¿Cómo identificar la Tarjeta de comfenalco?</a>
		  </h5>
		</div>
		<div id="c88" class="collapse" role="tabpanel" aria-labelledby="h88" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p></p>
		  </div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" role="tab" id="h89">
		  <h5 class="mb-0">
		    <a class="collapsed" data-toggle="collapse" href="#c89" aria-expanded="false" aria-controls="c89">¿Qué Es La Tarjeta Comfenalco?</a>
		  </h5>
		</div>
		<div id="c89" class="collapse" role="tabpanel" aria-labelledby="h89" data-parent="#accordion8">
		  <div class="card-body pt-3 pb-3 pl-4 pr-4">
		 	<p>La TARJETA COMFENALCO es la nueva credencial de identificación por ser afiliado a la Caja de Compensación Familiar Comfenalco Cartagena. Es tan importante como la cédula porque con ella accederá como AFILIADO categorizado a todos los Servicios que ofrece la Caja, además de las ofertas y descuentos especiales con los almacenes en convenio.</p>
		  </div>
		</div>
	</div>
</div>