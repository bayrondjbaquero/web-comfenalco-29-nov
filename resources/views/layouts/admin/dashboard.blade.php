<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('titulo_dash', 'Administrador') - Comfenalco</title>
    <meta name="author" content="DoDigital">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="robots" content="noindex, nofollow" />
    <meta name="description" content="">
    <!-- Font Awesome -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Custom Theme Style -->
    <link href="{{asset('css/custom.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/datatables.min.css')}}"/>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
        .btn-language,.btn-language-2{
            display: inline-block;
            padding: 10px 15px;
        }
        .btn-language-2{
            background-color: #eb5b2a;
            color: #fff;
        }
        .mycontent-left {
            border-right: 1px dashed #333;
        }
    </style>
    @yield('styles')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="{{URL::route('home')}}" class="site_title" style="line-height:55px;"><span style="display: inline-block;margin-left: 8px;vertical-align: middle;"><img src="{{asset('img/logo.png')}}" alt="Comfenalco" width="32" height="32" class="img-responsive"></span> <span style="vertical-align: middle;"> Comfenalco</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu perfil -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="{{asset('img/profile.svg')}}" alt="perfil-admin" class="img-circle profile_img">
                        <br>
                    </div>
                    <div class="profile_info">
                        <span>Bienvenido/a,</span>
                        <h2>Administrador</h2>
                    </div>
                </div>
                <!-- /menu perfil -->

                <br />
                <!-- Indicators -->
                 
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-sitemap" aria-hidden="true"></i> Menús<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('menus.create') }}">Agregar Menú</a></li>
                                    <li><a href="{{ route('menus.index') }}">Listar Menús</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-ellipsis-h" aria-hidden="true"></i> Secciones [Tabs]<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('tabs.create') }}">Agregar Tabs</a></li>
                                    <li><a href="{{ route('tabs.index') }}">Listar Tabs</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-picture-o" aria-hidden="true"></i> Banners Principales<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('banners.create') }}">Agregar Banner</a></li>
                                    <li><a href="{{ route('banners.index') }}">Listar Banners</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-newspaper-o" aria-hidden="true"></i> Entradas<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('entries.create') }}">Agregar Entrada</a></li>
                                    <li><a href="{{ route('entries.index') }}">Listar Entradas</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-tags" aria-hidden="true"></i> Categorías<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('categories.create') }}">Agregar Categoría</a></li>
                                    <li><a href="{{ route('categories.index') }}">Listar Categorías</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-calendar" aria-hidden="true"></i> Eventos<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('events.create') }}">Agregar Evento</a></li>
                                    <li><a href="{{ route('events.index') }}">Listar Eventos</a></li>
                                </ul>
                            </li>
                            <li><a><i class="fa fa-building" aria-hidden="true"></i> Proyectos<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('proyects.create') }}">Agregar Proyecto</a></li>
                                    <li><a href="{{ route('proyects.index') }}">Listar Proyectos</a></li>
                                    <li><a href="{{ route('galleries.index') }}">Listar Galerías</a></li>
                                </ul>
                            </li>
                            
                            <li><a><i class="fa fa-file-image-o" aria-hidden="true"></i> Banners Internos<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('second_banners.create') }}">Agregar Banner interno</a></li>
                                    <li><a href="{{ route('second_banners.index') }}">Listar Banners internos</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-file-text-o" aria-hidden="true"></i>Contenido<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('contenido.create') }}">Crear Contenido</a></li>
                                    <li><a href="{{ route('contenido.listado') }}">Listar Contenido</a></li>
                                </ul>
                            </li>

                            <li><a><i class="fa fa-question-circle" aria-hidden="true"></i>Preguntas Frecuentes<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('questions.create') }}">Agregar Pregunta</a></li>
                                    <li><a href="{{ route('questions.index') }}">Listar Preguntas</a></li>
                                    <li><a>Categorías<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li class="sub_menu"><a href="{{ route('question_categories.index') }}">Listar Categorías</a></li>
                                            <li><a href="{{ route('question_categories.create') }}">Agregar Categoría</a></li>
                                            <li><a href="{{ route('question_categories.order.edit') }}">Ordenar Categoría</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </div>
                    <div class="menu_section">
                        <h3>OTROS</h3>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-file-text-o" aria-hidden="true"></i>Campañas Digitales<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{ route('campanas.index') }}">Listar campañas</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><i class="fa fa-cog"></i> Configuración <span class=""></span></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <!-- /sidebar menu -->

                <!-- menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="right" title="Cerrar sesión" class="pull-right" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <span class="fa fa-sign-out" aria-hidden="true"></span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="http://placehold.it/100x100" alt=""> Administrador
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="{{ route('usuario.edit', Auth::id()) }}"> Mi cuenta</a></li>
                                <li><a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
                <div class="title_left" style="width:100%">
                    <h3 class="text-rojo">@yield('titulo_page')</h3>
                </div>
            </div>

            <div class="clearfix"></div>
            @yield('content')            
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                Administrador Comfenalco - 2017
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>

<!-- Custom Theme Scripts -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/custom.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/datatables.min.js')}}"></script>
<script>
    $(document).ready(function () {
        $('.table').DataTable({
            "language": {
                "url": "{{asset('js/Spanish.json')}}"
            }
        });
    });
    </script>
@yield('scripts')

</body>
</html>
