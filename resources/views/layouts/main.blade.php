<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>@yield('title')@yield('line','- ')COMFENALCO Cartagena / Bolívar</title>
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    <meta name="author" content="DoDigital">
    <meta name="robots" content="index, follow">
    @yield('styles')
    <script type="text/javascript" src="{{ asset('js/chatlivecomfenalco.js') }}"></script>
</head>
<body>
    <div id="wrapper" class="box-default">
        @inject('menu','App\Http\Controllers\Menu\MenuController')
        <div id="menu-top" class="box-default">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-md-5 col-12">
                        <a href="{{route('home')}}"><figure class="text-center _hover-memutop">
                            <img class="img-fluid" src="{{asset('img/logo-principal.png')}}" alt="Comfenalco Cartagena">
                        </figure></a>
                    </div>
                    <div class="col-sm-8 col-md-7 col-12 d-flex align-items-center hidden-xs-down">
                        <div class="d-flex justify-content-center mx-auto _menu-effect">
                            @foreach($menu->links() as $link)
                                @if($link['type_menu'] == 2)
                                    <a class="{{ (Request::is($link['slug']) ? "active" : '') }} _hover-memutop" href="{{ url($link['slug'])}}">
                                        <figure class="text-center mb-0">
                                            <img class="img-fluid" src="{{asset($link['icon'])}}" alt="{{ $link['title'] }}">
                                            <figcaption class="text-figcaption pt-2 text-blue font-weight-bold">{{ $link['title'] }}</figcapition>
                                        </figure>
                                    </a>
                                @endif
                            @endforeach
                            <div class="text-center m-auto text-ley">
                                <p class="text-white text-figcaption bg-red mb-0 rounded p-1">Ley de <br>Transparencia <br>y acceso a la <br>información Pública</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="nav" class="box-default">
            <nav class="p-0 navbar navbar-inverse bg-blue navbar-toggleable-md text-white">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleCenteredNav" aria-controls="navbarsExampleCenteredNav"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-md-center" id="navbarsExampleCenteredNav">
                    <ul class="navbar-nav main-menu">
                        @foreach($menu->links() as $link)
                            @if($link['type_menu'] == 1)
                                <li class="{{ (Request::is($link['slug']) ? "active" : '') }} menu-{{ $link['slug'] }} nav-item ">
                                    <a class="nav-link text-white" href="{{ route($link['url'])}}">{{ $link['title'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </nav>
        </div>
        <div id="menu-top-hide" class="d-flex flex-wrap align-content-stretch align-items-stretch text-center hidden-sm-up justify-content-center">
            <a class="bg-default justify-content-center flex-column d-flex text-center p-2 w-25" href="{{route('organizacion')}}">
                <img class="img-fluid align-self-center" src="{{asset('img/organizacion.png')}}" alt="Organización">
                <p class="text-blue mb-0 font-weight-bold">Organización</p>
            </a>
            <a class="bg-default justify-content-center flex-column d-flex text-center p-2 w-25 bg-faded" href="{{route('afiliado')}}">
                <img class="img-fluid align-self-center" src="{{asset('img/afiliado.png')}}" alt="Afiliado">
                <p class="text-blue mb-0 font-weight-bold">Afiliado</p>
            </a>
            <a class="bg-default justify-content-center flex-column d-flex text-center p-2 w-25" href="{{route('servicios_en_linea')}}">
                <img class="img-fluid align-self-center" src="{{asset('img/servicios-en-linea.png')}}" alt="Servicios en linea">
                <p class="text-blue mb-0 font-weight-bold">Servicios en Línea</p>
            </a>
            <a class="bg-default justify-content-center flex-column d-flex text-center p-2 w-25 bg-faded" href="{{route('contacto')}}">
                <img class="img-fluid align-self-center" src="{{asset('img/contacto.png')}}" alt="Contáctanos">
                <p class="text-blue mb-0 font-weight-bold">Contáctanos</p>
            </a>
            <p class="bg-red text-white w-100 p-2 mb-0 w-100">Ley de Transparencia y acceso a la información Pública</p>
        </div>
        @yield('content')
        <div id="footer" class="box-default pt-4 pb-4 text-white">
            <div id="redes" class="d-flex align-items-center justify-content-center flex-column pt-3 pb-3">
                <div class="container hidden-md-up">
                    <div class="row">
                        <div class="col-12">
                            <h4>Siguenos en redes</h4>
                        </div>
                    </div>
                </div>
                <ul class="list-unstyled mb-0 d-flex flex-md-column align-items-md-center flex-wrap justify-content-center">
                    <li class="bg-blue"><a href="https://www.facebook.com/comfenalco.ctg/" target="_blank"><img src="{{asset('img/facebook.png')}}" alt=""></a></li>
                    <li class="bg-blue-2"><a href="https://twitter.com/ctg_comfenalco" target="_blank"><img src="{{asset('img/twitter.png')}}" alt=""></a></li>
                    <li class="bg-blue"><a href="https://www.instagram.com/comfenalco_cartagena/" target="_blank"><img src="{{asset('img/instagram.png')}}" alt=""></a></li>
                    <li class="bg-blue-2"><a href="https://www.youtube.com/channel/UCiNNY0d8LzzeRkyQCxcf3Dg?view_as=subscriber" target="_blank"><img src="{{asset('img/youtube.png')}}" alt=""></a></li>
                    <li class="vigilado-supersubsidio"><a href="http://www.ssf.gov.co/wps/portal/" target="_blank"><img src="{{ asset('img/supersub.png') }}" alt="Vigilado Super Subsidio"></a></li>
                </ul>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="bg-gray footer-gray rounded mb-2 mb-md-0">
                            <h4 class="bg-red d-inline-block pl-2 pr-2 pt-1 pb-1 rounded">Contacto</h4>
                            <div class="d-flex flex-column">
                                <strong>Cartagena de Indias</strong>
                                <span>PBX: (+57 5) 6723800</span>
                                <span class="pb-1">Zaragocilla Diagonal 30 No.50-187</span>
                                <strong>Magangué</strong>
                                <span class="pb-1">Tel.: 687 8634</span>
                                <strong>Turbaco</strong>
                                <span class="pb-1">Tel.: 6639668</span>
                                <strong>Carmen de Bolivar</strong>
                                <span class="pb-1">Tel.: 6861302</span>
                                <strong>Santa Rosa del Sur</strong>
                                <span>Tel.: 6861302</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="bg-gray footer-gray rounded mb-2 mb-md-0">
                            <h4 class="bg-red d-inline-block pl-2 pr-2 pt-1 pb-1 rounded">Links destacados</h4>
                            <ul class="pl-1 mb-0 links-footer list-unstyled">
                                <li><a class="text-white" href="http://afiliate.comfenalco.com/" target="_blank">Aﬁliate Comfenalco</a></li>
                                <li><a class="text-white" href="">Consulta de Aﬁliados</a></li>
                                <li><a class="text-white" href="https://www.enlace-apb.com/interssi/.plus" target="_blank">Pago de Aportes Planilla Única</a></li>
                                <li><a class="text-white" href="https://crediweb.comfenalco.com/" target="_blank">Credi Web</a></li>
                                <li><a class="text-white" href="" target="_blank">Subsidio</a></li>
                                <li><a class="text-white" href="" target="_blank">Certificado Tributario</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="bg-gray footer-gray rounded">
                            <h4 class="text-white bg-red d-inline-block pl-2 pr-2 pt-1 pb-1 rounded">Alianzas y Gremios</h4>
                            <div class="comfenalco-info d-sm-flex d-block bg-white rounded flex-sm-wrap justify-content-sm-center align-items-sm-center pb-1 pt-1">
                                <figure><img class="rounded img-fluid" src="{{asset('img/ancham.jpg')}}" alt="Ancham"></figure>
                                <figure><img class="rounded img-fluid" src="{{asset('img/asopagos.jpg')}}" alt="Asopagos"></figure>
                                <figure><img class="rounded img-fluid" src="{{asset('img/asocajas.jpg')}}" alt="Asocajas"></figure>
                                <figure><img class="rounded img-fluid" src="{{asset('img/bancompartir.png')}}" alt="Bancompartir"></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom box-default bg-gray">
            <span class="text-center text-white text-bold mx-auto pt-2 pb-2 d-block">© Copyright 2018. Todos los derechos reservados</span>
        </div>
    </div>
    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script> --}}
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    @yield('scripts')
</body>

</html>